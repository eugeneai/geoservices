var fs = require("fs");

/**
 * Class for preparing WPSManager environment.
 */

function WPSManagerBootstrap(databaseclient) {
	var _databaseclient = databaseclient;

	/**
	 * Setting up the environment.
	 *
	 * @param Object calipso Calipso CMS main object
	 *
	 * @return void
	 */

	this.setup = function(calipso) {
		if (calipso.config.getModuleConfig('wpsmanager', 'executablepath') === "") {
			calipso.warn("WPSMAnager: executablepath (path to WPS executor) is not set");
		}

		if (calipso.config.getModuleConfig('wpsmanager', 'cwd') === "") {
			calipso.warn("WPSMAnager: cwd (working directory) is not set");
		}
	} //end setup()


} //end class


module.exports = WPSManagerBootstrap;