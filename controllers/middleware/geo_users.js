var crypto = require('crypto');
var config = require("../../conf.json");

function _hashmd5(string, salt) {    
    var hashedsalt     = crypto.createHash('md5').update(salt).digest("hex");    
    var hashedpassword = crypto.createHash('md5').update(string).digest("hex");    
	var overallhash    = crypto.createHash('md5').update(hashedsalt + hashedpassword).digest("hex");    
	return overallhash;
} //end hashmd5()


exports.passwordhash=function(password) {
    var hashedpswd    = _hashmd5(password, config.session.secret);
    return hashedpswd;
}

exports.userupdate=function(req, res, next) {    
    document=req.body.document;
    var dataset_id = req.queryString('f');
    if(dataset_id==5 && document){  
        if(typeof document=='string'){            
            document=JSON.parse(document);
        }      
        if((req.user && (req.user.isadmin || document.f_id==req.user.id)) || req.originalUrl=='/dataset/add?f=5'){
            document.hash=undefined;            
            if(req.user && 'isadmin' in document && !req.user.isadmin){
                document.isadmin=undefined;
            }
            if(req.user && 'locked' in document && !req.user.isadmin){
                document.locked=undefined;
            }
            if(document.password && typeof document.password=='string' && document.password!=''){
                if(document.password!=document.password2){
                    res.end(JSON.stringify({status: "error", data: 'The passwords do not match.'}), "UTF-8");
                    return;
                }
                if(document.password.length<6){
                    res.end(JSON.stringify({status: "error", data: 'The password is very short.'}), "UTF-8");
                    return;
                }
                document.hash=exports.passwordhash(document.password);
                console.log('document.hash=',document.hash,document.password);
            }
            document.password=undefined;
            document.password2=undefined;

                        
            next();
        }
        else{            
            res.end(JSON.stringify({status: "error", data: 'You do not have admin privilege.'}), "UTF-8");
        }
    }
    else{
        next();
    }
}

