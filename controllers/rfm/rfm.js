/**
 * Template module
 */
var rootpath = process.cwd() + '/',
  path = require('path'),
  fs = require('fs'),  
  mime = require('mime');


var dir_separator='/';

var config = require(rootpath+"/conf.json");
// var passgen=require('password-generator'); //Password generator


fs = require('fs');


function getqueryparams(req){
	var qparams = [];
	for (var attrname in req.query)
		qparams[attrname] = req.queryString(attrname);
	for (var attrname in req.body)
		qparams[attrname] = req.bodyString(attrname);
	return qparams;
}
/**
 * Small block that will appear on every page
 */
exports.list = function(req, res, next){
	res.format = "json";
    var nodes = new Array();

	if (req.user===undefined || req.user==null){
		res.end(JSON.stringify({"error" : "You should log in."}),"UTF-8"); 
		console.log('Session is undefined.');
		return;
	}
	
	try{
		addfile = (req.query.mode === undefined || req.query.mode=='1');
		adddir = (req.query.mode === undefined || req.query.mode=='2');
		
		// root_dir already has the user folder in it
		var root_dir = req.user.foldername;
		console.log("root_dir", root_dir);

		fhesh = req.query.id;
		if (fhesh == '1' || fhesh=='root' || fhesh===undefined || fhesh==''){
			console.log("I am here");
			var cpath = root_dir;
			var chash = gethash(root_dir);
			nodes.push({
				"attr": {
					"id": chash,
					"rel": "folder"
				},
				"data": root_dir,
				"state":"closed"
			});
		} else {
			cpath = root_dir;
			console.log("fhesh=", fhesh);
			if(fhesh && fhesh!='')
				cpath = getpath(fhesh);
			console.log("cpath", cpath);
			var files = fs.readdirSync(cpath);
			pos=cpath.indexOf(root_dir);
			var relativedir=root_dir;
			if (pos==0){
				relativedir=cpath.slice(root_dir.length);					
			}
			// console.log("relativedir" , relativedir);
			if (cpath.charAt(cpath.length - 1) !== dir_separator) {
				cpath = cpath + dir_separator;
			}
			for(var i in files) {
				if (files[i].charAt(0)=='.') continue;
				

				var cname = cpath + files[i];
				var relname = relativedir  + dir_separator + files[i];
				var chash = gethash(cname);
				var stats = fs.statSync(cname);
				if (stats.isDirectory() && adddir){
					nodes.push({
						"attr": {
							"id": chash,
							"rel": "folder",
							"originalPath": relname
						},
						"data": files[i],
						"state": "closed"
					});
				}

				if (stats.isFile() && addfile){
					nodes.push({
						"attr": {
							"id": chash,
							"rel": "default",
							"originalPath": relname
						},
						"data": files[i],
						"state": ""
					});
				}
			}
		}
	} catch (e) {
		console.log("Error occured", e);

		res.end(JSON.stringify({"error" : e}),"UTF-8"); 
		return;		
	}

	res.end(JSON.stringify(nodes),"UTF-8"); 
	return;
	
	
};



function nsep(cpath){
	var prev='';
	var res='';
	for (var i = 0; i < cpath.length; i++) {
		if(!((cpath.charAt(i)=='/' || cpath.charAt(i)=='\\') && (prev=='/' || prev=='\\' ) )){
			prev=cpath.charAt(i);
			res=res+cpath.charAt(i);
		}
	}
	return res;
}

function get_fd_desc(req, cpath){
	
	var root = config.modules.rfm.config.mainrootdir;

	var roots=req.root_dirs;
	var root_id=-1;
	var hash=gethash(cpath);	
	var name=cpath.slice(cpath.lastIndexOf(dir_separator)+1);
//	calipso.log("name="+name);
	for(var i in roots){
		if(roots[i]==cpath){
			root_id=i;
			break;
		}
	}
	if (root_id!=-1){
		var phash=hash;
	}
	else{
		var ppath=cpath.slice(0,-name.length-1);
		var phash=gethash( ppath);
	}
	rcpath=fs.realpathSync(cpath);
	var stats=fs.statSync(rcpath);
	if (stats.isDirectory()){
		var f={"mime":"directory","ts":1359191956,"read":1,"write":1,"size":0,"hash":hash, "name":name,"date":"Today 18:19","dirs":1};
		if (hash!=phash)
			f.phash=phash;

	}
	else if (stats.isFile()){
		var f={"mime":"file","ts":1359191956,"read":1,"write":1,"size":0,"hash":hash,"phash":phash,"volumeid":"l1_","name":name,"date":"Today 18:19","locked":0,"dirs":0};
	}
	else return null;

	return f;
};

function gethash(cpath){
    

    cpath = cpath + '';

	var root = config.modules.rfm.config.mainrootdir;
	
	
    pos=cpath.indexOf(root);
    if (pos==0){
        cpath=cpath.slice(root.length);
        cpath=cpath.replace(/^\\+/g,"");
    }

	if (cpath == '') {
		cpath = dir_separator;
	}

	var b = new Buffer.from(cpath);
	var fhash = b.toString('base64');
	fhash=fhash.replace(/\+/g,"-");
	fhash=fhash.replace(/\//g,"_");
	fhash=fhash.replace(/=/g,".");
	fhash=fhash.replace(/\.+$/g,"");

    // root_id = parseInt(root_id) + 1;
    // var result = 'l' + root_id + '_' + fhash;

    // console.log('### gethash result: ' + cpath + ' > ' + result);

	return fhash;
}

function getpath(hash){
	if (hash=== undefined || hash=='' || hash=="l1_XA") {		
		return '';
	}
	// drive=hash.substr(1,1);
//	console.log('drive='+drive);
	dir=hash;//.slice(3);
	dir=dir.replace(/-/g,"+");
	dir=dir.replace(/_/g,"/");
	dir=dir.replace(/\./g,"=");	
	var b = new Buffer.from(dir, 'base64');
	res=b.toString();
	
	var root = config.modules.rfm.config.mainrootdir;
	if (root.charAt(root.length - 1) !== dir_separator) {
		root = root + dir_separator;
	}
	res=root+res;
	res=nsep(res);
	//res=fs.realpathSync(res);
	// console.log(res);
	return res;
}


function GetTree(req,dirpath, depth, maxdepth, addroot, addfile ){
	var ResArray=new Array();
	calipso.log("dirpath="+dirpath);
	var stats=fs.statSync(dirpath);
	if (!stats.isDirectory()){
		calipso.log("is not dir");
		return ResArray;
	}
	
	if(dirpath.charAt(dirpath.length-1)==dir_separator)
		dirpath=dirpath.slice(0,-1);
	var name=dirpath.slice(dirpath.lastIndexOf(dir_separator)+1);
	var chash=gethash(dirpath);
	var root_id=-1;
	var root_dirs=req.user.foldername;
	for(var i in root_dirs){
		if(root_dirs[i]==dirpath){
			root_id=i;
			break;
		}
	}
	if (root_id!=-1){
		var phash=chash;
	}
	else{
		var parentpath=dirpath.slice(0,-name.length-1);
		var phash=gethash( parentpath);
	}
	
	if(addroot==1){
		var f=get_fd_desc(req,dirpath);
		ResArray[ResArray.length]=f;
	}
	if (depth<=maxdepth){
		rdirpath=fs.realpathSync(dirpath);
				console.log('readdir 3');
	
		var readresults = fs.readdirSync(rdirpath);
				console.log('readdir end 3 readresults=');		
				console.log(readresults);
		for(var i in readresults) {	
			if(readresults[i].charAt(0)=='.') continue;
			var cname=dirpath+dir_separator+readresults[i];
			rcname=fs.realpathSync(cname);
			var stats=fs.statSync(rcname);
			if (addfile!=1 && stats.isDirectory()){
				var res=GetTree(req,cname, depth+1,maxdepth,1, addfile);
				var ResArray = ResArray.concat(res);
			}		  		
			if (addfile==1 && stats.isFile()){
				var f=get_fd_desc(req,cname);
				ResArray[ResArray.length]=f;	  	
			}		
		}
	}
	return ResArray;
}

function getf(files, hash){
	for(var i in files){
		if(files[i].hash==hash){
			return files[i];
		}
	}
	return 0;
}

/**

 */
exports.op=function(req, res, next) {
	if(req.user===undefined || req.user==null){
		res.end(JSON.stringify({"error" : "You should log in."}),"UTF-8"); 
		return;
	}
	console.log('I am here begin');

	var params=getqueryparams(req);
	
	var root_dirs=req.user.foldername;
	if(params.cmd!='file')
		res.format = "json";
	var files = new Array();
	var result = new Object();
	result.status='ok';
	
	try {
		switch (params.cmd) {
			case 'open':{
//				calipso.log("open!");
				if(params.init=='1'){
					cpath=root_dirs[0]+dir_separator;
					fhesh=gethash(cpath);
					result.options={"path":"files","url":"\\/1\\/php\\/..\\/files\\/","tmbUrl":"\\/1\\/php\\/..\\/files\\/.tmb\\/","disabled":[],"separator":"\\","copyOverwrite":1,"archivers":{"create":["application\\/zip"],"extract":["application\\/zip"]}};
					result.api ="2.0";
					result.uplMaxSize ="2M";
				}
				else{
					fhesh=params.target;
					cpath=getpath(fhesh);
				}
				if(cpath==''){
					res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
					return;
				}
				if (params.tree=='1')
					files=GetTree(req,cpath,0,0,1,1);
				else
					files=GetTree(req,cpath,0,1,1,1);
				result.files=files;
				result.cwd=getf(files,fhesh);
				break;
			}
			case 'tree':{			
				fhesh=params.target;
				var cpath=getpath(fhesh);
				if(cpath==''){
					res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
					return;
				}					
				result.tree=GetTree(req,cpath,0,0,1,0);
			break;
			}
			case 'mkdir':{			
				fhesh=params.target;
				var cpath=getpath(fhesh);
				console.log('cpath=',cpath)
				if(cpath==''){
					res.end(JSON.stringify({"error" : "Folder does not exist."}),"UTF-8"); 		
					return;
				}					
				cpath=cpath+dir_separator + params.name;
				fs.mkdirSync(cpath, 0755);
				// result.added=GetTree(req,cpath,0,0,1,0);
				result.newid=gethash(cpath);
				console.log('okkkk')
			break;
			}
			case 'rename':{			
				fhesh=params.target;
				var newname=params.name;
				var cpath=getpath(fhesh);
				// console.log(cpath, newname);
				if(cpath=='' || newname==''){
					res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
					return;
				}					
				var oldname=cpath.slice(cpath.lastIndexOf(dir_separator)+1);
				var parentpath=cpath.slice(0,-oldname.length-1);
				var newpath=parentpath+dir_separator+newname;
				cpath=fs.realpathSync(cpath);
				fs.renameSync(cpath, newpath);
				result.newid=gethash(newpath);
		
			break;
			}
			case 'parents':{			
				fhesh= params.target;
				var cpath=getpath(fhesh);
				if(cpath==''){
					res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
					return;
				}					
				var name=cpath.slice(cpath.lastIndexOf(dir_separator)+1);
				var parentpath=cpath.slice(0,-name.length-1);
				result.tree=GetTree(req,parentpath,0,0,1,0);
			break;
			}
			case 'ping':{//to do			
				result.removed=new Array();			
			break;
			}
			case 'duplicate':{//to do			
				result.removed=new Array();			
			break;
			}
			case 'rm':{		
				var target = params.target;				
				fhesh=target;
				var cpath=getpath(fhesh);
				// console.log(cpath);
				resdirs=fm_rec_delete(req,cpath);
			break;
			}
			case 'ls':{			
				var targets=params.targets;
				var cpath=getpath(fhesh);
				if(cpath==''){
					res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
					return;
				}					
				result.tree=GetTree(req,cpath,0,0,0,1);
			break;
			}
			case 'file':{		
				var target = params.target;
				var cpath=getpath(target);					
				if(cpath!='' && fs.existsSync(cpath)){
					var filename = path.basename(cpath);
					var mimetype = mime.lookup(cpath);
					res.setHeader('Content-disposition', 'attachment; filename=' + filename);
					res.setHeader('Content-type', mimetype);						
					var filestream = fs.createReadStream(cpath);						
					filestream.pipe(res);
					return;
				}
				res.end(JSON.stringify({"error" : "File does not exist."}),"UTF-8"); 		
				return;
			break;
			}
			case 'read':{		
				var target = params.target;
				var cpath=getpath(target);
				if(cpath==''){
					res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
					return;
				}					
				result.content=fs.readFileSync(cpath);
			break;
			}
			case 'edit':{// to debug
				var target = params.target;
				var cpath=getpath(target);
				if(cpath==''){
					res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
					return;
				}					
				fs.writeFileSync(cpath,req.body.content);
				result.file=get_fd_desc(req,cpath);
			break;
			}
			case 'paste':{
				var targets = params.targets;
				var dst=req.query.dst;
				var dstpath=getpath(req,dst);
				if(dstpath==''){
					res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
					return;
				}
				result.removed=new Array();
				for(var i in targets){
					fhesh=targets[i];
					var cpath=getpath(fhesh);
					if(cpath==''){
						res.end(JSON.stringify({"error" : "Path does not exist."}),"UTF-8"); 		
						return;
					}						
					result.removed=new Array();
					result.added=new Array();
					result=fm_rec_copy(req, cpath,dstpath,req.query.cut=='1', result);
				}
			break;
			}
			default:{
				calipso.log("default!");
				break;
			}
		}
	} catch (e) {			
		console.log(e);
		res.end(JSON.stringify({"error" : e}),"UTF-8"); 
		return;		
	}	
	
	res.end(JSON.stringify(result),"UTF-8"); 
};

function dirExistsSync(d) {
  try { return fs.statSync(d).isDirectory() } 
  catch (er) { return false } 
} 

function fm_rec_copy(req,cpath, dst, cut, result){
	var files = new Array();
	var stats=fs.statSync(cpath);
	var fname=cpath.slice(cpath.lastIndexOf(dir_separator)+1);
	if(stats.isFile()){
		var dst_name=dst+dir_separator+fname;
		if(cut){
			var fhash=gethash(cpath);
			result.removed[result.removed.length]=fhash;
//			fs.unlinkSync(cpath);
			fs.renameSync(cpath, dst_name);
		}
		else{
			fs.createReadStream(cpath).pipe(fs.createWriteStream(dst_name));		
		}
		result.added[result.added.length]=get_fd_desc(req,dst_name);
	}
	else{
		var dst_child_path=dst+dir_separator+fname;
		if(!dirExistsSync(dst_child_path)){
			fs.mkdirSync(dst_child_path, 0755);
			result.added[result.added.length]=get_fd_desc(req,dst_child_path);			

		}
				console.log('readdir 4');
		
		var readresults = fs.readdirSync(cpath);
				console.log('readdir end 4');		
		for(var i in readresults) {
			var cname=cpath+dir_separator+readresults[i];	
			result=fm_rec_copy(cname,dst_child_path,cut,result);
		}
		if(cut){
			var fhash=gethash(cpath);
			result.removed[result.removed.length]=fhash;			
			fs.rmdirSync(cpath);
		}
	}
	return result;
}

function fm_rec_delete(req,cpath){
	var files = new Array();
	console.log('fm_rec_delete cpath=',cpath)
	var stats=fs.statSync(cpath);
	files[files.length]=gethash(cpath);
	if(stats.isFile()){
		fs.unlinkSync(cpath);
	}
	else{
		
	
		var readresults = fs.readdirSync(cpath);
		
		for(var i in readresults) {
			var cname=cpath+dir_separator+readresults[i];
			var stats=fs.statSync(cname);
			if (stats.isDirectory()){
				
				var res=fm_rec_delete(req, cname);
				var files = files.concat(res);
			}		  		
			if (stats.isFile()){
				fs.unlinkSync(cname);
				var fhash=gethash(cname);
				files[files.length]=fhash;				
			}
		}
		fs.rmdirSync(cpath);
	}
	return files;
}

exports.upload = function(req, res, next) {
	res.format = "json";
	console.log('0 point ');
	if(req.user===undefined || req.user==null){
		res.end(JSON.stringify({"error" : "You should log in."}),"UTF-8"); 
		return;
	}
	
	var user_id=req.user.id;
	var username=req.user.username;
	var cmd = req.bodyString('cmd')
	console.log('1 point ', cmd);
	try{		
		if (cmd=='upload') {		
			
			fhesh=req.bodyString('target');
			cpath=getpath(fhesh);
			console.log(cpath);
			console.log(req.body);
			if(dirExistsSync(cpath)){
				console.log('aaaaaaaa = '+req.file.originalname);				
				if (fs.existsSync(req.file.path) && req.file.originalname!=undefined) {
					fs.createReadStream(req.file.path).pipe(fs.createWriteStream(cpath + dir_separator + req.file.originalname));
				}
				res.end(JSON.stringify({status:'ok'}),"UTF-8");  
			}
			else
			{
				res.end(JSON.stringify({status:'error'}),"UTF-8"); 
			}
			return;
		}
	}
	catch (e) {
		console.log(e);
		res.end(JSON.stringify({"error" : e}),"UTF-8"); 
		return;		
	}
};



function copyFile(source, target, cb) {
  var cbCalled = false;

  var rd = fs.createReadStream(source);
  rd.on("error", function(err) {
    done(err);
  });
  var wr = fs.createWriteStream(target);
  wr.on("error", function(err) {
    done(err);
  });
  wr.on("close", function(ex) {
    done();
  });
  rd.pipe(wr);

  function done(err) {
    if (!cbCalled) {
      cb(err);
      cbCalled = true;
    }
  }
}


exports.getpath=getpath;
