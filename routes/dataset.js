var express = require('express');
var router = express.Router();
var dataset = require('../controllers/dataset/main');
var meta_manager = require('../controllers/dataset/geo_meta_table');
var map = require('../controllers/map/map');



router.pr_init=function(client){
    var db_client=client; 
    var meta_manager_object=meta_manager(100, db_client)
    global.meta_manager = meta_manager_object;
    dataset.init('', {meta_manager:meta_manager_object});
    map.init('', {meta_manager:meta_manager_object})
}



/* GET document listing. */
router.get('/list', dataset.list  );
router.post('/list', dataset.list);


/* GET file. */
router.get('/file', dataset.file  );


/* Add document. */
router.post('/add', dataset.add);

/* Edit document. */
router.post('/update', dataset.update);

/* Edit document. */
router.post('/delete', dataset.delete);


/* Edit table structure. */
router.get('/tabstructure', function(req, res, next) {
    res.render('tabstructure', { title: 'Express' });
});

/* Save table structure. */
router.post('/savestructure', dataset.savestructure);

/* Clear table. */
router.get('/clear', dataset.clear);

/* Drop table. */
router.get('/drop', dataset.drop);

/* table users. */
router.get('/user/list', dataset.users);

/* add table users. */
router.post('/user/add', dataset.useradd);

/* delete table users. */
router.post('/user/delete', dataset.userdelete);
/* Edit table structure. */
router.get('/users', function(req, res, next) {
    res.render('tabusers', { title: 'Express' });
});


module.exports = router;