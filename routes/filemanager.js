var express = require('express');
var router = express.Router();
var rfm = require('../controllers/rfm/rfm');
var multer  = require('multer');
var config = require("../conf.json");


var upload = multer({ dest: config.modules.rfm.config.upload+'/' })




/* GET file listing. */
router.get('/list', rfm.list  );

/* Run command. */
router.get('/op', rfm.op  );

/* Run command. */
router.post('/op', rfm.op  );

/* Upload file. */
router.post('/upload', upload.single('files'),rfm.upload  );


module.exports = router;