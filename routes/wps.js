var express = require('express');
var router = express.Router();
var wpsmanager = require('../controllers/wpsmanager');
var filelink = require('../controllers/wps/filelink');






/* import file. */
router.post('/execute', wpsmanager.execute);

/* get service status. */
router.get('/output', wpsmanager.output);

/* get file. */
router.get('/getfile*', filelink.getfile);

/* Edit service info. */
router.get('/edit', function(req, res, next) {
    res.render('wps', { title: 'Express' });
});


/* proxy GetCapabilities. */
router.get('/proxy', wpsmanager.proxy);


module.exports = router;