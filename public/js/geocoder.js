Zoom = 17;
var OBJLATLON = new Array();
var init_geocoder=false;
var markers;
function getGeo(mType, address){
	if(!init_geocoder){
		init_geocoder=true;
		markers = new L.LayerGroup();
		map.addLayer(drawnItems);
		map.on('baselayerchange',function(e){
			map.removeLayer(markers);
			emptyGeoList();
		});		
		$('#dg-objects-list').delegate('.fa-home', 'click', function(e) {
			var i = $(this).parent().attr('id');
			map.setView(OBJLATLON[i],Zoom);
		});
		$('#dg-objects-list').delegate('a', 'click', function(e) {
			var i = $(this).parent().attr('id');
			map.setView(OBJLATLON[i],Zoom);
		});
		$('#dg-objects-list').delegate('.icon-info-sign', 'click', function(e) {
			var i = $(this).parent().attr('id');
			map.setView(OBJLATLON[i],Zoom);			
			smart_i.empty_info();
			smart_i.get_info(OBJLATLON[i].lng,OBJLATLON[i].lat, 0);
			$('#info_address').html($('a', $(this).parent()).html());
			emptyGeoList();
			
		});
		
		
	}	
	//saddress = 'Иркутск ' + address; 
	saddress = address; 
	switch (mType){
		case 'tgis': 
			return DGSearch(saddress);
			break;
		case 'google':
			return GoogleSearch(saddress);
			break;
		case 'yam':
			return YandexSearch(saddress);
			break;
		case 'osm':
			return OSMSearch(saddress);
			break;
		case 'cad':
			return RRSearch(address);
			break;
	}
}	

function emptyGeoList(){			
	var dgObjects = $("#dg-objects-list");
	dgObjects.empty();
	OBJLATLON = [];
	return dgObjects;
}




L.Control.ActiveLayers = L.Control.Layers.extend({

  getActiveBaseLayer: function () {
    return this._activeBaseLayer
  },

  getActiveOverlayLayers: function () {
    return this._activeOverlayLayers
  },

  onAdd: function (map) {
    var container = L.Control.Layers.prototype.onAdd.call(this, map)

    this._activeBaseLayer = this._findActiveBaseLayer()
    this._activeOverlayLayers = this._findActiveOverlayLayers()
    return container
  },

  _findActiveBaseLayer: function () {
    var layers = this._layers
    for (var layerId in layers) {
      if (this._layers.hasOwnProperty(layerId)) {
        var layer = layers[layerId]
        if (!layer.overlay && this._map.hasLayer(layer.layer)) {
          return layer
        }
      }
    }
    throw new Error('Error')
  },

  _findActiveOverlayLayers: function () {
    var result = {}
    var layers = this._layers
    for (var layerId in layers) {
      if (this._layers.hasOwnProperty(layerId)) {
        var layer = layers[layerId]
        if (layer.overlay && this._map.hasLayer(layer.layer)) {
          result[layerId] = layer
        }
      }
    }
    return result
  },

  _onInputClick: function () {
    var i, input, obj,
        inputs = this._form.getElementsByTagName('input'),
        inputsLen = inputs.length,
        baseLayer

    this._handlingClick = true

    for (i = 0; i < inputsLen; i++) {
      input = inputs[i]
      obj = this._layers[input.layerId]

      if (input.checked && !this._map.hasLayer(obj.layer)) {
        this._map.addLayer(obj.layer)
        if (!obj.overlay) {
          baseLayer = obj.layer
          this._activeBaseLayer = obj
        } else {
          this._activeOverlayLayers[input.layerId] = obj
        }
      } else if (!input.checked && this._map.hasLayer(obj.layer)) {
        this._map.removeLayer(obj.layer)
        if (obj.overlay) {
          delete this._activeOverlayLayers[input.layerId]
        }
      }
    }

    if (baseLayer) {
      this._map.setZoom(this._map.getZoom())
      this._map.fire('baselayerchange', {layer: baseLayer})
    }

    this._handlingClick = false
  }

})

L.control.activeLayers = function (baseLayers, overlays, options) {
  return new L.Control.ActiveLayers(baseLayers, overlays, options)
}


var map, objects = [];
var addMarkers = function(markerObj)
{
    if(typeof markers !== 'undefined')
    {
		markers.clearLayers();
        map.removeLayer(markers);
        objects = [];   
    }

    $.each(markerObj, function(index, obj)
    {
        var profileLocation = new L.LatLng(obj.lat, obj.lng),
            locationMarker = new L.Marker(profileLocation);

        markers.addLayer(locationMarker);
        objects.push(locationMarker);
    });
    map.addLayer(markers);   
};
	
function DGSearch(address){
	dgObjects = emptyGeoList();
   	DG.Geocoder.get(address, {
		types: ['city', 'settlement', 'district','street','house','living_area','place', 'sight'], 
        limit: 10,
        success: function(geocoderObjects) {
			OBJINFO = new Array();
			var len = geocoderObjects.length;
			for(var i = 0;i < len; i++) {
				var geocoderObject = geocoderObjects[i];
				var all = geocoderObject.getAttributes();
				var geoPoint = geocoderObject.getCenterGeoPoint();
				//var geometry = geocoderObject.getSelection();	
				//L.polygon(geometry.getPoints()).addTo(map);		
				latlon = L.latLng(geoPoint.lat,geoPoint.lon);				
				OBJLATLON[i] = latlon;
				OBJINFO[i] = (function(geocoderObject) {
					var info = '';
					info += geocoderObject.getName() + ' \n';
					return info;					
				})(geocoderObject);
				dgObjects.append('<li id="' + i + '" class = "dg-place-address">'+ 
					'<i class="fas fa-home"></i><i class="icon-info-sign glyphicon"></i><a class="at-place-title" href="javascript:void(0);"> ' + OBJINFO[i] + '</a>' +
				'</li>');
			}
			addMarkers(OBJLATLON);
			map.setView(geoPoint,Zoom);
		},
        failure: function(code, message) {
            alert(code + ' ' + message);
        }
    });
};
	
function GoogleSearch(address) {
	dgObjects = emptyGeoList();
	geocoder = new google.maps.Geocoder();
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			var len = results.length;
			OBJINFO = new Array();
			for(i=0; i < len; i++){
				var P = results[i].geometry.location;
				var lonlat = L.latLng(P.lat(),P.lng());
				OBJLATLON[i] = lonlat;
				dgObjects.append('<li id="' + i + '" class = "dg-place-address">'+ 
					'<i class="fas fa-home"></i><i class="icon-info-sign glyphicon"></i><a class="at-place-title" href="javascript:void(0);"> ' + results[i].formatted_address + '</a>' +
				'</li>');
			}
		addMarkers(OBJLATLON);
		map.setView(lonlat,Zoom);
		} else {
			alert("Status: " + status);
		}	
    });
}

function YandexSearch(address){
	dgObjects = emptyGeoList();
	
	$.getJSON('http://geocode-maps.yandex.ru/1.x/?' + $.param({geocode: address}) + '&format=json&callback=?',
		function(data, textStatus) {
			var first;
			for(var i=0; i<data.response.GeoObjectCollection.featureMember.length; i++){
				name=data.response.GeoObjectCollection.featureMember[i].GeoObject.metaDataProperty.GeocoderMetaData.text;
				dgObjects.append('<li id="' + i + '" class = "dg-place-address">'+ 
				'<i class="fas fa-home"></i><a class="at-place-title" href="javascript:void(0);"> ' + name + '</a>' +
			'</li>');
				var P=data.response.GeoObjectCollection.featureMember[i].GeoObject.Point.pos.split(' ')
				//var P = GeoObject.geometry.getCoordinates();
				var lonlat = L.latLng(P[1],P[0]);
				if(first===undefined)
					first=lonlat;
				OBJLATLON[i] = lonlat
			
			}
			addMarkers(OBJLATLON);
			if(first)
				map.setView(first,Zoom);
		});
		/*
    ymaps.geocode(address, { results: 10 }).then(function (res) {
		OBJINFO = new Array();
		var len = res.geoObjects.getLength();
		for(i=0; i < len; i++){
			var GeoObject = res.geoObjects.get(i);
			var name = GeoObject.properties.get('text');
			dgObjects.append('<div id="' + i + '" class = "dg-place-address">'+ 
				'<i class="icon-home"></i><a class="at-place-title" href="javascript:void(0);"> ' + name + '</a>' +
			'</div>');
			var P = GeoObject.geometry.getCoordinates();
			var lonlat = L.latLng(P[0],P[1]);
			OBJLATLON[i] = lonlat
		}
		addMarkers(OBJLATLON);
		map.setView(GeoObject.geometry.getCoordinates(),Zoom);
     }, function (err) {
        alert(err.message);
    });*/
}

function OSMSearch(address){
	dgObjects = emptyGeoList();
	$.ajax({
        url : 'http://nominatim.openstreetmap.org/search',
        dataType : 'jsonp',
        jsonp : 'json_callback',
        data : {
            'q' : address,
            'format' : 'json'
        }
    }).done(function(data){
		var len = data.length;
		OBJINFO = new Array();
		if (len>0) {
			for(i=0; i < len; i++){
				var res=data[i];
				var P = new L.LatLng(res.lat, res.lon);
				OBJLATLON[i] = P;
				var name = res.display_name;
				dgObjects.append('<li id="' + i + '" class = "dg-place-address">'+ 
					'<i class="fas fa-home"></i><i class="icon-info-sign glyphicon"></i><a class="at-place-title" href="javascript:void(0);"> ' + name + '</a>' +
				'</li>');
			}
			addMarkers(OBJLATLON);
			map.setView(P,Zoom);
        };
    })
};

function RRSearch(cadnum) {
	dgObjects = emptyGeoList();
    var that=this
    cadparts = cadnum.split(':'),
    cadjoin = cadparts.join(''),
    unproject = function (x, y) {
        var earthRadius = 6378137;
        return L.CRS.EPSG900913.projection.unproject((new L.Point(x, y)).divideBy(earthRadius));
    }
    ajaxtype;
    if (cadparts.length==4) {
        ajaxtype='find';
        ajaxopt = {
            url : 'http://maps.rosreestr.ru/ArcGIS/rest/services/CadastreNew/Cadastre/MapServer/exts/GKNServiceExtension/online/parcel/find',
            dataType : 'jsonp',
            data : {
                'f' : 'json',
                'cadNum' : cadnum,
                'onlyAttributes' : 'false',
                'returnGeometry' : 'true',
            }
        }
    } else {
        var ajaxtype='query';

        if (cadjoin.length < 3) {
            Zoom = 5;
        } else if (cadjoin.length < 5) {
            Zoom = 10;
        } else {
            Zoom = 15;
        }

        ajaxopt = {
            url : 'http://maps.rosreestr.ru/ArcGIS/rest/services/CadastreNew/Cadastre/MapServer/'+Zoom+'/query',
            dataType : 'jsonp',
            data : {
				'f' : 'json',
                'where' : 'PKK_ID like \''+cadjoin+'%\'',
                'returnGeometry' : 'true',
                'spatialRel' : 'esriSpatialRelIntersects',
                'outFields' : '*'
            }
        }
    }

    $.ajax(ajaxopt).done(function(data){
        if (data.features.length>0) {
            var res=data.features[0].attributes
			var P = unproject(res['XC'],res['YC'])
			var lonlat = L.latLng(P.lat,P.lng);
			OBJLATLON[0] = lonlat;
			addMarkers(OBJLATLON);
			map.setView(P,Zoom);	
        }
    });
}


$(document).ready(function(){
	var gcoder=$('#geo-coder').click(function() {
		emptyGeoList();
		var address = $('#geo-address').val();
		blname=map.getblayername();
		if(blname=='')
			return;
		getGeo(blname,address);
	});

	$('#geo-address').keyup(function(e){
		if(e.keyCode == 13){
			gcoder.trigger("click");		
		}
	});
});  
