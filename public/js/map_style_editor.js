var chartfield_str='\
	<div class="card">	\
		<div class="card-header">    Chart field  \
			<button type="button" id="field_delete" class="close" aria-label="Close">  <span aria-hidden="true">×</span></button>\
		</div>\
		<div class="container">\
			<div class="form-group">\
				<label class="" for="title">Title</label>\
				<input class="form-control" id="title" />\
			</div>\
			<div class="form-group">\
				<label class="" for="fieldname">Fieldname</label>\
				<select id="fieldname" class="form-control"></select>\
			</div>\
			<div class="color_type class_type">\
				<div class="form-group" id="color_range">\
					<label class="" for="color">Color range</label>\
					<input class="form-control style_color" id="color" type="color" />\
				</div>\
			</div>\
		</div>\
	</div>';

function chartfield(layer_json){
	this.form_div=$(chartfield_str);
	var f=this;	
	this.title='';
	this.fieldname='';	
	this.color='#000000';	
	
	this.changeform=function(){
		for (var key in this) {
			if(typeof this[key] ==='number' || typeof this[key] ==='string'){
				v=$("#"+key, this.form_div).val();
				if(v)
					this[key]=v;
			}
		}
		this.changemodel();	
	}
	this.changemodel=function(){
		$('#fieldname',this.form_div).empty();
		
		for(var ii=0; ii<layer_json.columns.length; ii++){
			var wtype = layer_json.columns[ii].type;
			if(layer_json.columns[ii].fieldname===undefined || layer_json.columns[ii].title===undefined)
				continue;
			if(wtype==='number' || wtype==='date' || wtype==='serial' || wtype==='integer'){
				opt=$('<option value="'+layer_json.columns[ii].fieldname+'">'+layer_json.columns[ii].title+'</option>');					
				$('#fieldname',this.form_div).append(opt);
			}			
		}
		for (var key in this) {
			if(typeof this[key] ==='number' || typeof this[key] ==='string'){
				var inp=$('#'+key,this.form_div);
				inp.val(this[key]);
			}
		}
		
		var color_div=$('#color_div', this.form_div);
		var color=$('<input class="style_color" id="color" type="color"/>');
		color_div.empty();
		color_div.append(color);
		color.val(this.color);

		
		
		$('input',this.form_div).change(function(){
			f.changeform();
		});
		$('select',this.form_div).change(function(){
			f.changeform();
		});
		
		$('#field_delete',this.form_div).unbind();
		$('#field_delete',this.form_div).click(function(){
			var ind=f.parent.chartfields.indexOf(f);
			f.parent.chartfields.splice(ind,1);
			f.form_div.detach();
		});
		
		//this.Draw();
	}
	

	return this;
}

/*
	this.bar_heigth=20;
	this.pie_minsize=10;
	this.pie_maxsize=50;
	this.pie_minval=10;
	this.pie_maxval=50;*/

var Map_style_editor_str='\
<div id="layer_options" style="" title="Map style editor">\
	<form class="form-horizontal">\
		<div class="row">\
			<div id="general" class="col-12 stylegeneral">\
				<div class="container">\
					<div class="form-row">\
						<div class="form-group col-md-6">\
							<label class="" for="layer_title">Title</label>\
							<input class="form-control" type="text" id="title" value="" />\
						</div>\
						<div class="form-check col-md-3">\
							<input type="checkbox" class="" id="published">\
							<label class="" for="exampleCheck1">Publish</label>\
						</div>\
						<div class="form-group col-md-3">\
							<label class="" for="style_type">Classify type</label>\
							<select id="style_type" class="form-control">\
							<option value="single">Single symbol</option>\
							<option value="wizard">Classify wizard</option>\
							<option value="rules">Rules</option>\
							<option value="chart">Chart</option>\
							<option value="gradient">Gradient</option>\
							<option value="initial">Initial colors</option>\
							</select>\
						</div>\
					</div>\
					<div class="form-row">\
						<div class="form-group col-md-6">\
							<label class="" for="authors">Authors</label>\
							<input class="form-control" type="text" id="authors" value="" />\
						</div>\
						<div class="form-group col-md-6">\
							<label class="" for="labelfieldname">Label</label>\
							<select class="form-control type_style" id="labelfieldname"></select>\
						</div>\
					</div>\
					<div class="form-row">\
							<div id="" class="single style_type_div col-md-4">\
							</div>\
							<div id=""  class="rules style_type_div col-md-4">\
								<a class="btn btn-small" id="r_add"  href="#"><i class="fas fa-plus"></i> Add rule</a> \
							</div>\
							<div id="wizard_btns" class="wizard style_type_div col-md-4">\
								<a class="btn btn-small" id="add_field"  href="#"><i class="fas fa-plus"></i> Add field</a>\
								<a class="btn btn-small" id="generaterules"  href="#"><i class="fas fa-plus"></i> Generate rules</a>\
							</div>\
					</div>\
				</div>\
			</div>\
		</div>\
		<div id="style_type_cont">\
			<div id="single" class="style_type_div">\
			</div>\
			<div id="rules"  class="rules style_type_div" >\
			</div>\
			<div id="wizard" class="wizard_list style_type_div" >\
			</div>\
			<div id="chart" class="container style_type_div">\
				<div class="form-row">\
					<label class="" for="chart_type">Chart type</label>\
					<div class="input-group">\
						<select id="chart_type" class="form-control col-2"><option value="pie">Pie</option><option value="bar">Bar</option><option value="vbar">Vertical bar</option></select>\
						<div class="input-group-append">\
							<span class="input-group-text" id="add_chartfield" >Add field</span>\
						</div>\
					</div>\
				</div>\
				<div class="form-row">\
					<div class="" style="display:None">\
						<div class="form-group" id="">\
							<label class="" for="chart_dx">dx</label>\
							<input class="form-control" type="text" id="chart_dx" value="" />\
						</div>\
						<div class="form-group" id="">\
							<label class="" for="chart_dy">dy</label>\
							<input class="form-control" type="text" id="chart_dy" value="" />\
						</div>\
					</div>\
					<div class="form-group bar_options col-md-2" id="">\
						<label class="" for="bar_width">Bar width</label>\
						<input class="form-control" type="text" id="bar_width" value="" />\
					</div>\
					<div class="form-group bar_options col-md-2" id="">\
						<label class="" for="bar_heigth">Bar heigth</label>\
						<input class="form-control" type="text" id="bar_heigth" value="" />\
					</div>\
					<div class="form-group pie_options col-md-2">\
						<label class="" for="chart_size">Chart size</label>\
						<select class="form-control type_style" id="chart_size"></select>\
					</div>\
					<div class="form-group pie_options static_chart_size col-md-2" id="static_chart_size_div">\
							<label class="" for="static_chart_size">Chart size</label>\
							<input class="form-control" type="text" id="static_chart_size" value="" />\
					</div>\
					<div class="form-group pie_options dyn_chart_size col-md-2" id="">\
							<label class="" for="pie_minsizeh">Pie minsize</label>\
							<input class="form-control" type="text" id="pie_minsize" value="" />\
					</div>\
					<div class="form-group pie_options dyn_chart_size col-md-2" id="">\
							<label class="" for="pie_maxsize">Pie maxsize</label>\
							<input class="form-control" type="text" id="pie_maxsize" value="" />\
					</div>\
					<div class="form-group pie_options dyn_chart_size col-md-2" id="">\
							<label class="" for="pie_minval">Pie minval</label>\
							<input class="form-control" type="text" id="pie_minval" value="" />\
					</div>\
					<div class="form-group pie_options dyn_chart_size col-md-2" id="">\
							<label class="" for="pie_maxval">Pie maxval</label>\
							<input class="form-control" type="text" id="pie_maxval" value="" />\
					</div>\
				</div>\
				<div id="chartfields"  class="row" style="overflow-y: auto;">\
				</div>\
			</div>\
		</div>\
	</form>\
</div>';
/*	res+='\
	LABEL\n\
		COLOR  255 255 254\n\
		OUTLINECOLOR 205 128 67\n\
		FONT "verdana"\n\
		TYPE truetype\n\
		SIZE '+this.fontsize+'\n\
		ANGLE 0\n\
		POSITION AUTO\n\
		PARTIALS FALSE\n\
	END \n\
END\n';	*/
var def_label={
	'labelfieldname':'none',
	'fontsize': 12,
	'label_color':'#000000',
	'label_dx':0,
	'label_dy':0,
	'font':'verdana'
};
var def_layer={'stroke_type':'solid', 
					'stroke_width':1,
					'stroke_opacity':100, 
					'stroke_color':'#0000FF', 
					'stroke_sign_size':8,
					'stroke_sign_angle':0,
					'stroke_sign_dx':0,
					'stroke_sign_dy':0, 
					'stroke_gap':10,
					'fill_type':'solid', 
					'fill_width':1, 
					'fill_opacity':100, 
					'fill_color':'#00FF00', 
					'fill_sign_size':10,
					'fill_sign_angle':0,
					'fill_sign_dx':0,
					'fill_sign_dy':0,
					'fill_gap':10,
					'fill_sign':false,
					'point_type':'circle',
					'point_opacity':100,
					'point_color':'#FF0000',
					'point_sign_size':10,
					'point_sign_angle':0,
					'point_sign_dx':0,
					'point_sign_dy':0,					
					'image':'',
					'point_font':'MapInfoSymbols',
					'point_font_character':'&#68;'};

function update_layer(sign_layer){
	for (var key in def_layer) {
		if(!(key in sign_layer)){
			sign_layer[key]=def_layer[key];
		}
	}
}
					
function sld_rule(name){
	this.form_div=$('\
	<div class="sld_rule notsinglerule card">\
		<div class="row align-items-start">\
			<div class="vectorrule">\
				<div class="col-2 form-group" id="comp_sign" >\
					<canvas id="rule_canvas" width="80" height="45">Обновите браузер</canvas>\
				</div>\
				<div class="col-10 form-row">\
					<div class="expression_div form-group col-md-4" id="">\
						<label>Expression</label>\
						<input class="form-control" id="expression"/>\
					</div>\
					<div class="expression_div form-group col-md-2">\
						<label>Title</label>\
						<input id="mark"  type="text" class="form-control class_mark_input"/>\
					</div>\
					<div class="form-group col-md-2">\
						<label class="" for="font_size">Font size</label>\
						<input class="form-control" type="text" id="font_size"/>\
					</div>\
					<div class="form-group col-md-2">\
						<label class="" for="font_size">Font color</label>\
						<input class="form-control style_color" id="font_color" type="color"/>\
					</div>\
					<div class="form-group col-md-2">\
						<button id="italic" class="btn btn-small" type="button"><i class="fas fa-italic"></i></button>\
						<button id="bold" class="btn btn-small" type="button"><i class="fas fa-bold"></i></button>\
					</div>\
				</div>\
			</div>\
			<div class="rastrrule container">\
				<div class="col-10 form-row align-items-center">\
					<div class="form-group col">\
						<input class="form-control"  id="gradientfrom" type="color" />\
					</div>\
					<div class="form-group col gradientto">\
						<input class="form-control"  id="gradientto" type="color" />\
					</div>\
					<div class="form-group col">\
						<label class="" for="rastropacity">Rastr opacity (%)</label>\
						<input class="form-control"  id="rastropacity" type="text" />\
					</div>\
				</div>\
			</div>\
			<button type="button" id="field_delete" class="close" aria-label="Close">  <span aria-hidden="true">×</span></button>\
		</div>\
	</div> ');
	this.name=name;
	this.styletype=0;	//0 - simple; 1- exspression
	this.operator='PropertyIsEqualTo';
	this.mark=name;	
	this.PropertyName='';
	this.val='';
	this.val2='';
	this.expression='';
	this.labelfieldname='';
	this.font_size=7;
	this.font_color='#000000';
	this.font_bold=false;
	this.font_italic=false;
	this.rastropacity=100;
	this.gradientfrom='';
	this.gradientto='';
	this.style_type='';
	
	//this.compare_sign='PropertyIsEqualTo';
	this.symbolizers=new Array();
	this.x=1;
	this.y=1;
	var f=this;
	//this.fontsize=8;
	this.changeform=function(){
		for (var key in this) {
			if(typeof this[key] ==='number' || typeof this[key] ==='string'){
				v=$("#"+key, this.form_div).val();
				if(v)
					this[key]=v;
			}
		}
		
		this.font_bold=$('#bold',this.form_div).hasClass('active');
		this.font_italic=$('#italic',this.form_div).hasClass('active');
		
		this.changemodel();
	}
	
	this.changemodel=function(){	
		for (var key in this) {
			if(typeof this[key] ==='number' || typeof this[key] ==='string'){
				$('#'+key,this.form_div).val(this[key]);
			}
		}		
		if(this.font_bold)
			$('#bold',this.form_div).addClass('active');
		else
			$('#bold',this.form_div).removeClass('active');
		if(this.font_italic)
			$('#italic',this.form_div).addClass('active');
		else
			$('#italic',this.form_div).removeClass('active');
		
		$('#bold, #italic', this.form_div).unbind();
		$('#bold, #italic', this.form_div).click(function(){
			if($(this).hasClass('active'))
				$(this).removeClass('active');
			else
				$(this).addClass('active');				
			f.changeform();
		});
		
		$('#field_delete', this.form_div).click(function(){
			var ind=f.parent.rules.indexOf(f);
			f.parent.rules.splice(ind,1);
			f.form_div.detach();
		});
		
		$('input,textarea,select',this.form_div).unbind();
		
		$('input,textarea,select',this.form_div).change(function(){
			f.changeform();
		});
		$('#rule_canvas',this.form_div).unbind();
		$('#rule_canvas',this.form_div).click(function(){			
			s=new show_symbolizer(f.symbolizers, f.geometry_type, function (layers){		
				f.symbolizers=layers;
				//frm.g_sld.rules[0].symbolizers=layers;
				f.changemodel();
			});
		});
		
		$('#r_delete',this.form_div).unbind();
		$('#r_delete',this.form_div).click(function(){
			var ind=f.parent.rules.indexOf(f);
			f.parent.rules.splice(ind,1);
			f.form_div.detach();	
		});
		if(this.geometry_type=="tiff"){
			$(".vectorrule").hide();
			$(".rastrrule").show();
			if(this.style_type == "single" && this.symbolizers.length>0){
				this.symbolizers[0].fill_color = this.gradientfrom;
			}
		}
		else{
			$(".vectorrule").show();
			$(".rastrrule").hide();
		}
			
		this.Draw();
	}
	
	
		
	this.readPointSymbolizer=function (node){
		var sign_layer={	sign_type: $(node).find("WellKnownName").text() ,	size: $(node).find("Size").text() , stroke_color: $(node).find("CssParameterCssParameter [name='stroke']").text() ,	bgcolor: $(node).find("CssParameter[name='fill']").text() ,	dx:0,	dy:0, angle:0};
		if(sign_layer.size===undefined || sign_layer.size=='')
			sign_layer.size=1;
		return sign_layer;
	}
  
	this.savePointSymbolizer=function (sld_obj){
		str='<se:PointSymbolizer>  <se:Graphic>   <se:Mark><se:WellKnownName>'+sld_obj.sign_type+'</se:WellKnownName><se:Fill><se:CssParameter name="fill">'+sld_obj.bgcolor+'</se:CssParameter ></se:Fill><se:Stroke><se:CssParameter  name="stroke">'+sld_obj.stroke_color+'</se:CssParameter></se:Stroke></se:Mark><se:Size>'+sld_obj.size+'</se:Size></se:Graphic></se:PointSymbolizer>';
		return str;
	}

	this.readLineSymbolizer=function readLineSymbolizer(node){
		var sign_layer={	sign_type: '',	stroke_width: $(node).find("CssParameter[name='stroke-width']").text() , stroke_color: $(node).find("CssParameter[name='stroke']").text() ,	bgcolor: '' ,	dx:0,	dy:0, angle:0};
		if(sign_layer.stroke_width===undefined || sign_layer.stroke_width=='')
			sign_layer.stroke_width=1;	
		if(sign_layer.stroke_color===undefined || sign_layer.stroke_color=='')
			sign_layer.stroke_color='#000000';	
		return sign_layer;
	}
  
	this.saveLineSymbolizer=function (sld_obj){
		str='<se:LineSymbolizer><Geometry><PropertyName>center-line</PropertyName></Geometry><Stroke><CssParameter name="stroke">'+sld_obj.stroke_color+'</CssParameter><CssParameter name="stroke-width">'+sld_obj.stroke_width+'</CssParameter></Stroke></se:LineSymbolizer>'; 
		return str;
	}
  
	this.readPolygonSymbolizer=function (node){
		var sign_layer={	sign_type: '',	stroke_width: $(node).find("CssParameter[name='stroke-width']").text() , stroke_color: $(node).find("CssParameter[name='stroke']").text() ,	bgcolor: $(node).find("CssParameter[name='fill']").text() ,	dx:0,	dy:0, angle:0, fillStyle: $(node).find("WellKnownName").text() };
		if(sign_layer.stroke_width===undefined || sign_layer.stroke_width=='')
			sign_layer.stroke_width=1;	
		if(sign_layer.stroke_color===undefined || sign_layer.stroke_color=='')
			sign_layer.stroke_color='#000000';	

		if(sign_layer.fillStyle===undefined || sign_layer.fillStyle=='')
			sign_layer.fillStyle='solid';	

		return sign_layer;
	}
  
	this.savePolygonSymbolizer=function (sld_obj){
		str='<se:PolygonSymbolizer><Geometry><PropertyName>the_area</PropertyName></Geometry><se:Fill><se:GraphicFill><se:Graphic><se:Mark><se:WellKnownName>' + sld_obj.fillStyle + '</sld:WellKnownName>\
          <se:Stroke><se:CssParameter name="stroke">'+sld_obj.bgcolor+'</se:CssParameter><se:CssParameter name="stroke-width">'+sld_obj.stroke_width+'</se:CssParameter></se:Stroke></se:Mark>\
        <se:Size>8</sld:Size></se:Graphic>  </se:GraphicFill></se:Fill></se:PolygonSymbolizer>'; 
		return str;
	}
  /*<se:GraphicFill><se:Graphic><se:Mark><se:WellKnownName>shape://slash</sld:WellKnownName>
          <se:Stroke>
            <se:CssParameter name="stroke">#0000ff</se:CssParameter>            
            <se:CssParameter name="stroke-width">4</se:CssParameter>
          </se:Stroke>
        </se:Mark>
        <se:Size>8</sld:Size>
      </se:Graphic>  </se:GraphicFill>
	*/
	this.readTextSymbolizer=function (node){
		var sign_layer={	label: $(node).find("Label").text(), fontsize: $(node).find("CssParameter[name='font-size']").text() };
		return sign_layer;
	}
  
	this.saveTextSymbolizer=function (sld_text){
	res='<TextSymbolizer>\
<Geometry>\
<PropertyName>locatedAt</PropertyName>\
</Geometry>\
<Label>'+sld_text.label+'</Label>\
<Font>\
<CssParameter name="font-family">arial</CssParameter>\
<CssParameter name="font-style">italic</CssParameter>\
<CssParameter name="font-weight">bold</CssParameter>\
<CssParameter name="font-size">'+sld_text.fontsize+'</CssParameter>\
</Font>\
<Fill>\
<CssParameter name="fill">#000000</CssParameter>\
</Fill>\
<LabelPlacement>\
<PointPlacement>\
<AnchorPoint>\
<AnchorPointX>0.5</AnchorPointX>\
<AnchorPointY>0.5</AnchorPointY>\
</AnchorPoint>\
<Displacement>\
<DisplacementX>20</DisplacementX>\
<DisplacementY>0</DisplacementY>\
</Displacement>\
<Rotation>0</Rotation>\
</PointPlacement>\
</LabelPlacement>\
<Halo/>\
</TextSymbolizer>';

		return res;
	} 
  	
	this.readFromXML=function(rule_node, geometry_type){		
		this.name=rule_node.find("Name").text();
		this.mark=this.name;
		this.geometry_type=geometry_type;
		
		TextSymbolizers=rule_node.find("TextSymbolizer");
		if(TextSymbolizers.length>0){
			this.label=this.readTextSymbolizer(TextSymbolizers[0])
		}		
		
		Filter=rule_node.find("Filter");
		if(Filter.length>0){
			this.styletype=1;
			this.PropertyName=Filter.find("PropertyName").text();
			eq=Filter.find("PropertyIsBetween");
			if(eq.length>0){
				this.operator='PropertyIsBetween';
				this.val=Filter.find("LowerBoundary").find("Literal").text();
				this.val2=Filter.find("UpperBoundary").find("Literal").text();
			}
			else{			
				this.val=Filter.find("Literal").text();				
				eq=Filter.find("PropertyIsNotEqualTo");
				if(eq.length>0){
					this.operator='PropertyIsNotEqualTo';
				}
				eq=Filter.find("PropertyIsLessThan");
				if(eq.length>0){
					this.operator='PropertyIsLessThan';
				}
				eq=Filter.find("PropertyIsLessThanOrEqualTo");
				if(eq.length>0){
					this.operator='PropertyIsLessThanOrEqualTo';
				}
				eq=Filter.find("PropertyIsGreaterThan");
				if(eq.length>0){
					this.operator='PropertyIsGreaterThan';
				}
				eq=Filter.find("PropertyIsGreaterThanOrEqualTo");
				if(eq.length>0){
					this.operator='PropertyIsGreaterThanOrEqualTo';
				}
			}		
		}
		
		PointSymbolizers=rule_node.find("PointSymbolizer");
		if(PointSymbolizers.length>0){
			this.symbolizers=new Array();
			for(var j=0; j<PointSymbolizers.length; j++){
				sld_layer=this.readPointSymbolizer(PointSymbolizers[j]);
				this.symbolizers.push(sld_layer);
			}
		}			
		LineSymbolizers=rule_node.find("LineSymbolizer");			
		if(LineSymbolizers.length>0){
			this.symbolizers=new Array();
			for(var j=0; j<LineSymbolizers.length; j++){
				sld_layer=this.readLineSymbolizer(LineSymbolizers[j]);
				this.symbolizers.push(sld_layer);
			}
		}
		PolygonSymbolizers=rule_node.find("PolygonSymbolizer");			
		if(PolygonSymbolizers.length>0){
			this.symbolizers=new Array();
			for(var j=0; j<PolygonSymbolizers.length; j++){
				sld_layer=this.readPolygonSymbolizer(PolygonSymbolizers[j]);
				this.symbolizers.push(sld_layer);
			}
		}
	}
	this.Draw=function(rule_canvas ){
		if(!rule_canvas){
			rule_canvas=$('#rule_canvas', this.form_div)
		}
		cnvs=rule_canvas[0];
		ctx=cnvs.getContext('2d');
		ctx.stroke();
		ctx.clearRect(0, 0, cnvs.width, cnvs.height);
		if(this.style_type != "gradient"){
			for(var i=0; i<this.symbolizers.length; i++){
				update_layer(this.symbolizers[i])
				draw_prim(ctx, 0, 0, this.symbolizers[i], this.geometry_type);
			}
		}
		else if (this.gradientfrom!=''){
			var grd = ctx.createLinearGradient(0, 0, cnvs.width, 0);
			grd.addColorStop(0, this.gradientfrom);
			grd.addColorStop(1, this.gradientto);

			// Fill with gradient
			ctx.fillStyle = grd;
			ctx.fillRect(10, 10, 150, 80);
		}
		
	}
	this.DrawSLD=function(){
		res='<se:Rule>\n';		
		if(this.styletype==0){
			res+='<se:Name>'+this.mark+'</se:Name>';				
		}
		else{
			res+='<se:Name>'+this.mark+'</se:Name>';
			if(this.operator!='PropertyIsBetween'){				
				res+='\
					  <ogc:Filter>\
			<ogc:'+this.operator+'>\
			  <ogc:PropertyName>'+this.PropertyName+'</ogc:PropertyName>\
			  <ogc:Literal>'+this.val+'</ogc:Literal>\
			</ogc:'+this.operator+'>\
		  </ogc:Filter>';
			}
			else{
				res+='\
					  <ogc:Filter>\
		<ogc:PropertyIsBetween>\
          <ogc:PropertyName>'+this.PropertyName+'</ogc:PropertyName>\
          <ogc:LowerBoundary>\
            <ogc:Literal>'+this.val+'</ogc:Literal>\
          </ogc:LowerBoundary>\
          <ogc:UpperBoundary>\
            <ogc:Literal>'+this.val2+'</ogc:Literal>\
          </ogc:UpperBoundary>\
      </ogc:PropertyIsBetween>\
	  </ogc:Filter>';
			}
		}

		for(var j=0; j<this.symbolizers.length; j++){
			if(this.geometry_type=='point')
				res+=this.savePointSymbolizer(this.symbolizers[j])+'\n';
			else if(this.geometry_type=='line')
				res+=this.saveLineSymbolizer(this.symbolizers[j])+'\n';
			if(this.geometry_type=='polygon')
				res+=this.savePolygonSymbolizer(this.symbolizers[j])+'\n';
		}
		if(this.label!=undefined)
			res+=this.saveTextSymbolizer(this.label);
		res+='</se:Rule>\n';
		return res;
	}
	
	this.DrawPolygonMAP=function(sign_layer){
		res='';
		if(sign_layer.fill_type=='solid')
			res+='\
	STYLE\n\
		COLOR  '+hex2rgb(sign_layer.fill_color)+'\n\
		OUTLINEWIDTH 10\n\
		OPACITY '+sign_layer.fill_opacity+'\n\
	END\n\
	';
	
		else if(sign_layer.fill_type!='none'){
			if(!sign_layer.fill_width || sign_layer.fill_width==null)
				sign_layer.fill_width=2;
			res+='\
	STYLE\n\
		SYMBOL "' + sign_layer.fill_type + '"\n\
		WIDTH ' + sign_layer.fill_width + '\n\
		COLOR  '+hex2rgb(sign_layer.fill_color)+'\n\
		SIZE '+sign_layer.fill_sign_size+'\n\
		GAP '+sign_layer.fill_gap+'\n\
		OFFSET '+sign_layer.fill_sign_dx+' '+sign_layer.fill_sign_dy+'\n\
		OPACITY '+sign_layer.fill_opacity+'\n\
	END\n\
	';
		}
		if(sign_layer.stroke_type=='solid')
			res+='\
	STYLE\n\
		OUTLINECOLOR  '+hex2rgb(sign_layer.stroke_color)+'\n\
		WIDTH '+sign_layer.stroke_width+'\n\
		OPACITY '+sign_layer.stroke_opacity+'\n\
	END\n\
	';
		return res;
	}

	this.DrawRastrMAP=function(sign_layer,  mapstyle){		
		var res='';
		if(this.style_type=='gradient'){
			res+='\
			STYLE\n\
				COLORRANGE  '+hex2rgb(this.gradientfrom)+'	'+hex2rgb(this.gradientto)+'\n\
				OPACITY '+this.rastropacity+'\n\
				DATARANGE '+mapstyle.layer_json.min+' '+mapstyle.layer_json.max+'\n\
				RANGEITEM "foobar"\n\
			END\n\
			';
		}
		else if(this.style_type=='initial'){
			res='';
		}
		else{
			res='\
			STYLE\n\
				COLOR'+hex2rgb(sign_layer.fill_color)+'\n\
				OPACITY '+sign_layer.fill_opacity+'\n\
			END\n\
			';
		}
	
	return res;
		
		
		if(sign_layer.fill_type=='solid')
			res+='\
	STYLE\n\
		COLOR  '+hex2rgb(sign_layer.fill_color)+'\n\
		OUTLINEWIDTH 10\n\
		OPACITY '+sign_layer.fill_opacity+'\n\
	END\n\
	';
	
		else if(sign_layer.fill_type!='none')
			res+='\
	STYLE\n\
		SYMBOL "' + sign_layer.fill_type + '"\n\
		COLOR  '+hex2rgb(sign_layer.fill_color)+'\n\
		SIZE '+sign_layer.fill_sign_size+'\n\
		GAP '+sign_layer.fill_gap+'\n\
		OFFSET '+sign_layer.fill_sign_dx+' '+sign_layer.fill_sign_dy+'\n\
		OPACITY '+sign_layer.fill_opacity+'\n\
	END\n\
	';
		if(sign_layer.stroke_type=='solid')
			res+='\
	STYLE\n\
		OUTLINECOLOR  '+hex2rgb(sign_layer.stroke_color)+'\n\
		OUTLINEWIDTH '+sign_layer.stroke_width+'\n\
		OPACITY '+sign_layer.stroke_opacity+'\n\
	END\n\
	';
		return res;
	}
	this.DrawLineMAP=function(sign_layer){
		res='';
		
		if(sign_layer.stroke_type=='solid')
			res+='\
	STYLE\n\
		OUTLINECOLOR  '+hex2rgb(sign_layer.stroke_color)+'\n\
		OUTLINEWIDTH '+sign_layer.stroke_width+'\n\
		OPACITY '+sign_layer.stroke_opacity+'\n\
	END\n\
	';
		else if(sign_layer.stroke_type!='none')
			res+='\
	STYLE\n\
		SYMBOL "' + sign_layer.stroke_type + '"\n\
		COLOR  '+hex2rgb(sign_layer.stroke_color)+'\n\
		SIZE '+sign_layer.stroke_sign_size+'\n\
		GAP '+sign_layer.stroke_gap+'\n\
		OPACITY '+sign_layer.stroke_opacity+'\n\
	END\n\
	';
	
		return res;
	}
	
	this.DrawPointMAP=function(sign_layer){
		if(sign_layer.point_type=='font symbol')
			res='\
	STYLE\n\
		SYMBOL "'+sign_layer.point_font + sign_layer.point_font_character+'"\n\
		COLOR  '+hex2rgb(sign_layer.point_color)+'\n\
		SIZE '+sign_layer.point_sign_size +'\n\
		OFFSET '+sign_layer.point_sign_dx+' '+sign_layer.point_sign_dy+'\n\
		OPACITY '+sign_layer.point_opacity+'\n\
	END\n\
	';
		else if(sign_layer.point_type=='image')
			res='\
	STYLE\n\
		SYMBOL "'+sign_layer.image+'"\n\
		COLOR  '+hex2rgb(sign_layer.point_color)+'\n\
		SIZE '+sign_layer.point_sign_size+'\n\
		OFFSET '+sign_layer.point_sign_dx+' '+sign_layer.point_sign_dy+'\n\
		OPACITY '+sign_layer.point_opacity+'\n\
	END\n\
	';
		else
			res='\
	STYLE\n\
		SYMBOL "'+sign_layer.point_type+'"\n\
		COLOR  '+hex2rgb(sign_layer.point_color)+'\n\
		SIZE '+sign_layer.point_sign_size+'\n\
		OFFSET '+sign_layer.point_sign_dx+' '+sign_layer.point_sign_dy+'\n\
		OPACITY '+sign_layer.point_opacity+'\n\
	END\n\
	';
		return res;
	}	
	
	this.DrawMAP=function(expression, mapstyle){
		if(this.title)
			res='\
CLASS\n\
			NAME    "'+this.title+'"    	\n';
		else
			res='\
CLASS\n\
			NAME    "unnamedclass"    	\n';
			if(expression && this.expression && this.expression!=''){
				res+='\
	EXPRESSION	('+this.expression+') \n\
	';
			}
			for(var j=0; j<this.symbolizers.length; j++){
				update_layer(this.symbolizers[j]);
				/*
				if(this.symbolizers[j].stroke_width===undefined){
					this.symbolizers[j].stroke_width=1;			
				}*/
				if(this.geometry_type=='point'){
					res+=this.DrawPointMAP(this.symbolizers[j]);					
				}
				else if(this.geometry_type=='line'){
					res+=this.DrawLineMAP(this.symbolizers[j]);
				}
				if(this.geometry_type=='polygon'){
					res+=this.DrawPolygonMAP(this.symbolizers[j]);
				}
				if(this.geometry_type=='tiff'){
					res+=this.DrawRastrMAP(this.symbolizers[j], mapstyle);
				}
			}			
			var font='verdana';
			if(this.font_bold && this.font_italic)
				font='verdana-bold-italic';
			else if(this.font_bold)
				font='verdana-bold';
			else if(this.font_italic)
				font='verdana-italic';

			if(this.font_size && this.font_size!=''){
				if(this.geometry_type=='line'){
					res+='\
	LABEL\n\
		COLOR  '+hex2rgb(this.font_color)+'\n\
		FONT "'+font+'"\n\
		TYPE truetype\n\
		SIZE '+this.font_size+'\n\
		ANGLE AUTO\n\
		POSITION AUTO\n\
		PARTIALS FALSE\n\
	END \n';
				}
				else
					res+='\
	LABEL\n\
		COLOR  '+hex2rgb(this.font_color)+'\n\
		FONT "'+font+'"\n\
		TYPE truetype\n\
		SIZE '+this.font_size+'\n\
		ANGLE 0\n\
		POSITION AUTO\n\
		PARTIALS FALSE\n\
	END \n';
			}
			res+='	END\n';
		return res;
	}
	var sign_layer=cloneArray(def_layer);// {sign_type:'square',	size: 20, stroke_color:'#000000',	bgcolor:'#00FF00',	dx:0,	dy:0, angle:0};
	this.symbolizers.push(sign_layer);
	return this;
}


function map_style(){	
	this.form_div=$(Map_style_editor_str);
	var base_symbolizer=$("#base_symbolizer", this.form_div);
	var m_style=this;
	this.layer_name='';
	this.style_type='single'; //0 - simple; 1- exspression
	this.labelfieldname='';	
	this.geometry_type='';
	this.layer_json;
	//this.BAND=0;
	this.rules=[];		
	this.base_rule=new sld_rule('');
	this.base_rule.geometry_type=m_style.geometry_type;
	this.rules.push(this.base_rule);
	
	this.chart_type='pie';
	this.chart_size='static';
	this.chart_dx=0;
	this.chart_dy=0;
	this.static_chart_size=30;
	this.chartfields=[];
	this.bar_width=20;
	this.bar_heigth=20;
	this.pie_minsize=10;
	this.pie_maxsize=100;
	this.pie_minval=10;
	this.pie_maxval=50;
	
	//r=new sld_rule('');
	//this.rules.push(r);
	this.title='';
	this.published =false;
	this.flds=[];
	this.readFromJSON=function(json_text){
		obj=JSON.parse(json_text);
		var tmp_geometry_type=this.geometry_type;
		this.rules=[];
		this.flds=[];
		this.chartfields=[];
		$.extend(this, obj);
		if(obj.geometry_type===undefined || obj.geometry_type=='')
			this.geometry_type=tmp_geometry_type;
		/*if(obj.base_rule){
			this.base_rule=new sld_rule('');
			this.base_rule.geometry_type=m_style.geometry_type;
			$.extend( this.base_rule, obj.base_rule);
		}*/
		for(var i=0; i<obj.rules.length; i++){
			r=new sld_rule('');
			$.extend(r, this.rules[i]);
			r.geometry_type=this.geometry_type;
			this.rules[i]=r;
			if((!r.expression || r.expression=='') && (r.PropertyName!='' && r.val!='') && r.operator){
				switch(r.operator) {
					case 'PropertyIsBetween':
						r.expression="'["+r.PropertyName+"]' >= '"+r.val+"' AND '["+r.PropertyName+"]' <='"+r.val2+"'";
						break
					case 'PropertyIsNotEqualTo':
						r.expression="'["+r.PropertyName+"]' != '"+r.val+"'";
						break					
					case 'PropertyIsLessThanOrEqualTo':
						r.expression="'["+r.PropertyName+"]' <= '"+r.val+"'";
						break
					case 'PropertyIsLessThan':
						r.expression="'["+r.PropertyName+"]' <'"+r.val+"'";
						break
					case 'PropertyIsGreaterThan':
						r.expression="'["+r.PropertyName+"]' >'"+r.val+"'";
						break
					case 'PropertyIsGreaterThanOrEqualTo':
						r.expression="'["+r.PropertyName+"]' >='"+r.val+"'";
						break
					default:
						r.expression="'["+r.PropertyName+"]' =='"+r.val+"'";
						break
				}
			}			
		}
		br=new sld_rule('');
		$.extend(br, this.base_rule);
		br.geometry_type=this.geometry_type;
		this.base_rule=br;
		
		//this.base_rule=this.rules[0];
		
		if(obj.flds){
			for(var i=0; i<obj.flds.length; i++){
				f=new field_classify(this.layer_json);
				$.extend(f, this.flds[i]);
				if(obj.flds[i].classes){
					var len=obj.flds[i].classes.length;
					for(var j=0; j<len; j++){
						cls=new fieldclass();
						$.extend(cls, obj.flds[i].classes[j]);
						obj.flds[i].classes[j]=cls;
						//f.classes.push(cls);
					}
				}
				this.flds[i]=f;			
			}
		}
		if(obj.chartfields){
			for(var i=0; i<obj.chartfields.length; i++){
				f=new chartfield(this.layer_json);
				$.extend(f, this.chartfields[i]);				
				this.chartfields[i]=f;			
			}
		}
		
		this.changemodel();
	};
//var sign_layer={	sign_type: '',	stroke_width: $(node).find("CssParameter[name='stroke-width']").text() , stroke_color: $(node).find("CssParameter[name='stroke']").text() ,	bgcolor: $(node).find("CssParameter[name='fill']").text() ,	dx:0,	dy:0, angle:0, fillStyle: $(node).find("WellKnownName").text() };
			
	this.save2JSON=function(){
			this.bar_width=20;
			/*
	this.bar_heigth=20;
	this.pie_minsize=10;
	this.pie_maxsize=50;
	this.pie_minval=10;
	this.pie_maxval=50;*/
		

		var keys=['rules', 'published','base_rule','sym','font_bold','font_italic','pie_maxval','chart_dx', 'chart_dy','pie_minval','pie_maxsize','pie_minsize', 'chart_type','chart_size', 'bar_width', 'bar_heigth','static_chart_size','chartfields', 'title', 'style_type', 'geometry_type', 'LABELITEM', 'label', 'PropertyName','symbolizers', 'val', 'val2', 'operator', 'mark', 'BAND','expression', 'classes', 'flds','gradientfrom', 'gradientto','rastropacity'];
		
		for (var key in def_layer) {
			keys.push(key);			
		}
		s = new field_classify(this.layer_json);
		for (var key in s) {
			if(typeof s[key] ==='number' || typeof s[key] ==='string')
				keys.push(key);
		}
		s = new fieldclass();
		for (var key in s) {
			if(typeof s[key] ==='number' || typeof s[key] ==='string')
				keys.push(key);
		}
		s = new sld_rule();
		for (var key in s) {
			if(typeof s[key] ==='number' || typeof s[key] ==='string')
				keys.push(key);
		}
		
		s = new chartfield();
		for (var key in s) {
			if(typeof s[key] ==='number' || typeof s[key] ==='string')
				keys.push(key);
		}
		for (var key in def_label) {
			keys.push(key);			
		}
		
		var ind=keys.indexOf('form_div');
		keys.splice(ind,1);
		return JSON.stringify(this, keys);
	}
	this.readFromSLD = function(sld_text){
		try{
			xmlDoc = $.parseXML(sld_text);
		} catch(e) {
			return;
		}
		xmlDoc=$(xmlDoc);
		this.layer_name=xmlDoc.find("NamedLayer > Name").text();
		FeatureTypeStyle=xmlDoc.find("FeatureTypeStyle");
		xml_rules=FeatureTypeStyle.find("Rule");
		for( var i=0; i<xml_rules.length; i++){	
			rule_node=$(xml_rules[i]);			
			Filter=rule_node.find("Filter");
			if(Filter.length>0)
				this.styletype=1;
		}
		this.rules=[];
		for( var i=0; i<xml_rules.length; i++){	
			rule_node=$(xml_rules[i]);
			if(rule_node.find("Name").text()=='default') continue;
			r=new sld_rule('');
			r.readFromXML(rule_node, '');			
			r.styletype = this.styletype;
			r.geometry_type=this.geometry_type;
			this.rules.push(r);
		}
		if(this.rules.length==0){
			r=new sld_rule('');			
			r.styletype = this.styletype;
			r.geometry_type=this.geometry_type;
			this.rules.push(r);
		}
	}
	this.save2SLD=function (){
		var res='<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">\
  <NamedLayer>\n\
    <se:Name>'+this.layer_name+'</se:Name>\n\
        <UserStyle>\n\
      <se:Name>'+this.layer_name+'</se:Name>\n\
      <se:FeatureTypeStyle>\n';
		
		//this.p
		var lastsymb='';
		for(var i=0; i < this.rules.length; i++){
			res+=this.rules[i].DrawSLD();
		}
		/*
		def_symb={	sign_type: 'star' ,	size: 1, stroke_color: '#000000' ,	bgcolor: '#000000' ,	dx:0,	dy:0, angle:0, stroke_width:1};
		if(frm.fieldtype=='point')
			res+='<se:Rule><se:Name>default</se:Name>'+this.savePointSymbolizer(def_symb)+'</se:Rule>';			
		else if(frm.fieldtype=='line')
			res+='<se:Rule><se:Name>default</se:Name>'+this.saveLineSymbolizer(def_symb)+'</se:Rule>';			
		if(frm.fieldtype=='polygon')
			res+='<se:Rule><se:Name>default</se:Name>'+this.savePolygonSymbolizer(def_symb)+'</se:Rule>';
		*/
		res+='			</se:FeatureTypeStyle>\n\
		</UserStyle>\n\
    </NamedLayer>\n\
</StyledLayerDescriptor>';
		//console.log(res);
		return res;
	};
		
	this.setgeometrytype=function(geom){
		this.geometry_type=geom;
		this.base_rule.geometry_type=geom;
		for(var i=0; i<this.rules.length; i++){
			rule=this.rules[i];	
			rule.geometry_type=geom;			
		}
	};
	this.save2Map=function(){
			var res='';
			if(this.geometry_type!='tiff')
				res="TYPE "+this.geometry_type+"\n";
			
			if(this.labelfieldname && this.labelfieldname!=''){
				res+='LABELITEM    "'+this.labelfieldname+'"\n			';
			}
			else if(this.label &&  this.label.labelfieldname !='none'  &&  this.label.labelfieldname !=null){
				res+='LABELITEM    "'+this.label.labelfieldname+'"\n			';
			}
			
			if(this.style_type=='single' || this.style_type==0){
				this.base_rule.style_type=this.style_type;
				if(this.geometry_type=='tiff'){
					this.base_rule.expression='[pixel] > -999999999'
					res+=this.base_rule.DrawMAP(true, this);
				}
				else					
					res+=this.base_rule.DrawMAP(false, this);
			}
			else if(this.style_type=='chart'){
				res='	TYPE CHART\n\
		PROCESSING "CHART_TYPE='+this.chart_type+'"\n\
	';
				if(this.chart_type!='pie'){
					res+='PROCESSING "CHART_SIZE='+this.bar_width+' '+this.bar_heigth+'"\n';
				}
				else{
					if(this.chart_size=='static')
						res+='PROCESSING "CHART_SIZE='+this.static_chart_size+'"\n';
					else
						res+='PROCESSING "CHART_SIZE_RANGE='+this.chart_size+' '+this.pie_minsize+' '+this.pie_maxsize+' '+this.pie_minval+' '+this.pie_maxval+'"\n';
				}
					for(var i=0; i<this.chartfields.length; i++){			
						res+='CLASS\n\
			NAME "'+this.chartfields[i].fieldname+'"\
			STYLE\n\
				SIZE ['+this.chartfields[i].fieldname+']\n\
				COLOR   '+hex2rgb(this.chartfields[i].color)+'\n\
			END\n\
		END\n';

				}
			}
			else if(this.style_type=='gradient'){
				res+=this.base_rule.DrawMAP(true, this);
			}
			else if(this.style_type=='initial'){
				res+='';
			}			
			else {
				for(var i=0; i<this.rules.length; i++){
					rule=this.rules[i];		
					res+=rule.DrawMAP(true, this);	
				}
			
			}
	/*		if(this.rules[0].PropertyName!=undefined && this.rules[0].PropertyName!='')
				res+='\
			CLASSITEM    "'+this.rules[0].PropertyName+'"\n\
			';*/
			//console.log(res);
			return res;		
		
	};

	
	var flds_form=$('#wizard', this.form_div);
	var rules_div=$('#rules', this.form_div);
	var chartfields_div=$('#chartfields', this.form_div);
	this.changemodel=function(){
		$('#labelfieldname',this.form_div).empty();
		opt=$('<option value="">None</option>');					
		$('#labelfieldname',this.form_div).append(opt);
		for(var ii=0; ii<this.layer_json.columns.length; ii++){
			var wtype = this.layer_json.columns[ii].type;
			if(this.layer_json.columns[ii].fieldname===undefined || this.layer_json.columns[ii].title===undefined)
				continue;
			if(wtype==='string' ||  wtype==='number' || wtype==='date' || wtype==='serial' || wtype==='region'  || wtype==='integer'){
				opt=$('<option value="'+this.layer_json.columns[ii].fieldname+'">'+this.layer_json.columns[ii].title+'</option>');					
				$('#labelfieldname',this.form_div).append(opt);
			}
		}
		
		$('#chart_size',this.form_div).empty();
		opt=$('<option value="static">Static</option>');					
		$('#chart_size',this.form_div).append(opt);
		for(var ii=0; ii<this.layer_json.columns.length; ii++){
			var wtype = this.layer_json.columns[ii].type;
			if(this.layer_json.columns[ii].fieldname===undefined || this.layer_json.columns[ii].title===undefined)
				continue;
			if(wtype==='number' || wtype==='integer'){
				opt=$('<option value="'+this.layer_json.columns[ii].fieldname+'">'+this.layer_json.columns[ii].title+'</option>');					
				$('#chart_size',this.form_div).append(opt);
			}
		}
		
		if(this.chart_size=='static'){
			$('#static_chart_size_div',this.form_div).show();
			$('.dyn_chart_size',this.form_div).hide();			
		}
		else{
			$('#static_chart_size_div',this.form_div).hide();
			$('.dyn_chart_size',this.form_div).show();
		}
		
		if(this.chart_type=='pie'){
			$('.pie_options',this.form_div).show();
			$('.bar_options',this.form_div).hide();
		}
		else{
			$('.pie_options',this.form_div).hide();
			$('.bar_options',this.form_div).show();			
		}
		for (var key in this) {
			if(typeof this[key] ==='number' || typeof this[key] ==='string'){
				$('#'+key,this.form_div).val(this[key]);
			}
			else if(typeof this[key] ==='boolean' ){
				$('#'+key,this.form_div).prop('checked', this[key]);
			}
		}
		$('.style_type_div', this.form_div).hide();
		
		$('#single .expression_div:first').hide();
		//$('.wizard', this.form_div).hide();
		//$('#wizard', this.form_div).hide();
		if(this.style_type=='wizard'){
			$('.wizard', this.form_div).show();
			$('#wizard', this.form_div).show();
			//$('#base_sign_div', this.form_div).show();	
			$('.rules', this.form_div).hide();
			$('.gradient_options', this.form_div).hide();	
			$('#single', this.form_div).show();	
			$('.gradientto', this.form_div).hide();

			//$('.sld_rule', this.form_div).hide();
			//$('.sld_rule:first', this.form_div).show();
			//$('.sld_rule:first > div.expression_div', this.form_div).hide();
			
		}
		else if(this.style_type=='rules'){
			$('.rules', this.form_div).show();
			
			//$('.sld_rule', this.form_div).show();
			//$('.sld_rule:first > div.expression_div', this.form_div).show();			
			$('#single', this.form_div).hide();
			$('.gradient_options', this.form_div).hide();	
			$('.gradientto', this.form_div).hide();
		}
		else if(this.style_type=='initial'){
			$('.rules', this.form_div).hide();
			
			//$('.sld_rule', this.form_div).show();
			//$('.sld_rule:first > div.expression_div', this.form_div).show();			
			$('#single', this.form_div).hide();
			$('.gradient_options', this.form_div).hide();	
			$('.gradientto', this.form_div).hide();
		}
		else if(this.style_type=='chart'){
			$('.rules', this.form_div).hide();
			//$('.sld_rule', this.form_div).hide();	
			$('#chart', this.form_div).show();
			$('#single', this.form_div).hide();
			$('.gradient_options', this.form_div).hide();	
		}
		// else if(this.style_type=='gradient'){
		// 	$('.rules', this.form_div).hide();
		// 	$('.gradient_options', this.form_div).show();	
		// 	$('#chart', this.form_div).hide();
		// 	$('#single', this.form_div).hide();
		// 	$('.gradientto', this.form_div).show();
		// }		
		else {
			$('.single', this.form_div).show();
			$('#single', this.form_div).show();
			$('.rules', this.form_div).hide();
			$('.gradient_options', this.form_div).hide();	
			if(this.style_type=='gradient')
				$('.gradientto', this.form_div).show();
			else
				$('.gradientto', this.form_div).hide();			
			//$('.sld_rule', this.form_div).hide();
			//$('.sld_rule:first', this.form_div).show();
			//$('.sld_rule:first > div.expression_div', this.form_div).hide();
			//$('#base_sign_div', this.form_div).show();			
		}
		
		$('#single', this.form_div).append(this.base_rule.form_div);			
		this.base_rule.geometry_type=this.geometry_type;
		this.base_rule.parent=this;
		this.base_rule.style_type=this.style_type;
		this.base_rule.changemodel();
	
		
		flds_form.empty();
		for(var i=0; i<this.flds.length; i++){
			flds_form.append(this.flds[i].form_div);
			if(!this.flds[i].sym && this.base_rule && this.base_rule.symbolizers && this.base_rule.symbolizers.length>0)
				this.flds[i].sym=cloneArray(this.base_rule.symbolizers[0]);
			//this.classes[i].geometry_type=this.geometry_type;
			//this.classes[i].sym=this.sym;			
			this.flds[i].geometry_type=this.geometry_type;
			this.flds[i].parent=this;
			this.flds[i].changemodel();
		}
		rules_div.empty();
		for(var i=0; i<this.rules.length; i++){
			rules_div.append(this.rules[i].form_div);			
			this.rules[i].geometry_type=this.geometry_type;
			this.rules[i].parent=this;
			this.rules[i].style_type=this.style_type;
			this.rules[i].changemodel();
		}
		chartfields_div.empty();
		for(var i=0; i<this.chartfields.length; i++){
			chartfields_div.append(this.chartfields[i].form_div);			
			this.chartfields[i].parent=this;
			this.chartfields[i].changemodel();
		}
		// $('#gradientfrom', this.form_div).val(this.gradientfrom);
		// $('#gradientto', this.form_div).val(this.gradientto);
		//this.base_rule.geometry_type=this.geometry_type;
		//this.base_rule.Draw(base_symbolizer,0,0, true);
	}
	this.changeform=function(){
		for (var key in this) {
			if(typeof this[key] ==='number'){
				v=$("#"+key, this.form_div).val();
				if(v)
					this[key]=v;
			}
			else if(typeof this[key] ==='string'){
				v=$("#"+key, this.form_div).val();
				if(v || key=='title')
					this[key]=v;
			
			}
			else if(typeof this[key] ==='boolean'){
				v=$("#"+key, this.form_div).prop('checked');
				if(v )
					this[key]=v;
			
			}		
		}
		this.changemodel();
	}
	$('select', this.form_div).change(function(){
		m_style.changeform();
	});
	
	$('input', this.form_div).change(function(){
		m_style.changeform();
	});
	var wizard_btns=$('#wizard_btns',this.form_div);
	
	
	$("#add_field",wizard_btns).click(function (){
		var fld=new field_classify(m_style.layer_json);
		fld.sym=m_style.base_rule.symbolizers[0];
		fld.geometry_type=m_style.geometry_type;
		m_style.flds.push(fld);					
		m_style.changemodel();
	});
	
	$("#add_chartfield",this.form_div).click(function (){
		var fld=new chartfield(m_style.layer_json);		
		m_style.chartfields.push(fld);					
		m_style.changemodel();
	});
	
	
	
	$('#r_add', this.form_div).click(function(){
		r=new sld_rule('');
		r.symbolizers=cloneArray(m_style.base_rule.symbolizers);
		r.geometry_type=m_style.geometry_type;
		m_style.rules.push(r)
		m_style.changemodel();
	});
	//wizard_btns	
	
	
	base_symbolizer.dblclick(function(){			
			s=new show_symbolizer(m_style.base_rule.symbolizers, m_style.geometry_type,  function (layers){		
				m_style.base_rule.symbolizers=layers;				
				m_style.changemodel();
			});
	});
	
	$('canvas',flds_form).dblclick(function(){			
		s=new show_symbolizer(m_style.base_rule.symbolizers, m_style.geometry_type, function (layers){		
			m_style.base_rule.symbolizers=layers;				
			m_style.base_rule.Draw(cnvs, 0, 0, true);
		});
	});
	
	this.generaterules=function (numfield, classlist){
		if(numfield>=m_style.flds.length){								
			r=new sld_rule('');
			r.geometry_type=m_style.geometry_type;
			r.symbolizers=cloneArray(m_style.base_rule.symbolizers);
			if(classlist.length>0){
				r.font_size=classlist[0].font_size;
				r.font_color=classlist[0].font_color;
				r.font_bold=classlist[0].font_bold;
				r.font_italic=classlist[0].font_italic;
				r.symbolizers=cloneArray(classlist[0].sym);
			}
			for(var j=0; j<classlist.length; j++){
				if(r.expression!='')
					r.expression+=' AND ';
				if(classlist[j].low==classlist[j].up){
					if(m_style.flds[j].fieldtype=='number')
						r.expression+="["+m_style.flds[j].fieldname+"] == "+classlist[j].low+"";
					else
						r.expression+="'["+m_style.flds[j].fieldname+"]' == '"+classlist[j].low+"'";
					r.mark+=classlist[j].low+' ';
				}
				else{
					if(m_style.flds[j].fieldtype=='number')
						r.expression+="["+m_style.flds[j].fieldname+"] >= "+classlist[j].low+" AND ["+m_style.flds[j].fieldname+"] <="+classlist[j].up+"";
					else
						r.expression+="'["+m_style.flds[j].fieldname+"]' >= '"+classlist[j].low+"' AND '["+m_style.flds[j].fieldname+"]' <='"+classlist[j].up+"'";
				}
				if(m_style.flds.length>1){
					switch(m_style.flds[j].classify_type) {
						case 'color':  // if (x === 'value1')
							if(m_style.flds[j].geometry_type=='polygon')
								r.symbolizers[0].fill_color=classlist[j].val;
							else if(m_style.flds[j].geometry_type=='point')
								r.symbolizers[0].point_color=classlist[j].val;	
							else
								r.symbolizers[0].stroke_color=classlist[j].val;				
																
						break;
						case 'width':  // if (x === 'value2')
							r.symbolizers[0].stroke_width=classlist[j].val;
							r.symbolizers[0].fill_width=classlist[j].val;											
						break;
						case 'size':  // if (x === 'value2')
							r.symbolizers[0].fill_sign_size=classlist[j].val;
							r.symbolizers[0].point_sign_size=classlist[j].val;											
						break;										
					}
				}
			}
			this.rules.push(r);
		}
		else if(m_style.flds && m_style.flds.length>numfield){
			for (var i = 0; i < m_style.flds[numfield].classes.length; i++) {
				classlist[numfield] = m_style.flds[numfield].classes[i];
				m_style.generaterules(numfield+1, classlist);
			}
		}
		this.changemodel();
	}
	
	$('#generaterules', this.form_div ).click(function(){		
		$('#rules', this.form_div).empty();
		m_style.rules=[];
		m_style.generaterules(0,[]);
	});
	
	return this;
};	


// if(typeof (module)!== 'undefined')
// 	module.exports = fieldcalc;
