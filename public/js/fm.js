var dir_separator = '/';

/** 
 * Returns the file name from the full file path.
 *
 * @param string cpath Full path
 *
 * @return string
 */
function get_filename(cpath){
	return cpath.slice(cpath.lastIndexOf(dir_separator) + 1);
} //end get_filename()

/** 
 * Returns the directory of the full file path.
 *
 * @param string cpath Full path
 *
 * @return string
 */
function get_directory(cpath){
	if(cpath.lastIndexOf(dir_separator)==-1)
		return '';
	return cpath.slice(0, cpath.lastIndexOf(dir_separator));
} //end get_filename()


/**
 * Converts string to hex representation.
 *
 * @paaram string str Converted string
 *
 * @return void
 */
function toHex(str) {
    var hex = '';
    for(var i = 0; i < str.length; i++) {
        hex += '' + str.charCodeAt(i).toString(16);
    }

    return hex;
} //end toHex()


/**
* Base64 encode / decode
* http://www.webtoolkit.info/
*/
var Base64 = {

	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
 
	encode : function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;
 
		input = Base64._utf8_encode(input);
 
		while (i < input.length) {
 
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);
 
			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;
 
			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}
 
			output = output +
			this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
		}
 
		return output;
	},
 
	decode : function (input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;
 
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
 
		while (i < input.length) {
 
			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));
 
			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;
 
			output = output + String.fromCharCode(chr1);
 
			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}
 
		}
 
		output = Base64._utf8_decode(output);
 
		return output;
	},
 
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = ""; 
		for (var n = 0; n < string.length; n++) { 
			var c = string.charCodeAt(n); 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			} 
		} 
		return utftext;
	},
 
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
 
		while ( i < utftext.length ) {
 
			c = utftext.charCodeAt(i);
 
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
 
		}
 
		return string;
	}
} //end Base64


/**
 * Returns hashed file path.
 *
 * @param string path Relative file path
 *
 * @return string
 */
function gethash(path){
	path=path+'';
	if (path=='') path='\\';

	path=path.replace(/\\\\/g,"\\");
	path=path.replace(/^\\+/g,"");

	var fhash=Base64.encode(path);	
	fhash=fhash.replace(/\+/g,"-");
	fhash=fhash.replace(/\//g,"_");
	fhash=fhash.replace(/=/g,".");
	fhash=fhash.replace(/\.+$/g,"");

	
	return fhash;
} //end getha-sh()


/**
 * Extracts file path from the hash.
 *
 * @param string hash Processed hash
 *
 * @return string
 */
function getpath(hash){
	if (hash=== undefined || hash=='' || hash=="l1_XA") {
		return dir_separator;
	}

	drive = hash.substr(0,3);
	if('l1_'==drive)
		dir=hash.slice(3);
	else
		dir=hash;//.slice(3);
	dir=dir.replace(/-/g,"+");
	dir=dir.replace(/_/g,"/");
	dir=dir.replace(/\./g,"=");	

	var dir = Base64.decode(dir);
	while(dir.length>0 && dir.charCodeAt(dir.length - 1)==0) {
		dir = dir.substring(0, dir.length - 1);
	}
	return dir;
} //end getpath()


/**
 * Extracts file path from the hash.
 *
 * @param string hash Processed hash
 *
 * @return string
 */
function gettemplate(hash){
	if(!hash)
		return '';
	
	if(hash.substr(0,3)=='l1_'){
		dir=hash.slice(3);
	}
	else{
		dir=hash;
	}
	dir=dir.replace(/-/g,"+");
	dir=dir.replace(/_/g,"/");
	dir=dir.replace(/\./g,"=");	

	var dir = Base64.decode(dir);
	while(dir.length>0 && dir.charCodeAt(dir.length - 1)==0) {
		dir = dir.substring(0, dir.length - 1);
	}
	return dir;
} 
/**
 * Reads file from the file system.
 *
 * @param string   path     Path to the file
 * @param Function callback Callback that needs to be executed
 *
 * @return void
 */
function readFile(path, callback) {
	hash = gethash(path);
	$.get("/fm/op", { cmd: "file", target: hash } ).done(function(data) { callback(data); });
} //end readFile()

function filelink(path) {
	hash = gethash(path);
	return "/fm/op?cmd=file&target="+hash;
} //end readFile()
/**
 * Saves data as a certain file on the server.
 *
 * @param string path    Path to the file
 * @param string content File content
 *
 * @return void
 */
function saveFile(path, content) {
	hash = gethash(path);
	$.post("/fm/op", { cmd: "edit", target: hash, content:content } ).done(function(data) {  console.log("Data saved: " + data); });
} //end saveFile()


/**
 * Opens dialog with the file manager for selecting the file.
 *
 * @param Function callback Callback that needs to be executed
 * @param Object   options  File manager dialog settings
 *
 * @return void
 */
function OpenFileDlg(callback, options) {
	FileDlg(callback, 'open', options);
} //end OpenFileDlg()


/**
 * Opens dialog with the file manager for saving the file.
 *
 * @param Function callback Callback that needs to be executed
 * @param Object   options  File manager dialog settings
 *
 * @return void
 */
function SaveFileDlg(callback, options) {
	FileDlg(callback, 'save', options);
} //end SaveFileDlg()


/**
 * Invoking the file manager dialog.
 *
 * @param Function callback Callback that needs to be executed
 * @param string   mode     File manager mode
 * @param Object   options  File manager dialog settings
 *
 * @return void
 */
function FileDlg(callback, mode, options) {
	
//	return;
	/**
	 * Adjusts the overlay z-index in order to fix the known bug.
	 *
	 * @param Object dialog Dialog node
	 *
	 * @return void
	 */
	var _checkOverlay = function (dialog) {
		return;
		if (dialog.length === 1) {
			var zIndex = parseInt($(dialog[0]).parent().css("z-index"));
			if (zIndex > 0) {
				$(".ui-widget-overlay").css("z-index", zIndex - 1);
			} else {
				$(".ui-widget-overlay").css("z-index", "1004");
			}
		} else {
			throw "Unable to check the overlay z-index property";
		}
	} //end _checkOverlay()
	
	
	var dlg = $("#fm_OpenFileDlg");
	if(dlg.length == 0) {
 		dlg=$("<div id='fm_OpenFileDlg' title='Save File Dialog' >\
			<div style='display:None'><input id='fileupload' type='file' name='upload[]' data-url='/fm' multiple></div>\
			<div class='row'>\
				<div class='col-12'>\
					Path:<input id='filepath' size='1024' disabled='disabled' class='demo jstree jstree-0 jstree-focused jstree-default' style='width:655px' type='text'/>\
				</div>\
			</div>\
			<div class='row'>\
				<div class='col-3'>Folders<div id='dirtree' class='fmautoscroll'></div></div>\
				<div id='filelist' class='col-9'></div>\
			</div>\
			<div class='row'>\
				<div class='col-12' id='tr_filename'>\
					File name:<input size='1024' id='filename' class='demo jstree jstree-0 jstree-focused jstree-default' style='width:655px' type='text'/>\
				</div>\
			</div>\
		</div>");
 		dlg.appendTo("body");
		
		filelistmenu();

		
		
		
	}

	$('#filename').val('');	
	$('#filelist', dlg).html('');
	$('#filepath', dlg).val('');

	if (mode == 'open') {
		$('#filename',dlg).attr('disabled', 'disabled');
		dlgtitle = 'Open File Dialog';
		$("#tr_filename",dlg).hide();
	} else {
		$("#filename",dlg).removeAttr('disabled');
		$("#tr_filename",dlg).show();
		dlgtitle = 'Save File Dialog';
	}

	var filename       = $('#filename', dlg);
	var uploadfilename = '';
	filename.bind("keyup", function (e) {		
		var dir = getpath($('#dirtree').jstree('get_selected')[0]);
		// if ($('#dirtree').jstree('get_selected').attr('id')!=undefined && $('#dirtree').jstree('get_selected').attr('id')!='') {
		// 	var dir = getpath($('#dirtree').jstree('get_selected').attr('id'));
		// }

		var trg = $(e.target);
		var v   = dir + dir_separator + trg.val();
		$('#filepath').val(v);
	});

	$('#fileupload').change(function(){
		uploadfilename='';
		var control = document.getElementById("fileupload");
		var i = 0,
        files = control.files,
        len = files.length;		
		if(len==0){			
			return false;
		}	
		// var dir=$('#dirtree').jstree('get_selected');
		var dir=$('#filepath', dlg).val();
		if (dir==''){
			alert('You should select dir!');
			return false;
		}
		
		

		uploadfilename=dir+dir_separator+files[0].name;
		
		
		var form = new FormData();
		form.append("cmd", "upload");
		
		form.append("target", gethash($('#filepath',dlg).val()));
		form.append("files", control.files[0]);
		
		// отправляем через xhr
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			console.log("Отправка завершена");
			hash=gethash(dir);
			$('#dirtree').jstree('select_node', hash);
			
			$("#dirtree").trigger('changed.jstree', {selected: [hash]} );			

			// if ($('#dirtree').jstree('get_selected').attr('id')!=undefined && $('#dirtree').jstree('get_selected').attr('id')!='' && uploadfilename!=''){		
			// 	$("#dirtree").trigger('select_node.jstree', [{rslt:{obj:[{id:$('#dirtree').jstree('get_selected').attr('id') }]}}] );			
			// }				
		};
		xhr.open("post", "/fm/upload", true);
		xhr.send(form);
	});


	filename.change(function () {
//		$('#filepath').val(this.val());
	});
	dlg.attr('dlgmode', mode);
	if(mode == 'open'){
		var dialog = dlg.dialog({
			minWidth: 750,
			title: dlgtitle,
			position: ["center", "center"],
			close: function( event, ui ) {
				$('#fm_OpenFileDlg').remove();
			},
			buttons: {
			"Open": function() {
					if($('#filepath').val()=='' || $('#filename').val()==''	){
						alert('Select a file!');
						return;
					}
					callback($('#filepath').val());
	 				$(this).dialog("close");
					$('#fm_OpenFileDlg').remove();
				},
			"Upload": function() {
					var dir=$('#dirtree').jstree('get_selected');
					if (dir.length==0 || dir[0]=='')
						return false;
					var curent_dir=$('#filepath', dlg).val();
					if (curent_dir===undefined || curent_dir==''){
						alert('You should select dir!');
						return false;
					}
				
					$('#fileupload').trigger('click');					
				},
			},
			modal:false
		});
		
		_checkOverlay(dialog);
	} else {
		var dialog = dlg.dialog({
			minWidth: 750,
			title: dlgtitle,
			position: ["center","center"],
			close: function( event, ui ) {
				$('#fm_OpenFileDlg').remove();
			},			
			buttons: {
			"Save": function() {
					if($('#filepath').val()=='' || $('#filename').val()==''){
						alert('Write the file name!');
						return;
					}
					v=$('#filepath').val();
					if(options!=undefined && 'filter' in options && options.filter!='' && v.slice(v.length-options.filter.length).toLowerCase()!=options.filter.toLowerCase()){
						v=v+'.'+options.filter;
					}					
					callback(v /* + dir_separator + $('#filename').val()*/);
	 				$(this).dialog("close");
					$('#fm_OpenFileDlg').remove();
				},
			},
			modal:false
		});
		
		_checkOverlay(dialog);
	}
	
	var cur_command='';
	


	function customMenu(node)
	{
		var tmp = $.jstree.defaults.contextmenu.items();
		return tmp;
		

	
	}

	var dir_tree =$('#dirtree').jstree({
		'plugins' : [ 'contextmenu','state' ],
		'contextmenu' : {
			'items' : customMenu
		},
		'core' : {			
			'data' : function (obj, cb) {
				var _this=this;
				var cid='';
				if (obj.id!='#'){
					if(obj.id!='root')
						cid=obj.id;
					$.get("/fm/list", { id: cid } ).done(function(data) {
						files=JSON.parse(data);
						items=[]
						for (var i=0; i<files.length; i++) {
							if(files[i].attr.rel=="folder"){
								var item={id:files[i].attr.id, text: files[i].data}
								item.children = true;
								items.push(item);
							}
							// else{
							// 	item.icon = "/js/file.png";
							// }
							
						}
						cb.call(_this,
							items);	

					})
				}
				else{
					cb.call(_this,
						[{id:'root', text: 'root', children: true}]);	
				}
				
			},
			'check_callback' : function(o, n, p, i, m) {
				if(m && m.dnd && m.pos !== 'i') { return false; }
				if(o === "move_node" || o === "copy_node") {
					if(this.get_node(n).parent === this.get_node(p).id) { return false; }
				}
				return true;
			}
		}
		}).on("changed.jstree", function (e, data) {			

			var node_id=data.selected[0]

			p = getpath(node_id)+dir_separator+$('#filename').val();
			if(node_id=='root')
				$('#filepath',dlg).val(dir_separator);
			else
				$('#filepath',dlg).val(p);
			$('#filename',dlg).val('');	
			$('#filelist',dlg).empty();
			//$("#dirtree",dlg).jstree("open_node","#"+data.rslt.obj[0].id);
			$.get("/fm/list", { id: node_id } ).done(function(data) 
			{						
				files=JSON.parse(data);
				for (var i=0; i<files.length; i++) {
					if(files[i].attr.rel=="folder"){
						var item=$('<div/>', {
							id: files[i].attr.id,
							rel: 'default',
		//				    text: files[i].data,
							dblclick:function(eventobject) {
								mode=dlg.attr('dlgmode');
								node_item=$(eventobject.target).parents('.dir_item');
								if(node_item.length>0){							
									$("#dirtree",dlg).jstree("deselect_all");
									$("#dirtree",dlg).jstree("open_node","#"+node_item[0].id);
									$("#dirtree",dlg).jstree("select_node","#"+node_item[0].id); 
								}
							}
						}).appendTo('#filelist',dlg).addClass( "dir_item" );				
						$('<span id="span_id"><img id="img_id" src="/images/folder.png">'+files[i].data+'</span>').appendTo(item);				
					}
				}
				var f_exts=[];
				if(options!=undefined && 'filter' in options && options.filter!=undefined && options.filter!=''){
					f_exts=options.filter.toLowerCase().split(',');
				}
				

				var found1=0;
				for (var i=0; i<files.length; i++) {
					if(files[i].attr.rel!="folder"){
						if(f_exts.length>0){
							found1=0;						
							for (var j=0; j<f_exts.length; j++) {
								if(files[i].data.slice(files[i].data.length-f_exts[j].length).toLowerCase()==f_exts[j].toLowerCase()){
									found1=1;
									break;
								}
							}
							if(found1==0)
								continue;					
						}				
						var item=$('<div rel="default"><img  id="img_id"  src="/images/file.png">' + files[i].data+'</div>').appendTo('#filelist',dlg).addClass( "file_item" );
						item.attr('id',files[i].attr.id).click(function(eventobject) {
								mode=dlg.attr('dlgmode');
								if(mode=='open'){
									var v=getpath(this.id);
									$('#filepath',dlg).val(v);
									$('#filename',dlg).val(v);
								}														
							});
						
					}
				}
			});

			////////////////////////////////////////////
		})
		.on('rename_node.jstree', function (e, data) {
			$.get('/fm/op', {'cmd':'rename', 'target' : data.node.id, 'name' : data.text })
				.done(function (d) {
					d=JSON.parse(d);
					if(d.status=='ok')
						data.instance.set_id(data.node, d.newid);
					else
						data.instance.refresh();
				})
				.fail(function () {
					data.instance.refresh();
				});
		})
		.on('delete_node.jstree', function (e, data) {
			if(confirm("Delete?")){
				$.get('/fm/op', { 'cmd':'rm', 'target' : data.node.id })
				.fail(function () {
					data.instance.refresh();
				});
			}
			else
				data.instance.refresh();
		})
		.on('create_node.jstree', function (e, data) {
			$.get('/fm/op', {'cmd':'mkdir', 'type' : data.node.type, 'target' : data.node.parent, 'name' : data.node.text })
				.done(function (d) {
					d=JSON.parse(d);
					if(d.status=='ok')
						data.instance.set_id(data.node, d.newid);
					else
						data.instance.refresh();	
				})
				.fail(function () {
					data.instance.refresh();
				});
			
		})
;

	
}


function OpenFolderDlg(callback) {
	$("<div class='lang' id='fm_OpenFolderDlg' title='Open File Dialog' style='height:200px; width:210px'> <div id='dirtree' class='demo' style='height:200px; width:200px'></div></div>").appendTo("body");
	$("#fm_OpenFolderDlg").dialog({
		minWidth: 200,
		position: ["center","center"],
			close: function( event, ui ) {
				$('#fm_OpenFolderDlg').remove();
		},
		buttons: {
		"Open": function() {
				if ($('#dirtree').jstree('get_selected').attr('id')===undefined || $('#dirtree').jstree('get_selected').attr('id')==''){
					alert('Select a folder!');
					return;
				} 
				callback(getpath($('#dirtree').jstree('get_selected').attr('id')) );

 				$(this).dialog("close");
				$("#fm_OpenFolderDlg").remove();
			},
		},
		modal:false
	});
$("#dirtree")
//	.bind("before.jstree", function (e, data) {
//		$("#alog").append(data.func + "<br />");
//	})
	.jstree({
		"plugins" : [ 
			"themes","json_data","ui","crrm","dnd","search","types","contextmenu" 
		],
		"json_data" : { 
			"ajax" : {
				"cache" : "false",
				"url" : "/fm/list",
				"data" : function (n) { 
					// the result is fed to the AJAX request `data` option
					return { 
						"mode" : "2", 
						"id" : n.attr ? n.attr("id") : 1 
					}; 
				}
			}
		},
		"types" : {
			"max_depth" : -2,
			"max_children" : -2,
			"valid_children" : [ "drive" ],
			"types" : {
				"default" : {
					"valid_children" : "none",
					"icon" : {
						"image" : "/js/file.png"
					}
				},
				"folder" : {
					"valid_children" : [ "default", "folder" ],
					"icon" : {
						"image" : "/js/folder.png"
					}
				},
				"drive" : {
					"valid_children" : [ "default", "folder" ],
					"icon" : {
						"image" : "./root.png"
					},
					"start_drag" : false,
					"move_node" : false,
					"delete_node" : false,
					"remove" : false
				}
			}
		}
	});
}


var filelistmenu=(function() {
  
	"use strict";
  
	
	/**
	 * Function to check if we clicked inside an element with a particular class
	 * name.
	 * 
	 * @param {Object} e The event
	 * @param {String} className The class name to check against
	 * @return {Boolean}
	 */
	function clickInsideElement( e, className ) {
	  var el = e.srcElement || e.target;
	  
	  if ( el.classList.contains(className) ) {
		return el;
	  } else {
		while ( el = el.parentNode ) {
		  if ( el.classList && el.classList.contains(className) ) {
			return el;
		  }
		}
	  }
  
	  return false;
	}
  
	/**
	 * Get's exact position of event.
	 * 
	 * @param {Object} e The event passed in
	 * @return {Object} Returns the x and y position
	 */
	function getPosition(e) {
	  var posx = 0;
	  var posy = 0;
  
	  if (!e) var e = window.event;
	  
	  if (e.pageX || e.pageY) {
		posx = e.pageX;
		posy = e.pageY;
	  } else if (e.clientX || e.clientY) {
		posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	  }
  
	  return {
		x: posx,
		y: posy
	  }
	}
  

	
	/**
	 * Variables.
	 */
	var contextMenuClassName = "context-menu";
	var contextMenuItemClassName = "context-menu__item";
	var contextMenuLinkClassName = "context-menu__link";
	var contextMenuActive = "context-menu--active";
  
	var taskItemClassName = "file_item";
	var taskItemInContext;
  
	var clickCoords;
	var clickCoordsX;
	var clickCoordsY;
  
	var menu ;
	var menuItems ;
	var menuState = 0;
	var menuWidth;
	var menuHeight;
	var menuPosition;
	var menuPositionX;
	var menuPositionY;
  
	var windowWidth;
	var windowHeight;
  
	/**
	 * Initialise our application's code.
	 */
	function init() {

		menu = document.querySelector("#context-menu");
		if(menu==null){
				var contextmenu = $('<nav id="context-menu" class="context-menu">\
				<ul class="context-menu__items">\
				<li class="context-menu__item">\
					<a href="#" class="context-menu__link" data-action="Download"><i class="fas fa-download"></i> Download file</a>\
				</li>\
				<li class="context-menu__item">\
					<a href="#" class="context-menu__link" data-action="Delete"><i class="fas fa-trash-alt"></i> Delete file</a>\
				</li>\
				</ul>\
			</nav>');
			contextmenu.appendTo("body");
		}
		menu = document.querySelector("#context-menu");
		menuItems = menu.querySelectorAll(".context-menu__item");
	  contextListener();
	  clickListener();
	  keyupListener();
	  resizeListener();
	}
  
	/**
	 * Listens for contextmenu events.
	 */
	function contextListener() {
	  document.addEventListener( "contextmenu", function(e) {
		taskItemInContext = clickInsideElement( e, taskItemClassName );
  
		if ( taskItemInContext ) {
		  e.preventDefault();
		  toggleMenuOn();
		  positionMenu(e);
		} else {
		  taskItemInContext = null;
		  toggleMenuOff();
		}
	  });
	}
  
	/**
	 * Listens for click events.
	 */
	function clickListener() {
	  document.addEventListener( "click", function(e) {
		var clickeElIsLink = clickInsideElement( e, contextMenuLinkClassName );
  
		if ( clickeElIsLink ) {
		  e.preventDefault();
		  menuItemListener( clickeElIsLink );
		} else {
		  var button = e.which || e.button;
		  if ( button === 1 ) {
			toggleMenuOff();
		  }
		}
	  });
	}
  
	/**
	 * Listens for keyup events.
	 */
	function keyupListener() {
	  window.onkeyup = function(e) {
		if ( e.keyCode === 27 ) {
		  toggleMenuOff();
		}
	  }
	}
  
	/**
	 * Window resize event listener
	 */
	function resizeListener() {
	  window.onresize = function(e) {
		toggleMenuOff();
	  };
	}
  
	/**
	 * Turns the custom context menu on.
	 */
	function toggleMenuOn() {
	  if ( menuState !== 1 ) {
		menuState = 1;
		menu.classList.add( contextMenuActive );
	  }
	}
  
	/**
	 * Turns the custom context menu off.
	 */
	function toggleMenuOff() {
	  if ( menuState !== 0 ) {
		menuState = 0;
		menu.classList.remove( contextMenuActive );
	  }
	}
  
	/**
	 * Positions the menu properly.
	 * 
	 * @param {Object} e The event
	 */
	function positionMenu(e) {
	  clickCoords = getPosition(e);
	  clickCoordsX = clickCoords.x;
	  clickCoordsY = clickCoords.y;
  
	  menuWidth = menu.offsetWidth + 4;
	  menuHeight = menu.offsetHeight + 4;
  
	  windowWidth = window.innerWidth;
	  windowHeight = window.innerHeight;
  
	  if ( (windowWidth - clickCoordsX) < menuWidth ) {
		menu.style.left = windowWidth - menuWidth + "px";
	  } else {
		menu.style.left = clickCoordsX + "px";
	  }
  
	  if ( (windowHeight - clickCoordsY) < menuHeight ) {
		menu.style.top = windowHeight - menuHeight + "px";
	  } else {
		menu.style.top = clickCoordsY + "px";
	  }
	}
  
	/**
	 * Dummy action function that logs an action when a menu item link is clicked
	 * 
	 * @param {HTMLElement} link The link that was clicked
	 */
	function menuItemListener( link ) {
	    console.log( "Task ID - " + taskItemInContext.getAttribute("id") + ", Task action - " + link.getAttribute("data-action"));
	    toggleMenuOff();
		var action=link.getAttribute("data-action")
	    if(action=='Delete' && confirm("Delete?")){
			$.get('/fm/op', { 'cmd':'rm', 'target' : taskItemInContext.getAttribute("id") })
				.done(function(){
					var hash=$('#dirtree').jstree('get_selected')[0];
					$("#dirtree").trigger('changed.jstree', {selected: [hash]} );
				})
				.fail(function () {
					data.instance.refresh();
				});
		}
		if(action=='Download' ){
			window.location.href='/fm/op?cmd=file&target='+taskItemInContext.getAttribute("id");			
		}
	}
  
	/**
	 * Run the app.
	 */
	return init;
  
  })();