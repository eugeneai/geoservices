/**
 * Real-time console.
 *
 * @param DOMElement domnode Specifies where the console should be drawn 
 */
var ServiceConsoleLog = function(domnode, clearconsolenode) {
	this.domnode = domnode;
	var self     = this;

	$(domnode).attr('class', 'executionconsole');

	$(clearconsolenode).click(function() {
		clearTimeout(CONSOLE_INTERVAL);
		
		$("#tabsContainer").children().empty();
		
		self.clear();
	});

	this.log = function(message, type) {
		var date = new Date;
		var spanel = $('<span>' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '.' + date.getMilliseconds() + ' ~ </span><span>' + message + '</span>');
		$(this.domnode).append(spanel);

		if (isset(type)) {
			switch(type) {
				case 'error':
					spanel.attr('style', 'color: #C35151;');
					break;
				case 'success':
					spanel.attr('style', 'color: green;');
					break;
			}
		}
		$(this.domnode).append('<br>');
		var mydiv = $(this.domnode);
		mydiv.animate({scrollTop: mydiv.prop('scrollHeight')}, 0);
	}
	
	this.clear = function() {
		ERROR_OCCURED = false;
		$(this.domnode).html('');
	}
	
	this.endline = function() {
		$(this.domnode).append('<hr>');
	}
} //end ServiceConsoleLog()
