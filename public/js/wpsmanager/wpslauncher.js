/**
 * Launcher for the WPS service execution
 */

 
var CONSOLE_INTERVAL;
var CONSOLE_INTERVAL_VALUE = 4000;

/**
 * Proxy for WPS service oriented actions.
 *
 * @param string  action     Requested action
 * @param integer serviceid Identificator of the WPS service
 *
 * @return void
 */
function callWPSAction(action, serviceid) {
	if (action === "viewservice") {
		var numberofseconds = new Date().getTime();
		launchWPSService(numberofseconds, serviceid);
	} else {
		throw new Error("");
	}
} //end callWPSAction()


/**
 * Fetching service information and constructing execution form.
 *
 * @param integer serviceid Identifier of the WPS service
 *
 * @return void
 */
function launchWPSService(containerid, serviceid) {
	var _environmentVisualization;
	var _tasks = [];
	var servicecontainerid = "servicecontainer_" + containerid;
	var serviceid          = serviceid;
	var formwidgets        = new Array();
	var linesinpageconsole = 0;
	var ERROR_OCCURED      = false;
	var methoddescription;
	var serviceconsole;


	/**
	 * Renders service parameter.
	 *
	 * @param DOMNode domnode Where parameters should be rendered
	 * @param array   params  List of parameters
	 * @param string  type    Type of service parameters - input of output
	 *
	 * @return void
	 */
	function renderServiceParameter(domnode, params, type) {
		for (var i = 0; i < params.length; i++) {
			var currentparam = params[i];

			if (type === "input") {
				var container = $('<div style="border-bottom: 1px solid #DDD; padding-top: 3px; padding-bottom: 3px;" class="container-fluid" id="toputdiv_' + i + '"></div>');
			} else if (type === "output") {
				var container = $('<div style="border-bottom: 1px solid #DDD; padding-top: 3px; padding-bottom: 3px;" class="container-fluid" id="outputdiv_' + i + '"></div>');
			} else {
				throw "Specify the type of parameters";
			}

			if ("minoccurs" in currentparam) {
				var minoccurs = parseInt(currentparam.minoccurs);
				if (minoccurs >= 1) {
					container.append('<span id="mandatory_element" minoccurs="' + minoccurs + '"></span>');
				} //end if
			} //end if

			$(domnode).append(container);

			var labelContainer = $("<div class=\"span3\"/>");
			var widgetContainer = $("<div class=\"span9\"/>");
			
			var tempvid = $.widgets.get_widget(currentparam);
			formwidgets.push(tempvid);
			tempvid.assign(widgetContainer);

			var label = $('<label/>');
			label.text(currentparam.title);
			if (container.find('label').length === 0) {
				labelContainer.prepend(label);
			}

			var rowContainer = $("<div class=\"row-fluid\"></div>");
			rowContainer.append(labelContainer);
			rowContainer.append(widgetContainer);
			container.append(rowContainer);
			
			if (tempvid.constructor.name === 'w_file' || tempvid.constructor.name === 'w_file_save') {
				$('input', container).change(function() {
					var curval = $(this).val();
					//var curid = $(this).attr('id');
					var mapstyle={};
					if (curval.indexOf('.tif') !== -1 || curval.indexOf('.tiff') !== -1) {
						drawSingleRaster(curval, mapstyle);
					} else {
						console.log('Not suitable file format for WMS render');
					}
				});
			} //end if
		} //end for
	} //end renderServiceParameter()


	/**
	 * Build input form for the service.
	 *
	 * @param DOMElement domnode            Where form inputs should be placed
	 * @param Object     servicedescription Description of the service
	 *
	 * @return array
	 */

	function buildForm(domnode, servicedescription) {
		setTimeout(function() {
			domnode.html("");
			domnode.append("<strong>Inputs</strong>");
			renderServiceParameter(domnode, servicedescription.params, "input");
			if (servicedescription.output_params.length > 0) {
				var renderedoutputparameters = new Array();
				for (var i = 0; i < servicedescription.output_params.length; i++) {
					console.log(servicedescription.output_params[i].widget.name);
					if (servicedescription.output_params[i].widget.name === "file_save") {
						renderedoutputparameters.push(servicedescription.output_params[i]);
					}
				}

				if (renderedoutputparameters.length > 0) {
					domnode.append("<strong>Output settings</strong>");
					renderServiceParameter(domnode, renderedoutputparameters, "output");
				}
			} //end if
		}, 1000);
	} //end buildForm()


	/**
	 * Returns current console output for a service.
	 *
	 * @param integer meid Service identifier
	 *
	 * @return void
	 */

	function getCurrentConsoleOutput(meid) {
		$.ajax({
			url: "/wps/output?id=" + meid,
			dataType: 'json',
			method: 'GET',
			async: false,
			success: function(data) {
				if (data.status == 'success') {
					// Displaying tasks and nodes
					var tasks, nodes;
					_environmentVisualization.visualize(data.data.environment);

					var consoleoutput = data.data.lines[0].console_output;
					var erroroutput   = data.data.lines[0].error_output;
					var linesarray    = explode("\n", consoleoutput);

					if ((linesarray.length - 1) > linesinpageconsole) {
						var numberofnewlines = (linesarray.length - 1) - linesinpageconsole;
						for (var i = numberofnewlines; i > 0; i--) {
							if (ERROR_OCCURED === false) {
								serviceconsole.log(linesarray[linesarray.length - i], 'success');
							}
						}
					}

					linesinpageconsole = linesarray.length - 1;

					console.log(data);
					if (data.data.lines[0].status == 'METHOD_EXAMPLE_SUCCEEDED') {
						serviceconsole.log('Result: ' + '<b>' + data.data.lines[0].result + '</b>', 'success');
						serviceconsole.log('Execution succeded', 'success');

						var resultObj = JSON.parse(data.data.lines[0].result);
						for (var i = 0; i < methoddescription.output_params.length; i++) {
							var widgetName = methoddescription.output_params[i].widget.name;
							if (methoddescription.output_params[i].title in resultObj && widgetName === "file_save") {
								var l = new MLayer(methoddescription.output_params[i].title + "(" + resultObj[methoddescription.output_params[i].title] + ")", true);
								l.assign_file(resultObj[methoddescription.output_params[i].title], methoddescription.output_params[i].widget.properties.mapfilestyle);
							}
						}
							
						_tasks = [];
						clearInterval(CONSOLE_INTERVAL);
					} else if (data.data.lines[0].status == 'METHOD_EXAMPLE_FAILED') {
						ERROR_OCCURED = true;
						serviceconsole.log('Execution failed, see error log below', 'error');

						var errorlines = explode("\n", erroroutput);
						for (var i = 0; i < errorlines.length; i++) {
							serviceconsole.log(errorlines[i], 'error');
						}

						_tasks = [];
						clearInterval(CONSOLE_INTERVAL);
					} //end if					
				} else if (data.status == 'error') {
					c(data.error);
					serviceconsole.log('Error occured while getting current process state', 'error');
					_tasks = [];
					clearInterval(CONSOLE_INTERVAL);
				}
			}
		});
	} //end getCurrentConsoleOutput()

	//requirejs(["visjs", "jquery.coordinates"], function (visjs) {
		//window.vis = visjs;

		$.ajax({url: '/dataset/list?f=185&f_id=' + serviceid,
			type: 'get',
			dataType: 'json',
			cache: false,
			success: function(res) {
				methoddescription=res.aaData[0];
				// methoddescription = data.method;

				var container = create_tab_item(false, "[WPS] " + methoddescription.name);
				
				var panelContainer     = $("<div class=\"container-fluid wps-launcher-tab\" id=\"" + servicecontainerid + "\"><div class=\"row h100\"/><div/>");
				var parametersPanel    = $("<div class=\"col-4 h100\"></div>");
				var consolePanel       = $("<div class=\"col-4 executionconsole-container\"/>");
				var visualizationPanel = $("<div class=\"col-4\" style=\"position: relative;\"/>");
				
				_environmentVisualization = new EnvironmentVisualization(visualizationPanel);
				
				panelContainer.children().first().append(parametersPanel);
				panelContainer.children().first().append(consolePanel);
				panelContainer.children().first().append(visualizationPanel);
				container.append(panelContainer);

				parametersPanel.append('<div id="' + servicecontainerid + '" class="wps-launcher-tab__container" title="Service execution">\
				<div class="wps-launcher-tab__controls">\
					<div style="float: right;">\
						<div id="controls" class="btn-group">\
							<button type="button" id="execute" class="btn btn-small btn-success" title="Execute"><i class="fas fa-running"></i></button>\
							<button type="button" id="clearconsole" class="btn btn-small" title="Clear console"><i class="fas fa-eraser"></i> </button>\
							<a class="btn btn-small" target="_blank" href="/?action=showwpsservice&id=' + methoddescription.id + '" title="Link (in new window)"><i class="icon-share"></i></a>\
							<a class="btn btn-small" target="_blank" href="/wpsmanager/create?id=' + methoddescription.id + '" title="Modify method"><i class="icon-pencil"></i></a>\
						</div>\
					</div>\
					<h4>' + methoddescription.name + '</h4>\
				</div>\
				<div id="execute_wrapper" class="wps-launcher-tab__input-parameters">\
					<div class="wps-launcher-tab__progress-bar-container js-wps-launcher-tab__progress-bar-container">\
						<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>\
					</div>\
					<strong>Inputs</strong>\
					<div id="input_params_form"></div>\
					<strong>Outputs</strong>\
					<div id="output_params_form"></div>\
				</div>');
				
				consolePanel.append('<div id="flow_console"></div>');

				serviceconsole = new ServiceConsoleLog($("#" + servicecontainerid + ' #flow_console'), $("#" + servicecontainerid + ' #clearconsole'));

				$("#" + servicecontainerid + ' #clearconsole').click(function() {
					_environmentVisualization.clear();
				});

				methoddescription.params=JSON.parse(methoddescription.params);
				methoddescription.output_params=JSON.parse(methoddescription.output_params);
				
				mr = JSON.stringify(methoddescription);

				var methoddescriptionInput = JSON.parse(mr);
				methoddescriptionInput.columns = methoddescription.params;
				var docInputForm = new doc_template(methoddescriptionInput);
				docInputForm.init_form(function () {
					$("#" + servicecontainerid + " .js-wps-launcher-tab__progress-bar-container").remove();				
				});
				var formContainer = $("#" + servicecontainerid + ' #input_params_form');
				docInputForm.show_form(formContainer);
				
				var methoddescriptionOuput = JSON.parse(mr);
				methoddescriptionOuput.columns = methoddescriptionOuput.output_params;
				var docOutputForm = new doc_template(methoddescriptionOuput);
				docOutputForm.init_form(function () {
					$("#" + servicecontainerid + " .js-wps-launcher-tab__progress-bar-container").remove();				
				});
				var formContainer = $("#" + servicecontainerid + ' #output_params_form');
				docOutputForm.show_form(formContainer);
				
				$('#' + servicecontainerid + ' #execute').click(function() {
					serviceconsole.log('Starting...', 'success');
					linesinpageconsole = 0;

					_environmentVisualization.visualize(false);
					
					var inputparams = docInputForm.save_form(function() {});
					var inputClean = {};
					for (var i = 0; i < methoddescriptionInput.columns.length; i++) {
						if (parseInt(methoddescriptionInput.columns[i].minoccurs) === 1 && inputparams[methoddescription.params[i].fieldname] === "NULL" ) {
							alert('You have to fill the ' + methoddescriptionInput.columns[i].fieldname + ' parameter');
							return;
						} else if (inputparams[methoddescriptionInput.columns[i].fieldname] !== "NULL") {
							inputClean[methoddescriptionInput.columns[i].fieldname] = inputparams[methoddescriptionInput.columns[i].fieldname];
						}
					}

					var outputparams = docOutputForm.save_form(function() {});
					var outputClean = {};
					for (var i = 0; i < methoddescriptionOuput.columns.length; i++) {
						if (parseInt(methoddescriptionOuput.columns[i].minoccurs) === 1 && outputparams[methoddescriptionOuput.columns[i].fieldname] === "NULL") {
							alert('You have to fill the ' + methoddescription.output_params[i].fieldname + ' parameter');
							return;
						} else if (outputparams[methoddescriptionOuput.columns[i].fieldname] !== "NULL") {
							outputClean[methoddescriptionOuput.columns[i].fieldname] = outputparams[methoddescriptionOuput.columns[i].fieldname];
						}
					}

					ERROR_OCCURED = false;
					$.ajax({
						url      : "/wps/execute?id=" + methoddescription.id + "&inputparams=" +
						encodeURIComponent(JSON.stringify(inputClean)) + "&outputparams=" + encodeURIComponent(JSON.stringify(outputClean)),
						dataType : 'json',
						method   : 'POST',
						success  : function(data) {
							if (data.status == 'executing') {
								serviceconsole.log('Method <b>' + methoddescription.name + '</b> started its execution with assigned Method Example ID = ' + JSON.stringify(data.data), 'success');
								CONSOLE_INTERVAL = setInterval(function() {getCurrentConsoleOutput(data.data); }, CONSOLE_INTERVAL_VALUE);
							} else if (data.status == 'error') {
								serviceconsole.log('Method <b>' + methoddescription.name + '</b> failed, error: ' + JSON.stringify(data.error), 'error');
							} //end if
						},
						error: function(error) {
							serviceconsole.log('Server did not respond correctly', 'error');
						},
					});
				});
			}, error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
	//});
	
} //end launchWPSService()
