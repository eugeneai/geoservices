/**
 * Environment state visualization class.
 * Creates panel that displayes the state of the environment.
 */
function EnvironmentVisualization(node) {
	if (!node) throw "Provide root node";
	var _tasks = [];
	var _nodes = [];
	var _node = node;
	var _self = this;
	var _cachedEnvironment = false;
	
	var MODE_NODE_ASSIGNMENT   = 0;
	var MODE_TASK_DEPENDENCIES = 1;
	var MODE_TASK_SCHEDULE     = 2;

	var SD_JOB_STATUS_PENDING     = 30;
	var SD_JOB_STATUS_SUCCESS     = 31;
	var SD_JOB_STATUS_FAILURE     = 32;
	var SD_JOB_STATUS_STARTING    = 33;
	var SD_JOB_STATUS_IN_PROGRESS = 34;
	
	var _environmentId = false;
	
	var _dummyNode = $('<div style="text-align: center">\
		<p class="muted">Launch the task first</p>\
	</div>');
	
	var _loadingNode = $('<div style="text-align: center">\
		<div class="progress progress-striped active" style="width: 40%; margin: 0 auto">\
			<div class="bar" style="width: 100%;"></div>\
		</div>\
	</div>');

	var _nodeAssignmentContainer, _taskDependenciesContainer, _taskScheduleContainer;
	
	_construct();
	
	function _construct() {
		_environmentId     = false;
		_cachedEnvironment = false;

		_mode = MODE_NODE_ASSIGNMENT;
		
		_node.empty();
		_node.append('<div>\
			<ul class="nav nav-tabs">\
				<li class="active"><a href="#nodeAssignment" data-mode="MODE_NODE_ASSIGNMENT" data-toggle="tab">Node assignment</a></li>\
				<li><a href="#taskDependenciesContainer" data-mode="MODE_TASK_DEPENDENCIES" data-toggle="tab">Task dependencies</a></li>\
				<li><a href="#taskSchedule" data-mode="MODE_TASK_SCHEDULE" data-toggle="tab">Task schedule</a></li>\
			</ul>\
			<div id="tabsContainer"></div>\
		</div>');

		_nodeAssignmentContainer = $('<div id="nodeAssignment"></div>');
		_nodeAssignmentContainer.append(_dummyNode.clone());

		_taskDependenciesContainer = $('<div id="taskDependenciesContainer" style="display: none;"></div>');
		_taskDependenciesContainer.append(_dummyNode.clone());

		_taskScheduleContainer = $('<div id="taskSchedule" style="display: none;"></div>');
		_taskScheduleContainer.append(_dummyNode.clone());

		$(_node).find("#tabsContainer").append(_nodeAssignmentContainer);
		$(_node).find("#tabsContainer").append(_taskDependenciesContainer);
		$(_node).find("#tabsContainer").append(_taskScheduleContainer);
		
		$(_node).find("a").click(function (e) {
			e.preventDefault();

			$('[data-toggle="tab"]').parent().removeClass("active");
			$(this).parent().addClass("active");

			$("#tabsContainer").children().hide();
			$("#tabsContainer").find($(this).attr("href")).show();

			if ($(this).attr("data-mode") === "MODE_NODE_ASSIGNMENT") {
				_mode = MODE_NODE_ASSIGNMENT;
			} else if ($(this).attr("data-mode") === "MODE_TASK_DEPENDENCIES") {
				_mode = MODE_TASK_DEPENDENCIES;
			} else if ($(this).attr("data-mode") === "MODE_TASK_SCHEDULE") {
				_mode = MODE_TASK_SCHEDULE;
			}

			if (_environmentId) _self.visualize();
		});
	} //end _construct()


	/**
	 * Displays current tasks visualization.
	 *
	 * @return void
	 */
	function _displayTasks() {		
		if (_tasks && _tasks.length > 0) {		
			var graphNodes = [];
			var edges = [];
			for (var i = 0; i < _tasks.length; i++) {
				var taskRecord = {
					id: _tasks[i].id,
					label: "Task " + _tasks[i].id
				};

				if (_tasks[i].status === SD_JOB_STATUS_PENDING) {
					taskRecord.color = "#FFF";
					taskRecord.title = taskRecord.label + "<br/> (ready)";
				} else if (_tasks[i].status === SD_JOB_STATUS_SUCCESS) {
					taskRecord.color = "#99FF99";
					taskRecord.title = taskRecord.label + "<br/> (executing)";
				} else if (_tasks[i].status === SD_JOB_STATUS_FAILURE) {
					taskRecord.color = "#FF6666";
					taskRecord.title = taskRecord.label + "<br/> (executing)";
				} else if (_tasks[i].status === SD_JOB_STATUS_STARTING || _tasks[i].status === SD_JOB_STATUS_IN_PROGRESS) {
					taskRecord.color = "#99CCFF";
					taskRecord.title = taskRecord.label + "<br/> (executing)";
				}

				graphNodes.push(taskRecord);

				if ("dependencies" in _tasks[i] && _tasks[i].dependencies.length > 0) {
					for (var j = 0; j < _tasks[i].dependencies.length; j++) {
						edges.push({
							from: _tasks[i].dependencies[j],
							to: _tasks[i].id,
							arrows: "to"
						});
					}
				}
			}

			$("#taskDependenciesContainer").empty();
			$("#taskDependenciesContainer").append('<div id="taskDependencies" style="min-height: 200px"></div>');
			var container = document.getElementById("taskDependencies");
			var dddata = {
				nodes: new vis.DataSet(graphNodes),
				edges: new vis.DataSet(edges)
			};

			var options = {
				autoResize: true,
				height: '100%',
				layout: {
					hierarchical: {
						enabled: true,
						direction: "UD",
						sortMethod: "directed"
					}
				}
			};

			var network = new vis.Network(container, dddata, options);
		}
	} //end _displayTasks()
	
	
	/**
	 * Displays current nodes visualization.
	 *
	 * @return void
	 */
	function _displayNodes() {
		$("#nodeAssignment").empty();

		for (var i = 0; i < _nodes.length; i++) {
			var localContainer = $("<div/>");
			
			if (_nodes[i].active === true) {
				localContainer.append("<span style=\"color: green;\">Node " + _nodes[i].id + " (" + _nodes[i].ip + ", active): </span>");
			} else {
				localContainer.append("<span style=\"color: red;\">Node " + _nodes[i].id + " (" + _nodes[i].ip + ", inactive): </span>");
			}
			
			if (_nodes[i].assignedTasks && _nodes[i].assignedTasks.length > 0) {
				for (var j = 0; j < _nodes[i].assignedTasks.length; j++) {
					localContainer.append("<span> " + _nodes[i].assignedTasks[j] + ";</span>");
				}
			}

			$("#nodeAssignment").append(localContainer);
		}
	} //end _displayNodes()

	
	/**
	 * Visualizes the environment state in the sort of Gantt diagram view.
	 *
	 * @param Object environment The environment description
	 *
	 * @return void
	 */
	function _displaySchedule(environment) {
		var tasks = environment.tasks;
		var nodes = environment.nodes;

		if (tasks && tasks.length > 1 && nodes && nodes.length > 1) {
			var d3Tasks = [];
			var d3TaskNames = [];
			for (var i = 0; i < tasks.length; i++) {
				var showTask = true;
				var startDate, endDate;
				if (tasks[i].status === SD_JOB_STATUS_SUCCESS) {
					startDate = new Date(parseInt(environment.startTime) + parseInt(tasks[i].timing.started));
					endDate = new Date(parseInt(environment.startTime) + parseInt(tasks[i].timing.finished));
				} else if (tasks[i].status === SD_JOB_STATUS_STARTING || tasks[i].status === SD_JOB_STATUS_IN_PROGRESS) {
					startDate = new Date(parseInt(environment.startTime) + parseInt(tasks[i].timing.started));
					endDate = new Date();
				} else {
					showTask = false;
				}

				if (showTask) {
					var selectedNode = false;

					for (var j = 0; j < nodes.length; j++) {
						if (nodes[j].assignedTasks && nodes[j].assignedTasks.indexOf(tasks[i].id) !== -1) {
							selectedNode = nodes[j];
							break;
						}
					}

					if (selectedNode === false) throw "Unable to find corresponding node";

					d3Tasks.push({
						"startDate": startDate,
						"startDateTimestamp": (parseInt(+ startDate) - parseInt(environment.startTime)),
						"endDate":  endDate,
						"endDateTimestamp": (parseInt(+ endDate) - parseInt(environment.startTime)),
						"taskName": selectedNode.ip,
						"text": tasks[i].id,
						"status": tasks[i].status + ""
					});

					if (d3TaskNames.indexOf(selectedNode.ip) === -1) {
						d3TaskNames.push(selectedNode.ip);
					}
				}
			}

			for (var i = 0; i < nodes.length; i++) {
				if (nodes[i].occuredErrorsTimestamps && nodes[i].occuredErrorsTimestamps.length > 0) {
					for (var j = 0; j < nodes[i].occuredErrorsTimestamps.length; j++) {
						/*
							Looking for next, after timestamp, task. If there are no one, then use the current time.
						*/

						var errorOccured = parseInt(nodes[i].occuredErrorsTimestamps[j]);
						var selectedTask = false;

						var lastRecord = (parseInt(+ new Date()) - parseInt(environment.startTime));
						for (var t = 0; t < d3Tasks.length; t++) {
							if (nodes[i].ip === d3Tasks[t].taskName) {
								if (d3Tasks[t].startDateTimestamp < lastRecord && d3Tasks[t].startDateTimestamp > errorOccured) {
									lastRecord = d3Tasks[t].startDateTimestamp;
								}
							}
						}

						var startDate = new Date(parseInt(environment.startTime) + parseInt(errorOccured));
						var endDate   = new Date(parseInt(environment.startTime) + parseInt(lastRecord));
						d3Tasks.push({
							"startDate": startDate,
							"endDate":  endDate,
							"taskName": nodes[i].ip,
							"text": "Error",
							"status": SD_JOB_STATUS_FAILURE
						});
					}
				}
			}

			if (d3TaskNames.length > 0) {
				for (var i = 0; i < d3Tasks.length; i++) {
					if (d3Tasks[i].startDate > d3Tasks[i].endDate) {
						console.error("Bad time detected for", d3Tasks[i]);
					}	
				}

				var taskStatus = {};
				taskStatus[SD_JOB_STATUS_SUCCESS + ""]     = "bar-running";
				taskStatus[SD_JOB_STATUS_STARTING + ""]    = "bar";
				taskStatus[SD_JOB_STATUS_IN_PROGRESS + ""] = "bar";
				taskStatus[SD_JOB_STATUS_FAILURE + ""]     = "bar-failed";

				d3Tasks.sort(function(a, b) {
					return a.endDate - b.endDate;
				});

				var maxDate = d3Tasks[d3Tasks.length - 1].endDate;
				d3Tasks.sort(function(a, b) {
					return a.startDate - b.startDate;
				});
				
				var minDate = new Date(parseInt(environment.startTime));

				var format = "%H:%M";

				$("#taskSchedule").empty();
				var gantt = d3.gantt().taskTypes(d3TaskNames).taskStatus(taskStatus).tickFormat(format);
				gantt(d3Tasks, {
					selector: "#taskSchedule",
					width: ($("#taskSchedule").width() - 100),
					height: 200,
					startTimeMS: environment.startTime
				});
			}
		}
	} //end _displaySchedule()
	
	
	/**
	 * Entry function for displaying the computational environment state.
	 *
	 * @param Object environment Environment description
	 *
	 * @return void
	 */
	this.visualize = function (environment) {
		if (!_environmentId) {
			var localDate = new Date();
			_environmentId = localDate.getMilliseconds();
			
			_nodeAssignmentContainer.empty();
			_nodeAssignmentContainer.append(_loadingNode.clone());

			_taskDependenciesContainer.empty();
			_taskDependenciesContainer.append(_loadingNode.clone());

			_taskScheduleContainer.empty();
			_taskScheduleContainer.append(_loadingNode.clone());
		}

		if (!environment) {
			if (_cachedEnvironment) environment = _cachedEnvironment;
		}

		if (environment && environment.tasks && environment.nodes) {
			_cachedEnvironment = environment;
			if (_mode === MODE_NODE_ASSIGNMENT) {
				if (environment.nodes) {
					_nodes = environment.nodes;
					_displayNodes();
				}
			} else if (_mode === MODE_TASK_DEPENDENCIES) {
				if (environment.tasks.length >= _tasks.length) {
					_tasks = environment.tasks;
					_displayTasks();
				}
			} else if (_mode === MODE_TASK_SCHEDULE) {
				_displaySchedule(environment);
			}
		}
	}
	
	
	this.clear = function() {
		_construct();
	} //clear()
}