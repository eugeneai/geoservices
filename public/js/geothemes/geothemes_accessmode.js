/*
	0 - view
	1 - edit
	2 - moderation
	3 - publication

*/

var a_view_options   = ['Disable','View only own records and publicated','View all records'];
var a_edit_options   = ['Disable','Edit only own records','Edit all records', 'Moderation','Edit group of rows'];
var a_publication_options   = ['Disable','Enable'];
	

function accessmode_getdata(){	
	userslist = new Array();
	$('.user_box').each(function() {
		var user_box=$(this);
		var userrecordnode = $(this).find('#username');		
		a_str=$('#view',user_box).val() + $('#edit',user_box).val() + $('#publication',user_box).val();		
		var userrecord = {
			userid     : $(userrecordnode).attr('userid'),
			accesstype : a_str,
		};
		userslist.push(userrecord);
	});
	return userslist;
}

/**
 * Initializing table and record access instruments
 */

function accessmode_init(jquerynode, themejson, dataset_id){
	
	function fill_select(input, value, list){
		for (var i = 0; i < list.length; i++) {
			if(value==i)
				option = $('<option selected value="'+i+ '">' + list[i]+ '</option>');
			else
				option = $('<option value="' + i + '">' + list[i] + '</option>');
			input.append(option);
		}
	}

	function addUser(username, userid, rights ) {
		if(username==null)
			username = userid;
		var user_div = $('\
		<div class="row user_box align-items-center">\
		<div class="col-2">\
			<span id="username" rid="' + userid + '"  userid="' + userid + '">' + username + '</span>\
		</div>\
		<div class="col-3 form-group">\
			<label for="view">View</label><div class="form-control" id="view"/>\
		</div>\
		<div class="col-3 form-group">\
			<label for="edit">Edit</label><div class="form-control" id="edit"/>\
		</div>\
		<div class="col-3 form-group">\
			<label for="publication">Publication</label><div class="form-control" id="publication"/>\
		</div>\
		<div class="col-1">\
			<button type="button" class="btn btn-mini access_mode_remove_user_record_button" id=""><i class="fas fa-user-times"></i></button>\
		</div>\
		</div>');
		$('#view',user_div).html( a_view_options[ rights.charAt(0)] );
		$('#edit',user_div).html( a_edit_options[ rights.charAt(1)] );
		$('#publication',user_div).html( a_publication_options[ rights.charAt(2)] );
		$('#user_list').append(user_div);
		$('.access_mode_remove_user_record_button',user_div).click(function() {
			
			$.post("/dataset/user/delete", {userid: userid, f:dataset_id}, "json")
						.done(function(data) {
							data = JSON.parse(data);
							if (data.status === 'ok') {
								drawusers();				
								
							} else {
								alert('Error occured: ' + JSON.stringify(data.data));
							}
						})
						.fail(function() { 
							alert("error"); 
						});
		});
	}
	function drawusers(){
		if (dataset_id!==undefined && dataset_id!='') {
			$('#user_list').empty();
			$.ajax({
				url: '/dataset/user/list?f='+ dataset_id,
				dataType: "json",						
				success: function(responce) {
					if(responce.status!='ok'){
						alert(responce.data);
						return;
					}
						

					for(var j = 0; j < responce.data.length; j++){
						addUser(responce.data[j].username, responce.data[j].user_id, responce.data[j].access_mode);
					}				
				}
			});
		}
	}
	function userdlg(user){
		var user_div = $('\
		<div class="user_box">\
			<div class="form-check">\
				<input class="form-check-input" type="radio" name="usertype" id="user" value="user" checked>\
				<label class="form-check-label" for="user">\
					User\
				</label>\
					<div id="user_search_dataset5"></div>\
				</div>\
				<div class="form-check">\
				<input class="form-check-input" type="radio" name="usertype" id="registered" value="registered">\
				<label class="form-check-label" for="registered">\
					Registered user\
				</label>\
				</div>\
				<div class="form-check disabled">\
				<input class="form-check-input" type="radio" name="usertype" id="anonym" value="anonym">\
				<label class="form-check-label" for="anonym">\
					Anonymous user\
				</label>\
			</div>\
			<div class=" form-group">\
				<label for="view">View</label>\
				<select class="form-control" id="view"/>\
			</div>\
			<div class=" form-group">\
				<label for="edit">Edit</label><select class="form-control" id="edit"/>\
			</div>\
			<div class=" form-group">\
				<label for="publication">Publication</label><select class="form-control" id="publication"/>\
			</div>\
		</div>');
		rights='000';
		fill_select($('#view',user_div), rights.charAt(0), a_view_options);
		fill_select($('#edit',user_div), rights.charAt(1), a_edit_options);		
		fill_select($('#publication',user_div), rights.charAt(2), a_publication_options);		
		var metaUser = {
			"fieldname": "user5",					
			"visible": true,
			"widget": {
				"name": "user",
				properties: {
				}
			}
		};

		var widgetUser = $.widgets.get_widget(metaUser);
				widgetUser.assign($('#user_search_dataset5', user_div), '');


		$('input[type=radio][name=usertype]',user_div).change(function() {
			if (this.value != 'user') {
				$('#user_search_dataset5', user_div).hide();
			}
			else {
				$('#user_search_dataset5', user_div).show();
			}
		});

		user_div.dialog({
			resizable: true,
			height: 'auto',
			width: 'auto',
			modal: false,
			buttons: {
				"Add": function () {
					user=$('input[name=usertype]:checked', user_div).val();

					if(user=='user'){
						user=widgetUser.getVal();
					}
					accesstype=''+$('#view',user_div).val()+$('#edit',user_div).val()+$('#publication',user_div).val();
					
					$.post("/dataset/user/add", {userid: user, accesstype:accesstype, f:dataset_id}, "json")
						.done(function(data) {
							data = JSON.parse(data);
							if (data.status === 'ok') {
								drawusers();				
								user_div.dialog("close");
								user_div.remove();
							} else {
								alert('Error occured: ' + JSON.stringify(data.data));
							}
						})
						.fail(function() { 
							alert("error"); 
						});
					
				},
				"Close": function () {				
					$(this).dialog("close");
					user_div.remove();
				}
			}
		});
	} 

	drawusers();
	$('#add_user').click(function() {
		userdlg();		
	});			
}