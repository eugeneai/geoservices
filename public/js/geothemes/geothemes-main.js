$('#table_edit_themeeditor').live('click', function() {
	OpenFileDlg(function(path){
		readFile(path, function(data){
			var table_json = JSON.parse(data);
			themeeditorBuildInterface('fill', table_json, path);		
		});
	});
});

$("#example tbody tr").live("hover", function () {
	var id = this.id;
	$(this).attr('title', 'ID: ' + id);
});

$('#theme_edit').click(function() {
	OpenFileDlg(function(path){
		readFile(path, function(data){
			var table_json = JSON.parse(data);
			frm = createform(table_json);
			$("#table_editor").empty().append(frm);
			$("#tablepath",frm).val(path);			
		});
		$('#geothemes_init').toggle();
	});
});

var user_schema='';

$.getJSON("/geothemes/gettables",{ajax: 'true'}, function(j){
          var options = '';
          for (var i = 0; i < j.length; i++) {
			if(i==0)
				user_schema=j[i].schema;
            options += '<option value="'+j[i].name+ '">' + j[i].name+ '</option>';
          }
		  
		  options = '<select id="db_table" name="db_tables">' + options + '</select>';
		  $("#db_table_container").html(options);
        });

$('#table_create').click(function() {			
		frm = createform(null);
		$("#table_editor").empty().append(frm);		
		$('#geothemes_init').toggle();	
});

$('#table_edit').click(function() {
	window.location.href ="/managejson?action=create&tablename="+encodeURIComponent($("#db_table").val());
});