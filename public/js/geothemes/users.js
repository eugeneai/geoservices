function gp_user(){
	var userDataString = $.cookie('userData') ||  '{}';
	var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));	
	$.extend(this,user);
	var guser=this;
	this.loginform=function(container, callback){
		function geo_authorize(){
			username=$('#username', container).val();
			password=$('#password', container).val();
			//alert(username+' '+password);
			var str_res=JSON.stringify({username:username, password:password});
			$.ajax({
				type: "POST",
				url: "/users/login",
				contentType : 'application/json',
				data: str_res,
				success: function(msg){
					if(msg.status=='ok'){
						if(msg.userData){
							var user = msg.userData;
							//localStorage.setItem('user', JSON.stringify(user));
						}
						else{
							var userDataString = $.cookie('userData') || '{}';
							var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));	
						}
						
						
						$.extend(guser,user);
						for(var i=0; i < datasets.length; i++){
							datasets[i]._fnReDraw();
						}
						if(callback)
							callback();
					}
					else{
						$.ajax({
							type: "POST",
							url: "/user/ajax_login",
							contentType : 'application/json',
							data: str_res,
							success: function(msg){
								if(msg=='ok'){
									var userDataString = $.cookie('userData') || '{}';
									var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));	
									$.extend(guser,user);
									for(var i=0; i < datasets.length; i++){
										datasets[i]._fnReDraw();
									}
									if(callback)
										callback();
								}
								else{
									$('.alert-danger',container).html('The login or password is not correct');
									$('.alert-danger',container).show();
								}
							}
						});													
					}
				}
			});	
			return false;
		}
		$('#login_submit', container).click(geo_authorize);
		
		$('#username,#password', container).keypress(function (e) {
			if (e.which == 13) {
				geo_authorize();
				return false;    //<---- Add this line
			}
		});
			
	};
	
	this.recovery_password=function(container){
		$('#recovery_submit', container).click(function(){
			email=$('#recemail', container).val();
			
			str_res=JSON.stringify({email:email});
			$.ajax({
				type: "POST",
				url: "/geothemes/recovery",
				contentType : 'application/json',
				data: str_res,
				success: function(msg){
					if(msg=='ok'){
						$('.alert-success', container).show();
						$('.alert-success', container).html('The recovery link was sent to email.');
					}
					else{
						$('.alert-error', container).show();
						$('.alert-error', container).html(msg);
					}
				}
			});	
			return false;
		});
	};
	
	this.recovery_change_password=function(container, username, hash){
		$('#recovery_submit', container).click(function(){
			password1=$('#password1', container).val();
			password2=$('#password2', container).val();
			if(password1!=password2){
				$('.alert-error', container).show();
				$('.alert-error', container).html('The passwords do not correspond.');
				return;
			}
			
			str_res=JSON.stringify({password:password1, hash: hash});
			$.ajax({
				type: "POST",
				url: "/geothemes/change_password",
				contentType : 'application/json',
				data: str_res,
				success: function(msg){
					if(msg=='ok'){
						$('.alert-success', container).show();
						$('.alert-success', container).html('The password was changed.');
					}
					else{
						$('.alert-error', container).show();
						$('.alert-error', container).html(msg);
					}
				}
			});	
			return false;
		});
	};	
	this.register=function(container, form){		
					
		var alert_success=$('.alert-success', container);
		alert_success.hide();
		var error_box=$('.alert-danger', container);
		error_box.hide();
		getdatasetmeta(5, function(error, table_json) {			
			if(error != null)
				return;
			
			table_json.metaid = 5;
			var doc_frm= new doc_template(table_json);
			doc_frm.init_form(function(){
				doc_data={};
				doc_frm.show_form(form, doc_data);
			});
			
			var btnSubmit=$('#regbtn', container);
			
			
			btnSubmit.click(function(){
				
				if($('input[title="password"]').val()!=$('input[title="password2"]').val()){
					var msg='The passwords do not correspond.'
					if(error_box.length>0){
						error_box.html(msg);
						error_box.show();
						alert_success.hide();
					}
					else
						alert(msg);
					return;
				}
				res=doc_frm.save_form(function(res, msg){
					if(msg.status!='ok'){						
						if(error_box.length>0){
							error_box.html(msg.data);
							error_box.show();
							alert_success.hide();
						}
						else
							alert(msg.data);
						return;
					}
					else{
						error_box.hide();
						alert_success.show();
						form.empty();
						alert_success.html('The confirmation message was send to your email. To complete registration you should open a link in the message.')
					}
				}, true);
				
			});
			
							
			
		});
		
	};

	this.profile=function(container, form){		
		var alert_success=$('.alert-success', container);
		alert_success.hide();
		var error_box=$('.alert-danger', container);
		error_box.hide();
		if(!guser.id)
			return;


		$.getJSON("/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=5", function(data) {			
			if(data.aaData.length==0)
				return;
			var table_json    = JSON.parse(data.aaData[0].JSON);
			table_json.metaid = 5;
			var doc_frm= new doc_template(table_json);
			doc_frm.init_form(function(){
				doc_data={};				
				$.getJSON("/dataset/list?f=5&iDisplayStart=0&iDisplayLength=100&f_id="+guser.id, function(res){
					doc_data=res.aaData[0];
					doc_frm.show_form(form, doc_data);
				});
				
			});
			
			var btnSubmit=$('.submit',container.parent());
			btnSubmit.click(function(){
				if($('input[title="password"]').val()!=$('input[title="password2"]').val()){
					var msg='The passwords do not correspond'
					if(error_box.length>0){
						error_box.html(msg);
						error_box.show();
						alert_success.hide();
					}
					else
						alert(msg);
				}
				res=doc_frm.save_form(function(res, msg){
					if(msg.status!='ok'){						
						if(error_box.length>0){
							error_box.html(msg);
							error_box.show();
							alert_success.hide();
						}
						else
							alert(msg);
						return;
					}
					else{
						alert_success.html('The profile has been saved.');
						alert_success.show();
						error_box.hide();
						
					}
				}, true);
			});
		});
	};
	
	
	return this;
}