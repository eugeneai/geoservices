var number_operators = {
	PropertyIsEqualTo              : '=',
	PropertyIsNotEqualTo           : '!=',
	PropertyIsLessThan             : '<',
	PropertyIsLessThanOrEqualTo    : '<=',
	PropertyIsGreaterThan          : '>',
	PropertyIsGreaterThanOrEqualTo : '>='
}

var THEME_JSON = '';
var THEME_JSON = '';
var THEME_METAID = '';
var inherit_table_val='';
function createfield(field_json, field_list) {
	if (field_json!=null && field_json.widget===undefined)
		return;
	var fieldcontainer=$("<div id='fieldcontainer' class='fieldcontainer'>\
    <div class='row'>\
        <div class='form-group col-md-2'>\
			<input class='form-control' type='text' name='title' id='title' placeholder='Name'/>	\
		</div>\
		<div class='form-group col-md-2'>\
			<select class='form-control' id='field_widget_select' name='field_widget_select' placeholder='Widget'/>\
		</div>\
		<div class='form-group col-md-2'>\
			<button class='show_prop btn btn-mini' type='button'>Show properties</button>\
        </div>\
		<div class='col-md-1 offset-md-5'>\
			<i title='Delete field' id='delete_field' class='fas fa-window-close'></i> 	\
        </div>\
	</div>\
	<div class='field_properties card'><div class='card-body'><div class='row'>	\
				<div class='form-group col-2'>\
					<label class='' for='description'>Description</label>\
					<TEXTAREA class='form-control' name='description' id='description' placeholder='Type the description' /></TEXTAREA>\
				</div>\
				<div class='form-group col-1'>\
					<label class='' for='field_name'>DB name</label>\
					<input type='text' id='field_name' class='form-control'/>\
					<input type='hidden' id='parentfield'/><input type='hidden' id='sqlname'/>\
					<input type='hidden' id='tablename'/><input type='hidden' id='sqltablename'/>\
					<input type='hidden' id='inherit_table'/>\
				</div>\
				<div class='form-group col-2'>\
					<label class=''>Visibility condition</label>\
					<input type='text' id='condition' class='form-control' placeholder='Type the field visibility condition'/>\
				</div>\
				<div class='form-group col-2'>\
					<label class=''>Copy value</label>\
					<input type='text' class='form-control copy_value' placeholder=''/>\
				</div>\
				<div class='form-group col-1'>\
					<label class='' >Size</label>\
					<input class='form-control' type='text' id='field_size' placeholder='Size'/>\
				</div>\
				<div class='form-check col-1'>\
					<input class='form-check-input' id='field_required' type='checkbox'/><label class='form-check-label'> Required</label> \
				</div>\
				<div class='form-check col-1'>\
					<input class='form-check-input' id='field_visible' type='checkbox'/><label class='form-check-label'>Visible</label>\
				</div>\
				<div class='form-group col-2'>\
					<label class='' >Default value</label>\
					<input class='form-control' type='text' id='default_value' placeholder='Default value'/>\
				</div>\
    </div></div></div>\
	<div class='field_properties card'><div class='row'>\
		<div class='card-body'><h5 class='card-title'>Custom widget properites</h5>\
		<div id='widget_options'/>	</div>	\
    </div></div>\
</div>	");
	var conditions=$("#conditions", fieldcontainer);
	function addcondition(condition){
		var c_node=$("<div class='field_condition'><input style='width: 40px;' type='text' id='c_field' /><select style='width: 60px;' id='c_sign'/><input type='text' style='width: 40px;' id='c_value' /><button type='button' class='submit btn' id='c_del'><i class='icon-minus'></i></button></div>");
		var signs=$('#c_sign', c_node)
		for(s in number_operators){
			o=$('<option value="'+s+'">' + number_operators[s] + '<option>');
			signs.append(o);
		}
		if(condition.c_field)
			$('#c_field', c_node).val(condition.c_field);
		if(condition.c_sign)
			$('#c_sign', c_node).val(condition.c_sign);
		if(condition.c_value)
			$('#c_value', c_node).val(condition.c_value);		
		conditions.append(c_node);
		$('#c_del',c_node ).click(function(){
			c_node.remove();
		});		
	}
	$("#c_add", fieldcontainer).click(function(){
		addcondition({});
	});
	
	//<input type='text' id='c_field' /><select id='c_sign'/><input type='text' id='c_value' />
	var sqltablename=$("#sqltablename", fieldcontainer);
	var tablename=$("#tablename", fieldcontainer);
	var sqlname=$("#sqlname", fieldcontainer);
	var field_name=$("#field_name", fieldcontainer);
	var field_title=$("#title", fieldcontainer);
	var wselect=$("#field_widget_select", fieldcontainer);
	var condition=$("#condition", fieldcontainer);
	var field_visible=$("#field_visible", fieldcontainer).prop("checked", true);
	var field_required=$("#field_required", fieldcontainer);
	var field_size=$("#field_size", fieldcontainer);
	var default_value=$("#default_value", fieldcontainer);
	var copy_value=$(".copy_value", fieldcontainer);
	var inherit_table=$("#inherit_table", fieldcontainer);
	for (var k = 0; k < $.widgets.widgetlist.length; k++) {
		var widget=$.widgets.widgetlist[k][1]();
		if(widget.user_visible){
			wselect.append( $('<option value="'+$.widgets.widgetlist[k][0]+'">'+widget.rname+'</option>'));
		}
	}
	wselect.change(function (){
		widget_options=$('#widget_options', fieldcontainer);
		widget_options.empty();
		if(field_json==null)
			var attr_json={ widget:{name:$(this).val()} };
		else
			var attr_json=field_json;
		var widget=$.widgets.get_widget(attr_json);
		wselect.data('widget', widget);
		widget.viewPropForm(widget_options);
		if(widget.fieldtype=='thememoderation'){
			field_name.val('waiting_for_approval');
			if(field_title.val()=='')
				field_title.val('waiting_for_approval');
		}
	});
	
	field_name.attr('disabled', 'disabled');
	
	field_title.change(function() {
		if(field_name.val()=='')
			field_name.val(getcorrectobjname(field_title.val()));
		//alert('Handler for .change() called.');
	});
	
	//translit 
	if(field_json!=null){		
		field_name.val(field_json.fieldname);
		var field_description=$("#description", fieldcontainer);
		field_description.val(field_json.description);
		condition.val(field_json.condition);
		field_title.val(field_json.title);
		field_size.val(field_json.size);
		sqlname.val(field_json.sqlname);
		
		if(field_json.inherit_table){
			inherit_table.val(field_json.inherit_table);
			fieldcontainer.addClass('inherit_field');
		}
		default_value.val(field_json.default_value);
		copy_value.val(field_json.copy_value);
		if(field_json.parentfield)
			$('#parentfield', fieldcontainer ).val(field_json.parentfield);		
		tablename.val(field_json.tablename);
		
		
		sqltablename.val(field_json.sqltablename);
		if(field_json.widget!=undefined && field_json.widget.name!=undefined && field_json.widget.name!='') {
			$("#field_widget_select [value='"+field_json.widget.name+"']", fieldcontainer).attr("selected", "selected");	
			fieldcontainer.data("sld", field_json.widget.sld);
		}
		if(field_json.visible ===undefined || field_json.visible)
			field_visible.prop("checked", true);
		else
			field_visible.prop("checked", false);
		if(field_json.required !=undefined && field_json.required)
			field_required.prop("checked", true);
		if(field_json.conditions){
			for(var i=0; i<field_json.conditions.length; i++){
				addcondition(field_json.conditions[i])
			}
		}		
	}
	wselect.trigger('change');	
	
/*	
	if (field_json !== null) {
		if ((field_json.fieldname == 'fid') || (field_json.fieldname == 'id')) {
			//$(fieldcontainer).hide();
			//$("#field_widget_select [value='w_hidden']", fieldcontainer).attr("selected", "selected");	
			wselect.hide();	
		}
	}
*/
	field_json = null;
	field_ul=$('#field_list',field_list);
	var li=$('<li/>');
	li.append(fieldcontainer);
	field_ul.append(li);
	
	all_fields=$('.fieldcontainer', field_ul);
	if(all_fields.length>1){
		field_ul.sortable({
			revert: true
		});
	}
	else if (all_fields.length>2)
		field_ul.sortable( "refresh" );	
	
	$("#delete_field", fieldcontainer).click(function(){
		//var pdiv = $(this).parents('#fieldcontainer');
		li.remove();
		field_ul.sortable( "refresh" );	
		return false
	});	
	
	$('.show_prop', fieldcontainer).click(function(){
		$('.field_properties').hide();
		if($(this).html()=='Hide properties'){
			$(this).html('Show properties');
			return;
		}
		$('.show_prop').html('Show properties');
		$(this).html('Hide properties');
		var fld=$(this).parents('.fieldcontainer');
		$('.field_properties',fld).show();
	});
	$('.field_properties',fieldcontainer).hide();
	return fieldcontainer;
}

function createparamsform(params, container){	
	if (params==null) {
		var field=createfield(null, container);
		//container.append(field);
	}
	else{
		for (var i =0; i < params.length; i++) {
			//if(params[i].fieldname!='id'){
				var field=createfield(params[i], container);
				//container.append(field);
			//}
		}
	}
}


var cnt=0;

function createprintform(template){
	var frm=$("<div class='print_template'>\
	<div class='form-group'>\
		<label for='print_name'>Print template name</label>\
		<input type='text' id='print_name' class='form-control'/>\
	</div>\
	<div class='form-group'>\
		<label>Print template</label>\
		<TEXTAREA class='body_print form-control' id='print_template"+cnt+"' placeholder='Type the print template'/></TEXTAREA>\
	</div>\
	</div>");
	$('#print_templates').append(frm);
	
	if(template!=null){
		$('#print_name', frm).val(template.name);
		$("#print_template"+cnt,frm).val(gettemplate(template.body));
	}
	
	
	tinymce.init({
    selector: "#print_template"+cnt,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	});
	cnt++;

}
var btn=$("<button type='button' class='submit btn' id='tablesave'><i class='icon-plus'></i> Save table</button>");
function createform(table_json, dataset_id){
	var form=$("<div id='table_form' class='field_form'/>");
	var tableoptions=$("<div id='tableoptions'>\
    <div class='form-group'>\
        <label for='tabletitle'>Table name</label>\
        <input class='form-control' type='text' name='tabletitle' id='tabletitle' value=''>\
        <input type='hidden' class='text' name='tablepath' id='tablepath' value=''>\
		<input type='hidden' class='text' name='tablename' id='tablename' value=''>\
		<input type='hidden' id='sequencename'/>\
        <input type='hidden' class='text' id='schema' value=''>\
        <input type='hidden' class='text' name='dataset_id' id='dataset_id' value=''>\
        <input type='hidden' class='text' name='input_form' id='input_form' value=''>\
        <input type='hidden' class='text' name='output_form' id='output_form' value=''>\
    </div>\
    <div class='form-group'>\
        <label class='span2 tab_field_label'>Description</label>\
        <textarea class='form-control' id='tabdescription'></textarea>\
    </div>\
    <div class='form-group form-check'>\
        <input class='form-check-input' id='inherits' type='checkbox'/><label class='form-check-label'> Inherits</label>\
	</div>\
	<div class='' id='inherit_table'> </div>\
	<div class='form-group form-check inherits'>\
	</div>\
    <div class='form-group'>\
		<button class='btn btn-primary load_inherited_fields'>Load inherited fields</button>\
    </div>\
    <div class='form-group form-check'>\
        <input class='form-check-input' id='objectstory' type='checkbox'/><label class='form-check-label'>Object story</label>\
    </div>\
</div>");
	//<button class='btn' type='button' onclick='window.open(&quot;/managetable?id="+dataset_id+"&quot;, &quot;_blank&quot;)'><i class='icon-play'></i></button>

	var ontology_table = 0;
	var generalinfocontainer = $('#general_info_container');
	generalinfocontainer.append(tableoptions);
	
	$("#droptable").click(function() {
		if(confirm("Delete the table?")){
			dataset_id=$("#dataset_id").val();
			$.ajax({
				type: "GET",
				url: "/dataset/drop?f=" + dataset_id,
				success: function(msg){
					msg=JSON.parse(msg);
					alert(msg.status);
				}
			});
		}
	});

	$("#cleartable").click(function() {
		if(confirm("Delete all data?")){
			dataset_id=$("#dataset_id").val();
			$.ajax({
				type: "GET",
				url: "/dataset/clear?f=" + dataset_id,
				success: function(msg){
					msg=JSON.parse(msg);
					alert(msg.status);
				}
			});
		}
	});

	
	$("#templateadd").click(function() { 
				createprintform(null);
			});

	
	if(dataset_id!=undefined && dataset_id!=null && !isNaN(dataset_id))
		$("#dataset_id").val(dataset_id);
	else
		$("#dataset_id").val(-1);


	$("#tablepath").click(function() { 
				SaveFileDlg(function(path){ 
					 $("#tablepath").val(path); 
				} );
			});
	$("#input_form").click(function() { 
				SaveFileDlg(function(path){ 
					$("#input_form").val(path);
				} );
			});
	$("#output_form").click(function() { 
				SaveFileDlg(function(path){ 
					$("#output_form").val(path); 
				} );
			});

	$('#tableplay').click(function() {
				dataset_id=$("#dataset_id").val();
				window.open("/managetable?id="+dataset_id, "_blank");
			});
	$('#mapplay').click(function() {
				dataset_id=$("#dataset_id").val();
				window.open("/managetable?id=2", "_blank");
			});
			
			
	var tabletitle=$("#tabletitle");
	var tablename=$("#tablename");
	var scheme=$("#scheme");
	tablename.attr('disabled', 'disabled');
	tabletitle.change(function() {
		if(tablename.val()=='')
			tablename.val(getcorrectobjname(tabletitle.val()));
		//alert('Handler for .change() called.');
	});
			

	var buttons=$("<div class='btn-group' id='buttons'/>");
	form.append(buttons);
	
	var fields=$("<div id='tablefields'><form><ul id='field_list' style='list-style-type: none;'/></form></div>");
	
	
	$('#tablecreatefield').click(function() {
		f = createfield(null, fields);
	});
	
	
	// var dlg = $("#delimeterDlg");
	// if(dlg.length == 0) {
 	// 	dlg=$("<div id='delimeterDlg' title='Save File Dialog' class='invisinle'>\
	// 		<div class='row'>\
	// 			<div class='span9'>delimeter\
	// 				<select id='delimeter'><option value='t'>Tab</option><option value=';'>;</option><option value=','>,</option></select>\
	// 			</div>\
	// 		</div>\
	// 	</div>");
 	// 	// dlg.appendTo("body");
	// }
	$('#loadfilestructure').click(function() {
		OpenFileDlg(function(path){
			var dlg=$("<div id='delimeterDlg' title='Save File Dialog' class='invisinle'>\
			<div class='row'>\
				<div class='span9'>delimeter\
					<select id='delimeter'><option value='t'>Tab</option><option value=';'>;</option><option value=','>,</option></select>\
				</div>\
			</div>\
		</div>");
			var dialog = dlg.dialog({
				minWidth: 750,
				title: dlgtitle,
				position: ["center", "center"],
				close: function( event, ui ) {
					$('#delimeterDlg').remove();
				},
				buttons: {
				"Open": function() {
					var load_id=gethash(path)+'&delimeter='+$('#delimeter').val();
					$.getJSON("/datafile/meta?path=" + load_id+'&first_is_head=true', function(res) {
						if(res.status!='ok'){
							alert(res.message);
							return;
						}
						var table_json    = res.JSON;
						for (var i = 0; i < table_json.columns.length; i++) {
							table_json.columns[i].fieldname=getcorrectobjname(table_json.columns[i].fieldname);
							createfield(table_json.columns[i], fields);
						}
					});						
					$(this).dialog("close");
					$('#delimeterDlg').remove();
				}
				},		
				modal:false
			});
			
		});
		//f = createfield(null, fields);
	});
	var metaTableSelect = {
		"fieldname": "",
		"title": "",
		"description": "",
		"visible": true,
		"widget": {
			"name": "theme_select",
			properties: {}
		}
	};
	
	
	$('#importtable').click(function() {
		var Tablesdlg=$('<div id="tabsDlg" title="Table import" class="">\
			<form>\
				<div class="form-group">\
					<label for="url">URL</label>\
					<input type="text" class="form-control" id="url"  placeholder="Enter url">\
				</div>\
	  		</form>\
		</div>');
			
			
		
	
		var dialog = Tablesdlg.dialog({
			minWidth: 750,
			title: 'Select table',
			position: ["center", "center"],
			close: function( event, ui ) {
				$('#delimeterDlg').remove();
			},
			buttons: {
			"Import table": function() {
				var url=$('#url',Tablesdlg).val();
				

				
				$.getJSON("/bridge/list?url=" + encodeURIComponent(url), function (result) {
						if(result.status!='ok'){
							alert(result.data);
							return;
						}
						
						var res = JSON.parse(result.data);						
						var table_json = JSON.parse(res.aaData[0].JSON);				
						alert(table_json);
						$(this).dialog("close");
						Tablesdlg.remove();
					});
				// getdatasetmeta(load_dataset_id, function (error, table_json) {						
				// 	if (error!=null)
				// 		return;					
					
				// 	for (var i = 0; i < table_json.columns.length; i++) {
				// 		table_json.columns[i].fieldname=getcorrectobjname(table_json.columns[i].fieldname);
				// 		createfield(table_json.columns[i], fields);
				// 	}
					
				// });
							
				
			}
			},		
			modal:false
		});		

	});
	$('#loadtablestructure').click(function() {
		var tableSelect = $.widgets.get_widget(metaTableSelect);
		
			var Tablesdlg=$("<div id='tabsDlg' title='Select File Dialog' class='invisinle'>\
				<div class='row'>\
					<div class='span9 tabselect'>\
					</div>\
				</div>\
			</div>");
			
			
			tableSelect.assign($('.tabselect', Tablesdlg), '');
		
			var dialog = Tablesdlg.dialog({
				minWidth: 750,
				title: 'Select table',
				position: ["center", "center"],
				close: function( event, ui ) {
					$('#delimeterDlg').remove();
				},
				buttons: {
				"Add fields": function() {
					var load_dataset_id=tableSelect.getVal();
					
					getdatasetmeta(load_dataset_id, function (error, table_json) {						
						if (error!=null)
							return;					
						
						for (var i = 0; i < table_json.columns.length; i++) {
							table_json.columns[i].fieldname=getcorrectobjname(table_json.columns[i].fieldname);
							createfield(table_json.columns[i], fields);
						}
						
					});
								
					$(this).dialog("close");
					$('#delimeterDlg').remove();
				}
				},		
				modal:false
			});
	});
	
	
	function loadinheritedfields(load_dataset_id){		
		if(load_dataset_id=='')
			return;
		$.getJSON("/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=" + load_dataset_id, function (data) {						
			if (data.aaData.length == 0)
				return;
			curmeta = get_table_json($("#table_form"));
			var inheritedtable = JSON.parse(data.aaData[0].JSON);			
			if(inheritedtable.sequencename ===undefined){
				$('#sequencename').val(inheritedtable.tablename + "_id_seq");
			}
			else
				$('#sequencename').val(inheritedtable.sequencename);
			for (var i = 0; i < inheritedtable.columns.length; i++) {
				if(inheritedtable.columns[i].fieldname=='id')
					continue;
				found=false;
				for (var j = 0; j < curmeta.columns.length; j++) {
					if(curmeta.columns[j].fieldname == inheritedtable.columns[i].fieldname){
						found=true;
						break;
					}
				}
				if(found){
					continue;
				}
				inheritedtable.columns[i].fieldname=getcorrectobjname(inheritedtable.columns[i].fieldname);
				inheritedtable.columns[i].inherit_table=load_dataset_id;
				createfield(inheritedtable.columns[i], fields);
			}			
		});	
	}
	if(table_json && table_json.inherit_table)
		inherit_table_val=table_json.inherit_table;
	
	var inherit_Select = $.widgets.get_widget(metaTableSelect);
	inherit_Select.assign($('#inherit_table'), inherit_table_val);
	
	var changeobject={};
	changeobject.changevalue = function (fieldname, value) {	
		if(value==inherit_table_val)
			return;
		loadinheritedfields(value);	
		inherit_table_val=value;	
		// if(value=='')
		// 	return;
		// $.getJSON("/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=" + load_dataset_id, function (data) {						if (data.aaData.length == 0)
		// 		return;
		// 	var table_json = JSON.parse(data.aaData[0].JSON);			
		// 	for (var i = 0; i < table_json.columns.length; i++) {
		// 		if(table_json.columns[i].fieldname=='id')
		// 			continue;
		// 		table_json.columns[i].fieldname=getcorrectobjname(table_json.columns[i].fieldname);
		// 		table_json.columns[i].inherit_table=load_dataset_id;
		// 		createfield(table_json.columns[i], fields);
		// 	}			
		// });		
	}
	inherit_Select.listnerlist.push(changeobject);
	$('.load_inherited_fields').click(function() {
		loadinheritedfields(inherit_table_val);	
	});
	
	
	
	buttons.append(btn);
	var btn=$("<button type='button' class='submit btn' id='tablesave'><i class='icon-plus'></i> Save table</button>");
	buttons.append(btn);
	$('#tablesave').click(function() {
		f = get_table_json($("#table_form"));
		if (f !== null) {
			var table_json_data = JSON.stringify(f);
		
			$.post("/dataset/savestructure", {tablejson: table_json_data}, "json")
				.done(function(data) {
					data = JSON.parse(data);
					if (data.status === 'ok') {
						$('#actionresult').html('Successfully saved');
						// alert('Successfully saved');
						newtableid = data.data;
						$("#dataset_id").val(newtableid);
						if (getQueryVariable('action') === 'create') {
							window.location.href = window.location.origin + '/managejson?id=' + newtableid;
						}
					} else {
						alert('Error occured: '+JSON.stringify(data.data));
					}
				})
				.done(function(data) { 
					table                    = JSON.parse(data).data;
					var ontology_table       = getUrlVars()["ontology"];
					var ontologies_json_data = JSON.stringify({
						ontologies : ontology_table,
						table:       table
					});
					/*
					$.post("/ontology/tablesave", {ontologies: ontologies_json_data} )
					.done(function(data) {
						alert("Ontology status: " + data);
					});*/
				})
				.fail(function() { alert("error"); });
		}
		else
		{
			alert('Error occured');
		}
	});

	var btn=$("<button type='button' class='submit' id='tablesavetofile'>Save to file</button>");
	btn.click(function() {
		f = get_table_json($("#table_form"));
		var table_json_data=JSON.stringify(f);
		if ($("#tablepath").val()==''){
			SaveFileDlg(function(path){
				$("#tablepath").val(path);
				saveFile(path, table_json_data);
			});
		}
		else{
			var path=$("#tablepath").val();
			saveFile(path, table_json_data);
		}
	});
	
	var fieldscontainer = $('#fields_container');
	fieldscontainer.append(fields);
	
	var inherits=$('#inherits');
	var objectstory=$('#objectstory');

	if (table_json == null) {
			createparamsform(null,fields);
			createprintform(null);
	} else {
		$("#tablename").val(table_json.tablename);		
		$("#schema").val(table_json.schema);
		$("#input").val(table_json.input);
		$("#tabdescription").val(table_json.description);
		$('#sequencename').val(table_json.sequencename);
		$('#addlink').val(table_json.addlink);
		$('#editlink').val(table_json.editlink);
		
		
		if(table_json.print_template!=null){
			if($.isArray(table_json.print_template)){
				for(var i = 0; i < table_json.print_template.length; i++){
					createprintform(table_json.print_template[i]);
				}
			}
			else{
				var o={name:'print', body:table_json.print_template};
				createprintform(o);
			}			
		}
		else
			createprintform(null);
		//$("#print_template", tableoptions).val(getpath(table_json.print_template));
		$("#form_template").val(gettemplate(table_json.form_template));
		$("#view_template").val(gettemplate(table_json.view_template));
		$("#tabletitle").val(table_json.title);
		
		
		
		if(table_json.inherits){
			inherits.prop("checked", true);
			$('#inherit_table').show();
		}
		else{
			inherits.prop("checked", false); 
			$('#inherit_table').hide();
		}
		
		if(table_json.objectstory){
			objectstory.prop("checked", true);			
		}
		else{
			objectstory.prop("checked", false);			
		}
		

		/*
		if ("moderated" in table_json) {
			if (table_json.moderated === true) {
				$('#moderation_is_enabled').attr('checked', 'checked');
			}
		}
	*/
		createparamsform(table_json.columns,fields);
	}
	inherits.click(function(){
		if(inherits.prop("checked"))
			$('#inherit_table').show();
		else
			$('#inherit_table').hide();
	});
						
	

	tinymce.init({
    selector: "#form_template",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	});
	
	$("#tabs" ).tabs();
	return form;
}

function exist_field(name, columns){
	for (var k = 0; k < columns.length; k++) {
		if(columns[k].fieldname==name)
			return true;
	}
	return false;
}

function get_params_json(table_container){
	var columns = new Array();	

	
		$( ".fieldcontainer" , table_container).each(function( index ) {
			var fieldcontainer=$(this);
			f_name=$("#field_name",fieldcontainer).val();
			if(!exist_field(f_name, columns)){
				if($("#field_name",fieldcontainer).val()=='')
					throw "The field name is empty";
				if($("#description",fieldcontainer).val()=='')
					$("#description",fieldcontainer).val($("#field_name",fieldcontainer).val());
				var field={fieldname:f_name, title: $("#title",fieldcontainer).val(), description: $("#description",fieldcontainer).val(), visible:true };			
				var widget_name;
				var widget_propoties;			
				widget_name=$("#field_widget_select option:selected",fieldcontainer).val();
				var attr_json={ widget:{name:widget_name} };
				var widget=$.widgets.get_widget(attr_json);
				var wselect=$("#field_widget_select", fieldcontainer);
				var widget=wselect.data('widget');
				widget_propoties=widget.getPropForm($("#widget_options",fieldcontainer));
				
				var field_required=$("#field_required", fieldcontainer);
				var field_size=$("#field_size", fieldcontainer);
				
								
				field.type=widget.fieldtype;
				if( $('#field_visible',fieldcontainer).prop("checked") )
					field.visible=true;
				else
					field.visible=false;
				field.required = field_required.prop("checked");
				field.widget={name:widget_name, properties:widget_propoties};
				field.widget.sld=fieldcontainer.data("sld");
				field.condition=$("#condition",fieldcontainer).val();
				field.conditions=[];
				field.size=field_size.val();
				field.tablename=$("#tablename", fieldcontainer).val();				
				field.sqltablename=$("#sqltablename", fieldcontainer).val();
				field.sqlname=$("#sqlname", fieldcontainer).val();
				field.parentfield=$("#parentfield", fieldcontainer).val();	
				field.default_value=$("#default_value", fieldcontainer).val();	
				field.inherit_table=$("#inherit_table", fieldcontainer).val();	
				field.copy_value=$(".copy_value", fieldcontainer).val();
				
				
				/*
				$('.field_condition',fieldcontainer).each(function(index){
					var cond={}
					c_node=$(this);
					cond.c_field=$('#c_field', c_node).val();
					cond.c_sign=$('#c_sign', c_node).val();
					cond.c_value=$('#c_value', c_node).val();
					field.conditions.push(cond);
					
				});			*/
		
				
				columns[columns.length]=field;
			}
		});
	
	if(!exist_field('id', columns) ){
		var field = {
			fieldname : "id",
			title     : "key",
			pk        : true,
			type      : "integer",
			visible   : false,
			widget    : {
				name : "number"
			}
		};
		columns[columns.length]=field;
	}
	else{
		for (var k = 0; k < columns.length; k++) {
			if(columns[k].fieldname=='id'){
				columns[k].type='integer';
				break;
			}
		}

	}
	
	return columns;
}


/**
 *	Getting theme object, composed according to user-defined values
 */

function get_table_json(table_container) {
	if($("#tablename", table_container).val()==''){
		alert('Please, enter the table title');
		return null;
	}
	var tablename=$("#tablename",table_container).val()
	
	
	var columns = get_params_json(table_container);
	if (columns === null) {
		return null;
	}
	var moderated      = false;//$('#moderation_is_enabled').prop('checked');
	for (var i = 0; i < columns.length; i++) {
		if(columns[i].widget.name=='thememoderation')
			moderated=true;
		if(columns[i].sqltablename===undefined || columns[i].sqltablename=='')
			columns[i].sqltablename=tablename;
		if(columns[i].sqlname===undefined || columns[i].sqlname=='')
			columns[i].sqlname=columns[i].fieldname;
		
	}
	
	var form_templates=[];
	$('.print_template').each(function( index ) {
		var printcontainer=$(this);
		area_id=$(".body_print",printcontainer).attr('id');
		form_template={name:$("#print_name",printcontainer).val(), body: gethash(tinyMCE.get(area_id).getContent()) };
		form_templates.push(form_template);
	});
	
	var table_json = {
		"tablename"   	: $("#tablename",table_container).val(),
		"sequencename"	: $("#sequencename",table_container).val(),
		"addlink"	  	: $("#addlink",table_container).val(),
		"editlink"	  	: $("#editlink",table_container).val(),
		"dataset_id"  	: $("#dataset_id",table_container).val(),
		"schema"      	: $("#schema",table_container).val(),
		"title"       	: $("#tabletitle",table_container).val(),
		"input"       	: $("#input",table_container).val(),
		"print_template": form_templates,
		"form_template" : gethash(tinymce.EditorManager.get('form_template').getContent()),
		"view_template" : gethash($("#view_template",table_container).val()),
		"columns"     	: columns,
		"moderated"   	: moderated,
		"description" 	: $("#tabdescription").val(),
		"inherit_table" : inherit_table_val,
		"inherits"		: $('#inherits').prop("checked"),
		"objectstory"	: $('#objectstory').prop("checked")		
		
	};
	
	if(table_json.sequencename ===undefined || table_json.sequencename ==''){
		table_json.sequencename=table_json.tablename + "_id_seq";
	}
		

		
	return table_json;
}
