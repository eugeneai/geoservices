(function($) {
	/*
	 * Function: fnGetColumnData
	 * Purpose:  Return an array of table values from a particular column.
	 * Returns:  array string: 1d data array 
	 * Inputs:   object:oSettings - dataTable settings object. This is always the last argument past to the function
	 *           int:iColumn - the id of the column to extract the data from
	 *           bool:bUnique - optional - if set to false duplicated values are not filtered out
	 *           bool:bFiltered - optional - if set to false all the table data is used (not only the filtered)
	 *           bool:bIgnoreEmpty - optional - if set to false empty values are not filtered from the result array
	 * Author:   Benedikt Forchhammer <b.forchhammer /AT\ mind2.de>
	 */

	$.fn.dataTableExt.oApi.fnGetColumnData = function ( oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty ) {
		// check that we have a column id
		if ( typeof iColumn == "undefined" ) return new Array();
		
		// by default we only wany unique data
		if ( typeof bUnique == "undefined" ) bUnique = true;
		
		// by default we do want to only look at filtered data
		if ( typeof bFiltered == "undefined" ) bFiltered = true;
		
		// by default we do not wany to include empty values
		if ( typeof bIgnoreEmpty == "undefined" ) bIgnoreEmpty = true;
		
		// list of rows which we're going to loop through
		var aiRows;
		
		// use only filtered rows
		if (bFiltered == true) aiRows = oSettings.aiDisplay; 
		// use all rows
		else aiRows = oSettings.aiDisplayMaster; // all row numbers
	
		// set up data array	
		var asResultData = new Array();
		
		for (var i=0,c=aiRows.length; i<c; i++) {
			iRow = aiRows[i];
			var aData = this.fnGetData(iRow);
			var sValue = aData[iColumn];
			
			// ignore empty values?
			if (bIgnoreEmpty == true && sValue.length == 0) continue;
	
			// ignore unique values?
			else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;
			
			// else push the value onto the result data array
			else asResultData.push(sValue);
		}
		
		return asResultData;
	};

	/**
	* Add backslashes to regular expression symbols in a string.
	* 
	* Allows a regular expression to be constructed to search for 
	* variable text.
	* 
	* @param string sText The text to escape.
	* @return string The escaped string.
	*/
	$.fnRegExpEscape = function( sText ) { 
		return sText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"); 
	};

	/**
	* Menu-based filter widgets based on distinct column values for a table.
	*
	* @class FilterWidgets 
	* @constructor
	* @param {object} oDataTableSettings Settings for the target table.
	*/
	var FilterWidgets = function( oDataTableSettings ) {	   
		var me = this;
		var sExcludeList = '';
		me.$AccordionContainer = 
		$('<div id="acc-filters" class="ui-accordion ui-widget ui-helper-reset ui-accordion-icons" role="tablist">'+
		'	<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" role="tab" aria-expanded="false" aria-selected="false" tabindex="0">'+
		'	<span class="ui-icon ui-icon-triangle-1-e"></span>'+
		'	<a href="#" tabindex="-1">Фильтры</a></h3>'+
		'</div>');
		/*
		me.$AccordionContainer.click(function(){			
			var container = $( '#acc-filters h3 span' );
			var aclass = container.attr( 'class' );
			var tr = $( '#dt_fltr' );
			if ( container.hasClass('ui-icon-triangle-1-e')){
				container.removeClass('ui-icon-triangle-1-e');
				container.addClass( 'ui-icon-triangle-1-s' );
				tr.show();				
			} else {
				container.removeClass( 'ui-icon-triangle-1-s' );	
				container.addClass('ui-icon-triangle-1-e');
				tr.hide();
			}
		});
		*/
		//me.$AccordionContainer = $( '<div id="acc-filters" class="accordion"></div>' );
		me.$Header = $( '<h3><a href="#">Фильтр</a></h3>' );
		me.$WidgetContainer = $( '<div class="column-filter-widgets"></div>' );
		me.$TrContainer = $( '#dt_fltr', oDataTableSettings.nTable );
		me.$MenuContainer = me.$WidgetContainer;
		me.$TermContainer = null;
		me.aoWidgets = [];
		me.sSeparator = '';
		me.$AutocompleteSource = '';
		if ( 'oFilterWidgets' in oDataTableSettings.oInit ) {
			if ( 'aiExclude' in oDataTableSettings.oInit.oFilterWidgets ) {
				sExcludeList = '|' + oDataTableSettings.oInit.oFilterWidgets.aiExclude.join( '|' ) + '|';
			}
			if ( 'bGroupTerms' in oDataTableSettings.oInit.oFilterWidgets && oDataTableSettings.oInit.oFilterWidgets.bGroupTerms ) {
				me.$MenuContainer = $( '<div class="column-filter-widget-menus"></div>' );
				me.$TermContainer = $( '<div class="column-filter-widget-selected-terms"></div>' ).hide();
			}
			if ( 'AutocompleteSource' in oDataTableSettings.oInit.oFilterWidgets ) {
				me.$AutocompleteSource = oDataTableSettings.oInit.oFilterWidgets.AutocompleteSource;
			}			
		}
		// Add a widget for each visible and filtered column
		$.each( oDataTableSettings.aoColumns, function ( i, oColumn ) {
			var $columnTh = $( oColumn.nTh );
			var $WidgetElem = $( '<div class="column-filter-widget"></div>' );
			if ( sExcludeList.indexOf( '|' + i + '|' ) < 0 ) {
				me.aoWidgets.push( new $[oColumn.sFilterName]( $WidgetElem, oDataTableSettings, i, me ) );				
			}
			
			//var $ThWidgetElem = $( '<th style="width: auto;" class="ui-state-default" role="columnheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1"></th>' );
			//$('#dt_fltr').append( $ThWidgetElem );	
			//if ( sExcludeList.indexOf( '|' + i + '|' ) < 0 ) {
			//	me.aoWidgets.push( new $[oColumn.sFilterName]( $ThWidgetElem, oDataTableSettings, i, me ) );				
			//}
			
			me.$MenuContainer.append( $WidgetElem );
			me.$ThContainer = $('<th class="ui-state-default"></th>')
			me.$ThContainer.append( $WidgetElem );
			me.$TrContainer.append( me.$ThContainer );
		
		} );

		if ( me.$TermContainer ) {						
			me.$WidgetContainer.append( me.$MenuContainer );
			me.$WidgetContainer.append( me.$TermContainer );
		}
		//me.$AccordionContainer.append( me.$Header ); 
		//me.$AccordionContainer.append( me.$WidgetContainer ); 
		//$('#example thead').append( me.$TrContainer );
		return me;
	};

	/**
	* Get the container node of the column filter widgets.
	* 
	* @method
	* @return {Node} The container node.
	*/
	//FilterWidgets.prototype.getContainer = function() {
		//return this.$WidgetContainer.get( 0 );
	//}
	
	FilterWidgets.prototype.getContainer = function() {
		//return this.$AccordionContainer.get( 0 );
		return this.$WidgetContainer.get( 0 );
	}

	


	/*
	 * Register a new feature with DataTables
	 */
	if ( typeof $.fn.dataTable === 'function' && typeof $.fn.dataTableExt.fnVersionCheck === 'function' && $.fn.dataTableExt.fnVersionCheck('1.7.0') ) {

		$.fn.dataTableExt.aoFeatures.push( {
			'fnInit': function( oDTSettings ) {
				var oWidgets = new FilterWidgets( oDTSettings );
				return oWidgets.getContainer();
			},
			'cFeature': 'A',
			'sFeature': 'FilterWidgets'
		} );

	} else {
		throw 'Warning: jquery.FilterWidgets requires DataTables 1.7 or greater - www.datatables.net/download';
	}
	var Filters={};


}(jQuery));
