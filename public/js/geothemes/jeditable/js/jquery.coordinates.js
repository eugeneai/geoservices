/*
	Masked Input plugin for jQuery
	Copyright (c) 2007-2011 Josh Bush (digitalbush.com)
	Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license) 
	Version: 1.3
*/
function ToDec(string) {
	in_arr  =  string.split(' ');
	var lon_vl = in_arr[1];
	var lan_vl = in_arr[0];
	
	arr = lon_vl.split(/[\D]/);
	var lon_sec = arr[2]/3600;
	var lon_mn = arr[1]/60;
	var lon = parseInt(arr[0],10)+lon_mn+lon_sec;
	
	arr = lan_vl.split(/[\D]/);
	var lan_sec = arr[2]/3600;
	var lan_mn = arr[1]/60;
	var lan = parseInt(arr[0],10)+lan_mn+lan_sec;
					
	var value = lan + " " + lon;
	return value;
};

function addzero(v, cnt){  
	v=v+'';
	while (cnt>v.length){
	  v='0'+v;
	}  
	return v;
}
function FromDec(string) {
	if(string=='')
	arr={0:'0',1:'0'};	
	
	else
	arr  =  string.split(' ');
		
	var lon_gr = addzero(Math.floor(arr[0]),3);
	var lon_mn = addzero(Math.floor((arr[0]-lon_gr)*60),2);
	var lon_sec = addzero(Math.round(((arr[0]-lon_gr)*60-lon_mn)*60),2);
		 
	var lan_gr = addzero(Math.floor(arr[1]),3);
	var lan_mn = addzero(Math.floor((arr[1]-lan_gr)*60),2);
	var lan_sec = addzero(Math.round(((arr[1]-lan_gr)*60-lan_mn)*60),2);
	var value =lan_gr+"°"+lan_mn+"'"+lan_sec+"''"+" "+lon_gr+"°"+lon_mn+"'"+lon_sec+"''";
	return value;
};

(function($) {
	var pasteEventName = ($.browser.msie ? 'paste' : 'input') + ".coordinates";
	var iPhone = (window.orientation != undefined);
	var geos_init=false;

	$.coordinates = {
		//Predefined character definitions
		definitions: {
			'9': "[0-9]",
//			'N': "[NW]",
			'a': "[A-Za-z]",
			'*': "[A-Za-z0-9]"
		},
		dataName:"rawMaskFn"
	};
	$.fn.extend({
		//Helper Function for Caret positioning
		caret: function(begin, end) {
			if (this.length == 0) return;
			if (typeof begin == 'number') {
				end = (typeof end == 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						var range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					var range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		coordinates: function(coordinates, settings) {
			geos_init=true;
			if (!coordinates && this.length > 0) {
				var input = $(this[0]);
				return input.data($.coordinates.dataName)();
			}
			settings = $.extend({
				editpointmode: "default",
				placeholder: "_",
				degree_template:"999°99'99''",
				completed: null,
				decimal:false
			}, settings);
	
			// Создание дополнительных элементов управления
			var input = $(this[0]);
			string=input.val();
			
			var lat= $( '<input class="coordinate" name="lan_geos" id="lat_geos_editing" size="10"/>' );
			lat.attr( 'autocomplete','off' );				
			this.after(lat);
			var lat_buffer=settings.degree_template.replace(/9/g,"0");
			var lat_firstValue='';

			var lon = $( '<input class="coordinate" name="lan_geos" id="lon_geos_editing" size="10"/>' );
			lon.attr( 'autocomplete','off' );
			var lon_buffer=settings.degree_template.replace(/9/g,"0");
			var lon_firstValue='';
			this.after(lon);
			
			var initval=false;
			input.attr('style', 'display: none;');
			input.val('MULTIPOINT()');
			
			input.bind("setvalue.coordinates", function (e) {
					//var input=$('#'+e.target.id, this.form);
					var input=$(this);
					val=input.val();
					var re=/MULTIPOINT\(([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\)/
					var arr = re.exec(val);			
					if (arr==null || arr.length<3) return;
					if(settings.decimal){
						lat_buffer=arr[2];
						lat_firstValue=arr[2];
						lon_buffer=arr[1];
						lon_firstValue=arr[1];						
					}
					else{
						val=FromDec(arr[1]+' '+arr[2]);					
						arr = val.split(' ');
						lat_buffer=arr[1];
						lat_firstValue=arr[1];
						lon_buffer=arr[0];
						lon_firstValue=arr[0];					
					}
					
					initval=true;					
					lon.val(lon_buffer);
					lat.val(lat_buffer);
					writeBuffer();				
					initval=false;
				}
			);
			
			function clearBuffer(sub_input, start, end) {
				for (var i = start; i < end && i < len; i++) {
					if (tests[i])
						if(sub_input.attr('id')=='lon_geos_editing') buffer_lon[i] = settings.placeholder;
						else buffer_lan[i] = settings.placeholder;
				}
			};
			function writeBuffer() {
				var s='';
				test=lat.val()+' '+lon.val();
				//try { input[0].subdecimal.val(ToDec(test)); } catch (e) { }
				if(settings.decimal)
					test='MULTIPOINT('+test+')';
				else
					test='MULTIPOINT('+ToDec(test)+')';
				input.val(test);
				return s;
			};			
		
			function testCoordinate(vl, lon){
				var numbers='0123456789';
				if(settings.decimal){					
					if(!test_number(vl,'.',3,0))
						return false;
					var parts = vl.split('.');
					if(parts.length==0 || (lon && (parts[0]>180 || parts[0]<-180)) || (!lon && (parts[0]>90 || parts[0]<-90)) )
						return false;
					else
						return true;
				}
				else{
					if (vl.length<3) return false;
					for (var j=0; j < settings.degree_template.length; j++){
						if(settings.degree_template.charAt(j)=='9'){
							if ((numbers.indexOf(vl.charAt(j))==-1) )
								return false;
						}
						else{
							if (vl.charAt(j)!=settings.degree_template.charAt(j))
								return false;					
						}					
					}
					var parts = vl.split(/\D/,3);
					if (parts[0]<0 || parts[0]>180)  return false;
					if (parts[1]<0 || parts[1]>60)  return false;
					if (parts[2]<0 || parts[2]>60)  return false;
				}
				return true;
			};
		
///////////////////////
			var defs = $.coordinates.definitions;
			var tests = [];
			var partialPosition = coordinates.length;
			var firstNonMaskPos = null;
			var len = coordinates.length;
			function keydownEvent(e) {
				var trg=$(e.target);
				var k=e.which;				
				//console.log('key down');				
					//backspace, delete, and escape get special treatment
				if(k == 8 || k == 46 || (iPhone && k == 127)){
					var pos = trg.caret(),
						begin = pos.begin,
						end = pos.end;
					
					var s_val= trg.val();
					var future_val='';
					if(pos.end-pos.begin!=0){						
						future_val=s_val.substring(0,pos.begin)+s_val.substring(pos.end,s_val.length);
					}
					else if (k==8){
						future_val=s_val.substring(0,pos.begin-1)+s_val.substring(pos.begin,s_val.length);						
					}
					else if (k==46){
						future_val=s_val.substring(0,pos.begin)+s_val.substring(pos.begin+1,s_val.length);						
					}
					
					if (testCoordinate(future_val, e.target.id=='lon_geos_editing')/* tests[p].test(c)*/) {							
						if(e.target.id=='lon_geos_editing'){
							lon_buffer=future_val;
							lon.val(lon_buffer);
						}
						else{
							lat_buffer=future_val;
							lat.val(lat_buffer);
						}
						writeBuffer();
//						var next = seekNext(p);
						trg.caret(pos.begin);						
					}
					return false;
				} else if (k == 27) {//escape
					if(e.target.id=='lon_geos_editing')
						lon.val(lon_firstValue);
					else
						lat.val(lat_firstValue);					
					
					//trg.caret(0, checkVal(trg));
					return false;
				}
				else{
					//console.log('write buffer');					
					//writeBuffer(trg);
				}				
				return true;
			};
			
			function seekNext(pos) {
				while (++pos <= settings.degree_template.length && settings.degree_template[pos]!='9');
				return pos;
			};
			
			function seekPrev(pos) {
				while (--pos >= 0 && settings.degree_template[pos]!='9');
				return pos;
			};
			
			function keypressEvent(e) {
				var trg=$(e.target);
				//console.log('keypress');		
				var k = e.which,
					pos = trg.caret();
				
				if (e.ctrlKey || e.altKey || e.metaKey || k<32) {//Ignore
					return true;
				} else if (k) {
					var c = String.fromCharCode(k);
					if(c==',')
						c='.';
					var s_val= trg.val();
					var future_val='';
					if(pos.end-pos.begin!=0){						
						future_val=s_val.substring(0,pos.begin)+c+s_val.substring(pos.end,s_val.length);
					}
					else {
						var p = seekNext(pos.begin - 1);
						if (p < len) {						
							future_val=s_val.substring(0,p)+c+s_val.substring(p+1,s_val.length);
						}
						else
							return false;
					}
					if (testCoordinate(future_val, e.target.id=='lon_geos_editing')/* tests[p].test(c)*/) {							
						if(e.target.id=='lon_geos_editing'){
							lon_buffer=future_val;
							lon.val(lon_buffer);
						}
						else{
							lat_buffer=future_val;
							lat.val(lat_buffer);
						}
						writeBuffer();
						var next = seekNext(p);
						trg.caret(next);
					}					
					return false;
				}
			};

			lon.bind("keydown.coordinates", keydownEvent);
			lat.bind("keydown.coordinates", keydownEvent);
			lon.bind("keypress.coordinates", keypressEvent);
			lat.bind("keypress.coordinates", keypressEvent);			
			writeBuffer();			
		}
		
	});
	
	
})(jQuery);
