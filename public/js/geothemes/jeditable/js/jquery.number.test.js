var numbers='0123456789';
function testInteger(v){
	v=v.trim();
	if(v=='')
		return true;

	if(v.charAt(0)=='-')
		v=v.substring(1,v.length);
	for (var j=0; j < v.length; j++){		
		if ((numbers.indexOf(v.charAt(j))==-1) )
			return false;		
	}
	return true;
}


function test_number(vl, default_point, before, after){
	if(vl=='') return false;
	if(vl.charAt(0)=='-')
		vl=vl.substring(1,vl.length);
	
	var parts = vl.split(default_point);					
	if(parts.length==0 || !testInteger(parts[0]) || (before!=0 && parts[0].length > before) )
		return false;
	if(parts.length>1 && (!testInteger(parts[1]) || (after!=0 && parts[1].length > after)) )
		return false;					
	if(parts.length>2)
		return false;
	return true;
};

			
(function($) {
	var pasteEventName = ($.browser.msie ? 'paste' : 'input') + ".coordinates";
	var iPhone = (window.orientation != undefined);


	$.fn.extend({
		//Helper Function for Caret positioning
		caret: function(begin, end) {
			if (this.length == 0) return;
			if (typeof begin == 'number') {
				end = (typeof end == 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						var range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					var range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		numberTest: function(coordinates, settings) {
			settings = $.extend({
				default_point: ".",
				before:0,
				after:0
			}, settings);
	
			// �������� �������������� ��������� ����������
			var input = $(this[0]);
			string=input.val();
			var val_buffer='';
			
			input.bind("setvalue.numberTest", function (e) {
					val=input.val();
					if( test_number(val,  settings.default_point, settings.before, settings.after ))
						val_buffer=val;
					writeBuffer();
				}
			);
			
			
			function clearBuffer(sub_input, start, end) {
				new_val_buffer='';
				for (var i = 0; i < val_buffer.length; i++) {
					if(i<start || i>end);
						new_val_buffer=new_val_buffer+val_buffer.charAt(i);
				}
				val_buffer=new_val_buffer;
			};
			function writeBuffer() {				
				input.val(val_buffer);
				return s;
			};
			
///////////////////////
			function keydownEvent(e) {
				var trg=$(e.target);
				var k=e.which;				
				//console.log('key down');				
					//backspace, delete, and escape get special treatment
				if(k == 8 || k == 46 || (iPhone && k == 127)){
					var pos = trg.caret(),
						begin = pos.begin,
						end = pos.end;					
					clearBuffer(trg, begin, end);
					writeBuffer();
					return false;
				} else if (k == 27) {//escape
					input.val(val_buffer);
					return false;
				}
				else{
					//console.log('write buffer');					
					//writeBuffer(trg);
				}				
				return true;
			};
			
			function seekNext(pos) {
				while (++pos <= settings.degree_template.length && settings.degree_template[pos]!='9');
				return pos;
			};
			
			function seekPrev(pos) {
				while (--pos >= 0 && settings.degree_template[pos]!='9');
				return pos;
			};
			
			function keypressEvent(e) {
				var trg=$(e.target);
				//console.log('keypress');		
				var k = e.which,
					pos = trg.caret();
				
				if (e.ctrlKey || e.altKey || e.metaKey || k<32) {//Ignore
					return true;
				} else if (k) {
					if(pos.end-pos.begin!=0){
						//clearBuffer(trg, pos.begin, pos.end);
						return false;
					}
					var p = seekNext(pos.begin - 1);
					if (p < len) {
						var c = String.fromCharCode(k);
						var s_val= trg.val();
						var future_val=s_val.substring(0,p)+c+s_val.substring(p+1,s_val.length);
						
						if (test_number(future_val,  settings.default_point, settings.before, settings.after)) {
							val_buffer=future_val;
							writeBuffer();
							var next = seekNext(p);
							trg.caret(next);
						}
					}
					return false;
				}
			};
			input.bind("keydown.numberTest", keydownEvent);			
			input.bind("keypress.numberTest", keypressEvent);			
			writeBuffer();			
		}
		
	});
	
	
})(jQuery);
