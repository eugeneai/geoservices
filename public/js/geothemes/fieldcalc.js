var fieldcalc = (function () {    

var cdoc;
var server_props;
var geo_table;
var calclist={};
var readycallback;
var refvals={};
var sparser;
var sjspath;

var getrefval=function(dataset_id, id, fieldname){
	//console.log('getrefval', fieldname, id);
	var doc=cdoc;//Сохранение ссылки на текущий документ в контексе вызова функции cdoc может поменяться.
	if(id==null || id==undefined)
		return '';
	if(typeof id==='object' && id.id)
		id=id.id;
	strReturn = "";
	if(id===undefined || id=='' ||  isNaN(parseInt(id)))
		return '';
	var key=fieldname+dataset_id + id;
	if (key in refvals && refvals[key]!=''){
		return refvals[key];
	}
	else if(key in refvals && refvals[key]==''){
		throw new UserException("calculating async");
	}
	if(server_props === undefined){
		$.ajax({
			url: '/dataset/list?f='+dataset_id+'&iDisplayStart=0&iDisplayLength=100&s_fields='+fieldname+'&f_id='+id,
			success: function(result) {
			  obj = JSON.parse(result);
			  if(obj.aaData.length==1){
				  strReturn = obj.aaData[0][fieldname];
			  }
			  else
				  strReturn = "";
			  
			  refvals[key]=strReturn;
			  iteratefields(doc);
			},
			async:true
		});
		throw new UserException("calculating async");
	}
	else{
		server_props.meta_manager.get_table_json({}, dataset_id, function (meta){
			var g_table = new geo_table(server_props.user_id, dataset_id, server_props.meta_manager, meta);
			qparams={f_id:id+'-PropertyIsEqualTo'};
			g_table.documentquery([fieldname],qparams, '', function(error, result){
				//console.log('get fer val result',result);
				if(result.aaData.length==1){
					strReturn=result.aaData[0][fieldname];
					refvals[key]=strReturn;
					iteratefields(doc);
				}
			});
		});
		throw new UserException("calculating async");
		
	}
}

var getdocvalue=function(path){
	//console.log('getdocvalue',path);
		if(sjspath)
			res= sjspath.apply(path, cdoc);
		else
			res= JSPath.apply(path, cdoc);
		//console.log('getdocvalue res=',res);
		if(Array.isArray(res)){
			if(res.length>0){
				if(typeof res[0]=='number')
					return res[0]+'';
				else
					return res[0];
			}
			else
				return '';
		}
		else
			return res;
	}
	
function calculatefield(expression, doc){
	//console.log('calculatefield',expression);
	if(expression=='')
		return '';
	if(expression.charAt(0)=='='){
		//console.log(expression);
		cdoc=doc;		
		if(sparser)
			return sparser.parse(expression.substr(1));
		else
			return parser.parse(expression.substr(1));
	
		
		
	}
	else if(expression.toUpperCase()=='EMPTY' || expression.toUpperCase()=='NULL'){
		return '';
	}
	else{
		return expression;
	}
}

function iteratefields(doc){
	//console.log('iteratefields',doc);
	for (var fieldname in calclist) {
		if (calclist.hasOwnProperty(fieldname)) {
			try{
				//console.log('iteratefields fieldname',fieldname, calclist[fieldname])
				doc[fieldname]=calculatefield(calclist[fieldname], doc);
				//console.log('iteratefields value',fieldname, doc[fieldname])
				delete calclist[fieldname];
			}
			catch(e){
				//calclist[fieldname]=null;
			}
		}
	}
	var cnt=Object.keys(calclist).length;	
	//console.log('iteratefields cnt',cnt);
	if(cnt==0 && readycallback){
		readycallback('', doc);
	}
}
var calculatefields = function (meta, doc, mode, server, ready) {
	//console.log('calculatefields',doc);
	readycallback=ready;
	if(server!=undefined){		
		geo_table = require("../../../controllers/dataset/geo_table");
		sparser = require("./parser");
		sjspath = require("jspath");
		server_props=server;
	}
	if(mode==='copy'){
		for(var i=0; i<meta.columns.length; i++) {
			if(meta.columns[i].copy_value && meta.columns[i].copy_value!=''){
				try{
					doc[meta.columns[i].fieldname]=calculatefield(meta.columns[i].copy_value, doc);
				}
				catch(e){
					calclist[meta.columns[i].fieldname]=meta.columns[i].copy_value;
				}
			}
		}		
	}
	else{
		for(var i=0; i<meta.columns.length; i++) {
			if(meta.columns[i].default_value && meta.columns[i].default_value!=''){
				if(meta.columns[i].fieldname=='published')
					continue;
				if(doc[meta.columns[i].fieldname]==undefined || doc[meta.columns[i].fieldname]=='NULL' || doc[meta.columns[i].fieldname]=='' || (meta.columns[i].widget && meta.columns[i].widget.name=='calculated'))
					try{
						doc[meta.columns[i].fieldname]=calculatefield(meta.columns[i].default_value, doc)
					}
					catch(e){
						console.log('calculatefields catch', meta.columns[i].fieldname)
						calclist[meta.columns[i].fieldname]=meta.columns[i].default_value;
					}
			}
		}
	}
	var cnt=Object.keys(calclist).length;
	if(cnt==0 && ready!==undefined){
		ready('', doc);
	}
	
}

return {
	calculatefields: calculatefields, 
	calculatefield:calculatefield,
	getdocvalue	:getdocvalue,
	getrefval: getrefval	
  }

})();

if(typeof (module)!== 'undefined')
	module.exports = fieldcalc;