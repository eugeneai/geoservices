var symbolyzer_str='\
<div id="layer_symbolyzer" class="row" title="Symbolyzer">\
	<div class="card symbolyzer symbolyzer_list" >	\
		<div class="card-header">PreView</div>\
		<div class="card-body">\
			<h6>Result</h6>\
			<div class="symbolyzer_result"><canvas id="sign_canvas" width="80" height="45" >Обновите браузер</canvas></div>\
			<div>\
				<button type="button" id="add_s_l" class="btn btn-secondary">Add layer</button>\
				<button type="button"  id="minus_s_l" class="btn btn-secondary">Remove layer</button>\
			</div>\
			<h5>Layer list</h5>\
			<ol type="1" id="s_l_list"></ol>\
		</div>\
	</div>\
	<div id="stroke_symbolyzer"  class="card symbolyzer" >\
		<div class="card-header">Stroke</div>\
		<div class="card-body">\
			<form class="form-horizontal">\
				<div class="form-group">\
					<label  for="stroke_type">Type</label>\
					<select id="stroke_type" class="form-control type_style">\
							<option value="none">None</option>\
							<option value="solid">solid</option>\
					</select>				\
				</div>\
				<div class="form-group">\
					<label   for="stroke_width">Width</label>\
					<input type="text" id="stroke_width" class="form-control" value="1"/>\
				</div>\
				<div class="form-group">\
					<label   for="stroke_opacity">Opacity (%)</label>\
					<input type="text" id="stroke_opacity" class="form-control" value="1"/>\
				</div>\
				<div class="form-group">\
					<label  for="stroke_color">Color</label>\
					<input type="color" id="stroke_color" value="#000000" class="form-control style_color" />\
				</div>\
				<div class="use_sign">\
					<div class="form-row">\
						<div class="form-group col-md-6">\
							<label  for="stroke_sign_size">Sign size</label>\
							<input type="text" id="stroke_sign_size" class="form-control"  value="1"/>\
						</div>\
						<div class="form-group col-md-6">\
							<label   for="stroke_sign_angle">Angle</label>\
							<input type="text" id="stroke_sign_angle" class="form-control"  value="0"/>\
						</div>\
					</div>\
					<div class="form-row">\
						<div class="form-group col-md-4">\
							<label   for="stroke_sign_dx">dx</label>\
							<input type="text" id="stroke_sign_dx" class="form-control"  value="1"/>\
						</div>\
						<div class="form-group col-md-4">\
							<label   for="stroke_sign_dy">dy</label>\
							<input type="text" id="stroke_sign_dy" class="form-control"  value="1"/>\
						</div>\
						<div class="form-group col-md-4">\
							<label   for="stroke_gap">Gap</label>\
							<input type="text" id="stroke_gap" class="form-control"  value="1"/>\
						</div>\
					</div>\
				</div>\
			</form>\
		</div>\
	</div>\
	<div id="fill_symbolyzer"  class="card symbolyzer" >\
		<div class="card-header">Fill</div>\
		<div class="card-body">\
			<form class="form-horizontal">\
				<div class="form-group">\
					<label   for="fill_type">Type</label>\
					<select id="fill_type" class="form-control type_style">\
							<option value="none">None</option>\
							<option value="solid">solid</option>\
					</select>\
				</div>\
				<div class="form-row">\
					<div class="form-group col-md-3">\
						<label   for="fill_width">Width</label>\
						<input type="text" id="fill_width" class="form-control" value="1"/>\
					</div>\
					<div class="form-group col-md-6">\
						<label   for="fill_opacity">Opacity (%)</label>\
						<input type="text" id="fill_opacity" class="form-control" value="1"/>\
					</div>\
					<div class="form-group col-md-3">\
						<label   for="fill_color">Color</label>\
						<input type="color" id="fill_color" value="#000000" class="form-control style_color" />\
					</div>\
				</div>\
				<div class="use_sign">\
					<div class="form-row">\
						<div class="form-group col-md-5">\
							<label   for="fill_sign_size">Sign size</label>\
							<input type="text" id="fill_sign_size" class="form-control"  value="1"/>\
						</div>\
						<div class="form-group col-md-3">\
							<label   for="fill_sign_angle">Angle</label>\
							<input type="text" id="fill_sign_angle" class="form-control"  value="0"/>\
						</div>\
					</div>\
					<div class="form-row">\
						<div class="form-group col-md-3">\
							<label   for="fill_sign_dx">dx</label>\
							<input type="text" id="fill_sign_dx" class="form-control"  value="1"/>\
						</div>\
						<div class="form-group col-md-3">\
							<label   for="fill_sign_dy">dy</label>\
							<input type="text" id="fill_sign_dy" class="form-control"  value="1"/>\
						</div>\
						<div class="form-group col-md-3">\
							<label   for="fill_gap">Gap</label>\
							<input type="text" id="fill_gap" class="form-control"  value="1"/>\
						</div>\
					</div>\
					<div class="form-group">\
						<label   for="fill_sign">Fill sign</label>\
						<input type="checkbox" id="fill_sign" class="" />\
					</div>\
				</div>\
			</form>\
		</div>\
	</div>\
	<div id="point_symbolyzer"  class="card symbolyzer" >\
		<div class="card-header">Point</div>\
		<div class="card-body">\
			<form class="form-horizontal">\
				<div class="form-group">\
					<label   for="point_type">Type</label>\
					<select id="point_type" class="form-control type_style">\
					</select>\
				</div>\
				<div id="imagepath" class="form-group">\
					<label   for="image">Path</label>\
					<img id="image_obj" src="">\
						<div id="image"/><button id="imageopen" class="btn" type="button">Open</button>\
				</div>\
				<div id="fontlistdiv" class="form-group fontdiv">\
					<label   for="point_font">Font</label>\
					<select id="point_font" class="form-control">\
						<option value="MapInfoSymbols">MapInfo Symbols</option>\
					</select>\
				</div>\
				<div id="charlistdiv" class="form-group fontdiv">\
					<label   for="point_font_character">Character</label>\
					<select id="point_font_character" class="mapinfo form-control">\
						<option value="\u0044" class="mapinfo">&#x0044</option>\
						<option value="\u0043" class="mapinfo">&#x0043</option>\
						<option value="68" class="mapinfo">&#x0042</option>\
					</select>\
				</div>\
				<div class="form-row">\
					<div class="form-group col-md-6">\
						<label for="point_opacity">Opacity(%)</label>\
						<input type="text" id="point_opacity" class="form-control" value="1"/>\
					</div>\
					<div class="form-group col-md-6">\
						<label   for="point_color">Color</label>\
						<input type="color" id="point_color" value="#000000" class="form-control style_color" />\
					</div>\
				</div>\
				<div class="form-row">\
					<div class="form-group col-md-3">\
						<label   for="point_sign_size">Size</label>\
						<input type="text" id="point_sign_size" class="form-control"  value="1"/>\
					</div>\
					<div class="form-group col-md-3">\
						<label   for="point_sign_angle">Angle</label>\
						<input type="text" id="point_sign_angle" class="form-control"  value="0"/>\
					</div>\
				</div>\
				<div class="form-row">\
					<div class="form-group col-md-3">\
						<label   for="point_sign_dx">dx</label>\
						<input type="text" id="point_sign_dx" class="form-control"  value="1"/>\
					</div>\
					<div class="form-group col-md-3">\
						<label   for="point_sign_dy">dy</label>\
						<input type="text" id="point_sign_dy" class="form-control"  value="1"/>\
					</div>\
					<div class="form-group col-md-3">\
						<label   for="point_gap">Gap</label>\
						<input type="text" id="point_gap" class="form-control"  value="1"/>\
					</div>\
				</div>\
			</form>\
		</div>\
	</div>\
</div>';




var size_unit=0.3;

var map_symbols={};
map_symbols.rect2=[[3,3], [3,42], [77,42],[77,3], [3,3]];
map_symbols.pie=[[4,0], [2,2], [1,5],[2,8], [4,10], [15,5], [4,0]];


var font_symbols=[{
	name:'MapInfoSymbols',
	startsymbol: 34,
	endsymbol: 68
}];

function parse_symbol(txt){
	o=[];
	if(txt===undefined || txt==null || txt=='')
		return o;
	lines=txt.split('\n');
	for(var i=0; i<lines.length; i++){
		pnts=lines[i].split(' ');
		if(pnts.length==2){
			x=parseFloat(pnts[0]);
			y=parseFloat(pnts[1]);
			if(!isNaN(x) && !isNaN(y))
				o.push([x,y])
		}
	}
	return o;
}
$.ajax({
	url: '/dataset/list?f=3&iDisplayStart=0&iDisplayLength=200&iSortingCols=1&iSortCol_0=1&s_fields=id,name,symbol',
	dataType: "json",					
	method: "POST",
	success: function(data) {
		for (var i = 0; i < data.aaData.length; i++) {				
			map_symbols[data.aaData[i].name]=parse_symbol(data.aaData[i].symbol);
		}
	}
});
/*
var map_symbols={};

map_symbols.square=[[10,10], [20,10], [20,20], [10,20]];
map_symbols.triangle=[[0,20], [20,20], [10,0],[0,20]];
map_symbols.star=[[0,0], [5,10], [0,20]];
map_symbols.cross=[[0,5], [10,5], [-10,-10],[5,0],[5,10]];
map_symbols.x=[[0,0], [10,10], [-5,-5],[0,10],[10,0]];
map_symbols.rect=[[0,0], [0,10], [20,10],[20,0],[0,0]];
map_symbols.line=[[3,3], [23,23], [43,3],[63,23]];
map_symbols['shape://slash']=[[0,10], [10,0]];
map_symbols['shape://backslash']=[[0,0], [10,10]];
map_symbols['shape://vertline']=[[5,0], [5,10]];
map_symbols['shape://horline']=[[0,5], [10,5]];
*/

var operators={
	PropertyIsEqualTo:'=',
	PropertyIsNotEqualTo:'!=',
	PropertyIsLessThan:'<',
	PropertyIsLessThanOrEqualTo:'<=',
	PropertyIsGreaterThan:'>',
	PropertyIsGreaterThanOrEqualTo:'>=',
	PropertyIsBetween:'Between',
	gradient:'Gradient'
}


function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function RGB2HTML(red, green, blue)
{
    var decColor =0x1000000+ blue + 0x100 * green + 0x10000 *red ;
    return '#'+decColor.toString(16).substr(1);
}
function getRangeColor(cur, min_value, max_value, from, to) {
	//console.log(from);
	//console.log(from.substr(1,2));
    var r1=parseInt(from.substr(1,2),16);
	var r2=parseInt(to.substr(1,2),16);
    var g1=parseInt(from.substr(3,2),16);
	var g2=parseInt(to.substr(3,2),16);
    var b1=parseInt(from.substr(5,2),16);
	var b2=parseInt(to.substr(5,2),16);	
	var coef=(cur-min_value)/(max_value-min_value);	
	
	//console.log(r1);
	var _r=Math.round(r1 + coef*(r2-r1));	
	var _g=Math.round(g1 + coef*(g2-g1));
	var _b=Math.round(b1 + coef*(b2-b1));
	
	return RGB2HTML(_r,_g,_b);
}


function get_rect(points){
	if(points===undefined  || points.length===undefined || points.length==0) return null;	
	this.left=points[0][0];
	this.right=points[0][0];
	this.top=points[0][1];
	this.bottom=points[0][1];
	for (var i=0; i<points.length; i++){		
		if(points[i][0]<0 || points[i][1]<0)
			continue;
		this.right=Math.max(points[i][0], this.right);
		this.left=Math.min(points[i][0], this.left);
		this.top=Math.max(points[i][1], this.top);
		this.bottom=Math.min(points[i][1], this.bottom);
	}	
	return this;
}

function cloneArray(source) {
	var newObj = (source instanceof Array) ? [] : {};
	for (i in source) {
		if (i == 'clone') continue;
		if (source[i] && typeof source[i] == "object") {
			newObj[i] = cloneArray(source[i]);
		}
		else
			newObj[i] = source[i]
	}
	return newObj;
}

function draw_line(ctx, points, x, y, s , angle) {	
	ctx.beginPath();
	my=2;
	mx=0;
	ctx.moveTo(points[0][0]+ x, points[0][1]+ y);
	for (var i=1; i<points.length; i++){		
		ctx.lineTo(points[i][0] + x, points[i][1]+ y);
	}
	//ctx.closePath();
	ctx.stroke(); // *22
	//ctx.fill();
}

function draw_object(ctx, mpoints, x, y, s , angle) {	
	if(!Array.isArray(mpoints))
		return;
	points=cloneArray(mpoints);
	var rect={};
	var kx=1;
	var ky=1;	
	
	if(s!=-1){
		rect=new get_rect(points);
		if(rect.right - rect.left!=0)
			kx=s/(rect.right - rect.left);
		if(rect.top - rect.bottom!=0)
			ky=s/(rect.top - rect.bottom);
		center={x:(rect.right + rect.left)/2, y:(rect.top + rect.bottom)/2};	
//		mx=x - center.x;
//		my=y - center.y;
		for (var i=0; i<points.length; i++){	
			if(points[i][0]<0 || points[i][1]<0){
				points[i]=true;
				continue;
			}		
			points[i][0]=points[i][0]-center.x;
			points[i][1]=points[i][1]-center.y;
			cx = points[i][0] * Math.cos(angle) - points[i][1] * Math.sin(angle);
			cy = points[i][1] * Math.cos(angle) + points[i][0] * Math.sin(angle);
			//dlen=Math.sqrt(cx*cx+cy*cy);
			cx=cx*kx;
			cy=cy*kx;
			points[i][0]=cx + center.x;//+mx;
			points[i][1]=cy + center.y;//+my;
		}	
		ctx.beginPath();
		//ctx.stroke();
		var moving=false;
		ctx.moveTo(points[0][0]  + x, points[0][1] + y);
		for (var i=1; i<points.length; i++){		
			if(points[i]===true){
				moving=true;
				continue;
			}
			if(moving){
				moving=false;
				ctx.stroke();
				ctx.moveTo(points[i][0] + x, points[i][1] + y);
				//ctx.beginPath();
			}
			else{
				ctx.lineTo(points[i][0] + x, points[i][1] + y);			
			}
		}
		if(points[0][0]==points[points.length-1][0] && points[0][1]==points[points.length-1][1])
			ctx.closePath();
		ctx.stroke(); // *22
		if(points[0][0]==points[points.length-1][0] && points[0][1]==points[points.length-1][1])
			ctx.fill();		
	}
	else{
		rect.left=0;
		rect.right=0;
		rect.top=0;
		rect.bottom=0;		
		ctx.beginPath();
		//ctx.stroke();
		var moving=false;
		ctx.moveTo(points[0][0] + x, points[0][1] + y);
		for (var i=1; i<points.length; i++){		
			if(points[i][0]<0 || points[i][1]<0){
				moving=true;
				continue;
			}
			if(moving){
				moving=false;
				ctx.stroke();
				ctx.moveTo(points[i][0] + x, points[i][1] + y);
				//ctx.beginPath();
			}
			else{
				ctx.lineTo(points[i][0] + x, points[i][1] + y);			
			}
		}
		if(points[0][0]==points[points.length-1][0] && points[0][1]==points[points.length-1][1])
			ctx.closePath();
		ctx.stroke(); // *22
		if(points[0][0]==points[points.length-1][0] && points[0][1]==points[points.length-1][1])
			ctx.fill();		
	}
	
	
}
var scalewidth=2;

function draw_prim(ctx, x,y, sld_object, fieldtype){	
	old_bgcolor=ctx.fillStyle;	
	old=ctx.strokeStyle;
//	x=x+sld_object.dx;
//	y=y+sld_object.dy;
	
	lineWidth=ctx.lineWidth;
	
	var r_l=x+10;
	var r_r=x + ctx.canvas.width-10;
	var r_t=y+10;
	var r_b=y + ctx.canvas.height-10;
	/*
	ctx.fillStyle='#FFFFFF';
	ctx.strokeStyle='#FFFFFF';
	ctx.rect(r_l, r_t, r_r, r_b);
	ctx.fill();
	ctx.stroke();
	*/
	
	var drawtype=sld_object.sign_type;
	if(fieldtype=='point'){
		drawtype=sld_object.point_type;
		ctx.fillStyle=sld_object.point_color;
		ctx.strokeStyle=sld_object.point_color;		
		x=(r_l + r_r)/2;
		y=(r_t + r_b)/2;
		if(drawtype==null)
			drawtype='circle';
		switch (drawtype) {
			case 'circle':
				ctx.beginPath();
				ctx.arc(x + sld_object.point_sign_dx, y + sld_object.point_sign_dy, sld_object.point_sign_size/2,0,2*Math.PI);
				ctx.closePath();				
				ctx.stroke();	
				ctx.fill();
				break;
			case 'font symbol':
				var oldfont=ctx.font;
				ctx.font         = sld_object.point_sign_size + 'px MapInfoSymbols';
				ctx.fillStyle = sld_object.point_color;
				ctx.textBaseline = 'top';
				ctx.fillText(String.fromCharCode(sld_object.point_font_character), x - (sld_object.point_sign_size/2), y - (sld_object.point_sign_size/2));				
				ctx.font=oldfont;
				break;				
			case 'image':
				var imgfileid=sld_object.image;
				console.log(getpath(imgfileid));
				var img=document.getElementById(imgfileid);
				if(img){
					if(img.complete){
						try{
							ctx.drawImage(img, x - sld_object.point_sign_size/2 + sld_object.point_sign_dx , y - sld_object.point_sign_size/2 + sld_object.point_sign_dy, sld_object.point_sign_size, sld_object.point_sign_size);	
						}
						catch (err){
							console.log(err);
						}
						
					}
					else
						img.onload = function(){						
							ctx.drawImage(img, x - sld_object.point_sign_size/2 + sld_object.point_sign_dx , y - sld_object.point_sign_size/2 + sld_object.point_sign_dy, sld_object.point_sign_size, sld_object.point_sign_size);	
						};
					//ctx.drawImage(img, 0,0,sld_object.point_sign_size, sld_object.point_sign_size);	
				}
				else{
					img=$('<img id="'+imgfileid+'">');					
					img.attr("src","/geothemes/get_icon?filename="+imgfileid);
					var imgcont=$( "#imgcont" );
					if(imgcont.length==0){
						imgcont=$( "<div id='imgcont' style='display:None'/>" );
						$( "body" ).append(imgcont);
					}					
					imgcont.append(img);					
					img[0].onload = function(){
						var isym=document.getElementById(imgfileid);
						ctx.drawImage(isym, 0,0,sld_object.point_sign_size, sld_object.point_sign_size);	
					};
					
					
				}
				break;				
			default:
				o=cloneArray(map_symbols[drawtype]);
				if(o)
					draw_object(ctx, o, x + sld_object.point_sign_dx, y + sld_object.point_sign_dy, sld_object.point_sign_size, sld_object.point_sign_angle*Math.PI/180);					
				break;
		}
	}
	else if (fieldtype=='line'){
		ctx.lineWidth=sld_object.stroke_width*scalewidth;
		if(sld_object.stroke_type=='solid' && 'line' in map_symbols){
			ctx.strokeStyle=sld_object.stroke_color;
			points=cloneArray(map_symbols['line']);
			draw_object(ctx, points, 20, 20, 40 , 0)
			/*ctx.beginPath();
			ctx.moveTo(points[0][0] , points[0][1]);
			for (var i=1; i<points.length; i++){		
				ctx.lineTo(points[i][0], points[i][1]);
			}			
			ctx.stroke();*/
		}		
		else{
			ctx.strokeStyle=sld_object.stroke_color;			
			o=cloneArray(map_symbols[sld_object.stroke_type]);
			var signsize=sld_object.size;
			if(sld_object.gap)
				signsize=sld_object.gap;
			
			boundary=cloneArray(map_symbols['rect2']);
			xcnt=Math.floor((r_r - r_l) / signsize)+1;
			ycnt=Math.floor((r_b - r_t) / signsize)+1;				
			var prev_x=boundary[0][0];
			var prev_y=boundary[0][1];
			for(var i=0; i<4; i++){
				c_x=boundary[(i+1)%4][0];
				c_y=boundary[(i+1)%4][1];
				cdx=0;
				cdy=0;
				cnt=1;
				if(c_x==prev_x){
					cdy=sld_object.stroke_gap;
					cnt=Math.abs(Math.trunc((prev_y - c_y)/sld_object.stroke_gap));
					if(prev_y - c_y>0)
						cdy=-cdy;					
				}
				else{
					cdx=sld_object.stroke_gap;
					cnt=Math.abs(Math.trunc((prev_x - c_x)/sld_object.stroke_gap));
					if(prev_x - c_x>0)
						cdx=-cdx;						
				}
				cc=sld_object.stroke_sign_size/2;	
				for(var j=0; j<cnt; j++){
					draw_object(ctx, o, prev_x + cdx*j + sld_object.stroke_sign_dx, prev_y + cdy*j + sld_object.stroke_sign_dy, sld_object.stroke_sign_size, sld_object.stroke_sign_angle*Math.PI/180);
				}					
				prev_x=boundary[(i+1)%4][0];
				prev_y=boundary[(i+1)%4][1];
			}			
		}
		drawtype='line';
	}
	else if (fieldtype=='polygon'){
		ctx.lineWidth=sld_object.stroke_width*scalewidth;
		if(sld_object.stroke_type==undefined)
			sld_object.stroke_type='solid';
		ctx.fillStyle=sld_object.fill_color;
		ctx.strokeStyle=sld_object.fill_color;
		
		if(sld_object.fill_type=='solid'){	
			boundary=cloneArray(map_symbols['rect2']);
			draw_object(ctx, boundary, 0, 0, -1, 0);
		}
		else if(sld_object.fill_type=='none'){	
			;
		}
		else{
			o=cloneArray(map_symbols[sld_object.fill_type]);
			var signsize=sld_object.fill_sign_size;
			if(sld_object.fill_gap)
				signsize=sld_object.fill_gap;
			ctx.lineWidth=sld_object.fill_width/2;
			xcnt=Math.floor((r_r - r_l) / signsize)+1;
			ycnt=Math.floor((r_b - r_t) / signsize)+1;				
			for(var ix=0; ix<xcnt; ix++){
				for(var iy=0; iy<ycnt; iy++){
					draw_object(ctx, o, r_l + ix * signsize + sld_object.fill_sign_dx - 5, r_t + iy * signsize + +sld_object.fill_sign_dy - 2, sld_object.fill_sign_size, sld_object.fill_sign_angle*Math.PI/180);
				}					
			}			
		}
		
		ctx.fillStyle=sld_object.stroke_color;
		ctx.strokeStyle=sld_object.stroke_color;
		
		if('stroke_width' in sld_object && sld_object.stroke_width>0)
			ctx.lineWidth=sld_object.stroke_width*scalewidth;
		
		if(sld_object.stroke_type=='solid'){
			ctx.strokeStyle=sld_object.stroke_color;
			points=cloneArray(map_symbols['rect2']);
			ctx.beginPath();
			ctx.moveTo(points[0][0] , points[0][1]);
			for (var i=1; i<points.length; i++){		
				ctx.lineTo(points[i][0], points[i][1]);
			}			
			ctx.stroke();
		}
		else if(sld_object.stroke_type=='none'){	
			;
		}		
		else{
			ctx.strokeStyle=sld_object.stroke_color;			
			o=cloneArray(map_symbols[sld_object.stroke_type]);
			var signsize=sld_object.size;
			if(sld_object.gap)
				signsize=sld_object.gap;
			
			boundary=cloneArray(map_symbols['rect2']);
			xcnt=Math.floor((r_r - r_l) / signsize)+1;
			ycnt=Math.floor((r_b - r_t) / signsize)+1;				
			var prev_x=boundary[0][0];
			var prev_y=boundary[0][1];
			for(var i=0; i<4; i++){
				c_x=boundary[(i+1)%4][0];
				c_y=boundary[(i+1)%4][1];
				cdx=0;
				cdy=0;
				cnt=1;
				if(c_x==prev_x){
					cdy=sld_object.stroke_gap;
					cnt=Math.abs(Math.trunc((prev_y - c_y)/sld_object.stroke_gap));
					if(prev_y - c_y>0)
						cdy=-cdy;					
				}
				else{
					cdx=sld_object.stroke_gap;
					cnt=Math.abs(Math.trunc((prev_x - c_x)/sld_object.stroke_gap));
					if(prev_x - c_x>0)
						cdx=-cdx;						
				}
				cc=sld_object.stroke_sign_size/2;	
				for(var j=0; j<cnt; j++){
					draw_object(ctx, o, prev_x + cdx*j + sld_object.stroke_sign_dx, prev_y + cdy*j + sld_object.stroke_sign_dy, sld_object.stroke_sign_size, sld_object.stroke_sign_angle*Math.PI/180);
				}					
				prev_x=boundary[(i+1)%4][0];
				prev_y=boundary[(i+1)%4][1];
			}			
		}
	}
	else if (fieldtype=='tiff'){
		ctx.fillStyle=sld_object.fill_color;
		ctx.strokeStyle=sld_object.fill_color;
		boundary=cloneArray(map_symbols['rect2']);
		draw_object(ctx, boundary, 0, 0, -1, 0);
	}
	
	

	ctx.strokeStyle=old;
	ctx.fillStyle=old_bgcolor;
	ctx.lineWidth=lineWidth;
}

function show_symbolizer(symbolizers, geometry_type, callback) {
	var sym_dlg=this;
	var p;
	this.symbolizers=symbolizers;
	this.fieldtype=geometry_type;	
	this.updating=false;
	this.refcnt=0;
	
	p=$(symbolyzer_str);

	if(this.fieldtype=='point'){
		$('#stroke_symbolyzer',p).css('display','none');
		$('#fill_symbolyzer',p).css('display','none');
	}
	else if(this.fieldtype=='line'){
		$('#fill_symbolyzer',p).css('display','none');
		$('#point_symbolyzer',p).css('display','none');
	}
	else if(this.fieldtype=='polygon'){
		$('#point_symbolyzer',p).css('display','none');
	}
	else if(this.fieldtype=='tiff'){
		$('#stroke_symbolyzer',p).css('display','none');
		$('#point_symbolyzer',p).css('display','none');	
	}
	
	
	
	
	var fill_type=$('#fill_type',p);	
	$.ajax({
		url: '/dataset/list?f=3&iDisplayStart=0&iDisplayLength=200&iSortingCols=1&iSortCol_0=1&s_fields=id,name,symbol&f_keywords=polygon',
		dataType: "json",					
		method: "POST",
		success: function(data) {
			for (var i = 0; i < data.aaData.length; i++) {				
				option = $('<option selected value="'+data.aaData[i].name+ '">' + data.aaData[i].name+ '</option>');				
				fill_type.append(option);
			}
			sym_dlg.refcnt++;
			sym_dlg.loadstyle();
		}
	});
	var stroke_type=$('#stroke_type',p);
	$.ajax({
		url: '/dataset/list?f=3&iDisplayStart=0&iDisplayLength=200&iSortingCols=1&iSortCol_0=1&s_fields=id,name,symbol&f_keywords=line',
		dataType: "json",					
		method: "POST",
		success: function(data) {
			for (var i = 0; i < data.aaData.length; i++) {
				option = $('<option selected value="'+data.aaData[i].name+ '">' + data.aaData[i].name+ '</option>');				
				stroke_type.append(option);
			}
			sym_dlg.refcnt++;
			sym_dlg.loadstyle();			
		}
	});
	var point_type=$('#point_type',p);
	$.ajax({
		url: '/dataset/list?f=3&iDisplayStart=0&iDisplayLength=200&iSortingCols=1&iSortCol_0=1&s_fields=id,name,symbol&f_keywords=point',
		dataType: "json",					
		method: "POST",
		success: function(data) {
			for (var i = 0; i < data.aaData.length; i++) {				
				option = $('<option selected value="'+data.aaData[i].name+ '">' + data.aaData[i].name+ '</option>');				
				point_type.append(option);
			}
			sym_dlg.refcnt++;
			sym_dlg.loadstyle();			
		}
	});
	
	
	
	// $(".style_color",p).colorpicker();
	// $(".style_color",p).parent().css('width', '100px');
	$('#imageopen',p).click(function() {
		var options = new Object();
		options.filter = 'gif,png';
		OpenFileDlg(function(path){			
			var imgfileid=gethash(path);
			$('#image',p).val(imgfileid);
			var img=$('#'+imgfileid,p);
			if(img.length==0){
				img=$('<img id="'+imgfileid+'">');
				var imgcont=$( "#imgcont" );
				if(imgcont.length==0){
					imgcont=$( "<div id='imgcont' style='display:None'/>" );
					$( "body" ).append(imgcont);
				}					
				imgcont.append(img);
				
			}
			img.attr("src",filelink(path));
			if(sym_dlg.updating)
				return;
			setTimeout(function(){
				sym_dlg.update_layer();
			}, 1000);
		},options);
	});
	
	$('.type_style', p).change(function(){
		//if(sym_dlg.updating)
			//return;
		sel= $(this);
		if(sel.val()=='solid' || sel.val()=='none')
			$('.use_sign',sel.parents('.symbolyzer')).css('display','none');
		else
			$('.use_sign',sel.parents('.symbolyzer')).css('display','block');
		if(sel.attr('id')=='point_type'){
			if(sel.val()=='image')
				$('#imagepath').show();
			else
				$('#imagepath').hide();
			if(sel.val()=='font symbol')
				$('.fontdiv').show();
			else
				$('.fontdiv').hide();	
		}
	});
	$(".style_color",p).on("change.color", function(event, color){
		if(sym_dlg.updating)
			return;		
		sym_dlg.update_layer();
		//setTimeout(function() { sym_dlg.update_layer(); }, 100);	
	});
	
	$("#l_type",p).change(function(){
		if(sym_dlg.updating)
			return;		
		sym_dlg.update_layer();
	});
	
	$("input",p).keyup(function(eventObject){
		if(sym_dlg.updating)
			return;
		sym_dlg.update_layer();
	});	

	$("select",p).change(function(event, color){
		if(sym_dlg.updating)
			return;		
		sym_dlg.update_layer();
		//setTimeout(function() { sym_dlg.update_layer(); }, 100);
	});	
	
	$('#add_s_l',p).click(function(){//creating new symbolyser layer.
		//sign_layer={ sign_type:'square',	size:10, stroke_width:1, stroke_color:'000000',	bgcolor:'000000',	dx:0,	dy:0, angle:0, gap:0};
		sign_layer=cloneArray(def_layer);
		sym_dlg.add_layer(sign_layer );
		sym_dlg.set_current_symbolyser_layer(sign_layer);
		sym_dlg.update_rule();
	});   	
	

	$('#minus_s_l',p).click(function(){
		l_div=$('.g_layer_active',p);
		if(l_div.length==0) return;
		li=l_div.parent();
		li.remove();
		sym_dlg.update_rule();
		sym_dlg.Draw_All_layers();		
		
		$('.sign_div:last',p ).addClass('g_layer_active');
		sign_l=$('.sign_div:last',p ).parent().data("sign_layer");
		sym_dlg.set_current_symbolyser_layer(sign_l);		
	});
	
	var point_font=$("#point_font", p);	
	point_font.empty();	
	for(var i=0; i<font_symbols.length; i++){
		option = $('<option selected value="'+font_symbols[i].name+ '">' + font_symbols[i].name+ '</option>');				
		point_font.append(option);
	}
	var point_font_character=$('#point_font_character',p);
	point_font_character.empty();
	for (var i = font_symbols[0].startsymbol; i <=font_symbols[0].endsymbol; i++) {				
		option = $('<option value="'+i+ '">' + String.fromCharCode(i)+ '</option>');				
		point_font_character.append(option);
	}
	
	p.dialog({
	      resizable: false,
	      height:'auto',
	      width: 'auto',
	      modal: false,
	      buttons: {
			"Save": function() {
				sym_dlg.update_layer();
				$( this ).dialog( "close" );
				var layers=new Array();
				$('.sign_layer',p).each(function(){
					sign_l=$(this).data("sign_layer");
					if(sign_l!=undefined)
						layers.unshift(sign_l);	
				});
				
				callback(layers);
			  
				p.remove();
			},
			Cancel: function() {
			  $( this ).dialog( "close" );
			  p.remove();
			}
	      }
	});
	this.update_rule=function(){
		this.symbolizers=new Array();
		$('.sign_layer',p).each(function(){
			sign_l=$(this).data("sign_layer");
			if(sign_l!=undefined)
				sym_dlg.symbolizers.unshift(sign_l);	
		});		
	};
	this.add_layer=function (sign_layer){
		li=$('<li class="sign_layer"><div class="sign_div"><canvas id="l_canvas" width="80" height="45" class="canvas_style">Обновите браузер</canvas></div></li>');		
		li.click(function(){//making curent symbolizer layer
			sign_l=$(this).data("sign_layer");
			sym_dlg.set_current_symbolyser_layer(sign_l);
			parent=$(this).parent();
			$( '.sign_div', parent).removeClass('g_layer_active');
			$('.sign_div',this).addClass('g_layer_active');			
		});
		var list=$('#s_l_list',p);
		list.prepend(li);	
		//li.append(d);
		li.data("sign_layer", sign_layer);

		items=$('.sign_layer',list);
		
		if(items.length>1){
			list.sortable({
				stop: function () {	
					sym_dlg.symbolizers=new Array();
					$('.sign_layer',p).each(function(){
						sign_l=$(this).data("sign_layer");
						if(sign_l!=undefined)
							sym_dlg.symbolizers.unshift(sign_l);	
					});					
					sym_dlg.Draw_All_layers();
				},
				revert: true
			});
		}
		else if (items.length>2)
			list.sortable( "refresh" );	
		$( '.sign_div', $('#s_l_list')).removeClass('g_layer_active');
		$('.sign_div',li ).addClass('g_layer_active');
		
		//setTimeout(function(){
			sym_dlg.set_current_symbolyser_layer(sign_layer);
			
			sctx=$('#l_canvas',li);
			ctx=sctx[0].getContext('2d');
			w=sctx[0].width;
			h=sctx[0].height;
			ctx.fillStyle = "white";
			ctx.fillRect(0, 0, w, h);		
			draw_prim(ctx, 0, 0, sign_layer,this.fieldtype);
			sym_dlg.Draw_All_layers();
			
		//}, 300);
		
	}
	
	
	this.getVal=function (input_id,default_value, v_type){
		stroke_width=$('#'+input_id,p);
		if(stroke_width.length>0){
			v=stroke_width.val();
			if(v_type=='f'){
				v=v.replace(/,/g, '.');
				v=parseFloat(v);
				if(v!=NaN)
					return v;
			}
			else if(v_type=='i'){
				v=parseInt(v);
				if(v!=NaN)
					return v;			
			}
			else{
				return v;
			}			
		}			
		return default_value;	
	}	
		

	
	this.update_layer=function(){
		var sign_layer={};
		for (var key in def_layer) {
			v=$("#"+key, p).val();
			if(v!=''){
				if(typeof def_layer[key] === 'number')
					sign_layer[key]=parseFloat(v);
				else
					sign_layer[key]=v;
			}
			else{
				sign_layer[key]=def_layer[key];
			}
		}
		sign_layer.fill_sign=$("#fill_sign", p).is(":checked");
		/*
		var point_font=$("#point_font", p);	
		point_font.empty();
		var font_number=0;
		for(var i=0; i<font_symbols.length; i++){
			option = $('<option selected value="'+font_symbols[i].name+ '">' + font_symbols[i].name+ '</option>');				
			point_font.append(option);	
			if(font_symbols[i].name==sign_layer.point_font)
				font_number=i;
		}
		var point_font_character=$('#point_font_character',p);
		point_font_character.empty();
		for (var i = font_symbols[font_number].startsymbol; i <=font_symbols[font_number].endsymbol; i++) {				
			option = $('<option selected value="'+String.fromCharCode(i)+ '">&#x' + i.toString(16)+ '</option>');				
			point_font_character.append(option);
		}*/

		if(sign_layer.stroke_width==0)
			sign_layer.stroke_width=1;
		if(sign_layer.size==0)
			sign_layer.size=1;		
		l_div=$('.g_layer_active',p);		
		if(l_div.length==0) return;
		li=l_div.parent();
		li.data("sign_layer", sign_layer);
		sctx=$('#l_canvas',li);
		ctx=sctx[0].getContext('2d');
		w=sctx[0].width;
		h=sctx[0].height;
		ctx.fillStyle = "white";
		ctx.fillRect(0, 0, w, h);		
		draw_prim(ctx, 0, 0, sign_layer, this.fieldtype);
		sym_dlg.update_rule();
		sym_dlg.Draw_All_layers();
	}
	this.Draw_All_layers=function(){
		var sctx=$('#sign_canvas',p);
		ctx=sctx[0].getContext('2d');	
		ctx.clearRect(0, 0, sctx[0].width, sctx[0].height);
		this.Draw(sctx, 0, 0, false);
	}
	this.set_current_symbolyser_layer= function(sign_layer){
		if(sign_layer){
			this.updating=true;
			for (var key in def_layer) {		
				if(key in sign_layer){				
					// if(key.indexOf('_color')!=-1)
					// 	$("#"+key, p).colorpicker("val", sign_layer[key] );
					// else				
						$("#"+key, p).val(sign_layer[key]);
				}
				else{
					// if(key.indexOf('_color')!=-1)
					// 	$("#"+key, p).colorpicker("val", def_layer[key] );
					// else				
						$("#"+key, p).val(def_layer[key]);
					
				}
			}
			$('.type_style', p).trigger('change');
			this.updating=false;
		}
	}	
	
	this.loadstyle=function(){
		if(this.refcnt<3)
			return;
		if(this.symbolizers.length>0){
			for(var j=0; j<this.symbolizers.length; j++){
				update_layer(this.symbolizers[j]);
				sym_dlg.add_layer(this.symbolizers[j]);
			}
		}
		else{
			sym_dlg.add_layer(cloneArray(def_layer));

		}
	}
	
	this.Draw=function(rule_canvas){
		if(!rule_canvas){
			rule_canvas=$('#rule_canvas', this.form_div)
		}
		cnvs=rule_canvas[0];
		ctx=cnvs.getContext('2d');
		ctx.stroke();
		ctx.clearRect(0, 0, cnvs.width, cnvs.height);
		for(var i=0; i<this.symbolizers.length; i++){
			update_layer(this.symbolizers[i])
			draw_prim(ctx, 0, 0, this.symbolizers[i], this.fieldtype);
		}
	}

}


function hex2rgb(hex) { // 16-тиричный в rgb
	try{
		hex=hex.trim();
		if(!hex || hex=='')
			return '0 0 0';
		if (hex[0]=="#") hex=hex.substr(1);
		if (hex.length<7){
		if (hex.length==1)hex='00000'+hex;else
		if (hex.length==2)hex='0000'+hex;else
		if (hex.length==4)hex='00'+hex;else
		if (hex.length==5)hex='0'+hex;else
		if (hex.length==3) {
		var temp=hex; hex='';
		temp = /^([a-f0-9])([a-f0-9])([a-f0-9])$/i.exec(temp).slice(1);
		for (var i=0;i<3;i++) hex+=temp[i]+temp[i];
		}
		var triplets = /^([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})$/i.exec(hex).slice(1);
		return ' '+parseInt(triplets[0],16)+' '+parseInt(triplets[1],16)+' '+parseInt(triplets[2],16);
		}
	}
	catch{
		return '0 0 0';
	}
	
}