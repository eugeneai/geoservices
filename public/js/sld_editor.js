
//class="style_options" 
//<div class="sld_rule_head"><div id="comp_sign_head" class="sign_container"> <canvas id="rule_sign_head" width="80" height="45">Обновите браузер</canvas></div><div class="rule_val">Value</div><div class="rule_val">Mark</div> //</div>
ss='<div id="fontproperties" class="span3 panel symbolyzer">\
			<div class="panel-heading">Label</div>\
			<div class="panel-body">\
				<form class="form-horizontal">\
					<div class="form-group">\
						<label class="control-label" for="labelfieldname">Label</label>\
						<select class="type_style" id="labelfieldname"></select>\
					</div>\
					<div class="form-group">\
						<label class="control-label" for="fontsize">Font size</label>\
						<input id="fontsize" type="text" class="input-small" value="12"/>\
					</div>\
					<div class="form-group">\
						<label class="control-label" for="label_color">Color</label>\
						<input type="text" id="label_color" value="#000000" class="style_color" />\
					</div>\
						<div class="form-group">\
							<label  class="control-label" for="label_dx">dx</label>\
							<input type="text" id="label_dx" class="input-small"  value="0"/>\
						</div>\
						<div class="form-group">\
							<label  class="control-label" for="label_dy">dy</label>\
							<input type="text" id="label_dy" class="input-small"  value="0"/>\
						</div>\
				</form>\
			</div>\
		</div>';
		
var classify_form_str='<div id="classify_form" style="" title="Classify form">\
	<p><select id="classify_type">\
	<option value="random">Random color</option>\
	<option value="range">Range color</option>    \
	</select></p>\
	Color range: <input class="form-control"  id="from_color" type="color" /> -	<input class="form-control" id="to_color" type="color"/>\
	<input class="form-control" id="class_cnt" />\
</div>';

var field_class_str='\
<div class="card class_item">\
	<div class="card-header">\
		Class\
		<a id="cls_delete" href="#" class="" style="float: right;" role="button"><span class="ui-icon ui-icon-closethick">close</span></a>\
	</div>\
	<div class="form-row align-items-center">\
		<div class="form-group">\
			<canvas id="class_canvas" width="80" height="38" >Обновите браузер</canvas>\
		</div>\
		<div class="form-group col-md-1">\
			<label class="control-label" for="lows">Low</label>\
			<input class="form-control" type="text" id="low"/>\
		</div>\
		<div class="form-group col-md-1">\
			<label class="control-label" for="lows">Up</label>\
			<input class="form-control" type="text" id="up"/>\
		</div>\
		<div class="form-group col-md-1">\
			<label class="control-label" for="val">Value</label>\
			<input class="form-control class_value" type="text" id="val"/>\
		</div>\
		<div class="form-group col-md-2">\
			<label class="control-label" for="font_size">Font size</label>\
			<input class="form-control" type="text" id="font_size"/>\
		</div>\
		<div class="form-group col-md-2">\
			<label class="control-label" for="font_size">Font color</label>\
			<input class="form-control style_color" type="color" id="font_color"/>\
		</div>\
		<div class="form-group col-md-2">\
			<label class="control-label" for="title">Title</label>\
			<input class="form-control class_value" id="title"></input>\
		</div>\
		<div class="form-group col-md-2">\
			<button id="italic" class="btn btn-small" type="button"><i class="fas fa-italic"></i></button>\
			<button id="bold" class="btn btn-small" type="button"><i class="fas fa-bold"></i></button>\
		</div>\
	</div>\
</div>\
';

function fieldclass(){
	this.form_div=$(field_class_str);
	var fcls=this;
	this.low=0;
	this.up=0;
	this.val='';
	this.title='';
	//this.labelfieldname='';
	this.font_size=7;
	this.font_color='#000000';
	this.font_bold=false;
	this.font_italic=false;	
	this.classify_type='color';	
	this.geometry_type='point';
	this.Draw=function(class_canvas){
		if(!class_canvas){
			class_canvas=$('#class_canvas', this.form_div)
		}
		cnvs=class_canvas[0];
		ctx=cnvs.getContext('2d');			
		ctx.stroke();
		ctx.clearRect(0, 0, cnvs.width, cnvs.height);
		if(Array.isArray(this.sym)){
			for(var i=0; i<this.sym.length; i++){
				draw_prim(ctx, 0, 0, this.sym[i], this.geometry_type);	
			}
		}
		else
			draw_prim(ctx, 0, 0, this.sym, this.geometry_type);	
	}
	
	this.changeform=function(){
		for (var key in this) {
			if(typeof this[key] ==='number' || typeof this[key] ==='string'){
				v=$("#"+key, this.form_div).val();
				if(v)
					this[key]=v;
			}
		}
		this.font_bold=$('#bold',this.form_div).hasClass('active');
		this.font_italic=$('#italic',this.form_div).hasClass('active');
		
		this.changemodel();
		if(!this.sym)
			this.sym=cloneArray(m_style.base_rule.symbolizers);		
	}
	var changingmodel=false;
	this.changemodel=function(){
		changingmodel=true;
		for (var key in this) {
			if(typeof this[key] ==='number' || typeof this[key] ==='string'){
				var inp=$('#'+key,this.form_div);
				// if(inp.hasClass( "style_color" ))
				// 	inp.colorpicker("val", this[key]);
				// else
					inp.val(this[key]);
			}
		}
		
		$('#bold, #italic', this.form_div).unbind();
		$('#bold, #italic', this.form_div).click(function(){
			if($(this).hasClass('active'))
				$(this).removeClass('active');
			else
				$(this).addClass('active');				
			f.changeform();
		});
		//$(".class_value", this.form_div).colorpicker("disable");
		var class_value_div=$('#class_value_div', this.form_div);
		var input=$('<input class="input-small class_value" type="text" id="val"/>');
		class_value_div.empty();
		class_value_div.append(input);
		input.val(this.val);
		// if(this.classify_type=='color'){
		// 	input.colorpicker().on("change.color", function(event, color){
		// 		if(!changingmodel)
		// 			fcls.changeform();
		// 	});		
		// }
		
		
		var font_color_div=$('#font_color_div', this.form_div);
		var font_color=$('<input class="input-small style_color" type="text" id="font_color"/>');
		font_color_div.empty();
		font_color_div.append(font_color);
		font_color.val(this.font_color);
		
		// font_color.colorpicker().on("change.color", function(event, color){
		// 	if(!changingmodel)
		// 		fcls.changeform();
		// });		
		
		
		$('input',this.form_div).change(function(){
			if(!changingmodel)
				fcls.changeform();
		});
		$('textarea',this.form_div).change(function(){
			if(!changingmodel)
				fcls.changeform();
		});		
		$('select',this.form_div).change(function(){
			if(!changingmodel)
				fcls.changeform();
		});
		if(!Array.isArray(this.sym))
			this.sym=[this.sym];
		/*
		for(var i=0; i< this.sym.length; i++){
			switch(this.classify_type) {
				case 'color':  // if (x === 'value1')
					if(this.geometry_type=='polygon')
						this.sym[i].fill_color=this.val;
					else if(this.geometry_type=='point')
						this.sym[i].point_color=this.val;	
					else
						this.sym[i].stroke_color=this.val;												
				break;
				case 'width':  // if (x === 'value2')
					this.sym[i].stroke_width=this.val;
					this.sym[i].fill_width=this.val;											
				break;
				case 'size':  // if (x === 'value2')
					this.sym[i].fill_sign_size=this.val;
					this.sym[i].point_sign_size=this.val;											
				break;			
			}
		}*/
		$('#cls_delete',this.form_div).unbind();
		$('#cls_delete',this.form_div).click(function(){
			var ind=fcls.parent.classes.indexOf(fcls);
			fcls.parent.classes.splice(ind,1);
			fcls.form_div.detach();
		});
		
		if(this.classify_type=="color" && this.geometry_type=="polygon"){
			for(var i=0; i<this.sym.length; i++){
				this.sym[i].fill_color=this.val;
			}
			
		}
		
		this.Draw();
		class_canvas=$('#class_canvas', this.form_div);
		class_canvas.unbind();
		class_canvas.dblclick(function(){			
			s=new show_symbolizer(fcls.sym, fcls.geometry_type, function (layers){		
				fcls.sym=layers;
				//frm.g_sld.rules[0].symbolizers=layers;
				fcls.changemodel();
			});
		});
		changingmodel=false;
	}	
	
	// $(".style_color", this.form_div).colorpicker();

	return this;
}

var field_classify_form='\
	<div class="card field_classify">	\
		<div class="card-header" >Field classify<a id="field_delete" href="#" class="" style="float: right;" role="button"><span class="ui-icon ui-icon-closethick">close</span></a></div>\
		<div class="card-body">\
			<div class="form-group">\
				<label class="control-label" for="title">Title</label>\
				<input type="text" class="form-control" id="title" />\
			</div>\
			<div class="form-group">\
				<label class="control-label" for="fieldname">Fieldname</label>\
				<select id="fieldname" class="form-control"></select>\
			</div>\
			<div class="form-group">\
				<label class="control-label" for="classify_type">Change type</label>\
				<select id="classify_type" class="form-control"><option value="color">Color</option><option value="size">Size</option><option value="width">Width</option></select>\
			</div>\
			<div class="form-group class_type color_type">\
				<label class="control-label" for="fieldname">Way</label>\
				<select id="color_way_type" class="form-control">\
					<option value="random">Random color</option>\
					<option value="range">Range color</option>\
				</select>\
			</div>\
			<div class="form-row class_type color_type" id="color_range">\
				<label class="control-label" for="from_color">Color range</label>\
				<div class="col">\
					<input class="form-control style_color" id="from_color" type="color"/>\
				</div>\
				<div class="col-auto"> :\
				</div>\
				<div class="col">\
					<input class="form-control style_color" id="to_color"  type="color"/>\
				</div>\
			</div>\
			<div class="form-row class_type">\
				<label class="control-label" for="classify_type">Size range</label>\
				<div class="col">\
					<input class="form-control" id="from_size" />\
				</div>\
				<div class="col-auto">:</div>\
				<div class="col">\
					<input class="form-control" id="to_size" />\
				</div>\
			</div>\
			<div class="form-group class_type">\
				<label class="control-label" for="size_way_type">Order</label>\
				<select id="size_way_type" class="form-control">\
					<option value="increase">Increase order</option>\
					<option value="decrease">Decrease order</option>\
				</select>\
			</div>\
			<div class="form-row width_type class_type">\
				<label class="control-label" for="from_width">Width range</label>\
				<div class="col-auto">\
					<input class="form-control" id="from_width" />\
				</div>\
				<div class="col-auto"> :\
				</div>\
				<div class="col-auto">\
					<input class="form-control" id="to_width" />\
				</div>\
			</div>\
			<div class="form-group width_type class_type">\
				<label class="control-label" for="width_way_type">Order</label>\
				<select id="width_way_type" class="form-control" >\
					<option value="increase">Increase order</option>\
					<option value="decrease">Decrease order</option>\
				</select>\
			</div>\
			<div class="form-group">\
				<label class="control-label" for="class_count">Class count</label>\
				<input id="class_count" class="form-control" /><a class="btn btn-small" id="createclasses"  href="#"><i class="icon-wrench"></i>Load classes</a>\
				<a class="btn btn-small" id="loadunique"  href="#"><i class="icon-wrench"></i>Unique values</a>\
			</div>\
			<div id="classes">\
			</div>\
		</div>\
	</div>';

function field_classify(layer_json){
	this.form_div=$(field_classify_form);
	var f=this;
	
	this.fieldname='';
	this.fieldtype='';
	this.title='';
	this.classify_type='color';	
	this.classes=[];
	this.from_color='#9bbb59';
	this.to_color='#ff0000';
	this.color_way_type='range';
	this.size_way_type='range';
	this.width_way_type='range';
	this.from_size=5;
	this.to_size=20;
	this.from_width=1;
	this.to_width=5;
	this.class_count=5;
	this.geometry_type='point';
	this.min_value = 0;
	this.max_value = 0;
	
	this.changeform=function(){
		/*
		for (var key in this) {
			if(typeof this[key] ==='number' || typeof this[key] ==='string'){
				v=$("#"+key, this.form_div).val();
				if(v)
					this[key]=v;
			}
		}*/
		
		for (var key in this) {
			if(typeof this[key] ==='number'){
				v=$("#"+key, this.form_div).val();
				if(v)
					this[key]=v;
			}
			else if(typeof this[key] ==='string'){
				v=$("#"+key, this.form_div).val();
				if(v || key=='title')
					this[key]=v;
			
			}			
		}
		this.changemodel();
	}
	
	this.changemodel=function(){
		$('#fieldname',this.form_div).empty();
		
		for(var ii=0; ii<layer_json.columns.length; ii++){
			var wtype = layer_json.columns[ii].type;
			if(layer_json.columns[ii].fieldname===undefined || layer_json.columns[ii].title===undefined)
				continue;
			if(wtype==='string' ||  wtype==='number' || wtype==='date' || wtype==='serial' || wtype==='region'  || wtype==='integer'){
				opt=$('<option value="'+layer_json.columns[ii].fieldname+'">'+layer_json.columns[ii].title+'</option>');					
				$('#fieldname',this.form_div).append(opt);
			}
			if(layer_json.columns[ii].fieldname==this.fieldname)
				this.fieldtype=layer_json.columns[ii].type;
		}
		color_range	= $("#color_range_div", this.form_div);
		color_range.empty();
		color_range.append($('<input class="form-control style_color" id="from_color"  type="color"/> -	<input class="form-control style_color" id="to_color"  type="color"/>'));
		// $(".style_color", color_range).colorpicker();
		
		for (var key in this) {
			if(typeof this[key] ==='number' || typeof this[key] ==='string'){
				var inp=$('#'+key,this.form_div);
				// if(inp.hasClass( "style_color" ))
				// 	inp.colorpicker("val", this[key]);
				// else
					inp.val(this[key]);
			}
		}
		
		//$(".style_color", color_range).change();		
		
		$('#field_delete', this.form_div).unbind();
		$('#field_delete', this.form_div).click(function(){
			var ind=f.parent.flds.indexOf(f);
			f.parent.flds.splice(ind,1);
			f.form_div.detach();
		});
		
		$('.class_type',this.form_div).hide();
		if(this.classify_type=='color'){
			$('.color_type',this.form_div).show();
		}
		else if (this.classify_type=='size'){
			$('.size_type',this.form_div).show();
		}
		else{
			$('.width_type',this.form_div).show();
		}
		$('input',this.form_div).unbind();
		$('select',this.form_div).unbind();

		$('input',this.form_div).change(function(){
			f.changeform();
		});
		$('select',this.form_div).change(function(){
			f.changeform();
		});
		$('#createclasses',this.form_div).unbind();
		$('#loadunique',this.form_div).unbind();
		
		$('#createclasses',this.form_div).click(function(){
			f.loadclasses('interval');		
		});
		$('#loadunique',this.form_div).click(function(){
			f.loadclasses('unique');
		});
		
		var cont=$('#classes',this.form_div);
		while(this.classes.length<this.class_count){
			var cls=new fieldclass();			
			this.classes.push(cls);
		}
		if(this.classes.length>this.class_count)
			this.classes.length=this.class_count;
		cont.empty();
		for(var i=0; i<this.classes.length; i++){
			cont.append(this.classes[i].form_div);
			this.classes[i].geometry_type=this.geometry_type;
			if(!this.classes[i].sym)
				this.classes[i].sym=cloneArray(this.sym);
			this.classes[i].classify_type=this.classify_type;
			this.classes[i].parent=this;
			this.classes[i].changemodel();
		}
	}
	
	this.loadclasses = function (mode){
		this.changeform();
		var isnum=true;
		function updateclasses(){
			f.from_width=parseFloat(f.from_width);
			f.to_width=parseFloat(f.to_width);
			f.from_size=parseFloat(f.from_size);
			f.to_size=parseFloat(f.to_size);
			var dsize=1;
			if(f.classes.length>1){
				dsize=((f.to_size - f.from_size)/(f.classes.length-1));						
			}
			var dwidth=1;
			if(f.classes.length>1){
				dwidth=((f.to_width - f.from_width)/(f.classes.length-1));						
			}
			
			
			for (var i=0; i<f.classes.length; i++) {
				f.classes[i].sym=cloneArray(f.parent.base_rule.symbolizers);
				if(!Array.isArray(f.classes[i].sym))
					f.classes[i].sym=[f.classes[i].sym];
				if(f.classify_type=='color'){
					if(f.color_way_type=='range'){
						if(is_number(f.classes[i].low) && is_number(f.classes[i].up) /*is_number(items[items.length-1]) && is_number(items[0])*/)
							f.classes[i].val=getRangeColor(f.classes[i].low, f.min_value, f.max_value, f.from_color, f.to_color);
						else
							f.classes[i].val=getRangeColor(i, 0, f.classes.length-1, f.from_color, f.to_color);
					}
					else
						f.classes[i].val=getRandomColor();
					for(var jj=0; jj<f.classes[i].sym.length; jj++){
						f.classes[i].sym[jj].fill_color=f.classes[i].val;
						f.classes[i].sym[jj].point_color=f.classes[i].val;
					}
				}
				else if(f.classify_type=='size'){
					if(f.size_way_type=='increase' )
						f.classes[i].val=f.from_size + i*dsize;
					else
						f.classes[i].val=f.to_size - i*dsize;
					for(var jj=0; jj<f.classes[i].sym.length; jj++){
						f.classes[i].sym[jj].point_sign_size=f.classes[i].val;
					}			
				}
				else if(f.classify_type=='width'){
					if(f.width_way_type=='increase' )
						f.classes[i].val=f.from_width + i*dwidth;
					else
						f.classes[i].val=f.to_width - i*dwidth;
					for(var jj=0; jj<f.classes[i].sym.length; jj++){
						f.classes[i].sym[jj].stroke_width=f.classes[i].val;
					}
				}
				if(f.classes[i].mode=='interval'){
					if($.isNumeric(f.classes[i].low)){
						if(f.classes[i].low ==f.classes[i].up)
							f.classes[i].title=f.classes[i].low.toFixed(3);
						else
							f.classes[i].title=f.classes[i].low.toFixed(3)  + ' - ' + f.classes[i].up.toFixed(3);
					}
					else{
						f.classes[i].title=f.classes[i].low;
					}
				}
				//f.classes[i].sym=cloneArray(f.parent.base_rule.symbolizers);
			}
			f.changemodel();
		}
		if(this.geometry_type=='tiff'){
			if(layer_json.columns.length>0){
				minval=layer_json.columns[0].min;
				maxval=layer_json.columns[0].max;
			}
			else{
				return;
			}
			var d=1;
			
			if(is_number(minval) && is_number(maxval)){
				d = (maxval - minval)/f.classes.length;								
			}
			else{
				isnum=false;
			}
			
			for (var i = 0; i < f.classes.length; i++) {
				if(isnum){
					f.classes[i].low=minval + d*i;
					f.classes[i].up=minval + d*i +d;
				}
			}
			updateclasses();
		}
		else{
			$.getJSON("/dataset/list?f="+layer_json.metaid+"&iDisplayStart=0&iDisplayLength=10000&s_fields="+this.fieldname+"&distinct=true",{ajax: 'true'}, function(data){
				var items = [];
				var fieldisobject=false;
				
				for (var i = 0; i < data.aaData.length; i++) {
					if(typeof data.aaData[i][f.fieldname] ==='object'){
						fieldisobject=true;						
					}	
					items.push(data.aaData[i][f.fieldname]);
				}
				if(fieldisobject){
					items.sort(function (a, b){
						var aName = a.id;//.toLowerCase();
						var bName = b.id;//.toLowerCase(); 
						return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
					});
					f.min_value = 0;
					f.max_value =items.length-1;										
				}
				else{
					items.sort(function (a, b){
						var aName = a;//.toLowerCase();
						var bName = b;//.toLowerCase(); 
						return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
					});
	
					if(is_number(items[0])){
						f.min_value = items[0];
						f.max_value = items[items.length-1];
					}
				}
				

				
				if(mode=='interval'){
					if(items.length>0 && f.classes.length>0){
						minval=items[0];
						maxval=items[items.length-1];
						var d=1;
						
						if(is_number(minval) && is_number(maxval)){
							d = (maxval - minval)/f.classes.length;								
						}
						else{
							isnum=false;
						}
						
						for (var i = 0; i < f.classes.length; i++) {
							if(isnum){
								f.classes[i].low=minval + d*i;
								f.classes[i].up=minval + d*i +d;
							}
						}						
					}
				}
				else{
					if(items.length>0 && f.classes.length>0){
						f.class_count=items.length;					
						f.classes.length=0;
						for (var i = 0; i < items.length; i++) {
							var cls=new fieldclass();
							cls.mode=mode;
							if(fieldisobject){
								cls.low=items[i].id;
								cls.up=items[i].id;
								if('name' in items[i])
									cls.title = items[i].name;
								else
									cls.title = items[i].id;
							}
							else{
								cls.low=items[i];
								cls.up=items[i];
								cls.title = items[i];
							}							
							f.classes.push(cls);
						}						
					}						
				}
				updateclasses();
				
			});
		}	
	}
	
	return this;
}




function layer_sld_form(map_style_sourse, layer_json, callback){
	var m_style=new map_style();
	m_style.layer_json=layer_json;
	m_style.readFromJSON(map_style_sourse.save2JSON());
	var frm=this;
	var widget;
	
	var p=m_style.form_div;
	this.p=p;
	$( "body" ).append(p);	
	
	var options = new Object();
	options.filter='style';
	p.dialog({
	      resizable: false,
	      height:'auto',
	      width:'1200px',
	      modal: false,
		  position: { my: "center top", at: "center top", of: window },
	      buttons: {
			"Save": function() {
				if(m_style.style_type=='wizard'){
					m_style.rules=[];
					m_style.generaterules(0,[]);
				}
				callback( m_style);
				$( this ).dialog( "close" );
				p.remove();
			},
			Cancel: function() {
			  $( this ).dialog( "close" );
			   p.remove();
			},
			"Save to file": function() {
				m_style.rules=[];
				m_style.generaterules(0,[]);				
				SaveFileDlg(function(path){
					saveFile(path, m_style.save2JSON());
				}, options);
			},
			"Load from file": function() {
				OpenFileDlg(function(path){
					readFile(path, function(data){
						// m_style=new map_style();
						m_style.readFromJSON(data);
						m_style.changemodel();
					});
				}, options);
			},
		}
	});	
	

	m_style.changemodel();	
};