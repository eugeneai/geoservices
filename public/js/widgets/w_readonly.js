﻿function w_readonly(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'readonly';
	// Additional properties array
	this.form_element = '';
	
	this.user_visible = true;
	this.rname = 'readonly';
	this.fieldtype = 'string';

	
    this.assign = function(form_element, value){
		this.value=value;
		return value;
    }
    
	// Getting value of input field
    this.getVal = function (){
		return this.value;
    }
    
	// Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		return val;
	    // return $('#'+this.object.fieldname).val();
    }

	// Additional properties form
    this.viewPropForm = function (container){
		//var sz=$("<label>Size of field</label><input type='text' name='w_edit_size' id='w_edit_size' placeholder='20'/>");
		//container.append(sz);		
    }
    this.getPropForm = function (container){
		return {};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'autoedit';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		return tempobj;
	}

	this.hasFilter = function (){
		return false;
	}	

    return this;
}

extend(w_readonly, w_base);
$.widgets.RegisterWidget('readonly', w_readonly);

