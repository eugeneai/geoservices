/**
	* A filter widget based on data in a table column.
	* 
	* @class GeometryFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/
	$.GeometryFilterWidget = function ($Container, column, dt_table) {
		var widget = this;
		widget.SelectionLayer = null;
		widget.selectionControl = null;
		widget.column = column;
		widget.$Container = $Container;
		widget.oDataTable = dt_table;
	
		widget.SelectionItems = new L.FeatureGroup();
		widget.refFilterList = [];
	
		var metaDistrict = {
			"fieldname": "2171",
			"title": "Район",
			"description": "confreport",
			"visible": true,
			"type": "integer",
			"widget": {
				"name": "classify",
				"properties": {
					"dataset_id": 2172,
					"db_field": ["full_name"], // TODO full_name
					"control_type": "autoselect"
				}
			}
		};
	
		var metaRegion = {
			"fieldname": "2172",
			"title": "Субъект РФ",
			"description": "confreport",
			"visible": true,
			"type": "integer",
			"widget": {
				"name": "classify",
				"properties": {
					"dataset_id": 2171,
					"db_field": ["name"],
					"control_type": "autoselect"
				}
			}
		};
		// var user_widget=$.widgets.get_widget_byname('user');
	
		var widgetDistrict = $.widgets.get_widget(metaDistrict);
		var widgetRegion = $.widgets.get_widget(metaRegion);
	
		this.changevalue = function (fieldname, value) {
			var refName = ''
			switch (widget.asFilter) {
				case 'District':
					refName = widgetDistrict.getUserVal(value);
					break;
				case 'Region':
					refName = widgetRegion.getUserVal(value);
					break;
				default:
					throw new Error('Unsupported filter type');
					break;
			}		
	
			item = {
				dataset_id: fieldname,
				name: refName,
				value: value
			};
			this.refFilterList.push(item);
			this.fnFilter();
			widgetDistrict.input.val('');
		}	
		widgetDistrict.listnerlist.push(this);
		widgetRegion.listnerlist.push(this);
	
		widget.$btnOpenFile = $('<a class="" id="btnOpen">Open file</a>');
		widget.$regionAutocomplete = $('<div>Субъект РФ</div>');
		widget.$districtAutocomplete = $('<div>Район субъекта РФ</div>');
	
		widgetDistrict.assign(widget.$districtAutocomplete, '');
		widgetRegion.assign(widget.$regionAutocomplete, '');
	
		var options = new Object();
		options.filter = 'kml,kmz,wkt';
		widget.$btnOpenFile.click(function () {
			
		});
	
		widget.$regionAutocomplete.hide();
		widget.$districtAutocomplete.hide();	
	
		widget.asFilter = 'All';
		widget.sSeparator = ';';
		widget.iMaxSelections = -1;
		widget.$GeometryRange = $('<select class="form-control fltr_geom" />');
		var v = 'All';
		var option = $('<option />').val(v).append('Вся карта');
		widget.$GeometryRange.append(option);
	
		var v = 'Extent';
		var option = $('<option />').val(v).append('Экстент');
		widget.$GeometryRange.append(option);
	
		var v = 'Selection';
		var option = $('<option />').val(v).append('Внутри полигона');
		widget.$GeometryRange.append(option);
	
		var v = 'Region';
		var option = $('<option />').val(v).append('Регион');
		widget.$GeometryRange.append(option);
	
		var v = 'District';
		var option = $('<option />').val(v).append('Район');
		widget.$GeometryRange.append(option);
	
		var v = 'File';
		var option = $('<option />').val(v).append('Файл');
		widget.$GeometryRange.append(option);
	
		var SelectionItems;
		widget.$GeometryRange.change(function () {
			widget.asFilter = $(this).val();
			widget.$regionAutocomplete.hide();
			widget.$districtAutocomplete.hide();
			if (widget.asFilter == 'Selection') {
				map.addLayer(widget.SelectionItems);
				add_map_handle(function (gobject, mode) {
					//var wkt = new Wkt.Wkt();
					//wkt.fromObject(gobject);
					//var value = wkt.write();
					widget.SelectionItems.addLayer(gobject);
					widget.fnFilter();
					//alert(value);
					//widget.setvalue(value);			
					//widget.updateLayer();
				}, "polygon", this);
	
			}
			else if (widget.asFilter == 'Extent') {
				map.addLayer(widget.SelectionItems);
				add_map_handle(function (gobject, mode) {
					var wkt = new Wkt.Wkt();
					wkt.fromObject(gobject);
					var value = wkt.write();
					widget.SelectionItems.addLayer(gobject);
					widget.fnFilter();
					//alert(value);
					//widget.setvalue(value);			
					//widget.updateLayer();
				}, "rectangle", this);
	
			}
			else if (widget.asFilter == 'Region') {
				widget.$regionAutocomplete.show();
			}
			else if (widget.asFilter == 'District') {
				widget.$districtAutocomplete.show();
			}
			else if (widget.asFilter == 'File') {
				widget.$GeometryRange.val('All');
				OpenFileDlg(function (path) {
					readFile(path, function (data) {
						widget.$GeometryRange.val('Selection');
						widget.$GeometryRange.trigger('change');
		
						if (path.slice(path.lastIndexOf('.') + 1) == 'wkt') {
							wkt = new Wkt.Wkt();
		
							try { // Catch any malformed WKT strings
								wkt.read(data);
							} catch (e1) {
								try {
									wkt.read(data.value.replace('\n', '').replace('\r', '').replace('\t', ''));
								} catch (e2) {
									if (e2.name === 'WKTError') {
										alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
										return;
									}
								}
							}
		
							obj = wkt.toObject(this.map.defaults); // Make an object
		
							if (Wkt.isArray(obj)) { // Distinguish multigeometries (Arrays) from objects
								for (i in obj) {
									if (obj.hasOwnProperty(i) && !Wkt.isArray(obj[i])) {
										obj[i].name = get_filename(path) + '_' + i;
										widget.SelectionItems.addLayer(obj[i]);
									}
								}
							} else {
								obj.name = get_filename(path);
								widget.SelectionItems.addLayer(obj);
							}
						}
						else {
							layers = L.KML.parseKML(data);
							/*var wkt = new Wkt.Wkt();
							wkt.fromObject(layers);
							var value = wkt.write();*/
							for (i = 0; i < layers.length; i++) {
								if (layers[i]._popup != undefined && layers[i]._popup._content != undefined && layers[i]._popup._content != '')
									layers[i].name = layers[i]._popup._content;
								else
									layers[i].name = path;
		
								widget.SelectionItems.addLayer(layers[i]);
							}
						}
						widget.fnFilter();
					});
		
		
				}, options);
			}		
			else {
				map.removeLayer(widget.SelectionItems);
				stop_map_edit(this);
			}
	
			widget.fnFilter();
		});
	
		//widget.$label = $('<label for="from_' + widget.oColumn.sName + '">' + widget.oColumn.sTitle + '</label><br>');
		//widget.$Container.append( widget.$label );
		widget.$Container.append(widget.$GeometryRange);
		widget.$Container.append(widget.$regionAutocomplete);
		widget.$Container.append(widget.$districtAutocomplete);
		//widget.$Container.append(widget.$btnOpenFile);
		widget.filter_items = $('<div/>');
		widget.$Container.append(widget.filter_items);
		//widget.filter_items.hide();
	
		//widget.fnDraw();		
	};
	
	
	$.GeometryFilterWidget.prototype.update_list = function () {
		var widget = this;
		widget.filter_items.empty();
		var i = 0;
		sel_range_val = widget.$GeometryRange.val();
		if (sel_range_val == 'Selection' || sel_range_val == 'Extent') {
			widget.SelectionItems.eachLayer(function (layer) {
				i++;
				var TermCont = $('<div><a id="btnSave" title="Save the polygon"><i class="icon-briefcase"></i></a></div>');
				var layer_id = widget.SelectionItems.getLayerId(layer);
				if (layer.name)
					sText = $('<div>' + layer.name + '</div>').text();
				else
					sText = $('<div>Polygon ' + i + '</div>').text();
				var $TermLink = $('<a class="filter-term" href="#"></a>')
					.addClass('filter-term-' + sText.toLowerCase().replace(/\W/g, ''))
					.text(sText)
					.click(function () {
						// Remove from current filters array
						layer_id = $(this).data("layer_id");
						widget.SelectionItems.removeLayer(layer_id);
						widget.fnFilter();
						return false;
					});
				$TermLink.data("layer_id", layer_id);
	
				TermCont.append($TermLink);
				widget.filter_items.append(TermCont);
				$('#btnSave', TermCont).click(function () {
					var wkt = new Wkt.Wkt();
					wkt.fromObject(layer);
					var value = wkt.write();
					SaveFileDlg(function (path) {
						saveFile(path, value);
					}, { filter: 'wkt' });
	
				});
	
			});
		}
		else if (sel_range_val == 'District' || sel_range_val == 'Region') {
		// else if (sel_range_val == 'District' ) {
			widget.refFilterList.forEach(function (item) {
				var $displayItem = $('<div class="filter-item"></div>');
				var $TermLink = $('<a class="filter-term" href="#"></a>')
					.data("spatial-bound", item)
					.text(item.name)
					.click(function () {
						var thisSpatialBound = $(this).data("spatial-bound");
						// Remove from current filters array
						widget.refFilterList = widget.refFilterList.filter(function(i) {
							return thisSpatialBound != i;
						});
						widget.fnFilter();
						return false;
					});
	
				$displayItem.append($TermLink);
				widget.filter_items.append($displayItem);
			});		
		}	
		if ($('.filter-term', widget.filter_items).length > 0) {
			widget.filter_items.show();
		}
		else {
			widget.filter_items.hide();
		}
	};
	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.GeometryFilterWidget.prototype.fnFilter = function () {
		var widget = this;
		if (widget.asFilter == '' || widget.asFilter == 'All') {
			widget.oDataTable.fnFilter('', widget.column.fieldname);
		}
		else if (widget.asFilter == 'Selection' || widget.asFilter == 'Extent') {
			var s_branch = '';
			this.SelectionItems.eachLayer(function (layer) {
				coords='';
				for(var i=0; i<layer._latlngs[0].length; i++){
					if(coords!='')
						coords +=', ';
					coords +=layer._latlngs[0][i].lng + ' ' + layer._latlngs[0][i].lat;
				}
				coords +=', ';
				coords +=layer._latlngs[0][0].lng + ' ' + layer._latlngs[0][0].lat;
				coords='POLYGON(('+coords+'))';
				
				//var wkt = new Wkt.Wkt();
				//wkt.fromObject(layer);
				//var value = wkt.write();
				if (s_branch != '')
					s_branch += ';';
				s_branch += coords;
				
			});
			widget.oDataTable.fnFilter(s_branch, widget.column.fieldname, true, false);
		}
		else if (widget.asFilter == 'District' || widget.asFilter == 'Region') {
			var s_branch = '';
			var geoTableId = null;
			switch (widget.asFilter) {
				case 'District':
					geoTableId = '2172';
					break;
				case 'Region':
					geoTableId = '2171';
					break;
				default:
					throw new Error('Unsupported fiter type');
					break;
			}
			widget.refFilterList.forEach(function (item) {
				var itemString = 'geotable:' + geoTableId + ':' + item.value + ':geom';
				if (s_branch != '') { s_branch += ';' };
				s_branch += itemString;
			});
			// debugger;
			widget.oDataTable.fnFilter(s_branch, widget.column.fieldname, true, false);
		}
		this.update_list();
	
		setTimeout(function () {
			$('.g_layer', $('#layer_list')).each(function (index) {
				ml = $(this).data("mlayer");
				//if(widget.oColumn.oWidget.metaid==ml.layer_id)
				ml.redraw();
			});
		}, 150);
	};
	
	