(function($) {
	/**
	* A filter widget based on data in a table column.
	* 
	* @class DateRangeFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/
	$.BooleanFilterWidget = function( $Container, column, dt_table, filtervalue ){
		var widget = this;
		widget.column = column;		
		widget.$Container = $Container;
		widget.oDataTable = dt_table;		
		widget.value = filtervalue;
	

    
        this.input = $( '<select title="Filter" class="form-control" />' );
		var input=this.input;
		widget.$Container.append(this.input);
		var vals={'да':'true', 'нет': 'false', 'Все':'NULL'};
		if(filtervalue===undefined || filtervalue==='')
            widget.value ='NULL';
		for (index in vals) {
			if(vals[index]==widget.value || vals[index]==''+widget.value)
				option = $('<option selected value="'+vals[index]+ '">' + index+ '</option>');
			else
				option = $('<option value="'+vals[index]+ '">' + index+ '</option>');
			input.append(option);
		}
	
		
		this.input.change(function(event, ui) {
            widget.value=widget.input.val();
			
			widget.fnFilter();	
		});
		
		
	};


	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.BooleanFilterWidget.prototype.fnFilter = function() {
		var widget = this;
		if (  widget.value == 'NULL' ) {
            widget.oDataTable.fnFilter( '', widget.column.fieldname );
		} else { 
			widget.oDataTable.fnFilter( widget.value, widget.column.fieldname );			
		}
	};		
	
}(jQuery));