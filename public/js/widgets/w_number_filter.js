var number_operators={
PropertyIsEqualTo:'=',
PropertyIsNotEqualTo:'!=',
PropertyIsLessThan:'>',
PropertyIsLessThanOrEqualTo:'>=',
PropertyIsGreaterThan:'<',
PropertyIsGreaterThanOrEqualTo:'<='
}

var rigth_operators={
PropertyIsEqualTo:'=',
PropertyIsNotEqualTo:'!=',
PropertyIsLessThan:'<',
PropertyIsLessThanOrEqualTo:'<=',
PropertyIsGreaterThan:'>',
PropertyIsGreaterThanOrEqualTo:'>='
}

	/**
	* A filter widget based on data in a table column.
	* 
	* @class AutocompleteFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/	
	$.number_filter = function( $Container, column, dt_table ){
		var widget = this;
		widget.column = column;		
		widget.$Container = $Container;
		widget.oDataTable = dt_table;
		
		widget.asFilters = [];
		widget.sSeparator = ';';
		widget.iMaxSelections = -1;
		
		//
		//widget.$label = $('<label for="f_' + widget.oColumn.sName + '">' + widget.oColumn.sTitle + '</label><br>');
		//widget.$Container.append( widget.$label );
		d=$('<div class="input-group fltr_number"></div>');
		widget.$Container.append( d );
		
		var input=$( '<input class="form-control fltr_'+widget.column.fieldname+'"  type="text" />' );
		d.append( input );
		
		cf1=$('<div class="input-group-append">\
		<span class="input-group-text"><i class="fas fa-filter"></i></span>\
	  </div>');
		d.append( cf1 );
		widget.$label=$('span', cf1)
		//widget.$Container.width('100px');		
		
		// d=$('<div style="display: inline-block;vertical-align: top;"></div>');
		// widget.$label = $( '<a class="btn" style="padding: 0px 3px;" href="#"><i class="icon-edit"></i></a>' );
		// widget.$Container.append( d );
		// d.append( widget.$label );	
		
		var term_container=$('<div></div>');
		widget.$Container.append( term_container );
		
		input.keypress(function(e) {
			if(e.which == 13) {
				if(input.val()=='')
					return false;
				if(!test_number(input.val()))
					return false;				
				var sText=input.val()+'='+widget.column.title;
				var sSelected=input.val()  + '-PropertyIsEqualTo';
				if  (!($.inArray(sSelected, widget.asFilters))) {
					return;
				}
				input.val('');
				var $TermLink = $( '<a class="filter-term" href="#"></a>' )
					.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
					.text( sText )
					.click( function() {
						// Remove from current filters array
						widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
							return sFilter != sSelected;
						} );
						$TermLink.remove();
						if ( widget.$TermContainer && 0 === widget.$TermContainer.find( '.filter-term' ).length ) {
							widget.$TermContainer.hide();
						}
						widget.fnFilter();
						return false;
					} );
				widget.asFilters.push( sSelected );
				term_container.append( $TermLink ); 
				widget.fnFilter();
			}
		});
		
		
		widget.$label.click(function(){
			var p=$( '<div  title="Adding a rule"><form class="form-inline">\
			<input class="form-control mb-2 mr-sm-2" type="text" id="from" />\
			<select class="form-control mb-2 mr-sm-2" id="left_operand" /> \
			'+widget.column.title+'\
			<select class="form-control mb-2 mr-sm-2" id="right_operand" />\
			<input class="form-control mb-2 mr-sm-2" type="text" id="to" />\
			</form></div>');
			var left_value=$('#from', p);
			var rigth_value=$('#to', p);
			var left_operand=$('#left_operand', p);
			for(var key in number_operators) {
				if(key=='PropertyIsGreaterThanOrEqualTo')
					op=$('<option selected value="'+key+'">'+number_operators[key]+'</option>');
				else
					op=$('<option value="'+key+'">'+number_operators[key]+'</option>');
				
				left_operand.append(op);
			}
			
			var right_operand=$('#right_operand', p);
			for(var key in rigth_operators) {
				if(key=='PropertyIsLessThanOrEqualTo')
					op=$('<option selected value="'+key+'">'+rigth_operators[key]+'</option>');
				else
					op=$('<option value="'+key+'">'+rigth_operators[key]+'</option>');
				right_operand.append(op);
			}
			
			p.dialog({
				  resizable: false,
				  height:'auto',
				  width:'auto',
				  modal: true,
				  buttons: {
					"Add": function() {
						var val_d_from = left_value.val(),
							val_d_to = rigth_value.val();
						if (( val_d_from == '' ) && ( val_d_to == '' )) {
							// The blank range
							return;
						}				
						
						var sSelected = '';
						var sText='';
						if(val_d_from!=''){
							sSelected+=val_d_from  + '-' + left_operand.val();
							sText=val_d_from + number_operators[left_operand.val()];
						}
						sText+=widget.column.title;
						if(val_d_to!=''){
							if(sSelected!='')
								sSelected+='-';
							sSelected+=val_d_to+'-'+right_operand.val();
							sText+=rigth_operators[right_operand.val()]+val_d_to;
						}
						var $TermLink, $SelectedOption; 
						
						if  (!($.inArray(sSelected, widget.asFilters))) {
							return;
						}
						//sText = $( '<div>' + sText + '</div>' ).text();
						$TermLink = $( '<a class="filter-term" href="#"></a>' )
							.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
							.text( sText )
							.click( function() {
								// Remove from current filters array
								widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
									return sFilter != sSelected;
								} );
								$TermLink.remove();
								if ( widget.$TermContainer && 0 === widget.$TermContainer.find( '.filter-term' ).length ) {
									widget.$TermContainer.hide();
								}
								widget.fnFilter();
								return false;
							} );
						widget.asFilters.push( sSelected );
						term_container.append( $TermLink ); 
						widget.fnFilter();
						
						
						$( this ).dialog( "close" );
						p.remove();
					},
					Cancel: function() {
						$( this ).dialog( "close" );
						p.remove();
					}
				}
			});		
		
		});
	};
	
	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.number_filter.prototype.fnFilter = function() {
		var widget = this;
		var asEscapedFilters = [];
		var sFilterStart, sFilterEnd;
		if ( widget.asFilters.length > 0 ) {			
			widget.oDataTable.fnFilter( widget.asFilters.join(';') , widget.column.fieldname, true, false );
		} else { 
			// Clear any filters for this column
			widget.oDataTable.fnFilter( '', widget.column.fieldname);
		}
		setTimeout( function() {
			$('.g_layer',$('#layer_list')).each(function(index){
				ml=$(this).data("mlayer");
				ml.redraw();
			});							
		}, 150 );
	};
