function w_file_save(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	// Common name of widget
	var widget_name = 'file_save';


	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;

		var form_element_container = $('<div/>');
		form_element_container.addClass('form-inline');
		var input = $('<input type="text" id="' + this.object.fieldname + '"/>');
		form_element_container.append(input);
		var img=$('<button class="btn" type="button">Save</button>');
		form_element_container.append(img);
		form_element.append(form_element_container);
		var options = new Object();
		options.filter=this.object.widget.properties.exts;
		img.click(function() {
			SaveFileDlg(function(path){
				//c('Path is: ' + path);
				input.val(path);
			}, options);
		});
		
		if(value!=undefined && value!='')
			input.val(value);
    }
    
	// Getting value of input field
    this.getVal = function (){
		return $('#'+this.object.fieldname,this.form_element).val();
    }
   
	// Additional properties form
    this.viewPropForm = function (container){	   
		var ps=$("<label>File extensions (write extensions using comma)</label><input type='text' id='f_ext' value=''/><br/>");
		container.append(ps);
		var ps=$("<label>File style </label><textarea style='padding: 0px; width: 450px; height:80px;  margin-bottom: 1px;' id ='mapfilestyle'></textarea><br/>");
		container.append(ps);		
		var open = $('<input value="open" type="button" class="btn"/>');		
		container.append( open );

		if(this.object.widget.properties!=undefined ){
			if(this.object.widget.properties.exts!=undefined   ){
			   $('#f_ext', container).val(this.object.widget.properties.exts);
			}
			
			if(this.object.widget.properties.mapfilestyle!=undefined   ){
			   $('#mapfilestyle', container).val(this.object.widget.properties.mapfilestyle);
			}
		}
	    open.click(function(){
			var options = new Object();
			options.filter='style';

			OpenFileDlg(function(path){
					readFile(path, function(data){
						$('#mapfilestyle', container).val(data);
					});
				}, options);
		});
		return;
    }    
    this.getPropForm = function (container){		
	    return {exts: $('#f_ext', container).val(), mapfilestyle: $('#mapfilestyle', container).val()};
    } 
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'text';		
		tempobj.sWidget = 'file';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';

		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_file_save, w_base);
$.widgets.RegisterWidget('file_save', w_file_save);

