function w_ref(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'ref';
	// Additional properties array
	var addproparr = new Array();
	this.user_visible = true;
	this.rname = 'Кнопка';
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;

		var unique = this.object.fieldname;
		if (this.object.htmlname === undefined) {
			this.object.htmlname = this.object.fieldname;
		}
		var tempstr = '';
		form_element.html(tempstr);
		var el = $('#' + unique, form_element);
		if(value!=undefined && value!='')
			el.val(value);		
		
		el.attr( 'widget', widget_name)
		$.widgets.formlist.push([widget_name, form_element]); 
    }

	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname).val();
    }

    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		var props = this.object.widget.properties;		
		$('div').first().append('<div id="vrBerg234gsdv" style="display:none;"></div>');
			if (typeof props.type !== undefined) {
				switch (props.type) {
					case 'button':
						var obj = $('<button />');
						obj.attr('class', 'btn btn-mini');
						obj.attr('type', 'button');
						if ("href" in props) obj.attr('onclick', 'window.open("' + this.applyTaxonomy(props.href) + '", "_blank")');
						if ("exec" in props)
						{
							if (props.exec !== '')
							{
								obj.attr('onclick', this.applyTaxonomy(props.exec));
							}
						}
						obj.html(props.inner);
						if (props.inner == "Modify") obj.html('<i class="icon-wrench"></i>');
						if (props.inner == "View") obj.html('<i class="icon-pencil"></i>');						
						break;						
					case 'link':
						var obj = $('<a />');
						obj.attr('href', this.applyTaxonomy(props.href));
						obj.attr('target', props.target);
						obj.attr('style', 'cursor: pointer; border-bottom: 1px dashed black;');
						obj.html(props.inner);
						break;
				}
				$('#vrBerg234gsdv').append(obj);
			} else {
				//c('Unable to detect REF type');
			}
		var tempo = $('#vrBerg234gsdv').html();
		$('#vrBerg234gsdv').remove();
		return tempo;
    }
    
	/**
	*	applyTaxonomy - substituting predefined constants
	*/
	var taxonomy = new Array();
	/* Filling the contants */
	taxonomy['CONST_TABLE_ID'] = this.cntr;
	taxonomy['ROWID'] = this.cntr;
	this.applyTaxonomy = function(link) {
		taxonomy['CONST_TABLE_ID'] = this.cntr;
		taxonomy['ROWID'] = this.cntr;
		for(var index in taxonomy) {
			link = link.replace(index, taxonomy[index]);
		}
		return link;
	}
	
    this.viewPropForm = function (container){
		var ps = "<label>Вид</label><br><select id='type' name='type'><option value='button'>Кнопка</option><option value='link'>Ссылка</option></select><br><label>Внутренний текст <span style='cursor: pointer; border-bottom: 1px dashed black;' id='infoabout_inner'>(?)</span></label><br><input type='text' id='inner'  name='inner' /><br><label>Ссылка</label><span style='cursor: pointer; border-bottom: 1px dashed black;' id='infoabout_href'>(?)</span><br><input type='text' id='href'  name='href' /><br><label>Открывать ссылку в новом окне?</label><select id='target'><option value='_blank'>Открывать</option>option value='_self'>Не открывать</option></select><br><label>Выполнить функцию</label><span style='cursor: pointer; border-bottom: 1px dashed black;' id='infoabout_exec'>(?)</span><br><input type='text' id='exec' name='exec' />";
		container.append(ps);
		
		$('#infoabout_inner',container).click(function() {
			alert('Текст, который будет отображаться внутри кнопки или ссылки');
		});
		$('#infoabout_href',container).click(function() {
			alert('Ссылка, которая будет открываться при клике');
		});
		$('#infoabout_exec',container).click(function() {
			var taxonomy_disp = '';
			for (var key in taxonomy) {
				taxonomy_disp += key + ';';
			}
			alert('Введите функцию, которая бы вызывалась по нажатию на виджет. Словарь: ' + taxonomy_disp);
		});
		try {
			$('#exec',container).val(this.object.widget.properties.exec);
			$('#type',container).val(this.object.widget.properties.type);
			$('#inner',container).val(this.object.widget.properties.inner);
			$('#href',container).val(this.object.widget.properties.href);
			$('#target',container).val(this.object.widget.properties.target);
		} catch(e) { }
		
		return;
    }
	
    this.getPropForm = function (container){
	    return {
			exec: $('#exec',container).val(),
			type: $('#type',container).val(),
			inner: $('#inner',container).val(),
			href: $('#href',container).val(),
			target: $('#target',container).val()
		};
    }
	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'autoedit';
		tempobj.oWidget = this;		
		tempobj.sWidget = 'ref';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}	
    return this;
}

extend(w_ref, w_base);
$.widgets.RegisterWidget('ref', w_ref);
