function removeHtmlSymbols(text) {
	  return text
		  .replace(/&amp;/g, "&")
		  .replace(/&lt;/g, "<")
		  .replace(/&gt;/g, ">")
		  .replace(/&quot;/g, '')
		  .replace(/&#039;/g, '');
	}

/**
* A filter widget based on data in a table column.
* @class AutocompleteFilterWidget
* @constructor
* @param {object} $Container The jQuery object that should contain the widget.
*/	
$.AutocompleteFilterW_Edit = function( $Container, column, dt_table ) {
	var widget = this;
	widget.column = column;		
	widget.$Container = $Container;
	widget.oDataTable = dt_table;
	widget.asFilters = [];
	widget.sSeparator = ';';
	widget.iMaxSelections = -1;
	var thead=$Container.parents('thead');
	var th=$('th[fieldname="'+widget.column.fieldname+'"]', thead);
	
	//widget.$Autocomplete = $( '<input type="text" id="'+widget.column.fieldname+'" size="'+widget.oColumn.sWidth+'"/>' );
	widget.$Autocomplete = $( '<input class="form-control fltr_'+widget.column.fieldname+'" type="text"/>' );
	// setTimeout( function() {
	// 	if(th.width()>0) 
	// 		widget.$Autocomplete.width(th.width());
	// }, 250 );
	// if(th.width()>0)
	// 	widget.$Autocomplete.width(th.width());
	
	widget.$TermContainer = $( '<div/>' );
	
		w_settings = {
			autofill    : true,
			cacheLength : 10,
			max         : 5,
			autofocus   : true,
			highlight   : true,
			mustMatch   : true,
			selectFirst : true,
			delay: 500,
			source		: function (request, response) {
				if(request.term.length<3)
					return;
				var result=[];
				if(widget.column.$AutocompleteSource.indexOf("f=-2")==-1){
					$.ajax({
						url: widget.column.$AutocompleteSource + '&iDisplayStart=0&iDisplayLength=10&iSortingCols=1&iSortCol_0=1&distinct=t&f_' + widget.column.fieldname +'=' + request.term + '&s_fields=' + widget.column.fieldname,
						dataType: "json",						
						success: function(data) {
							for (var i = 0; i < data.aaData.length; i++) {	
								v=data.aaData[i][widget.column.fieldname ];
								v=removeHtmlSymbols(v);
								result.push(v);
							}
							response(result);
						}
					});
				}
			},
			select: function(event, ui) {
				var sSelected = removeHtmlSymbols(ui.item.value), sText, $TermLink, $SelectedOption; 
				ui.item.value = "";
				if ( '' === sSelected ) {
					// The blank option is a default, not a filter, and is re-selected after filtering
					return;
				}
				if  (!($.inArray(sSelected, widget.asFilters))) {
					return;
				}
				sText = $( '<div>' + sSelected + '</div>' ).text();
				var $TermLink = $( '<a class="filter-term" href="#"></a>' )
					.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
					.text( sText )
					.click( function() {
						// Remove from current filters array
						var c_val=$TermLink.text();
						widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
							return sFilter != c_val;
						} );
						$TermLink.remove();
						if ( widget.$TermContainer && 0 === widget.$TermContainer.find( '.filter-term' ).length ) {
							widget.$TermContainer.hide();
						}
						widget.fnFilter();
						return false;
					} );
				widget.asFilters.push( sSelected );
				if ( widget.$TermContainer ) {
					widget.$TermContainer.show();
					widget.$TermContainer.prepend( $TermLink );
				} else {
					widget.$Autocomplete.after( $TermLink );
				}
			
				widget.$Autocomplete.val( '' );
				widget.fnFilter();				
			},				
		};

		widget.$Autocomplete.autocomplete(w_settings, {
			dataType:'json',
			change: function(event, ui) {
				//con.change();
			},
			formatItem: function(row, i, n) {                                                        
			  return row.value;
			},
		});			
	
		widget.$Autocomplete.keypress(function(e) {
			if(e.which == 13) {					
				sSelected=removeHtmlSymbols(widget.$Autocomplete.val());
				if(sSelected==='') return;
				sText = $( '<div>' + sSelected + '</div>' ).text();
				var $TermLink = $( '<a class="filter-term" href="#"></a>' )
					.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
					.text( sText )
					.click( function() {
						// Remove from current filters array							
						var c_val=$TermLink.text();
						widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
							return sFilter != c_val;
						} );							
						$TermLink.remove();							
						if ( widget.$TermContainer && 0 === widget.$TermContainer.find( '.filter-term' ).length ) {
							widget.$TermContainer.hide();
						}							
						widget.fnFilter();
						return false;
					} );
				widget.asFilters.push( sSelected );
				if ( widget.$TermContainer ) {
					widget.$TermContainer.show();
					widget.$TermContainer.prepend( $TermLink );
				} else {
					widget.$Autocomplete.after( $TermLink );
				}				
				widget.$Autocomplete.val( '' );
				widget.$Autocomplete.autocomplete( "close" );
				widget.fnFilter();					
			}
		});
	

	widget.$Container.append( widget.$Autocomplete );
	widget.$Container.append(widget.$TermContainer);
	//widget.fnDraw();
};

/**
* Perform filtering on the target column.
*/
$.AutocompleteFilterW_Edit.prototype.fnFilter = function() {
	var widget = this;
	var asEscapedFilters = [];
	var sFilterStart, sFilterEnd;
	if ( widget.asFilters.length > 0 ) {
		// Filters must have RegExp symbols escaped
		$.each( widget.asFilters, function( i, sFilter ) {
			asEscapedFilters.push( sFilter );
		} );
		// This regular expression filters by either whole column values or an item in a comma list
		sFilterStart = widget.sSeparator ? '(^|' + widget.sSeparator + ')(' : '^(';
		sFilterEnd = widget.sSeparator ? ')(' + widget.sSeparator + '|$)' : ')$';
		widget.oDataTable.fnFilter( asEscapedFilters.join(';') , widget.column.fieldname);
	} else { 
		// Clear any filters for this column
		widget.oDataTable.fnFilter( '', widget.column.fieldname );
	}
	setTimeout( function() {
		$('.g_layer',$('#layer_list')).each(function(index){
			ml=$(this).data("mlayer");
//				if(widget.column.oWidget.metaid==ml.layer_id)
				ml.redraw();
		});							
	}, 150 );
};


