﻿


function w_details(object) {
	var widget=this;	
	// Object for creation of widget	
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'details';
	// Additional properties array
	var addproparr = new Array();
	this.user_visible = true;
	this.rname = 'details';		
	this.fieldtype = 'integer';
	// id of selected value
	var sel_id = '';
	var ft= new Array();
	this.tables=[];
	this.filter={};
	this.fields='';
	var widget=this;
	this.calllist=[];
	
	if(this.object!=undefined && this.object.widget!=undefined){
		//[8].parentfield ref_detail
		var props = this.object.widget.properties;
		if(props && props.dataset_id){
			getdatasetmeta(props.dataset_id  , function(error, table_json){
				if(error!=null)
					return;
				table_json.metaid = props.dataset_id;			
				var displayfields=widget.fields.split(',');
				displayfields.push('id');			
				for(var i=table_json.columns.length - 1; i>=0; i--){				
					if( table_json.columns[i].parentfield==props.ref_detail || table_json.columns[i].fieldname==props.ref_detail){
						//table_json.columns[i].visible=false;				
						table_json.columns.splice(i,1);
					}
					else if(widget.fields!=='' && displayfields.indexOf(table_json.columns[i].fieldname)==-1 )
						table_json.columns.splice(i,1);
				}
				widget.table_json=table_json;
				widget.doc_frm= new doc_template(table_json);
				widget.doc_frm.init_form(function(){		
			
				});
				
			});
			if(props!=undefined && props.db_field!=null && props.db_field!=undefined)
				this.fields=props.db_field.join(',');
			
		}
	}
	
	

    this.assign = function(form_element, value){		
		this.form_element = form_element;
		this.value=[];
		if(typeof value=== "object")
			this.value=value;
		var props = this.object.widget.properties;
		tab=$('<div><div class="subtab"></div></div>');
		//<a class="btn" href="#" id="btnEditTab" title="Edit the table"><i class="icon-pencil"></i></a>
		var subtab=$('.subtab',tab);
		//subtab.html(this.getUserVal(value));
		form_element.append(tab);
		var timerId = setTimeout(function() {
			if(widget.table_json){
				clearInterval(timerId);
				if($.isArray(value))		
					widget.gtable = new gdataset(widget.table_json, props.dataset_id, subtab, '', value, true);
				else{
					widget.gtable = new gdataset(widget.table_json, props.dataset_id, subtab, '', [], true);
					a=1;
				}
				panel=$('<div id="command_panel"><h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"><div class="btn-group mb10"></div></h3></div>');
				subtab.append(panel);
				panel=$('.btn-group', panel);
				widget.gtable.add_crud_panel(panel);
				widget.gtable.maxrowcount = props.maxrowcount;
				widget.gtable.drawtablehead();
				widget.gtable._fnReDraw();		
			};		  
		  
		}, 500);
		
		
    }
    
	// Getting value of input field
    this.getVal = function (){
		//if(this.visible!=undefined && this.visible==false)
		//	return '';
		res=[];
		if(this.gtable){
			for(var i=0; i<this.gtable.tabledata.length; i++){
				newrow={};
				for( var c =0; c<this.gtable.metadata.columns.length; c++){
					column=this.gtable.metadata.columns[c];
					if(column.fieldname in this.gtable.tabledata[i])
						newrow[column.fieldname]=this.gtable.tabledata[i][column.fieldname];
				}				
				res.push(newrow);
			}
			//for(var i=0; i<this.gtable.deleteddata.length; i++){
				//res.push(this.gtable.deleteddata[i]);
			//}
		}
		else{
			res=this.value;
			//serverlog('this.gtable id null!!!!');
		}
		//serverlog({action:'getting authors',authors:res});
		return res;
    }
		
	
	this.getUserVal=function (val, view){
		if(val=='NULL')
			return '';
		if(this.gr_fn=='none' )
			return '';
			
		var props = this.object.widget.properties;
		if($.isArray(val) && this.table_json && val.length>0){
			var unique=randomString(10);
			if(!view){
				if(props.tableview){					
					var result='';
					for(var i=0; i<val.length; i++){
						try{
							result+=fieldcalc.calculatefield(props.tableview, val[i]) + '; ';
						}
						catch(e){
							console.log(e);
						}
					}
					return result;
				}
				else
					return val.length;
			}
				
			
			res='<a href="#" class="detailslink" id="href'+unique+'">Hide details</a><table class="detailstab" id="'+unique+'"><tr>';
			for(var i=0; i<this.table_json.columns.length; i++){				
				if(this.table_json.columns[i].visible){
					res+='<td class="column_title">'+this.table_json.columns[i].title+'</td>';
					if(!this.table_json.columns[i].widget_object){
						this.table_json.columns[i].widget_object=$.widgets.get_widget(this.table_json.columns[i]);
						this.table_json.columns[i].widget_object.metaid=props.dataset_id;
					}
				}
			}
			res+='</tr>';

			
			for(var r=0; r<val.length; r++){
				res+='<tr>';//<td><a class="btn CustomView '+unique+'" href="#" row_id="'+r+'" title="View" disabled="disabled"><i class="view-doc-icon" /></i></a></td>
				for(var i=0; i<this.table_json.columns.length; i++){
					if(this.table_json.columns[i].visible){
						this.table_json.columns[i].widget_object.cntr=val[r].id;
						res+='<td>'+this.table_json.columns[i].widget_object.getUserVal(val[r][this.table_json.columns[i].fieldname], true)+'</td>';
					}
				}
				res+='</tr>';
			}
			res+='</table>';
			if(res=='')
				res=val.length;
			
			
			setTimeout(function(){
				var i=-1;
				$('tr', $('#'+unique)).each(function() {
					if(i>-1)
						$( this ).data('doc',val[i]);
					i++;
				});
				
				$('#href'+unique).click(function(){
					var com_form=$('#'+unique);
					if(com_form.css('display') == 'none'){
						com_form.show();
						$('#href'+unique).html('Hide details')
					}
					else{
						com_form.hide();
						$('#href'+unique).html('View details')
					}
					
				});
			}, 500);
			
			/*
			setTimeout(function(){
				$('.'+unique).click(function(){
					eRowform=$('<div title="'+widget.doc_frm.doc_description.title+'"></div>');					
					widget.doc_frm.show_form(eRowform, val[$(this).attr('row_id')], '', true);
					eRowform.dialog({
						  resizable: false,
						  height:'auto',
						  width: 'auto',
						  modal: true,
						  buttons: {
							"Close": function() {								
								$( this ).dialog( "close" );								
							}
						  }
					});
				});
			}, 500);*/

			
		//this.doc_frm.show_form(eRowform, val[0], '', true);
			return res;
		}
		else
			return '-';
    }    

	// Additional properties form
    this.viewPropForm = function (container){
		var widget=this;
		var props = this.object.widget.properties;
		var sz="\
			<div class='classify'/>\
			<label>Select database of the your classificator</label>\
			<div class='form-item' id='db_table'>\
			</div>\
			<div class='form-group'>\
				<label for='reltype'>Relation type with table</label>\
				<select class='form-control' id='reltype'><option value='ref'>many:1</option>\
					<option value='detail'>many:many</option>\
				</select>\
			</div>\
			<div class='form-group'>\
				<label for='control_type'>Control type</label>\
				<select class='form-control' id='control_type' name='control_type' ><option value='select'>Select</option><option value='autoselect'>Auto complete select</option><option value='radio'>Radio buttons</option></select>\
			</div>\
			<div class='form-group'>\
				<input type='hidden' id='type' name='type'></input>\
				<label for='ref_detail'>Select the ref detail field</label>\
				<select class='form-control' id='ref_detail' name='ref_detail' ><option value=''>Select the map field</option></select>\
			</div>\
			<div class='form-group'>\
				<label for='tableview'>Table view expression</label>\
				<input class='form-control' type='text' id='tableview'></input>\
			</div>\
			<div class='form-group'>\
				<label for='maxrowcount'>Max rowc ount</label>\
				<input class='form-control' type='text' id='maxrowcount'></input>\
			</div>\
			<div class='form-group'>\
			<h5>Filters</h5>\
			</div>\
			<div class='form-check' id='table_fields'>\
			</div>\
		";
		container.append(sz);
		var db_table=$("#db_table",container);
		var ref_detail=$("#ref_detail",container);
		this.table_name='';
		this.reftablename='t'+$('.classify').length;	

		var metaTableSelect = {
			"fieldname": "",
			"title": "",
			"description": "",
			"visible": true,
			"widget": {
				"name": "theme_select",
				properties: {}
			}
		};
		var d_id;
		this.tableSelect = $.widgets.get_widget(metaTableSelect);
		if(props){
			d_id=props.dataset_id;			
		}
		this.tableSelect.assign(db_table, d_id);
		
		
		
		var options = '';
		var table_fields=$("#table_fields",container);		
		var synonym_field=$("#synonym",container);
		
		if(props!=undefined && props.another){
			$('#another',container).val(props.another);
		}
		if(props!=undefined && props.synonym=='true'){
			synonym_field.prop("checked", true);
		}
		var control_type=$("#control_type",container);
		if(props!=undefined && props.control_type!=undefined){			
			control_type.val(props.control_type);
		}
		var reltype=$("#reltype",container);
		if(props!=undefined && props.reltype!=undefined){			
			reltype.val(props.reltype);
		}

		var tableview=$("#tableview",container);
		if(props!=undefined && props.tableview!=undefined){			
			tableview.val(props.tableview);
		}

		var maxrowcount=$("#maxrowcount",container);
		if(props!=undefined && props.maxrowcount!=undefined){			
			maxrowcount.val(props.maxrowcount);
		}

		

		
		
		
		var farr = ['point', 'polygon', 'line', 'rect', 'rectangle','baikalcity','political_division', 'links'];
		
		var swidget = ['number', 'edit', 'textarea', 'boolean', 'point', 'polygon', 'line', 'rect', 'date', 'classify','select','details', 'image','email', 'attachment', 'links'];
		helpobject={};
		helpobject.changevalue=function(fieldname, value){
			data = JSON.parse(widget.tableSelect.curentdoc.JSON);
			set_table(data);
		}
		this.tableSelect.listnerlist.push(helpobject);


		function set_table(data){
			ref_detail.empty();
			table_fields.empty();
			
			ref_detail.data('meta',data);
			for (var i = 0; i < data.columns.length; i++) {						
				if(data.columns[i].widget===undefined)
					continue;
				var selected='';
				//if (farr.indexOf(data.columns[i].widget.name) != -1) {
					if(props!=undefined && props.ref_detail!=undefined && props.ref_detail==data.columns[i].fieldname)
						selected= $('<option selected value="'+data.columns[i].fieldname+ '">'+ data.columns[i].title+ ' (' + data.columns[i].fieldname+ ') </option>');
					else
						selected= $('<option value="'+data.columns[i].fieldname+ '">'+ data.columns[i].title+ ' (' + data.columns[i].fieldname+ ') </option>');							
					ref_detail.append(selected);
				//}
				if(swidget.indexOf(data.columns[i].widget.name)==-1)
					continue;				
				if(props!=undefined && props.db_field!=undefined && $.isArray(props.db_field) && props.db_field.indexOf(data.columns[i].fieldname)!=-1)
					selected='checked';
				var filter_value='';
				if(props!=undefined && props.filter!=undefined && data.columns[i].fieldname in props.filter)
					filter_value=props.filter[data.columns[i].fieldname];
				field_filter= $('<div class="row" id="'+data.columns[i].fieldname+'">\
				<div class="form-check col-md-2"><input class="form-check-input" id="field_used" '+selected+' type="checkbox"/><label class="form-check-label" for="defaultCheck1">\
			  	'+ data.columns[i].title+ '</label></div>\
				<div class="form-group col-md-2"><label for="filter_value" >Filter value</label><input class="form-control" id="filter_value" type="text" value="'+filter_value+'"/></div></div>');
				table_fields.append(field_filter);
			}
		}

		if(d_id){
			$.getJSON("/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=" + d_id, function (data) {
				if (data.aaData.length == 0){
					callback('missing dataset meta');
					return;
				}
				meta=JSON.parse(data.aaData[0].JSON);
				set_table(meta);
			});
		}

		
		
		/*
		setTimeout( function() {
				db_table.change();
		}, 300 );
		*/
    }  

    this.getPropForm = function (container){
		var synonym_field=$("#synonym",container);
		var v;
		if(synonym_field.prop("checked"))
			v='true';
		else
			v='false';
			
		//db_field=$('#db_field',container).val();
		var ref_detail=$("#ref_detail",container);		
		var db_map_field=ref_detail.val();
		data=ref_detail.data('meta');
		var gtype='';
		for (var i = 0; i < data.columns.length; i++) {						
			if (data.columns[i].fieldname==db_map_field) {
				gtype=data.columns[i].widget.properties.gtype;
				break;
			}
		}
		
		val=this.tableSelect.getVal();
		if(val==-1 || val=='-1' || val==undefined || val=='')
			return {};
		//data=JSON.parse(tables[val].JSON);
		var filter={};
		var db_field=[];
		for (var i = 0; i < data.columns.length; i++) {
			var table_field_container=$('#'+data.columns[i].fieldname, container);
			v=$('#filter_value',table_field_container).val();
			if (v!=undefined && v!='') 
				filter[data.columns[i].fieldname]=v;			
			if ( $('#field_used',table_field_container).prop("checked") ){
				db_field.push(data.columns[i].fieldname);
				filter[data.columns[i].fieldname]=v;
			}

		}
		ref_detail=$("#ref_detail",container);
		var data=ref_detail.data('meta');
		

				
		return {
				dataset_id: val,
				db_field: db_field,  //$('#db_field option:selected',container).text(),
				ref_detail:ref_detail.val(),
				reftablename: this.reftablename,
				//table_name: this.table_name,
				type: $("#type",container).val(),
				synonym: v,
				gtype: gtype,
				control_type: $("#control_type",container).val(),
				reltype: $("#reltype",container).val(),
				another: $('#another',container).val(),
				filter: filter,
				tableview: $('#tableview',container).val(),
				maxrowcount: $('#maxrowcount',container).val(),
			};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.fieldname;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;		
		tempobj.type = 'widget_proxy';	
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'widget_filter';
		tempobj.sWidth = '200px';
		return tempobj;
	}

	this.hasFilter = function (){
		return false;
	}
	this.olap = function(node, value, meta){	
		this.init_olap(node, value);
		var vis=false;
		var sz=$("<div class='form-item' ><label>Select field</label><br>\
		<select  id='db_field' name='db_field' ><option>Select the field </option></select>min color<input style='width:100px;' class='min_color' value='000000'/>\
		max color<input style='width:100px;' class='max_color' value='000000'/><button id='change_map' class='btn btnApply' type='button'>Apply</button></div>");
		var change_map=$('#change_map',sz);
		change_map.click(function(){
			meta.layer.update_map();
		});
		
		sz.hide();
		this.group.after(sz);
		// $(".min_color",sz).colorpicker();
		// $(".max_color",sz).colorpicker();
		this.group.change(function(){
			if(vis)
				sz.hide();
			else
				sz.show();
			vis=!vis;
		});
		select_input=$('#db_field',sz);
		for (var i = 0; i < meta.columns.length; i++) {
			option = $('<option value="'+meta.columns[i].fieldname+ '">'+ meta.columns[i].title+ ' (' + meta.columns[i].fieldname+ ') </option>');
			select_input.append(option);
		}
	}
	

    return this;
}
extend(w_details, w_base);
$.widgets.RegisterWidget('details', w_details);