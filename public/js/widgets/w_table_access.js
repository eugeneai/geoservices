function w_table_access(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'table_access';
	// Additional properties array
	var addproparr = new Array();
	addproparr[0] = new Array();
	addproparr[0]['propname'] = 'size';
	addproparr[0]['proptitle'] = 'Размер';
	addproparr[0]['propdescription'] = '16';

	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		var label = $('<label for="'+this.object.fieldname+'">'+this.object.title+':</label>');
		var input = $('<input type="text"/>');
		input.attr('id', this.object.fieldname);		
		form_element.append(label);
		form_element.append(input);
		
		if(value!=undefined && value!='')
			input.val(value);
		w_settings= {autofill : true,
				cacheLength : 10,
				max : 5,
				autofocus : true,
				highlight : true,
				mustMatch : true,
				selectFirst : true,
				source  : '/dataset/list?f='+this.metaid+'&field='+this.object.fieldname,
				appendTo: form_element
		};
		input.autocomplete(w_settings, 
			{
				dataType:'json',
				parse : function(data) {                                                                                                                    
				  return $.map(data, function(item)
				  {
					  return {
							  data : item,
							  value : item.Key,
							  result: item.value                                                                                     
							 }
				  })
				 },
				formatItem: function(row, i, n) {                                                        
					  return row.value;
				  },
			});	
				
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname,this.form_element).val();
    }
    
	// Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		return val;
	    // return $('#'+this.object.fieldname).val();
    }

	// Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<label>Size of field</label><input type='text' name='w_edit_size' id='w_edit_size' placeholder='20'/>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined)
			$('#w_edit_size',container).val(this.object.widget.properties.size);
		return;
		var htmlstr = '';
		for (var i = 0; i < addproparr.length; i++) {
			var ttitle = '';
			try {
				eval('ttitle =  this.object.widget.properties.' + addproparr[i]['propname'] + ';');
			} catch (e) {
				c(e.name);
			}
			htmlstr = htmlstr + '<div class="form-field"><label>' +  addproparr[i]['proptitle'] + '</label><input type="text" name="additionalprops_' + addproparr[i]['propname'] + '_' + this.cntr + '" value="' + ttitle + '" placeholder="' + addproparr[i]['propdescription'] + '"/><input type="hidden" name="additionalprops_' + this.cntr + '_' + i + '" value="' + addproparr[i]['propname'] + '"></div>';
		}
		$('[name="type' + this.cntr + '"]').val('string');
	    return htmlstr;
    }
    this.getPropForm = function (container){
		var wsize=$('#w_edit_size',container).val();
	    return {size:wsize};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'w_table_access';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		return tempobj;
	}

	this.hasFilter = function (){
		return false;
	}	

    return this;
}

extend(w_table_access, w_base);

$.widgets.RegisterWidget('table_access', w_table_access);
