function w_theme_attr_value_select(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'w_theme_attr_value_select';
	// Additional properties array
	var _self = this;
	
	var tables=null;
	this.correspondingWidget;
	
	
	this.changevalue = function(fieldname, value) {
		var widgetContainer = _self.form_element.find(".js-widget-container").first();
		widgetContainer.empty();
		this.correspondingWidget = $.widgets.get_widget(value.themeColumn);			
		this.correspondingWidget.assign(widgetContainer);
	};
	
    this.assign = function(form_element, value) {
		this.form_element = form_element;
		
		var widgetContainer = $('<div class="js-widget-container"/>');
		this.form_element.append(widgetContainer);
		if (this.relationlist.length === 1) {
			var span = $('<span class="muted">Waiting for corresponding field change</span>');
			widgetContainer.append(span);
		} else {
			var span = $('<span class="muted">Unable to find related theme_attribute_select</span>');
			widgetContainer.append(span);
		}
    }
    
	// Getting value of input field
    this.getVal = function () {
		if (this.correspondingWidget) {
			return this.correspondingWidget.getVal();
		} else {
			return false;
		}
    }

    this.viewPropForm = function (container){
		var ps = $("<label>Corresponding theme attribute fieldname</label><input type='text' class='js-attribute-fieldname-to-watch' />");
		container.append(ps);
		$('.js-attribute-fieldname-to-watch', container).val("");
		if (this.object.widget.properties && this.object.widget.properties.themeAttributeSelectToWatch) {
			$('.js-attribute-fieldname-to-watch', container).val(this.object.widget.properties.themeAttributeSelectToWatch);
		}
    }
	
    this.getPropForm = function (container){
		var val = $('.js-attribute-fieldname-to-watch', container).val();
		var obj = {filter: {}};
		obj.filter.themeAttributeSelectToWatch = val;
		return obj;
    }
 	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'select';		
		tempobj.sWidget = 'select';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}	
	//extend(this, new w_edit(object)); 		
    return this;
}


extend(w_theme_attr_value_select, w_base);
$.widgets.RegisterWidget('theme_attr_value_select', w_theme_attr_value_select);