
function w_attachment(object) {
	// Object for creation of widget
    this.object = object;
	// Common name of widget
	var widget_name = 'attachment';
	this.user_visible = true;
	this.rname = 'attachment';
	// Additional properties array
	var addproparr = new Array();
	this.form_element = '';
	var images;
	var pluscard;
	
	this.addImg=function(user_id,path, title, link, mode){
		var exist=false;
		
		if(title===undefined || title==='undefined' || title==='')
			title=get_filename(path);
		
		if(mode=='view')
			var card=$('<li class="span2 img_path"> \
<a href="'+link+'" target="_blank" class="thumbnail"><img src="/img/file.jpeg" class="smallimage" alt="" onload="" style="margin-left: auto; margin-right: auto; margin-top: 2px; margin-bottom: 2px;" ><p>'+title+'</p></a>\
</li>');
		else
			var card=$('<li class="span2 img_path"> <i title="Delete file" class="icon-remove close"/>\
<a href="'+link+'" target="_blank" class="thumbnail"><img src="/img/file.jpeg" class="smallimage" alt="" onload="" style="margin-left: auto; margin-right: auto; margin-top: 2px; margin-bottom: 2px;" ></a><input type="text" class="span2" id="title" value="'+title+'"/>\
</li>');
		images.prepend(card);
		
		
		card.data('path', path);
		card.data('user_id', user_id);
		$('.close', card).click(function() {
			card.remove();
		});
	}
	
   this.assign = function(form_element, value){

		var widget=this;
		this.form_element = form_element;
		
		var userDataString = $.cookie('userData') || localStorage.user || '{}';
		var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));				
	
		var current_image_path=localStorage.getItem('current_image_path');		
		
		images = $('<ul class="thumbnails"/>');
		form_element.append(images);
		
		pluscardvar=$('<div class="span2 addcard"> \
<a href="#" class="thumbnail"><i class="icon-plus"></i>Add file</a>\
</div>');
		images.append(pluscardvar);
				
		
		var options = new Object();
		options.filter='';
		pluscardvar.click(function() {
			OpenFileDlg(function(path){
				widget.addImg(user.id, path, "", "/fm/op?cmd=file&target="+gethash(path));
			},options);
		});
		
		
		if(value!=undefined && value!=''){	
			if(widget.doc)
				var row_id=widget.doc.id;		
			else
				var row_id=-1;
			files=value.split(';');
			for (i=0; i < files.length; i++) {
				s=files[i].split('###');
				if(s!=undefined && s.length>0){
					p='';
					n='';
					user_id='';
					if(s.length==2){
						p=s[0];
						n=s[1];
					}
					else if (s.length==3){
						p=s[1];
						n=s[2];
						user_id=s[0];						
					}
					else
						continue;
					this.addImg(user_id,getpath(p), n, '/dataset/file?f='+widget.metaid+'&row_id='+row_id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id);
				}
			}
		}

    }
	// HTML element creation
    this.assign2 = function(form_element, value){

		var widget=this;
		this.form_element = form_element;

		//var current_image_path=$.cookie("current_image_path");
		var current_image_path=localStorage.getItem('current_image_path');		
		
		if(current_image_path==null && $.cookie('userData')!=null){
			userdata=$.cookie('userData');
			ud=JSON.parse(decodeURIComponent(userdata == 'undefined' ? '{username:""}' : userdata));
			current_image_path='/' + ud.username;
		}
		
		var label = $('<span>Current path for upload (click here to change):</span>');
		form_element.append(label);

		
		var cpath = $('<div></div>');
		label.append(cpath);
		label.click(function() {
			OpenFolderDlg(function(path){
				localStorage.setItem('current_image_path', path);
				//$.cookie('current_image_path', path);
				current_image_path=path;
				cpath.html(current_image_path);
			});
		});	
		
		
		cpath.html(current_image_path);
		

		var btnsdiv=$('<div style=""></div>');
		form_element.append(btnsdiv);
		var select_image=$('<button class="btn" type="button">Select file in the cloud</button>');
		btnsdiv.append(select_image);
		
		var uploaddiv=$('<div class="fileUpload btn"><span>Upload</span><input type="file" id="image_upload" class="upload" /></div>');
		var upload_image=$("input", uploaddiv);
		btnsdiv.append(uploaddiv);		
		var options = new Object();
		options.filter='';
		var label = $('<label>File list</label>');
		form_element.append(label);		
		images = $('<div style="display: inline-block;text-align: right;"/>');
		form_element.append(images);		
		
		
		upload_image.change(function(){
			uploadfilename='';
			var control = document.getElementById("image_upload");
			var i = 0,
			files = control.files,
			len = files.length;		
			if(len==0){			
				return false;
			}			
			if (current_image_path=='' || current_image_path===undefined || current_image_path==null){
				alert('You should select dir to upload!');
				return false;
			}
			uploadfilename=current_image_path+dir_separator+files[0].name;		
			
			var form = new FormData();
			form.append("cmd", "upload");
			dir=get_directory(uploadfilename);
			form.append("target", gethash(dir));
			form.append("files", control.files[0]);			
			
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				console.log("Отправка завершена");
				widget.addFile('', uploadfilename,'');		
			};
			xhr.open("post", "/fm", true);
			xhr.send(form);
		});
		 
		select_image.click(function() {
			OpenFileDlg(function(path){
				var userDataString = $.cookie('userData') || localStorage.user || '{}';
				var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));				
				widget.addFile('', path,'');
			},options);
		});
		
		
		if(value!=undefined && value!=''){			
			files=value.split(';');
			for (i=0; i < files.length; i++) {
				s=files[i].split('###');
				if(s!=undefined && s.length>0){
					p='';
					n='';
					user_id='';
					if(s.length==2){
						p=s[0];
						n=s[1];
					}
					else if (s.length==3){
						p=s[1];
						n=s[2];
						user_id=s[0];						
					}
					else
						continue;
					this.addFile(user_id,getpath(p), n);
				}
			}
		}

    }
    
	// Getting value of input field
    
    this.getVal = function (){
		var files=new Array();
		$( ".img_path" , this.form_element).each(function( index ) {
			var file_div=$(this);
			title=$('#title',this).val();
			path=file_div.data("path");
			
			
			var userDataString = $.cookie('userData') || localStorage.user || '{}';
			var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));
			if(user.id)
				user_id=user.id;
			else		
				user_id=file_div.data("user_id");
			files[files.length]=user_id+'###'+gethash(path)+'###'+title;
		});	
		return files.join(';');
    }
	this.hasFilter = function (){
		return false;
	}

    this.getUserVal=function (val){	
		if(this.gr_fn=='none' )
			return '';
		
		if(this.gr_fn!=null || typeof val =='number' )
			return val;
		res='';
		var links=new Array();		
		if(val!=undefined && val!=''){			
			var files=val.split(';');	
			
			
			var unique = 'img_'+randomString(10);
			var widget=this;
			if(files.length>0){				
				res='';
				var default_value=''
				for (i=0; i < files.length; i++) {
					s=files[i].split('###');
					if(s!=undefined && s.length>0 ){
						p='';
						n='';
						user_id='';
						if(s.length==2){
							p=s[0];
							n=s[1];
						}
						else if (s.length==3){
							p=s[1];
							n=s[2];
							user_id=s[0];						
						}
						else
							continue;
						if(n==''){
							n=default_value;										
						}
						link='/dataset/file?f='+widget.metaid+'&row_id='+widget.doc.id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id;							
						if(n=='')
							n='file';
						if(res!='')
							res+=', ';
						res+='<a href="'+link+'" >'+n+'</a>';

					}
				}
			}
		}
		
	    return res;
    }
	
   
    this.viewPropForm = function (container){
	   
		var ps=$("<label>Default image directory</label><input type='text' id='image_dir' value=''/><br/>");
		var input = $('#image_dir', ps);
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.image_dir!=undefined   )
		   $('#image_dir', ps).val(this.object.widget.properties.image_dir);

		var img=$('<button class="btn" type="button">Open</button>');
		container.append(img);
		
		var options = new Object();
		 
		img.click(function() {
			OpenFileDlg(function(path){
				input.val(path);
				input.trigger('change');
			},options);
		});
		   
		return;
    }    
    this.getPropForm = function (container){		
	    return {image_dir: $('#image_dir', container).val()};
    } 
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'file';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sWidth = '12px';

		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_attachment, w_base);

$.widgets.RegisterWidget('attachment', w_attachment);

