
/**
* A filter widget based on data in a table column.
* 
* @class AutocompleteFilterWidget
* @constructor
* @param {object} $Container The jQuery object that should contain the widget.
* @param {object} column The column metadata.
* @param {object} dt_table The table object.
* @param {object} filtervalue The initial filter value.
*/	
$.widget_filter = function( $Container, column, dt_table, filtervalue ){
	var widget = this;
	widget.column = column;		
	widget.$Container = $Container;
	widget.oDataTable = dt_table;
	widget.asFilters = [];
	widget.sSeparator = ';';

	
	widget.control = $('<div class="input-group"></div>');
	widget.$Container.append( widget.control );	
	widget.$TermContainer=$('<div></div>');
	widget.$Container.append( widget.$TermContainer );	

	function init(){
		widget.control.empty();
		widget.column.widget_object.assign(widget.control,'');
		var btn_div=$('<div class="input-group-append"><span class="input-group-text addconstraint"><i class="fas fa-search-plus"></i></span></div>');
		widget.control.append(btn_div);
		btn = $('.addconstraint',btn_div);
		btn.click(function() {		
			var val=widget.column.widget_object.getVal();
			widget.add(val);
			widget.fnFilter();
		});
		widget.btnOk=btn;
	}
	init();

	

	
	//widget.$label = $('<label for="f_' + widget.oColumn.sName + '">' + widget.oColumn.sTitle + '</label><br>');
	//widget.$Container.append( widget.$label );
	this.add=function(val){
		
		if ( val =='' ) {
			// The blank option is a default, not a filter, and is re-selected after filtering
			return;
		}
		if  (!($.inArray(val, widget.asFilters))) {
			return;
		}
		var strval=widget.column.widget_object.getUserVal(val);
		sText = $( '<div>' + strval + '</div>' ).text();
		var $TermLink = $( '<a class="filter-term" href="#"></a>' )
			.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
			.text( sText )
			.click( function() {
				// Remove from current filters array
				widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
					return sFilter != val;
				} );
				$TermLink.remove();
				widget.fnFilter();
				return false;
			} );
		widget.asFilters.push( val );		
		widget.$TermContainer.prepend( $TermLink );		
		init();
	}
	this.clear=function(){
		widget.asFilters=[];
		widget.$TermContainer.empty();
	}
	
	
	//widget.$Container.append( btn );
	if(filtervalue){
		widget.add(filtervalue);		
	}
	
};

/**
* Perform filtering on the target column.
* 
* @method fnFilter
*/
$.widget_filter.prototype.fnFilter = function() {
	var widget = this;
	var asEscapedFilters = [];
	if ( widget.asFilters.length > 0 ) {
		$.each( widget.asFilters, function( i, sFilter ) {
			asEscapedFilters.push( sFilter );
		} );
		widget.oDataTable.fnFilter( asEscapedFilters.join(';') , widget.column.fieldname );
	} else { 
		// Clear any filters for this column
		widget.oDataTable.fnFilter( '', widget.column.fieldname );
	}
	setTimeout( function() {
		$('.g_layer',$('#layer_list')).each(function(index){
			ml=$(this).data("mlayer");
			ml.redraw();
		});							
	}, 150 );
};

