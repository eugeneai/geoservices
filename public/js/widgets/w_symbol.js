﻿function w_symbol(object) {
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'symbol';
	// Additional properties array
	this.user_visible = true;
	this.rname = 'Map symbol';	

	var input;
	
	this.drawsymbol=function(canvas, txt){		
		ctx=canvas[0].getContext('2d');
		w=canvas[0].width;
		h=canvas[0].height;
		ctx.fillStyle = "white";
		ctx.fillRect(0, 0, w, h);
		o=[];
		lines=txt.split('\n');
		for(var i=0; i<lines.length; i++){
			pnts=lines[i].split(' ');
			if(pnts.length==2){
				x=parseFloat(pnts[0]);
				y=parseFloat(pnts[1]);
				if(!isNaN(x) && !isNaN(y))
					o.push([x,y])
			}
		}
		if(o.length>1)		
			draw_object(ctx, o, w/2, h/2, 40, 0);
	}
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		//var label = $('<div style="height: 30px;width:200px;display: inline-block;text-align: right;">'+this.object.title+'</div>');
		var label = $('<label for="'+this.object.fieldname+'">'+this.object.title+'</label>');
		input =  $('<textarea title="'+this.object.description+'" style="padding: 0px; width: 450px; height:80px;  margin-bottom: 1px;" id ="' + this.object.fieldname + '"></textarea>');
		form_element.append(input);
		//var el = $('#' + this.object.fieldname, form_element);
		if(value!=undefined && value!='')
			input.val(removeescapeHtml(value));
		
		input.attr( 'name', this.object.fieldname);
		input.attr( 'widget', widget_name);		
		
		
		canvas=$('<canvas id="rule_sign_head" width="80" height="45">Обновите браузер</canvas>');
		form_element.append(canvas);
		var widget=this;
		input.keyup(function(){
			widget.drawsymbol(canvas, input.val());
		});
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';		
	    return input.val();
    }
	
	this.getUserVal=function (val){
		//YYYY-MM-DD HH:MM:SS
		if(this.gr_fn=='none' )
			return '';
		if (val==undefined || val=='')
			return '';
		
	    return val;
    }
    
	// Getting JSON of field for further saving

	// Additional properties form
    this.viewPropForm = function (container){
			
    }    
    
    this.getPropForm = function (container){		
		
	    return {};
    }
   
	
	this.hasFilter = function (){
		return false;
	}

    return this;
}

extend(w_symbol, w_base);
$.widgets.RegisterWidget('symbol', w_symbol);