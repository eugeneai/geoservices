function w_date(object) {
	// Object for creation of widget
    this.object = object;
	// Common name of widget
	this.widget_name = 'date';
	this.fieldtype = 'date';
	// Additional properties array
	var addproparr = new Array();
	this.user_visible = true;
	this.rname = 'Дата';
	// HTML element creation
	this.setval = function(value){
		if(value!=undefined && value!=''){
				parts = value.match(/\d+/g);
				var res=parts[2]+'.'+parts[1]+'.'+parts[0];
				this.input.val(res);			
			}
	};
    this.assign = function(form_element, value){	
		var cont=form_element;
		this.form_element = form_element;		
		//var label = $('<div style="height: 24px;display: inline-block;text-align: right;">'+this.object.title+'</div>');
		var label = $('<label for="inp_'+this.object.fieldname+'">'+this.object.title+'</label>');
		if(this.object.description)
			var select_date = $('<input title="'+this.object.description+'" class="form-control " type="text" />');
		else
			var select_date = $('<input title="" class="form-control " type="text" />');
		this.input=select_date;
		//select_date.attr('id', this.object.fieldname);
		var parts;
		if(value!=undefined && value!=''){
			parts = value.match(/\d+/g);
			var res=parts[2]+'.'+parts[1]+'.'+parts[0];
			select_date.val(res);			
		}
		/*
		if(this.lvis){
			form_element.append(label);
		}*/
		form_element.append(select_date);
		select_date.datepicker({
				onSelect: function(dateText) {
					cont.change();				
				},
				dateFormat: 'dd.mm.yy'
			});
		//select_date.addClass('w_date');
		select_date.attr('widget', this.widget_name);
		this.select_date=select_date;
		
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_hour!=undefined  && this.object.widget.properties.use_hour=='1' ){
			var label = $('<br/><span class="t_label">H:</span>');
			var input = $('<input class="doubledigit" id="hour" type="text"/>');
			form_element.append(label);
			form_element.append(input);
			input.numberTest({default_point: ".", before:2, after:0, min:0, max:24} );
			if(value!=undefined && value!=''){
				input.val(parts[3]);
			}			
			label.addClass('t_label1');
			input.change(function(){
				cont.change();
			});
		}
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_minute!=undefined  && this.object.widget.properties.use_minute=='1' ){
			var label = $('<span class="t_label">M:</span>');
			var input = $('<input class="doubledigit" id="minute" type="text"/>');		
			form_element.append(label);
			form_element.append(input);
			input.numberTest({default_point: ".", before:2, after:0, min:0, max:59} );
			if(value!=undefined && value!=''){
				input.val(parts[4]);
			}			
			input.change(function(){
				cont.change();
			});			
		}
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_second!=undefined  && this.object.widget.properties.use_second=='1' ){
			var label = $('<span class="t_label">S:</span>');
			var input = $('<input class="doubledigit"  id="second" type="text"/>');		
			form_element.append(label);
			form_element.append(input);
			input.numberTest({default_point: ".", before:2, after:0, min:0, max:59} );
			if(value!=undefined && value!=''){
				input.val(parts[5]);
			}			
			input.change(function(){
				cont.change();
			});			
		}
    }
    
	// Getting value of input field
    this.getVal = function (){
		val=this.select_date.val();
		if(val=='') return '';
		var parts = val.match(/\d+/g);
		var res=parts[2]+'-'+parts[1]+'-'+parts[0];
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_hour!=undefined  && this.object.widget.properties.use_hour=='1' ){
			val=$('#hour',this.form_element).val();
			if(val===undefined || val.length==0)
				val='00';
			else if(val.length==1)
				val='0'+val;
				
			res+=' '+val+':';
			
			if(this.object.widget.properties!=undefined && this.object.widget.properties.use_minute!=undefined  && this.object.widget.properties.use_minute=='1' ){
				val=$('#minute',this.form_element).val();
				if(val===undefined || val.length==0)
					val='00';
				else if(val.length==1)
					val='0'+val;
			
				res+=val+':';
			}
			else
				res+='00:';
			if(this.object.widget.properties!=undefined && this.object.widget.properties.use_second!=undefined  && this.object.widget.properties.use_second=='1' ){
				val=$('#second',this.form_element).val();
				if(val===undefined || val.length==0)
					val='00';
				else if(val.length==1)
					val='0'+val;
			
				res+=val;
			}
			else
				res+='00';
		}
	    return res;
    }
    
	
    this.getUserVal=function (val){
		//YYYY-MM-DD HH:MM:SS
		if (val==undefined || val=='' || val==null || val=='NULL')
			return '';
		if(this.gr_fn=='none' )
			return '';
		if(this.gr_fn=='count')
			return val;
		var parts = val.match(/\d+/g);
		if(parts.length<3)
			return '';
		var res=parts[2]+'.'+parts[1]+'.'+parts[0];
		
		
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_hour!=undefined  && this.object.widget.properties.use_hour=='1' ){
			res+=' '+parts[3]+'h';
		}
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_minute!=undefined  && this.object.widget.properties.use_minute=='1' ){
			res+=', '+parts[4]+'m';
		}
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_second!=undefined  && this.object.widget.properties.use_second=='1' ){
			res+=', '+parts[5]+'s';
		}
	    return res;
    }

	// Additional properties form
    this.viewPropForm = function (container){
	   
		var ps=$("<label>Hour</label><input type='checkbox' id='use_hour' value='1'/><br/>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_hour!=undefined  && this.object.widget.properties.use_hour=='1' )
		   $('#use_hour', container).prop("checked", true);

		var ps=$("<label>Minute</label><input type='checkbox' id='use_minute' value='1'/><br/>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_minute!=undefined  && this.object.widget.properties.use_minute=='1' )
		   $('#use_minute', container).prop("checked", true);

		var ps=$("<label>Second</label><input type='checkbox' id='use_second' value='1'/>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_second!=undefined  && this.object.widget.properties.use_second=='1' )
		   $('#use_second', container).prop("checked", true);
		   
		return;
    }    
    this.getPropForm = function (container){
		var use_hour;
		if( $('#use_hour',container).prop("checked") )
			use_hour='1';
		else
			use_hour='0';

		var use_minute;
		if( $('#use_minute',container).prop("checked") )
			use_minute='1';
		else
			use_minute='0';

		var use_second;
		if( $('#use_second',container).prop("checked") )
			use_second='1';
		else
			use_second='0';		
		
	    return {use_hour: use_hour, use_minute:use_minute, use_second:use_second};
    }
	this.olap = function(node, value){	
		this.init_olap(node, value);		
		this.group.append($('<option value="groupby_hour">group by hour</optiion>'));
		this.group.append($('<option value="groupby_day">group by day</optiion>'));
		this.group.append($('<option value="groupby_week">group by week</optiion>'));
		this.group.append($('<option value="groupby_month">group by month</optiion>'));
		this.group.append($('<option value="groupby_quarter">group by quarter</optiion>'));
		this.group.append($('<option value="groupby_year">group by year</optiion>'));
		this.group.append($('<option value="groupby_decade">group by decade </optiion>'));
	}  
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = 'text';
		tempobj.type = 'widget_proxy';		
/*		tempobj.submit ='Ok';
		tempobj.cancel ='Cancel';		*/
		tempobj.sWidget = 'date';
		tempobj.sWidth = '1px';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'DateRangeFilterWidget';
		tempobj.sWidth = '30px';
		return tempobj;
	}
	this.hasFilter = function (){
		return true;
	}
	
    return this;
}
extend(w_date, w_base);
$.widgets.RegisterWidget('date', w_date);