function editcell(dt_table, doc_description, cell){	
	if(doc_description.rights.charAt(1)=='0')
		return;
	if(!cell.is("td"))
		cell=cell.parents('td');
	// var dv = $('<div id="test_div_id" style="width:100%"></div>');			
	// cell.append(dv);
	

	
	var fieldname=cell.attr('fieldname');
	var row=cell.parent("tr");
	var doc_data_base=row.data('doc');
	var value=doc_data_base[fieldname];
	var title=fieldname;
	var widget;
	for (var i = 0; i < doc_description.columns.length; i++) {					
		if(doc_description.columns[i].fieldname==fieldname){
			widget=doc_description.columns[i].widget_object;
			if(doc_description.columns[i].title && doc_description.columns[i].title!='')
				title=doc_description.columns[i].title;
			
			

			break;
		}
	}
	var test_div=$('<div title="Cell editing">\
	<form >\
		<div class="form-group">\
			<label for="">'+title+'</label>\
			<div class="editable-input" ></div>\
		</div>\
		<div class="form-group form-check">\
			<input type="checkbox" class="form-check-input" id="setall">\
			<label class="form-check-label" for="exampleCheck1">Set value to all empty.</label>\
		</div>\
		<button id="btn_ok" type="submit" class="btn btn-primary btn-sm editable-submit">Ok</button>\
		<button id="btn_cancel" type="button"= class="btn btn-default btn-sm editable-cancel">Cancel</button>\
  	</form>\
  	</div>');
	if(doc_description.rights.charAt(1)!='4')
		$('#setall',test_div).hide();
	
	
	function save(){
		var for_all=false;		
		if($('#setall',test_div).prop("checked") && doc_description.rights.charAt(1)=='4'){
			for_all = confirm("Отменить операцию нельзя. Вы уверены?");
		}
		var value=widget.getVal();
		res={};
		res.dataset_id=doc_description.metaid;
		if(value===undefined || value=='')
			v='NULL';
		else
			v=value;
		res[fieldname]=v;
		$.each(dt_table.FilterValues, function(index, value) {
			if(value!='' && value!==undefined)
				res['f_'+index]=value;					
		});		
		if(!for_all){			
			res.f_id=doc_data_base.id;			
		}
		else{
			res.group_update=true;
		}

		str_res=JSON.stringify({document:res});
		$.ajax({
			type: "POST",
			url: "/dataset/update?f=" + doc_description.metaid,
			contentType : 'application/json',
			data: str_res,
			success: function(msg){
				msg=JSON.parse(msg);
				if(msg.status=='ok'){
					test_div.dialog("destroy");					
					doc_data_base[fieldname]=value;
					widget.cntr=doc_data_base.id;
					element = widget.getUserVal(value);					
					var min_width='20px';
					if(widget.getDatatableProperties().sWidth!=undefined)
						min_width=widget.getDatatableProperties().sWidth;										
					cell.html('<div>'+element+'</div>');
					cell.attr('title',removeescapeHtml(element));
					if(for_all)
						dt_table._fnReDraw();
				}
				else
					alert(msg);
			}
		});
	}
	$('#btn_ok',test_div).click(function(){
		save();
	});
	$('test_div').bind('keydown', function(e) {
		if (e.which == 27) {
			test_div.dialog("destroy");
			var e = $.Event('keydown');
			e.keyCode = 27; 						
		}
		else if (e.which == 13) {
			save();
		}				
	 });
	
	$('#btn_cancel',test_div).click(function(){
		test_div.dialog("destroy");
		//dv.poshytip('destroy');
		var e = $.Event('keydown');
		e.keyCode = 27; 
		dv.remove();
	});
	test_div.dialog({
  width: 500
});
	// cell.popover({
	// 	template: test_div,
	// 	placement: "top",
	// 	html: true
	// }); 
	var d = $('.editable-input', test_div);
	widget.assign( d, value);
	setTimeout(function(){
		if($('input[type="text"]:first', test_div).length>0)
			$('input[type="text"]:first', test_div).focus();
		else
			$('textarea:first', test_div).focus();
	},500);
	if(widget)
		widget.activateDrawing();
	$("form", test_div).submit(function(e){
		e.preventDefault();
	});
};