function w_wps_method(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'wps_method';


	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		var select_input = $( '<select id="'+this.object.fieldname+'"/>' );
		form_element.append(select_input);
		var host=null;
		var port=null;
		var path=null;
		var paramcontainer=null;
		var jsbody=null;
		var jsname=null;
		var params_json="";
		var wpstype=null;
		function changeMethodList(){
			select_input.empty();
			if (port==null || host==null || path==null || host.val()=='')
				return;
			var p={identifier: '', wpshost: host.val(), wpsport: port.val(), wpspath: path.val() };
			//var exWPSM = new WPSMethod(p, MethCallback);
			//var maincont = exWPSM.DescribeProcess("makeParametersForm", "paramholder");
			var exWPSS = new WPSServer(p, null);
			exWPSS.GetCapabilitiesAsItem(function (mname){
				select_input.append($('<option value="'+mname+'">'+mname+'</option>'));
			}); 			
		}
		setTimeout(function(){
			host=$('#wpshost',form_element.parent());
			port=$('#wpsport',form_element.parent());
			path=$('#wpspath',form_element.parent());
			wpstype=$('#wpstype',form_element.parent());
			jsbody=$('#jsbody',form_element.parent());
			jsname=$('#name',form_element.parent());
			
			if (host.length>0){
				host.bind('change', function (e){
					changeMethodList();
				});
			}			
			if (port.length>0){
				port.bind('change', function (e){
					changeMethodList();
				});
			}	
			if (path.length>0){
				port.bind('change', function (e){
					changeMethodList();
				});
			}
			if (jsname.length>0){
				jsname.bind('change', function (e){
					generatejsbody();
				});
			}			
		}, 1000);
		select_input.bind('change',function(e){
			if (port==null || host==null || path==null || host.val()==''|| select_input.val()=='')
				return;
			var p={identifier: select_input.val(), wpshost: host.val(), wpsport: port.val(), wpspath: path.val() };
			var exWPSS = new WPSServer(p, null);
			exWPSS.DescribeProcessAsJSON(function(param_json){
				paramcontainer=$('#params',form_element.parent());
				paramcontainer.empty();
				createparamsform(param_json,paramcontainer);	
				params_json=param_json;
				generatejsbody();
			});
		
		} );
		function generatejsbody(){
			if(wpstype.val()=="js") return;
			jsbody.empty();
			if (port==null || host==null || path==null || jsname==null || host.val()==''|| select_input.val()=='')
				return;
			var txt="function "+jsname.val()+"("
			for(var i=0; i<params_json.length; i++){
				if(i!=0) txt+=", ";
				txt+=params_json[i].fieldname;
			}
			txt+="){\n";
			txt+="var parstr = "
			for(var i=0; i<params_json.length; i++){
				if(i!=0) txt+='+","+';
				txt+='"'+params_json[i].fieldname+':"'+params_json[i].fieldname;
			}
			txt+=";\n";
			txt+='var method = new WPSMethod({wpshost: "' + host.val() + '", wpsport: "' + port.val() + '", wpspath: "' + path.val() + '", identifier: "' + select_input.val() + '", params: { parstr }});\n';
			txt+="method.Execute();";
			txt+="}\n";
			jsbody.val(txt);
		};
		/*
		$.getJSON("/geothemes/gettables",{ajax: 'true'}, function(j){
		 //alert('ok');
          var str_options = '';
          for (var i = 0; i < j.length; i++) {
            str_options += '<option value="'+j[i].schema+'.'+ j[i].name+ '">' + j[i].schema+'.'+j[i].name+ '</option>';
          }
		  select_input.html(str_options);
        });*/
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname,this.form_element).val();
    }
    
    this.viewPropForm = function (container){
		var sz=$("<label>Size of field</label><input type='text' name='w_wps_method_size' id='w_wps_method_size' placeholder='20'/>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined)
			$('#w_wps_method_size',container).val(this.object.widget.properties.size);
		return;
    }
    this.getPropForm = function (container){
		var wsize=$('#w_wps_method_size',container).val();
	    return {size:wsize};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'edit';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_wps_method, w_base);
$.widgets.RegisterWidget('wps_method', w_wps_method);