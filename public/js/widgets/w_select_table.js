function w_select_table(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'select_table';


	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		input = $( '<select id="'+this.object.fieldname+'"/>' );
		form_element.append(input);
		$.getJSON("/geothemes/gettables",{ajax: 'true'}, function(j){
          var options = '';
          for (var i = 0; i < j.length; i++) {
			if(i==0)
				user_schema=j[i].schema;
            options += '<option value="'+j[i].schema+'.'+ j[i].name+ '">' + j[i].schema+'.'+j[i].name+ '</option>';
          }
		  input.html(options);
        });		
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname,this.form_element).val();
    }
    
	// Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<label>Size of field</label><input type='text' name='w_select_table_size' id='w_select_table_size' placeholder='20'/>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined)
			$('#w_select_table_size',container).val(this.object.widget.properties.size);
		return;
    }
    this.getPropForm = function (container){
		var wsize=$('#w_select_table_size',container).val();
	    return {size:wsize};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'edit';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_select_table, w_base);
$.widgets.RegisterWidget('select_table', w_select_table);