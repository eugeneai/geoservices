(function($) {
	/**
	* A filter widget based on data in a table column.
	* 
	* @class DateRangeFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/
	$.ThemeModerationFilterWidget = function($Container, oDataTableSettings, i, widgets) {
		var widget = this;
		widget.iColumn = i;
		widget.oColumn = oDataTableSettings.aoColumns[i];
		widget.$Container = $Container;
		widget.oDataTable = oDataTableSettings.oInstance;
		widget.asFilters = [];
		widget.sSeparator = '';
		widget.iMaxSelections = -1;
		
		widget.$Container.append('<label>Show<label><br><select id="view_moderated_records_mode"><option value="0">all records</option><option value="1">Accepted records</option><option value="2">waiting for modearation</option><option value="3">rejected records</option></select>');

		$('#view_moderated_records_mode', widget.$Container).change(function() {
			var selectedvalue = $('#view_moderated_records_mode').val();
			var searchvalue = '';
			
			$.cookie('moderation_records_view_' + THEME_METAID, selectedvalue);			
			widget.asFilters = new Array();
			if(selectedvalue!='0')
				widget.asFilters.push(searchvalue);
			widget.fnFilter();
		});
		
		if ($.cookie('moderation_records_view_' + THEME_METAID) !== null) {
			setTimeout(function() {
				$('#view_moderated_records_mode', widget.$Container).val($.cookie('moderation_records_view_' + THEME_METAID));
				$('#view_moderated_records_mode', widget.$Container).trigger('change');
			}, 150);
		}
	};


	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.ThemeModerationFilterWidget.prototype.fnFilter = function() {
		var widget = this;
		widget.oDataTable.fnFilter(widget.asFilters, widget.iColumn, true, false);
	};		
	
}(jQuery));