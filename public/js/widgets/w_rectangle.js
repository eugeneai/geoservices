function w_rectangle(object) {
	var widget=this;
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	this.fieldtype = 'polygon';
	var widget_name = 'rectangle';
	// Additional properties array
	this.x1;
	this.x2;
	this.y1;
	this.y2;
    var BoxLayer;
	// HTML element creation
    this.assign = function(form_element, value){
		this.form_element = form_element;
		form_element.addClass('g_rectangle_container');
		var widget=this;
		var tab = $('\
		<div>\
		<div class="row"><div class="col"></div><div class="col"><input class="form-control" id="Y1"  type="text"/></div><div class="col"></div></div>\
		<div class="row"><div class="col"><input class="form-control" id="X1"  type="text"/></div><div class="col"></div><div class="col"><input class="form-control" id="X2"  type="text"/></div></div>\
		<div class="row"><div class="col"></div><div class="col"><input class="form-control" id="Y2"  type="text"/></div><div class="col"></div></div>\
		</div>');

		save = $('<input value="save"  type="button" class="btn"/>');
		open = $('<input value="open" type="button" class="btn"/>');
		form_element.append( tab );
		form_element.append( save );
		form_element.append( open );

		this.x1=$('#X1', form_element);
		this.y1=$('#Y1', form_element);
		this.x2=$('#X2', form_element);
		this.y2=$('#Y2', form_element);
/*
		this.x1.coordinates("999°99\'99\'\'N", {placeholder: "0"} );
		this.y1.coordinates("999°99\'99\'\'N", {placeholder: "0"} );
		this.x2.coordinates("999°99\'99\'\'N", {placeholder: "0"} );
		this.y2.coordinates("999°99\'99\'\'N", {placeholder: "0"} );
*/
		save.click(function() {
			SaveFileDlg(function(path){
				saveFile(path, 'POLYGON(('+widget.x1.val()+' '+widget.y1.val()+','+widget.x1.val()+' '+widget.y2.val()+', '+widget.x2.val()+' '+widget.y2.val()+', '+widget.x2.val()+' '+widget.y1.val()+', '+widget.x1.val()+' '+widget.y1.val()+'))');
			});
		});

		open.click(function() {
			OpenFileDlg(function(path){
			readFile(path, function(data){
				wkt = new Wkt.Wkt();
				try { // Catch any malformed WKT strings
					wkt.read(data);
				} catch (e1) {
					alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
					return;
				}
				widget.setvalue(wkt);
				return;
				var reg = /([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)/;
			var myArray=reg.exec(data);
			widget.x1.val(FromDec2(myArray[1]));
			widget.y1.val(FromDec2(myArray[2]));
			widget.x2.val(FromDec2(myArray[5]));
			widget.y2.val(FromDec2(myArray[6]));
		    widget.x1.trigger('setvalue.coordinate');
			widget.y1.trigger('setvalue.coordinate');
			widget.x2.trigger('setvalue.coordinate');
			widget.y2.trigger('setvalue.coordinate');
			});
			});
		});

		if (value!=undefined && value!=''){
			wkt = new Wkt.Wkt();
			try { // Catch any malformed WKT strings
				wkt.read(value);
			} catch (e1) {
				alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
				return;
			}
			this.setvalue(wkt);
		}

		form_element.click(function() {
			widget.activateDrawing();
		});

		$(document).click(function(event) { 
			if (!$(event.target).closest(form_element).length) {
				$('.g_activate', widget.form_element[0].parentNode).addClass('g_activate well');
				$('#tabs', widget.form_element[0].parentNode).hide();
				$('.g_activate', widget.form_element[0].parentNode).removeClass('g_unactivate');
				widget.form_element.removeClass('g_activate well');
				widget.form_element.addClass('g_unactivate');
				$('#tabs', this.form_element).show();
				
				stop_map_edit(widget);
			}        
		});
	}


	// Getting value of input field
    this.getVal = function (){
	// WKT
	    return 'MULTIPOLYGON((('+this.x1.val()+' '+this.y1.val()+','+this.x1.val()+' '+this.y2.val()+', '+this.x2.val()+' '+this.y2.val()+', '+this.x2.val()+' '+this.y1.val()+', '+this.x1.val()+' '+this.y1.val()+')))';
    }
    // Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		if (val == null) return 'Кликните для редактирвоания'
		else {
			var reg = /([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)/;
			var myArray=reg.exec(val);
			return 'Область('+FromDec2(myArray[1])+' '+FromDec2(myArray[2])+', '+FromDec2(myArray[5])+' '+FromDec2(myArray[6])+')';
		}
    }
	// Additional properties form

    this.viewPropForm = function (container){
		var ps=$("<label>Режим: </label><select id='point_mode' name='point_mode'><option value='single'>Одна точка</option><option value='multi'>Мультиточки</option></select><br>");
		container.append(ps);
		try {
			$('#point_mode',container).val(this.object.widget.properties.mode);
		} catch(e) { }
		var ps=$("<label>Ввод координат: </label><select id='edit_mode' name='edit_mode'><option value='default'>Градусы</option><option value='extended'>Градусы и десятичное представление</option></select>");
		container.append(ps);
		try {
			$('#edit_mode',container).val(this.object.widget.properties.editpointmode);
		} catch(e) { }
		return;
    }
    this.getPropForm = function (container){
	    return {mode: $('#point_mode',container).val(), editpointmode: $('#edit_mode',container).val()};
    }

	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';
		tempobj.sWidget = 'singlepolygon';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'GeometryFilterWidget';
		tempobj.submit ='Ok';
		tempobj.cancel ='Cancel';
		tempobj.onblur = '';
		return tempobj;
	}
	this.hasFilter = function (){
		return true;
	}

    return this;
}

//extend(w_rectangle, w_base);

function setpointval(input, v, lon){
	//tr_coord = FromDec4coord(v, lon);
	input.val(v);
	//input.trigger('setvalue.coordinates');
}

w_rectangle.prototype.setvalue = function(wkt){
	function wraplong(v){
		v=v % 360;
		if(v > 180){
			v= v - 360;
		}
		else if(v<-180){
			v=v+360;
		}
		return v;
	}
	if(wkt.components.length==0)
		return;
	var min_x=wraplong(wkt.components[0][0].x);
	var max_x=wraplong(wkt.components[0][0].x);
	var min_y=wkt.components[0][0].y;
	var max_y=wkt.components[0][0].y;
	pnts=[];
	for(var j=0; j<wkt.components[0].length-1; j++){
		min_x=Math.min(min_x, wraplong(wkt.components[0][j].x));
		min_y=Math.min(min_y, wkt.components[0][j].y);
		max_x=Math.max(max_x, wraplong(wkt.components[0][j].x));
		max_y=Math.max(max_y, wkt.components[0][j].y);
		el=[wkt.components[0][j].y, wkt.components[0][j].x];
		pnts.push(el);

	}
	setpointval(this.x1, min_x, true);
	setpointval(this.x2, max_x, true);
	setpointval(this.y1, min_y, false);
	setpointval(this.y2, max_y, false);
	if(drawnItems!==undefined){
		if(this.geom_object!=undefined){
			drawnItems.clearLayers();//removeLayer(this.geom_object);
		}
		this.geom_object=new L.Polygon(pnts);
		drawnItems.addLayer(this.geom_object);
	}
}

w_rectangle.prototype.activateDrawing = function(){
	var widget=this;
	//g_unactivate
	$('.g_activate', this.form_element[0].parentNode).addClass('g_unactivate well');
	$('#tabs', this.form_element[0].parentNode).hide();
	$('.g_activate', this.form_element[0].parentNode).removeClass('g_activate');
	this.form_element.removeClass('g_unactivate well');
	this.form_element.addClass('g_activate');
	$('#tabs', this.form_element).show();

	add_map_handle(function (gobject, mode){
		drawnItems.addLayer(gobject);
		var wkt = new Wkt.Wkt();
        wkt.fromObject(gobject);
		widget.setvalue(wkt);
	}, 'rectangle', widget);
}

//extend(w_rectangle, w_base);
$.widgets.RegisterWidget('rectangle', w_rectangle);
