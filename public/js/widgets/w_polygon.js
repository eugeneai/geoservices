function w_polygon(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'polygon';
	this.fieldtype = 'polygon';
	
	this.user_visible = true;
	this.rname = 'Полигон';		
	// HTML element creation
    this.assign = function(form_element, value){	
		if(this.object.widget.properties.mode == 'multi')
			this.multi=true;		
		else
			this.multi=false;
		this.g_type='polygon';		
		this.g_assign(form_element, value);
		return;		
    }
	
	// Getting value of input field
    this.getVal = function (){
		return this.getGeometry();
    }    
	
    this.getUserVal=function (val){
			if(this.gr_fn=='none' )
				return '';			
		if(this.gr_fn=='count' || typeof val =='number' )
			return val;
		
		if (val == null) return 'Кликните для редактирования'
		else return 'Полигон';
    }
    
	// Additional properties form
	this.viewPropForm = function (container){
		var ps=$("<label>Режим: </label><select id='polygon_mode' name='polygon_mode'><option value='single'>Один полигон</option><option value='multi'>Мультиполигон</option></select><br>");
		container.append(ps);
		var ps=$("<label>Подпись объекта: </label><input type='text' id='label'/><br>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.label!=undefined && this.object.widget.properties.label!='')
			$('#label',container).val(this.object.widget.properties.label);

		try {
			$('#polygon_mode',container).val(this.object.widget.properties.mode);
		} catch(e) { }
		var ps=$("<label>Ввод координат: </label><select id='edit_mode' name='edit_mode'><option value='default'>Градусы</option><option value='extended'>Градусы и десятичное представление</option></select>");
		container.append(ps);
		try {
			$('#edit_mode',container).val(this.object.widget.properties.editpointmode);
		} catch(e) { }
		return;		
    } 
    this.getPropForm = function (container){
	    return {mode: $('#polygon_mode',container).val(), editpointmode: $('#edit_mode',container).val(), label:$('#label',container).val(), gtype: 'polygon'};
    }
    return this;
}

extend(w_polygon, w_geometry);
$.widgets.RegisterWidget('polygon', w_polygon);