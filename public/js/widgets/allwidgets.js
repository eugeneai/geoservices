
function w_attachment(object) {
	// Object for creation of widget
    this.object = object;
	// Common name of widget
	var widget_name = 'attachment';
	this.user_visible = true;
	this.rname = 'attachment';
	// Additional properties array
	var addproparr = new Array();
	this.form_element = '';
	var images;
	var pluscard;
	
	this.addImg=function(user_id,path, title, link, mode){
		var exist=false;
		
		if(title===undefined || title==='undefined' || title==='')
			title=get_filename(path);
		
		if(mode=='view')
			var card=$('<li class="span2 img_path"> \
<a href="'+link+'" target="_blank" class="thumbnail"><img src="/img/file.jpeg" class="smallimage" alt="" onload="" style="margin-left: auto; margin-right: auto; margin-top: 2px; margin-bottom: 2px;" ><p>'+title+'</p></a>\
</li>');
		else
			var card=$('<li class="span2 img_path"> <i title="Delete file" class="icon-remove close"/>\
<a href="'+link+'" target="_blank" class="thumbnail"><img src="/img/file.jpeg" class="smallimage" alt="" onload="" style="margin-left: auto; margin-right: auto; margin-top: 2px; margin-bottom: 2px;" ></a><input type="text" class="span2" id="title" value="'+title+'"/>\
</li>');
		images.prepend(card);
		
		
		card.data('path', path);
		card.data('user_id', user_id);
		$('.close', card).click(function() {
			card.remove();
		});
	}
	
   this.assign = function(form_element, value){

		var widget=this;
		this.form_element = form_element;
		
		var userDataString = $.cookie('userData') || localStorage.user || '{}';
		var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));				
	
		var current_image_path=localStorage.getItem('current_image_path');		
		
		images = $('<ul class="thumbnails"/>');
		form_element.append(images);
		
		pluscardvar=$('<div class="span2 addcard"> \
<a href="#" class="thumbnail"><i class="icon-plus"></i>Add file</a>\
</div>');
		images.append(pluscardvar);
				
		
		var options = new Object();
		options.filter='';
		pluscardvar.click(function() {
			OpenFileDlg(function(path){
				widget.addImg(user.id, path, "", "/fm/op?cmd=file&target="+gethash(path));
			},options);
		});
		
		
		if(value!=undefined && value!=''){	
			if(widget.doc)
				var row_id=widget.doc.id;		
			else
				var row_id=-1;
			files=value.split(';');
			for (i=0; i < files.length; i++) {
				s=files[i].split('###');
				if(s!=undefined && s.length>0){
					p='';
					n='';
					user_id='';
					if(s.length==2){
						p=s[0];
						n=s[1];
					}
					else if (s.length==3){
						p=s[1];
						n=s[2];
						user_id=s[0];						
					}
					else
						continue;
					this.addImg(user_id,getpath(p), n, '/dataset/file?f='+widget.metaid+'&row_id='+row_id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id);
				}
			}
		}

    }
	// HTML element creation
    this.assign2 = function(form_element, value){

		var widget=this;
		this.form_element = form_element;

		//var current_image_path=$.cookie("current_image_path");
		var current_image_path=localStorage.getItem('current_image_path');		
		
		if(current_image_path==null && $.cookie('userData')!=null){
			userdata=$.cookie('userData');
			ud=JSON.parse(decodeURIComponent(userdata == 'undefined' ? '{username:""}' : userdata));
			current_image_path='/' + ud.username;
		}
		
		var label = $('<span>Current path for upload (click here to change):</span>');
		form_element.append(label);

		
		var cpath = $('<div></div>');
		label.append(cpath);
		label.click(function() {
			OpenFolderDlg(function(path){
				localStorage.setItem('current_image_path', path);
				//$.cookie('current_image_path', path);
				current_image_path=path;
				cpath.html(current_image_path);
			});
		});	
		
		
		cpath.html(current_image_path);
		

		var btnsdiv=$('<div style=""></div>');
		form_element.append(btnsdiv);
		var select_image=$('<button class="btn" type="button">Select file in the cloud</button>');
		btnsdiv.append(select_image);
		
		var uploaddiv=$('<div class="fileUpload btn"><span>Upload</span><input type="file" id="image_upload" class="upload" /></div>');
		var upload_image=$("input", uploaddiv);
		btnsdiv.append(uploaddiv);		
		var options = new Object();
		options.filter='';
		var label = $('<label>File list</label>');
		form_element.append(label);		
		images = $('<div style="display: inline-block;text-align: right;"/>');
		form_element.append(images);		
		
		
		upload_image.change(function(){
			uploadfilename='';
			var control = document.getElementById("image_upload");
			var i = 0,
			files = control.files,
			len = files.length;		
			if(len==0){			
				return false;
			}			
			if (current_image_path=='' || current_image_path===undefined || current_image_path==null){
				alert('You should select dir to upload!');
				return false;
			}
			uploadfilename=current_image_path+dir_separator+files[0].name;		
			
			var form = new FormData();
			form.append("cmd", "upload");
			dir=get_directory(uploadfilename);
			form.append("target", gethash(dir));
			form.append("files", control.files[0]);			
			
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				console.log("Отправка завершена");
				widget.addFile('', uploadfilename,'');		
			};
			xhr.open("post", "/fm", true);
			xhr.send(form);
		});
		 
		select_image.click(function() {
			OpenFileDlg(function(path){
				var userDataString = $.cookie('userData') || localStorage.user || '{}';
				var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));				
				widget.addFile('', path,'');
			},options);
		});
		
		
		if(value!=undefined && value!=''){			
			files=value.split(';');
			for (i=0; i < files.length; i++) {
				s=files[i].split('###');
				if(s!=undefined && s.length>0){
					p='';
					n='';
					user_id='';
					if(s.length==2){
						p=s[0];
						n=s[1];
					}
					else if (s.length==3){
						p=s[1];
						n=s[2];
						user_id=s[0];						
					}
					else
						continue;
					this.addFile(user_id,getpath(p), n);
				}
			}
		}

    }
    
	// Getting value of input field
    
    this.getVal = function (){
		var files=new Array();
		$( ".img_path" , this.form_element).each(function( index ) {
			var file_div=$(this);
			title=$('#title',this).val();
			path=file_div.data("path");
			
			
			var userDataString = $.cookie('userData') || localStorage.user || '{}';
			var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));
			if(user.id)
				user_id=user.id;
			else		
				user_id=file_div.data("user_id");
			files[files.length]=user_id+'###'+gethash(path)+'###'+title;
		});	
		return files.join(';');
    }
	this.hasFilter = function (){
		return false;
	}

    this.getUserVal=function (val){	
		if(this.gr_fn=='none' )
			return '';
		
		if(this.gr_fn!=null || typeof val =='number' )
			return val;
		res='';
		var links=new Array();		
		if(val!=undefined && val!=''){			
			var files=val.split(';');	
			
			
			var unique = 'img_'+randomString(10);
			var widget=this;
			if(files.length>0){				
				res='';
				var default_value=''
				for (i=0; i < files.length; i++) {
					s=files[i].split('###');
					if(s!=undefined && s.length>0 ){
						p='';
						n='';
						user_id='';
						if(s.length==2){
							p=s[0];
							n=s[1];
						}
						else if (s.length==3){
							p=s[1];
							n=s[2];
							user_id=s[0];						
						}
						else
							continue;
						if(n==''){
							n=default_value;										
						}
						link='/dataset/file?f='+widget.metaid+'&row_id='+widget.doc.id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id;							
						if(n=='')
							n='file';
						if(res!='')
							res+=', ';
						res+='<a href="'+link+'" >'+n+'</a>';

					}
				}
			}
		}
		
	    return res;
    }
	
   
    this.viewPropForm = function (container){
	   
		var ps=$("<label>Default image directory</label><input type='text' id='image_dir' value=''/><br/>");
		var input = $('#image_dir', ps);
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.image_dir!=undefined   )
		   $('#image_dir', ps).val(this.object.widget.properties.image_dir);

		var img=$('<button class="btn" type="button">Open</button>');
		container.append(img);
		
		var options = new Object();
		 
		img.click(function() {
			OpenFileDlg(function(path){
				input.val(path);
				input.trigger('change');
			},options);
		});
		   
		return;
    }    
    this.getPropForm = function (container){		
	    return {image_dir: $('#image_dir', container).val()};
    } 
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'file';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sWidth = '12px';

		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_attachment, w_base);

$.widgets.RegisterWidget('attachment', w_attachment);

function removeHtmlSymbols(text) {
	  return text
		  .replace(/&amp;/g, "&")
		  .replace(/&lt;/g, "<")
		  .replace(/&gt;/g, ">")
		  .replace(/&quot;/g, '')
		  .replace(/&#039;/g, '');
	}

/**
* A filter widget based on data in a table column.
* @class AutocompleteFilterWidget
* @constructor
* @param {object} $Container The jQuery object that should contain the widget.
*/	
$.AutocompleteFilterW_Edit = function( $Container, column, dt_table ) {
	var widget = this;
	widget.column = column;		
	widget.$Container = $Container;
	widget.oDataTable = dt_table;
	widget.asFilters = [];
	widget.sSeparator = ';';
	widget.iMaxSelections = -1;
	var thead=$Container.parents('thead');
	var th=$('th[fieldname="'+widget.column.fieldname+'"]', thead);
	
	//widget.$Autocomplete = $( '<input type="text" id="'+widget.column.fieldname+'" size="'+widget.oColumn.sWidth+'"/>' );
	widget.$Autocomplete = $( '<input class="form-control fltr_'+widget.column.fieldname+'" type="text"/>' );
	// setTimeout( function() {
	// 	if(th.width()>0) 
	// 		widget.$Autocomplete.width(th.width());
	// }, 250 );
	// if(th.width()>0)
	// 	widget.$Autocomplete.width(th.width());
	
	widget.$TermContainer = $( '<div/>' );
	
		w_settings = {
			autofill    : true,
			cacheLength : 10,
			max         : 5,
			autofocus   : true,
			highlight   : true,
			mustMatch   : true,
			selectFirst : true,
			delay: 500,
			source		: function (request, response) {
				if(request.term.length<3)
					return;
				var result=[];
				if(widget.column.$AutocompleteSource.indexOf("f=-2")==-1){
					$.ajax({
						url: widget.column.$AutocompleteSource + '&iDisplayStart=0&iDisplayLength=10&iSortingCols=1&iSortCol_0=1&distinct=t&f_' + widget.column.fieldname +'=' + request.term + '&s_fields=' + widget.column.fieldname,
						dataType: "json",						
						success: function(data) {
							for (var i = 0; i < data.aaData.length; i++) {	
								v=data.aaData[i][widget.column.fieldname ];
								v=removeHtmlSymbols(v);
								result.push(v);
							}
							response(result);
						}
					});
				}
			},
			select: function(event, ui) {
				var sSelected = removeHtmlSymbols(ui.item.value), sText, $TermLink, $SelectedOption; 
				ui.item.value = "";
				if ( '' === sSelected ) {
					// The blank option is a default, not a filter, and is re-selected after filtering
					return;
				}
				if  (!($.inArray(sSelected, widget.asFilters))) {
					return;
				}
				sText = $( '<div>' + sSelected + '</div>' ).text();
				var $TermLink = $( '<a class="filter-term" href="#"></a>' )
					.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
					.text( sText )
					.click( function() {
						// Remove from current filters array
						var c_val=$TermLink.text();
						widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
							return sFilter != c_val;
						} );
						$TermLink.remove();
						if ( widget.$TermContainer && 0 === widget.$TermContainer.find( '.filter-term' ).length ) {
							widget.$TermContainer.hide();
						}
						widget.fnFilter();
						return false;
					} );
				widget.asFilters.push( sSelected );
				if ( widget.$TermContainer ) {
					widget.$TermContainer.show();
					widget.$TermContainer.prepend( $TermLink );
				} else {
					widget.$Autocomplete.after( $TermLink );
				}
			
				widget.$Autocomplete.val( '' );
				widget.fnFilter();				
			},				
		};

		widget.$Autocomplete.autocomplete(w_settings, {
			dataType:'json',
			change: function(event, ui) {
				//con.change();
			},
			formatItem: function(row, i, n) {                                                        
			  return row.value;
			},
		});			
	
		widget.$Autocomplete.keypress(function(e) {
			if(e.which == 13) {					
				sSelected=removeHtmlSymbols(widget.$Autocomplete.val());
				if(sSelected==='') return;
				sText = $( '<div>' + sSelected + '</div>' ).text();
				var $TermLink = $( '<a class="filter-term" href="#"></a>' )
					.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
					.text( sText )
					.click( function() {
						// Remove from current filters array							
						var c_val=$TermLink.text();
						widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
							return sFilter != c_val;
						} );							
						$TermLink.remove();							
						if ( widget.$TermContainer && 0 === widget.$TermContainer.find( '.filter-term' ).length ) {
							widget.$TermContainer.hide();
						}							
						widget.fnFilter();
						return false;
					} );
				widget.asFilters.push( sSelected );
				if ( widget.$TermContainer ) {
					widget.$TermContainer.show();
					widget.$TermContainer.prepend( $TermLink );
				} else {
					widget.$Autocomplete.after( $TermLink );
				}				
				widget.$Autocomplete.val( '' );
				widget.$Autocomplete.autocomplete( "close" );
				widget.fnFilter();					
			}
		});
	

	widget.$Container.append( widget.$Autocomplete );
	widget.$Container.append(widget.$TermContainer);
	//widget.fnDraw();
};

/**
* Perform filtering on the target column.
*/
$.AutocompleteFilterW_Edit.prototype.fnFilter = function() {
	var widget = this;
	var asEscapedFilters = [];
	var sFilterStart, sFilterEnd;
	if ( widget.asFilters.length > 0 ) {
		// Filters must have RegExp symbols escaped
		$.each( widget.asFilters, function( i, sFilter ) {
			asEscapedFilters.push( sFilter );
		} );
		// This regular expression filters by either whole column values or an item in a comma list
		sFilterStart = widget.sSeparator ? '(^|' + widget.sSeparator + ')(' : '^(';
		sFilterEnd = widget.sSeparator ? ')(' + widget.sSeparator + '|$)' : ')$';
		widget.oDataTable.fnFilter( asEscapedFilters.join(';') , widget.column.fieldname);
	} else { 
		// Clear any filters for this column
		widget.oDataTable.fnFilter( '', widget.column.fieldname );
	}
	setTimeout( function() {
		$('.g_layer',$('#layer_list')).each(function(index){
			ml=$(this).data("mlayer");
//				if(widget.column.oWidget.metaid==ml.layer_id)
				ml.redraw();
		});							
	}, 150 );
};


function w_boolean(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'boolean';
	this.fieldtype = 'bool';
	this.user_visible = true;
	this.rname = 'Логический тип';		
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		this.input = $( '<select title="'+this.object.description+'" class="form-control" />' );
		var input=this.input;
		form_element.append(this.input);
		var vals={'да':true, 'нет': false, 'неизвестно':'NULL'};
		if(value===undefined || value==='')
			value='NULL';
		for (index in vals) {
			if(vals[index]==value || vals[index]==''+value)
				option = $('<option selected value="'+vals[index]+ '">' + index+ '</option>');
			else
				option = $('<option value="'+vals[index]+ '">' + index+ '</option>');
			input.append(option);
		}
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.input)
			return this.input.val();
		else
			return 'NULL';
    }
    
	// Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='count')
			return val;
		if(this.gr_fn=='none' )
			return '';
			
		if(val==true || val=='true')
			return '<img src="/images/icons/glyphicons_152_check.png"/>';
		else if(val==false || val=='false')
			return '<img src="/images/icons/glyphicons_153_unchecked.png"/>';
		else
			return '';
    }
    
	// Additional properties form
    this.viewPropForm = function (container){
		var ps=$("<label>Checkbox options</label><input type='text' id='select_options'  name='select_options' />");
		container.append(ps);
		try {
			$('#select_options',container).val(this.object.widget.properties.options);
		} catch(e) {
		
		}
		return;
    } 
	
    this.getPropForm = function (container){
		var wsize=$('#select_options',container).val();
	    return {options:wsize};
	}
	
	this.hasFilter = function (){
		return true;
	}	
	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'wboolean';		
		tempobj.sWidget = 'wboolean';
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'BooleanFilterWidget';
		tempobj.sWidth = '12px';

		return tempobj;
	}	
    
	extend(this, new w_edit(object)); 		
    return this;
}

extend(w_boolean, w_base);
$.widgets.RegisterWidget('boolean', w_boolean);
(function($) {
	/**
	* A filter widget based on data in a table column.
	* 
	* @class DateRangeFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/
	$.BooleanFilterWidget = function( $Container, column, dt_table, filtervalue ){
		var widget = this;
		widget.column = column;		
		widget.$Container = $Container;
		widget.oDataTable = dt_table;		
		widget.value = filtervalue;
	

    
        this.input = $( '<select title="Filter" class="form-control" />' );
		var input=this.input;
		widget.$Container.append(this.input);
		var vals={'да':'true', 'нет': 'false', 'Все':'NULL'};
		if(filtervalue===undefined || filtervalue==='')
            widget.value ='NULL';
		for (index in vals) {
			if(vals[index]==widget.value || vals[index]==''+widget.value)
				option = $('<option selected value="'+vals[index]+ '">' + index+ '</option>');
			else
				option = $('<option value="'+vals[index]+ '">' + index+ '</option>');
			input.append(option);
		}
	
		
		this.input.change(function(event, ui) {
            widget.value=widget.input.val();
			
			widget.fnFilter();	
		});
		
		
	};


	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.BooleanFilterWidget.prototype.fnFilter = function() {
		var widget = this;
		if (  widget.value == 'NULL' ) {
            widget.oDataTable.fnFilter( '', widget.column.fieldname );
		} else { 
			widget.oDataTable.fnFilter( widget.value, widget.column.fieldname );			
		}
	};		
	
}(jQuery));﻿function w_calculated(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'calculated';
	// Additional properties array
	this.form_element = '';
	
	this.user_visible = true;
	this.rname = 'Вычисляемое поле';
	this.fieldtype = 'calculated';

	
    this.assign = function(form_element, value){
		this.value=value;
		form_element.html(value);
		//return 'value11111111111';
    }
    
	// Getting value of input field
    this.getVal = function (){
		return this.value;
    }
    
	// Getting JSON of field for further saving
    this.getUserVal=function (val){		
		if(this.gr_fn=='none' )
			return '';

		return val;
	    // return $('#'+this.object.fieldname).val();
    }

	// Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<label>Size of field</label><input type='text' name='w_edit_size' id='w_edit_size' placeholder='20'/>");
		container.append(sz);		
    }
    this.getPropForm = function (container){
		return {};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'autoedit';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_calculated, w_base);
$.widgets.RegisterWidget('calculated', w_calculated);

function w_checkbox(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'checkbox';
	// Additional properties array
	var addproparr = new Array();
	addproparr[0] = new Array();
	addproparr[0]['propname'] = 'options';
	addproparr[0]['proptitle'] = 'Опции';
	addproparr[0]['propdescription'] = 'First, Second..';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		var optlist = '';
		var optarr = explode(',', this.object.widget.properties.options);
		for (var i = 0; i < optarr.length; i++) {
			optlist = optlist + '<input type="checkbox" name="' + this.object.htmlname + '" value="' + optarr[i].replace(/^\s+|\s+$/g, "") + '" name="" />' + optarr[i].replace(/^\s+|\s+$/g, "") + '<br>';
		}
		var unique = this.object.fieldname;
		var tempstr = this.object.title + '<br>' +  optlist;
		form_element.html(tempstr);
		var el = $('#' + unique, form_element);
		el.attr( 'name', unique);
		el.attr( 'widget', widget_name);
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname).val();
    }
    
	// Getting JSON of field for further saving
    // this.getUserVal=function (){
	    // return $('#'+this.object.fieldname).val();
    // }
    
	// Additional properties form
    this.viewPropForm = function (container){
		var ps=$("<label>Checkbox options</label><input type='text' id='select_options'  name='select_options' />");
		container.append(ps);
		try {
			$('#select_options',container).val(this.object.widget.properties.options);
		} catch(e) {
		
		}
		return;
    } 
	
    this.getPropForm = function (container){
		var wsize=$('#select_options',container).val();
	    return {options:wsize};
    }
	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'checkbox';		
		tempobj.sWidget = 'checkbox';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		tempobj.oWidget = this;
		return tempobj;
	}	
    
	extend(this, new w_edit(object)); 		
    return this;
}

extend(w_checkbox, w_base);
$.widgets.RegisterWidget('checkbox', w_checkbox);
﻿
function showfilteredvalues(unique, fieldname, value){
	ntable=$('.'+unique);
	if(ntable.length==0)
		return;
	table=ntable.data('table');
	table.olapreset();
	for (var i = 0; i < table.metadata.columns.length; i++) {
		if (table.metadata.columns[i].fieldname==fieldname) {
			if(table.metadata.columns[i].filter_object && table.metadata.columns[i].filter_object.clear){
				table.metadata.columns[i].filter_object.clear();
				table.metadata.columns[i].filter_object.add(value);
				table.metadata.columns[i].filter_object.fnFilter();
			}
			break;			
		}
	}

}

function w_classify(object) {
	var widget = this;
	// Object for creation of widget	
	this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	this.widget_name = 'Classificator';
	// Additional properties array
	var addproparr = new Array();
	this.user_visible = true;
	this.rname = 'Справочник';
	this.fieldtype = 'integer';
	// id of selected value
	var sel_id = '';
	var ft = new Array();
	this.tables = [];
	this.filter = {};
	this.fields = '';
	if (this.object != undefined && this.object.widget != undefined) {
		var props = this.object.widget.properties;
		if (props != undefined && props.db_field != null && props.db_field != undefined){
			this.fields = props.db_field.join(',');
			this.fields+=','+props.synonym;
		}
	}

	this.assign = function (form_element, value) {
		var widget = this;
		var displayfields = widget.fields.split(',');
		
		if((value=='' || value==null )&& object.default_value!='empty')
			value=object.default_value;

		if (typeof value === 'object' && value !== undefined) {
			widget.val = value.id;
			title = '';
			/*
			for (fname in value) {
				if (displayfields.indexOf(fname)!=-1)
					title+=' ' + value[fname];
			}*/
			for (var i = 0; i < displayfields.length; i++) {
				if (value[displayfields[i]])
					title += ' ' + value[displayfields[i]];
			}
			this.strval = title;
		}
		else {
			var id_val = parseInt(value);
			if (!isNaN(id_val)) {
				this.strval = this.getUserVal(id_val);
			}
			this.val = value;
		}

		this.form_element = form_element;
		var props = this.object.widget.properties;

		this.fieldname = this.object.fieldname;

		var key_val = '';
		if ($.isArray(value)) {
			key_val = value[0];
		}
		else {
			key_val = value;
		}

		switch (props.control_type) {
			case 'autoselect':
				this.input = $('<input title="' + this.object.description + '" type="text" class="form-control" />');
				var input = this.input;
				form_element.append(this.input);
				this.input.prop("disabled", true);
				break
			case 'select':
				this.input = $('<select title="' + this.object.description + '" class="form-control" />');
				var input = this.input;
				form_element.append(this.input);
				this.input.prop("disabled", true);
				break
			case 'radio':
				this.radio_list = $('<div/>');
				form_element.append(this.radio_list);
				break
			default:
				break;
		}

		this.changevalue('test1212121', '');
	}

	// Getting value of input field
	this.getVal = function () {
		if (this.visible != undefined && this.visible == false)
			return '';
		var props = this.object.widget.properties;
		if (props.db_field == null || props.db_field === undefined)
			return '';
		switch (props.control_type) {
			case 'radio':
				return $('input[name="' + this.object.fieldname + '"]:checked', this.form_element).val();
				break
			default:
				if(this.input.val()=='')
					return 'NULL';
				return this.val;
		}

		return this.val;//input.val();
	}

	this.changevalue = function (fieldname, value) {
		if (!this.checkVisibility(fieldname, value))
			return;

		var props = this.object.widget.properties;
		this.filter[fieldname] = value;
		var filter_values = {};
		if (props != undefined && props.filter != undefined) {
			for (fname in props.filter) {
				filter_values[fname] = props.filter[fname];
			}
			for (fname in props.filter) {
				for (fn in this.filter) {
					if (this.filter[fn] != '')
						filter_values[fname] = filter_values[fname].replace(new RegExp('#' + fn, 'g'), this.filter[fn]);
				}
			}
		}
		var ready = true;
		for (fn in filter_values) {
			if (filter_values[fn] != undefined && filter_values[fn] != '' && filter_values[fn].indexOf('#') != -1) {
				ready = false;
				break;
			}
		}

		var filter_str = '';
		for (fn in filter_values) {
			if (filter_values[fn] != undefined && filter_values[fn] != '') {
				filter_str += '&f_' + fn + '=' + filter_values[fn];
			}
		}

		var displayfields = widget.fields.split(',');
		//if(props.db_field==null || props.db_field===undefined)

		switch (props.control_type) {
			case 'autoselect':
				this.input.remove();
				this.input = $('<input class="form-control" type="text"  />');
				var input = this.input;
				this.form_element.append(this.input);
				if (!ready) {
					widget.input.prop("disabled", true);
					return;
				}
				w_settings = {
					autofill: true,
					cacheLength: 10,
					max: 5,
					autofocus: true,
					highlight: true,
					mustMatch: true,
					selectFirst: true,
					source: function (request, response) {
						widget.val='NULL';
						if (request.term.length < 3)
							return;
						$.ajax({
							url: '/dataset/list?f=' + props.dataset_id + '&iDisplayStart=0&iDisplayLength=10&s_fields=id,' + widget.fields + filter_str + '&f_' + displayfields[0] + '=' + request.term,
							dataType: "json",
							success: function (data) {
								console.log(data);
								response($.map(data.aaData, function (item) {
									console.log(item);
									title = '';
									for (var i = 0; i < displayfields.length; i++) {
										if (item[displayfields[i]] && typeof(item[displayfields[i]])!='object' ) {
											if (title != '')
												title += ' ';
											title += item[displayfields[i]];
										}
									}
									//if(props.synonym && props.synonym!='' && props.synonym in item ){
									//	title+=' ('+item[props.synonym]+')'
									//}
									return {
										row: item,
										label: title,
										id: item.id
									};
								}));
							}

						});
					},

					appendTo: widget.form_element
				};
				this.input.autocomplete(w_settings,
					{
						dataType: 'json',
						parse: function (data) {
							return $.map(data, function (item) {
								return {
									data: item,
									label: item.names,
									result: item.names,
									value: item.id
								}
							})
						},
						select: function (event, ui) {
							event.preventDefault();
							sel_id = ui.item.id;
							input.val(ui.item.label);
							if ($('input[title="binom"]').val() == '')
								$('input[title="binom"]').val(ui.item.label);
							if(props.synonym && props.synonym!='' && props.synonym in ui.item.row ){
								synonym=ui.item.row[props.synonym];
								if(synonym && synonym!='' ){
									if(typeof(synonym)=='object'){
										if(synonym.id)
											sel_id=synonym.id;										
									}
									else
										sel_id=synonym;
								}
							}
							widget.val = sel_id;
							widget.sendmessage(widget.val);
						},
	//					change: function( event, ui ) {
		//					widget.val = 'NULL';
			//			},
						search: function (event, ui) {
							widget.val = '';
						},
						formatItem: function (item, i, n) {
							return item.names;
						},
					});
				this.input.on("blur", function () {
					if (widget.val == '') {
						$(this).val('');
					}
				});
				
				if (this.val != '') {
					this.input.val(this.strval);
				};

				break
			case 'select':
				this.input.find('option').remove();
				if (!ready) {
					widget.input.prop("disabled", true);
					return;
				}

				$.ajax({
					url: '/dataset/list?f=' + props.dataset_id + '&iDisplayStart=0&iDisplayLength=200&iSortingCols=1&iSortCol_0=1&s_fields=id,' + this.fields + filter_str,
					dataType: "json",
					method: "POST",
					//data: {term: request.term},
					success: function (data) {
						widget.input.prop("disabled", false);
						widget.input.find('option').remove();
						if (widget.val == '' || widget.val === undefined)
							option = $('<option selected value="">----</option>');
						else
							option = $('<option value="">----</option>');

						widget.input.append(option);

						for (var i = 0; i < data.aaData.length; i++) {
							var str = '';
							for (var j = 0; j < props.db_field.length; j++) {
								if (props.db_field[j] in data.aaData[i] && data.aaData[i][props.db_field[j]] != null)
									str += ' ' + data.aaData[i][props.db_field[j]];
							}
							if (widget.val != '' && widget.val != undefined && widget.val == data.aaData[i].id)
								option = $('<option selected value="' + data.aaData[i].id + '">' + str + '</option>');
							else
								option = $('<option value="' + data.aaData[i].id + '">' + str + '</option>');
							widget.input.append(option);
						}

						if (props.another) {
							if (widget.val != '' && widget.val != undefined && widget.val == -1)
								option = $('<option selected value="-1">' + props.another + '</option>');
							else
								option = $('<option value="-1">' + props.another + '</option>');
							widget.input.append(option);
						}

						setTimeout(function () {
							widget.input.trigger('change');
						}, 500);
					}
				});
				this.input.change(function (select) {
					var newval = widget.input.val();
					if (widget.val == newval)
						return;
					widget.val = newval;
					widget.sendmessage(widget.val);
				});

				//widget.val=input.val();


				break
			case 'radio':
				this.radio_list.empty();
				if (!ready) {
					return;
				}

				$.ajax({
					url: '/dataset/list?f=' + props.dataset_id + '&iDisplayStart=0&iDisplayLength=100&iSortingCols=1&iSortCol_0=1&s_fields=id,' + this.fields + filter_str,
					dataType: "json",
					//data: {term: request.term},
					success: function (data) {
						for (var i = 0; i < data.aaData.length; i++) {
							var str = '';
							for (var j = 0; j < props.db_field.length; j++) {
								if (props.db_field[j] in data.aaData[i] && data.aaData[i][props.db_field[j]] != null)
									str += ' ' + data.aaData[i][props.db_field[j]];
							}
							if (widget.val != '' && widget.val != undefined && widget.val == data.aaData[i].id)
								option = $('<input class="form-control" checked type="radio" name="' + widget.object.fieldname + '" value="' + data.aaData[i].id + '"/> ' + str + '<br/>');
							else
								option = $('<input class="form-control" type="radio" name="' + widget.object.fieldname + '" value="' + data.aaData[i].id + '"/> ' + str + '<br/>');
							widget.radio_list.append(option);
						}
						if (props.another) {
							if (widget.val != '' && widget.val != undefined && widget.val == -1)
								option = $('<input class="form-control" checked type="radio" name="' + widget.object.fieldname + '" value="-1"/> ' + props.another + '<br/>');
							else
								option = $('<input class="form-control" type="radio" name="' + widget.object.fieldname + '" value="-1"/> ' + props.another + '<br/>');
							widget.radio_list.append(option);
						}
						$('input[type=radio]', widget.radio_list).change(function () {
							widget.val = this.value;
							widget.sendmessage(widget.val);
						});
					}
				});
				break
			default:
				break;
		}
	}

	this.getUserVal = function (val) {
		if(this.gr_fn=='none' )
			return '';

		if (val == 'NULL')
			return '';
		if (val === undefined || val == null || val == '') return "";
		if (this.gr_fn == 'count')
			return val.id;
		var props = this.object.widget.properties;
		if (typeof val === 'object' && this.gr_fn != 'groupby') {
			var resstr = '';
			for (fieldname in val) {
				fname = fieldname;

				if (fname != '' && props.db_field.indexOf(fname) != -1 && val[fieldname])
					resstr += val[fieldname] + ' ';
			}
			return resstr;
		}
		if (typeof val === 'object' && this.gr_fn == 'groupby')
			val = val.id;

		if (!isNaN(val)) {
			if (props.another && (val == -1 || val == '-1')) {
				return props.another;
			}
			if (props.db_field == null || props.db_field === undefined)
				return val;
			fields = props.db_field.join(',');
			res = JSON.parse($.ajax({
				type: "GET",
				url: '/dataset/list?f=' + props.dataset_id + '&iDisplayStart=0&iDisplayLength=1&s_fields=' + fields + '&f_id=' + val,
				async: false,
			}).responseText);

			if (res.aaData === undefined || res.aaData.length != 1) return 'id:' + val;
			var resstr = '';
			for (var i = 0; i < props.db_field.length; i++) {
				if (props.db_field[i] in res.aaData[0] && res.aaData[0][props.db_field[i]] != undefined && res.aaData[0][props.db_field[i]] != '')
					resstr += res.aaData[0][props.db_field[i]] + ' ';
			}
			
			return '<a href="#" onclick="showfilteredvalues(\''+this.table.metadata.unique+'\', \''+widget.object.fieldname+'\', '+val+')">'+resstr+'</a>';

		}
		if ($.isArray(val) && props.another && (val[0] == -1 || val[0] == '-1')) {
			return props.another;
		}

		str = '';
		for (var ff = 1; ff < val.length; ff++) {
			if (typeof val[ff] == 'string' || typeof val[ff] == 'number') {
				if (str != '')
					str += ' ';
				str += val[ff];
			}
			else if (typeof val[ff] == 'boolean') {
				if (str != '')
					str += ' ';
				if (val[ff])
					str += 'true';
				else
					str += 'false';
			}
		}
		return str;

	}

	// Additional properties form
	this.viewPropForm = function (container) {
		var widget = this;
		var props = this.object.widget.properties;
		var sz = "<div class='tab_field' ><label class='tab_field_label'>Ref. table</label>\
		<div  id='db_table' class='main_field_prop' ></div></div>\
		<div class='tab_field'><label class='small_tab_field_label'>Relation type</label>\
		<select id='reltype' class='form-control small_tab_field_input'><option value='bijection'>1:1</option>\
   <option value='ref'>many:1</option>\
   <option value='detail'>1:many</option></select>\
		<label class='small_tab_field_label'>Control</label>\
		<select class='form-control tab_field_select' id='control_type' name='control_type' ><option value='select'>Select</option><option value='autoselect'>Auto complete</option><option value='radio'>Radio buttons</option></select></div>\
		<div class='tab_field main_field_prop'><label class='tab_field_label'>Another value</label><input  class='form-control tab_field_input' type='text' id='another'/></div>\
		<div class='tab_field main_field_prop'><label class='tab_field_label'>Main synonym field</label><input  class='form-control tab_field_input synonym' type='text' /></div>\
		<div class='main_field_prop'><table><thead><tr><td class='td_head'>Field</td><td class='td_head'>View</td><td class='td_head'>Filter</td><td class='td_head'>Add column</td></tr></thead><tbody id='ref_fields'></tbody></table></div>\
		";
		container.append(sz);
		
		var metaTableSelect = {
					"fieldname": "",
					"title": "",
					"description": "",
					"visible": true,
					"widget": {
						"name": "theme_select",
						properties: {}
					}
				};
		this.tableSelect = $.widgets.get_widget(metaTableSelect);
		
		var db_table = $("#db_table", container);
		
			
		
		var db_map = $("#db_map", container);
		var ref_fields = $("#ref_fields", container);
		this.table_name = '';
		if (props != undefined && props.reftablename != undefined) {
			this.reftablename = props.reftablename;
		}
		else
			this.reftablename = 't' + randomString(10);// $('.classify').length;

		//createfield(null, container);
/*
		$.getJSON("/dataset/list?f=100&iDisplayStart=0&iDisplayLength=450&iSortingCols=1&iSortCol_0=0&sSortDir_0=asc&bSortable_0=true", function (data) {
			var sortedProp = "name";
			widget.tables = data.aaData.sort(function compare(a, b) {
				if (a[sortedProp] < b[sortedProp])
					return -1;
				else if (a[sortedProp] > b[sortedProp])
					return 1;
				else
					return 0;
			});//iSortingCols

			for (var i = 0; i < data.aaData.length; i++) {
				var option;
				if (props != undefined && props.dataset_id != undefined && props.dataset_id == data.aaData[i].id) {
					option = $('<option selected value="' + i + '">' + data.aaData[i].name + '(' + data.aaData[i].d_table + ')</option>');
					//this.table_name=data.aaData[i][3]+'.'+data.aaData[i][2];
				}
				else
					option = $('<option value="' + i + '">' + data.aaData[i].name + '(' + data.aaData[i].d_table + ')</option>');
				db_table.append(option);
			}
			db_table.change();
			//input.html(options);

		});*/

		var options = '';
		var table_fields = $("#table_fields", container);
		var synonym_field = $(".synonym", container);

		if (props != undefined && props.another) {
			$('#another', container).val(props.another);
		}
		if (props != undefined && props.synonym) {
			synonym_field.val(props.synonym)			
		}
		var control_type = $("#control_type", container);
		if (props != undefined && props.control_type != undefined) {
			control_type.val(props.control_type);
		}
		var reltype = $("#reltype", container);
		if (props != undefined && props.reltype != undefined) {
			reltype.val(props.reltype);
		}


		var farr = ['point', 'polygon', 'line', 'rect', 'rectangle', 'baikalcity', 'political_division'];

		var swidget = ['number', 'edit', 'textarea', 'boolean', 'point', 'polygon', 'line', 'rect'];

		function set_table(data){
			db_map.empty();
			ref_fields.empty();
			
			
			db_table.data('meta', data);
			for (var i = 0; i < data.columns.length; i++) {
				if (data.columns[i].widget === undefined)
					continue;
				var selected = '';
				//if (farr.indexOf(data.columns[i].widget.name) != -1) {
				if (props != undefined && props.geom != undefined && props.geom == data.columns[i].fieldname)
					selected = $('<option selected value="' + data.columns[i].fieldname + '">' + data.columns[i].title + ' (' + data.columns[i].fieldname + ') </option>');
				else
					selected = $('<option value="' + data.columns[i].fieldname + '">' + data.columns[i].title + ' (' + data.columns[i].fieldname + ') </option>');
				db_map.append(selected);
				//}
				//if(swidget.indexOf(data.columns[i].widget.name)==-1)
				//	continue;				
				if (props != undefined && props.db_field != undefined && $.isArray(props.db_field) && props.db_field.indexOf(data.columns[i].fieldname) != -1)
					selected = 'checked';
				var filter_value = '';
				if (props != undefined && props.filter != undefined && data.columns[i].fieldname in props.filter)
					filter_value = props.filter[data.columns[i].fieldname];
				field_filter = $('<div id="' + data.columns[i].fieldname + '"><input class="form-control" id="field_used" ' + selected + ' type="checkbox"/>' + data.columns[i].title + '<input id="filter_value" type="text" value="' + filter_value + '" style="width: 100px;"/></div>');
				//table_fields.append(field_filter);

				var field_row = $('<tr><td>' + data.columns[i].title + '(' + data.columns[i].fieldname + ')</td><td class="td_body"><input class="form-check-input" id="field_used_' + data.columns[i].fieldname + '" ' + selected + ' type="checkbox"/></td><td class="td_body"><input class="form-control tab_field_input" id="filter_' + data.columns[i].fieldname + '" type="text" value="' + filter_value + '" /></td><td class="td_body"><button type="button" class="submit btn" id="f_add"><i class="fas fa-plus"></i></button></td></tr>');
				ref_fields.append(field_row);
				$('.submit', field_row).data('column', data.columns[i]);
			}
			$('.submit', ref_fields).click(function () {
				var b = $(this);
				field = b.data('column');
				if (!field)
					field = {};
				//field.sqlname=$("#db_map",container).val();
				field.sqltablename = data.tablename;
				field.tablename = widget.reftablename;
				//field.fieldname=widget.reftablename+'_'+field.fieldname;
				if (!field.sqlname)
					field.sqlname = field.fieldname;
				field.fieldname = 'ref_' + field.fieldname + '_' + randomString(4);
				field.parentfield = $("#field_name", container.parent().parent()).val();
				createfield(field, $('#tablefields'));
			});
		}
		helpobject={};
		helpobject.changevalue=function(fieldname, value){
			data = JSON.parse(widget.tableSelect.curentdoc.JSON);
			set_table(data);
		}
		this.tableSelect.listnerlist.push(helpobject);
		d_id='';
		if(props)
			d_id=props.dataset_id
		this.tableSelect.assign(db_table, d_id);
		
		if(props.dataset_id ){
			getdatasetmeta(props.dataset_id  , function(error, data){
				if(error==null)
					set_table(data);
				
			});
			
		}
		
		
	}

	this.getPropForm = function (container) {
		var synonym_field = $(".synonym", container);


		//db_field=$('#db_field',container).val();
		var db_map = $("#db_map", container);
		var db_map_field = db_map.val();
		//data=db_map.data('meta');
		var db_table = $("#db_table", container);
		data = db_table.data('meta');
		/*
		var gtype='';
		for (var i = 0; i < data.columns.length; i++) {						
			if (data.columns[i].fieldname==db_map_field && data.columns[i].widget.properties) {
				gtype=data.columns[i].widget.properties.gtype;
				break;
			}
		}
		*/
		val = this.tableSelect.getVal();
		if (val == -1 || val == '-1' || val == undefined || val == '')
			return {};
		//data=JSON.parse(tables[val].JSON);
		var filter = {};
		var db_field = [];



		for (var i = 0; i < data.columns.length; i++) {
			v = $('#filter_' + data.columns[i].fieldname, container).val();
			if (v != undefined && v != '')
				filter[data.columns[i].fieldname] = v;
			if ($('#field_used_' + data.columns[i].fieldname, container).prop("checked")) {
				db_field.push(data.columns[i].fieldname);
				filter[data.columns[i].fieldname] = v;
			}

		}

		return {
			dataset_id: this.tableSelect.getVal(),
			db_field: db_field,  //$('#db_field option:selected',container).text(),
			geom: db_map.val(),
			reftablename: this.reftablename,
			//table_name: this.table_name,
			type: $("#type", container).val(),
			synonym: synonym_field.val(),
			//gtype: gtype,
			control_type: $("#control_type", container).val(),
			reltype: $("#reltype", container).val(),
			another: $('#another', container).val(),
			filter: filter
		};
	}

	this.getDatatableProperties = function () {
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.fieldname;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'widget_filter';
		tempobj.sWidth = '200px';
		return tempobj;
	}

	this.hasFilter = function () {
		return true;
	}
	this.olap = function (node, value, meta) {

		this.init_olap(node, value);
		return;
		var vis = false;
		var sz = $("<div class='form-item' ><label>Select field</label>\
		<select  id='db_field' name='db_field' ><option>Select the field </option></select>min color<input style='width:100px;' class='min_color' value='000000'/>\
		max color<input style='width:100px;' class='max_color' value='000000'/><button id='change_map' class='btn btnApply' type='button'>Apply</button></div>");
		var change_map = $('#change_map', sz);
		change_map.click(function () {
			meta.layer.update_map();
		});

		sz.hide();
		this.group.after(sz);
		// $(".min_color", sz).colorpicker();
		// $(".max_color", sz).colorpicker();
		this.group.change(function () {
			if (vis)
				sz.hide();
			else
				sz.show();
			vis = !vis;
		});
		select_input = $('#db_field', sz);
		for (var i = 0; i < meta.columns.length; i++) {
			option = $('<option value="' + meta.columns[i].fieldname + '">' + meta.columns[i].title + ' (' + meta.columns[i].fieldname + ') </option>');
			select_input.append(option);
		}
	}


	return this;
}
extend(w_classify, w_base);
$.widgets.RegisterWidget('classify', w_classify);function w_date(object) {
	// Object for creation of widget
    this.object = object;
	// Common name of widget
	this.widget_name = 'date';
	this.fieldtype = 'date';
	// Additional properties array
	var addproparr = new Array();
	this.user_visible = true;
	this.rname = 'Дата';
	// HTML element creation
	this.setval = function(value){
		if(value!=undefined && value!=''){
				parts = value.match(/\d+/g);
				var res=parts[2]+'.'+parts[1]+'.'+parts[0];
				this.input.val(res);			
			}
	};
    this.assign = function(form_element, value){	
		var cont=form_element;
		this.form_element = form_element;		
		//var label = $('<div style="height: 24px;display: inline-block;text-align: right;">'+this.object.title+'</div>');
		var label = $('<label for="inp_'+this.object.fieldname+'">'+this.object.title+'</label>');
		if(this.object.description)
			var select_date = $('<input title="'+this.object.description+'" class="form-control " type="text" />');
		else
			var select_date = $('<input title="" class="form-control " type="text" />');
		this.input=select_date;
		//select_date.attr('id', this.object.fieldname);
		var parts;
		if(value!=undefined && value!=''){
			parts = value.match(/\d+/g);
			var res=parts[2]+'.'+parts[1]+'.'+parts[0];
			select_date.val(res);			
		}
		/*
		if(this.lvis){
			form_element.append(label);
		}*/
		form_element.append(select_date);
		select_date.datepicker({
				onSelect: function(dateText) {
					cont.change();				
				},
				dateFormat: 'dd.mm.yy'
			});
		//select_date.addClass('w_date');
		select_date.attr('widget', this.widget_name);
		this.select_date=select_date;
		
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_hour!=undefined  && this.object.widget.properties.use_hour=='1' ){
			var label = $('<br/><span class="t_label">H:</span>');
			var input = $('<input class="doubledigit" id="hour" type="text"/>');
			form_element.append(label);
			form_element.append(input);
			input.numberTest({default_point: ".", before:2, after:0, min:0, max:24} );
			if(value!=undefined && value!=''){
				input.val(parts[3]);
			}			
			label.addClass('t_label1');
			input.change(function(){
				cont.change();
			});
		}
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_minute!=undefined  && this.object.widget.properties.use_minute=='1' ){
			var label = $('<span class="t_label">M:</span>');
			var input = $('<input class="doubledigit" id="minute" type="text"/>');		
			form_element.append(label);
			form_element.append(input);
			input.numberTest({default_point: ".", before:2, after:0, min:0, max:59} );
			if(value!=undefined && value!=''){
				input.val(parts[4]);
			}			
			input.change(function(){
				cont.change();
			});			
		}
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_second!=undefined  && this.object.widget.properties.use_second=='1' ){
			var label = $('<span class="t_label">S:</span>');
			var input = $('<input class="doubledigit"  id="second" type="text"/>');		
			form_element.append(label);
			form_element.append(input);
			input.numberTest({default_point: ".", before:2, after:0, min:0, max:59} );
			if(value!=undefined && value!=''){
				input.val(parts[5]);
			}			
			input.change(function(){
				cont.change();
			});			
		}
    }
    
	// Getting value of input field
    this.getVal = function (){
		val=this.select_date.val();
		if(val=='') return '';
		var parts = val.match(/\d+/g);
		var res=parts[2]+'-'+parts[1]+'-'+parts[0];
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_hour!=undefined  && this.object.widget.properties.use_hour=='1' ){
			val=$('#hour',this.form_element).val();
			if(val===undefined || val.length==0)
				val='00';
			else if(val.length==1)
				val='0'+val;
				
			res+=' '+val+':';
			
			if(this.object.widget.properties!=undefined && this.object.widget.properties.use_minute!=undefined  && this.object.widget.properties.use_minute=='1' ){
				val=$('#minute',this.form_element).val();
				if(val===undefined || val.length==0)
					val='00';
				else if(val.length==1)
					val='0'+val;
			
				res+=val+':';
			}
			else
				res+='00:';
			if(this.object.widget.properties!=undefined && this.object.widget.properties.use_second!=undefined  && this.object.widget.properties.use_second=='1' ){
				val=$('#second',this.form_element).val();
				if(val===undefined || val.length==0)
					val='00';
				else if(val.length==1)
					val='0'+val;
			
				res+=val;
			}
			else
				res+='00';
		}
	    return res;
    }
    
	
    this.getUserVal=function (val){
		//YYYY-MM-DD HH:MM:SS
		if (val==undefined || val=='' || val==null || val=='NULL')
			return '';
		if(this.gr_fn=='none' )
			return '';
		if(this.gr_fn=='count')
			return val;
		var parts = val.match(/\d+/g);
		if(parts.length<3)
			return '';
		var res=parts[2]+'.'+parts[1]+'.'+parts[0];
		
		
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_hour!=undefined  && this.object.widget.properties.use_hour=='1' ){
			res+=' '+parts[3]+'h';
		}
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_minute!=undefined  && this.object.widget.properties.use_minute=='1' ){
			res+=', '+parts[4]+'m';
		}
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_second!=undefined  && this.object.widget.properties.use_second=='1' ){
			res+=', '+parts[5]+'s';
		}
	    return res;
    }

	// Additional properties form
    this.viewPropForm = function (container){
	   
		var ps=$("<label>Hour</label><input type='checkbox' id='use_hour' value='1'/><br/>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_hour!=undefined  && this.object.widget.properties.use_hour=='1' )
		   $('#use_hour', container).prop("checked", true);

		var ps=$("<label>Minute</label><input type='checkbox' id='use_minute' value='1'/><br/>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_minute!=undefined  && this.object.widget.properties.use_minute=='1' )
		   $('#use_minute', container).prop("checked", true);

		var ps=$("<label>Second</label><input type='checkbox' id='use_second' value='1'/>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.use_second!=undefined  && this.object.widget.properties.use_second=='1' )
		   $('#use_second', container).prop("checked", true);
		   
		return;
    }    
    this.getPropForm = function (container){
		var use_hour;
		if( $('#use_hour',container).prop("checked") )
			use_hour='1';
		else
			use_hour='0';

		var use_minute;
		if( $('#use_minute',container).prop("checked") )
			use_minute='1';
		else
			use_minute='0';

		var use_second;
		if( $('#use_second',container).prop("checked") )
			use_second='1';
		else
			use_second='0';		
		
	    return {use_hour: use_hour, use_minute:use_minute, use_second:use_second};
    }
	this.olap = function(node, value){	
		this.init_olap(node, value);		
		this.group.append($('<option value="groupby_hour">group by hour</optiion>'));
		this.group.append($('<option value="groupby_day">group by day</optiion>'));
		this.group.append($('<option value="groupby_week">group by week</optiion>'));
		this.group.append($('<option value="groupby_month">group by month</optiion>'));
		this.group.append($('<option value="groupby_quarter">group by quarter</optiion>'));
		this.group.append($('<option value="groupby_year">group by year</optiion>'));
		this.group.append($('<option value="groupby_decade">group by decade </optiion>'));
	}  
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = 'text';
		tempobj.type = 'widget_proxy';		
/*		tempobj.submit ='Ok';
		tempobj.cancel ='Cancel';		*/
		tempobj.sWidget = 'date';
		tempobj.sWidth = '1px';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'DateRangeFilterWidget';
		tempobj.sWidth = '30px';
		return tempobj;
	}
	this.hasFilter = function (){
		return true;
	}
	
    return this;
}
extend(w_date, w_base);
$.widgets.RegisterWidget('date', w_date);(function($) {
	/**
	* A filter widget based on data in a table column.
	* 
	* @class DateRangeFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/
	$.DateRangeFilterWidget = function( $Container, column, dt_table ){
		var widget = this;
		widget.column = column;		
		widget.$Container = $Container;
		widget.oDataTable = dt_table;		
		widget.asFilters = [];
		widget.sSeparator = ';';
		widget.iMaxSelections = -1;
		var m1={sName:'from_'+widget.column.fieldname, title:'From', fieldname:'from_'+widget.column.fieldname, widget:widget.column.widget};
		var m2={sName:'to_'+widget.column.fieldname, title:'To', fieldname:'to_'+widget.column.fieldname, widget:widget.column.widget};
		var w_d_from=new w_date(m1);
		var w_d_to=new w_date(m2);
		
		var d_from = 'from_' + widget.column.fieldname;
		var d_to   = 'to_' + widget.column.fieldname;

		widget.$DateRange = $( '<div class="input-group fltr_date"></div>');
		//widget.$DateRange = $( '<div class="row fltr_date"><div class="col-5 fltr_date_div" id="d_from_' + widget.column.fieldname + '"/><div class="col-1"><a class="btn btnretweet" id="btnretweet" href="#"><i class="far fa-clone"></i></a></div><div class="col-5 fltr_date_div" id="d_to_' + widget.column.fieldname + '"/></div>' );
		
		
		
		widget.$Container.append( widget.$DateRange );		
		w_d_from.assign(widget.$DateRange);
		widget.$DateRange.append($('<div class="input-group-append"><span class="input-group-text btnretweet"><i class="far fa-clone"></i></span></div>'));
		w_d_to.assign(widget.$DateRange);
		widget.$DateRange.append($('<div class="input-group-append"><span class="input-group-text addconstraint"><i class="fas fa-plus"></span></div>'));
	
		
		$('.addconstraint', widget.$DateRange).click(function(event, ui) {
			var val_d_from = w_d_from.getVal(),
				val_d_to = w_d_to.getVal();
			if (( val_d_from == '' ) && ( val_d_to == '' )) {
				// The blank range
				return;
			}				
			var str_d_from = w_d_from.getUserVal(val_d_from),
				str_d_to = w_d_to.getUserVal(val_d_to);			
			val_d_from = (val_d_from == '') ? '∞' : val_d_from ;
			val_d_to = (val_d_to == '') ? '∞' : val_d_to ;
			str_d_from = (str_d_from == '') ? '∞' : str_d_from ;
			str_d_to = (str_d_to == '') ? '∞' : str_d_to ;
			
			var sSelected = val_d_from  + ' - ' + val_d_to, sText, $TermLink, $SelectedOption; 
			var strSelected = str_d_from  + ' - ' + str_d_to;
			$('input',widget.$Container).val('');
			$('input',widget.$Container).val('');
			if ( '' === sSelected ) {
				// The blank option is a default, not a filter, and is re-selected after filtering
				return;
			}
			if  (!($.inArray(sSelected, widget.asFilters))) {
				return;
			}
			sText = $( '<div>' + strSelected + '</div>' ).text();
			$TermLink = $( '<a class="filter-term" href="#"></a>' )
				.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
				.text( sText )
				.click( function() {
					// Remove from current filters array
					widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
						return sFilter != sSelected;
					} );
					$TermLink.remove();
					if ( widget.$TermContainer && 0 === widget.$TermContainer.find( '.filter-term' ).length ) {
						widget.$TermContainer.hide();
					}
					widget.fnFilter();
					return false;
				} );
			widget.asFilters.push( sSelected );
			if ( widget.$TermContainer ) {
				widget.$TermContainer.show();
				widget.$TermContainer.prepend( $TermLink );
			} else {
				//$( this ).after( $TermLink ); //  Add $TermLink to From <input>
				widget.$DateRange.after( $TermLink ); //  Add $TermLink to From <input>
			}
		
			//widget.$DateRange.val( '' );
			widget.fnFilter();	
		});
		
		$('.btnretweet',widget.$DateRange).click(function(){
			var str_d_from = w_d_from.getVal(),
				str_d_to = w_d_to.getVal();
			if(str_d_from=='' && str_d_to!='')
				w_d_from.setval(str_d_to);
			else if(str_d_from!='' && str_d_to=='')
				w_d_to.setval(str_d_from);
		
		});
		
		
		//val_d_to.append( widget.$button );
		//widget.fnDraw();
	};


	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.DateRangeFilterWidget.prototype.fnFilter = function() {
		var widget = this;
		//var aDateRange = [];
		//var sFilterStart, sFilterEnd; // TODO Необходимо добавить обработку c одной датой
		if ( widget.asFilters.length > 0 ) {
			var asEscapedFilters = [];		
			$.each( widget.asFilters, function( i, sFilter ) {
				aDateRange = sFilter.split(' - ');
				if(aDateRange[0]!='∞')
					sFilterStart = aDateRange[0];//$.datepicker.formatDate( "yy-mm-dd HH:MM:SS", $.datepicker.parseDate( $.datepicker._defaults.dateFormat, aDateRange[0] ));
				else
					sFilterStart = '∞';
				if(aDateRange[1]!='∞')
					sFilterEnd = aDateRange[1];//$.datepicker.formatDate( "yy-mm-dd HH:MM:SS", $.datepicker.parseDate( $.datepicker._defaults.dateFormat, aDateRange[1]));
				else
					sFilterEnd ='∞'
				asEscapedFilters.push( sFilterStart + ' - ' + sFilterEnd );				
			} );
			widget.oDataTable.fnFilter( asEscapedFilters.join(';'), widget.column.fieldname );
		} else { 
			// Clear any filters for this column
			widget.oDataTable.fnFilter( '', widget.column.fieldname );
		}
		

	};		
	
}(jQuery));﻿


function w_details(object) {
	var widget=this;	
	// Object for creation of widget	
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'details';
	// Additional properties array
	var addproparr = new Array();
	this.user_visible = true;
	this.rname = 'details';		
	this.fieldtype = 'integer';
	// id of selected value
	var sel_id = '';
	var ft= new Array();
	this.tables=[];
	this.filter={};
	this.fields='';
	var widget=this;
	this.calllist=[];
	
	if(this.object!=undefined && this.object.widget!=undefined){
		//[8].parentfield ref_detail
		var props = this.object.widget.properties;
		if(props && props.dataset_id){
			getdatasetmeta(props.dataset_id  , function(error, table_json){
				if(error!=null)
					return;
				table_json.metaid = props.dataset_id;			
				var displayfields=widget.fields.split(',');
				displayfields.push('id');			
				for(var i=table_json.columns.length - 1; i>=0; i--){				
					if( table_json.columns[i].parentfield==props.ref_detail || table_json.columns[i].fieldname==props.ref_detail){
						//table_json.columns[i].visible=false;				
						table_json.columns.splice(i,1);
					}
					else if(widget.fields!=='' && displayfields.indexOf(table_json.columns[i].fieldname)==-1 )
						table_json.columns.splice(i,1);
				}
				widget.table_json=table_json;
				widget.doc_frm= new doc_template(table_json);
				widget.doc_frm.init_form(function(){		
			
				});
				
			});
			if(props!=undefined && props.db_field!=null && props.db_field!=undefined)
				this.fields=props.db_field.join(',');
			
		}
	}
	
	

    this.assign = function(form_element, value){		
		this.form_element = form_element;
		this.value=[];
		if(typeof value=== "object")
			this.value=value;
		var props = this.object.widget.properties;
		tab=$('<div><div class="subtab"></div></div>');
		//<a class="btn" href="#" id="btnEditTab" title="Edit the table"><i class="icon-pencil"></i></a>
		var subtab=$('.subtab',tab);
		//subtab.html(this.getUserVal(value));
		form_element.append(tab);
		var timerId = setTimeout(function() {
			if(widget.table_json){
				clearInterval(timerId);
				if($.isArray(value))		
					widget.gtable = new gdataset(widget.table_json, props.dataset_id, subtab, '', value, true);
				else{
					widget.gtable = new gdataset(widget.table_json, props.dataset_id, subtab, '', [], true);
					a=1;
				}
				panel=$('<div id="command_panel"><h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"><div class="btn-group mb10"></div></h3></div>');
				subtab.append(panel);
				panel=$('.btn-group', panel);
				widget.gtable.add_crud_panel(panel);
				widget.gtable.maxrowcount = props.maxrowcount;
				widget.gtable.drawtablehead();
				widget.gtable._fnReDraw();		
			};		  
		  
		}, 500);
		
		
    }
    
	// Getting value of input field
    this.getVal = function (){
		//if(this.visible!=undefined && this.visible==false)
		//	return '';
		res=[];
		if(this.gtable){
			for(var i=0; i<this.gtable.tabledata.length; i++){
				newrow={};
				for( var c =0; c<this.gtable.metadata.columns.length; c++){
					column=this.gtable.metadata.columns[c];
					if(column.fieldname in this.gtable.tabledata[i])
						newrow[column.fieldname]=this.gtable.tabledata[i][column.fieldname];
				}				
				res.push(newrow);
			}
			//for(var i=0; i<this.gtable.deleteddata.length; i++){
				//res.push(this.gtable.deleteddata[i]);
			//}
		}
		else{
			res=this.value;
			//serverlog('this.gtable id null!!!!');
		}
		//serverlog({action:'getting authors',authors:res});
		return res;
    }
		
	
	this.getUserVal=function (val, view){
		if(val=='NULL')
			return '';
		if(this.gr_fn=='none' )
			return '';
			
		var props = this.object.widget.properties;
		if($.isArray(val) && this.table_json && val.length>0){
			var unique=randomString(10);
			if(!view){
				if(props.tableview){					
					var result='';
					for(var i=0; i<val.length; i++){
						try{
							result+=fieldcalc.calculatefield(props.tableview, val[i]) + '; ';
						}
						catch(e){
							console.log(e);
						}
					}
					return result;
				}
				else
					return val.length;
			}
				
			
			res='<a href="#" class="detailslink" id="href'+unique+'">Hide details</a><table class="detailstab" id="'+unique+'"><tr>';
			for(var i=0; i<this.table_json.columns.length; i++){				
				if(this.table_json.columns[i].visible){
					res+='<td class="column_title">'+this.table_json.columns[i].title+'</td>';
					if(!this.table_json.columns[i].widget_object){
						this.table_json.columns[i].widget_object=$.widgets.get_widget(this.table_json.columns[i]);
						this.table_json.columns[i].widget_object.metaid=props.dataset_id;
					}
				}
			}
			res+='</tr>';

			
			for(var r=0; r<val.length; r++){
				res+='<tr>';//<td><a class="btn CustomView '+unique+'" href="#" row_id="'+r+'" title="View" disabled="disabled"><i class="view-doc-icon" /></i></a></td>
				for(var i=0; i<this.table_json.columns.length; i++){
					if(this.table_json.columns[i].visible){
						this.table_json.columns[i].widget_object.cntr=val[r].id;
						res+='<td>'+this.table_json.columns[i].widget_object.getUserVal(val[r][this.table_json.columns[i].fieldname], true)+'</td>';
					}
				}
				res+='</tr>';
			}
			res+='</table>';
			if(res=='')
				res=val.length;
			
			
			setTimeout(function(){
				var i=-1;
				$('tr', $('#'+unique)).each(function() {
					if(i>-1)
						$( this ).data('doc',val[i]);
					i++;
				});
				
				$('#href'+unique).click(function(){
					var com_form=$('#'+unique);
					if(com_form.css('display') == 'none'){
						com_form.show();
						$('#href'+unique).html('Hide details')
					}
					else{
						com_form.hide();
						$('#href'+unique).html('View details')
					}
					
				});
			}, 500);
			
			/*
			setTimeout(function(){
				$('.'+unique).click(function(){
					eRowform=$('<div title="'+widget.doc_frm.doc_description.title+'"></div>');					
					widget.doc_frm.show_form(eRowform, val[$(this).attr('row_id')], '', true);
					eRowform.dialog({
						  resizable: false,
						  height:'auto',
						  width: 'auto',
						  modal: true,
						  buttons: {
							"Close": function() {								
								$( this ).dialog( "close" );								
							}
						  }
					});
				});
			}, 500);*/

			
		//this.doc_frm.show_form(eRowform, val[0], '', true);
			return res;
		}
		else
			return '-';
    }    

	// Additional properties form
    this.viewPropForm = function (container){
		var widget=this;
		var props = this.object.widget.properties;
		var sz="\
			<div class='classify'/>\
			<label>Select database of the your classificator</label>\
			<div class='form-item' id='db_table'>\
			</div>\
			<div class='form-group'>\
				<label for='reltype'>Relation type with table</label>\
				<select class='form-control' id='reltype'><option value='ref'>many:1</option>\
					<option value='detail'>many:many</option>\
				</select>\
			</div>\
			<div class='form-group'>\
				<label for='control_type'>Control type</label>\
				<select class='form-control' id='control_type' name='control_type' ><option value='select'>Select</option><option value='autoselect'>Auto complete select</option><option value='radio'>Radio buttons</option></select>\
			</div>\
			<div class='form-group'>\
				<input type='hidden' id='type' name='type'></input>\
				<label for='ref_detail'>Select the ref detail field</label>\
				<select class='form-control' id='ref_detail' name='ref_detail' ><option value=''>Select the map field</option></select>\
			</div>\
			<div class='form-group'>\
				<label for='tableview'>Table view expression</label>\
				<input class='form-control' type='text' id='tableview'></input>\
			</div>\
			<div class='form-group'>\
				<label for='maxrowcount'>Max rowc ount</label>\
				<input class='form-control' type='text' id='maxrowcount'></input>\
			</div>\
			<div class='form-group'>\
			<h5>Filters</h5>\
			</div>\
			<div class='form-check' id='table_fields'>\
			</div>\
		";
		container.append(sz);
		var db_table=$("#db_table",container);
		var ref_detail=$("#ref_detail",container);
		this.table_name='';
		this.reftablename='t'+$('.classify').length;	

		var metaTableSelect = {
			"fieldname": "",
			"title": "",
			"description": "",
			"visible": true,
			"widget": {
				"name": "theme_select",
				properties: {}
			}
		};
		var d_id;
		this.tableSelect = $.widgets.get_widget(metaTableSelect);
		if(props){
			d_id=props.dataset_id;			
		}
		this.tableSelect.assign(db_table, d_id);
		
		
		
		var options = '';
		var table_fields=$("#table_fields",container);		
		var synonym_field=$("#synonym",container);
		
		if(props!=undefined && props.another){
			$('#another',container).val(props.another);
		}
		if(props!=undefined && props.synonym=='true'){
			synonym_field.prop("checked", true);
		}
		var control_type=$("#control_type",container);
		if(props!=undefined && props.control_type!=undefined){			
			control_type.val(props.control_type);
		}
		var reltype=$("#reltype",container);
		if(props!=undefined && props.reltype!=undefined){			
			reltype.val(props.reltype);
		}

		var tableview=$("#tableview",container);
		if(props!=undefined && props.tableview!=undefined){			
			tableview.val(props.tableview);
		}

		var maxrowcount=$("#maxrowcount",container);
		if(props!=undefined && props.maxrowcount!=undefined){			
			maxrowcount.val(props.maxrowcount);
		}

		

		
		
		
		var farr = ['point', 'polygon', 'line', 'rect', 'rectangle','baikalcity','political_division', 'links'];
		
		var swidget = ['number', 'edit', 'textarea', 'boolean', 'point', 'polygon', 'line', 'rect', 'date', 'classify','select','details', 'image','email', 'attachment', 'links'];
		helpobject={};
		helpobject.changevalue=function(fieldname, value){
			data = JSON.parse(widget.tableSelect.curentdoc.JSON);
			set_table(data);
		}
		this.tableSelect.listnerlist.push(helpobject);


		function set_table(data){
			ref_detail.empty();
			table_fields.empty();
			
			ref_detail.data('meta',data);
			for (var i = 0; i < data.columns.length; i++) {						
				if(data.columns[i].widget===undefined)
					continue;
				var selected='';
				//if (farr.indexOf(data.columns[i].widget.name) != -1) {
					if(props!=undefined && props.ref_detail!=undefined && props.ref_detail==data.columns[i].fieldname)
						selected= $('<option selected value="'+data.columns[i].fieldname+ '">'+ data.columns[i].title+ ' (' + data.columns[i].fieldname+ ') </option>');
					else
						selected= $('<option value="'+data.columns[i].fieldname+ '">'+ data.columns[i].title+ ' (' + data.columns[i].fieldname+ ') </option>');							
					ref_detail.append(selected);
				//}
				if(swidget.indexOf(data.columns[i].widget.name)==-1)
					continue;				
				if(props!=undefined && props.db_field!=undefined && $.isArray(props.db_field) && props.db_field.indexOf(data.columns[i].fieldname)!=-1)
					selected='checked';
				var filter_value='';
				if(props!=undefined && props.filter!=undefined && data.columns[i].fieldname in props.filter)
					filter_value=props.filter[data.columns[i].fieldname];
				field_filter= $('<div class="row" id="'+data.columns[i].fieldname+'">\
				<div class="form-check col-md-2"><input class="form-check-input" id="field_used" '+selected+' type="checkbox"/><label class="form-check-label" for="defaultCheck1">\
			  	'+ data.columns[i].title+ '</label></div>\
				<div class="form-group col-md-2"><label for="filter_value" >Filter value</label><input class="form-control" id="filter_value" type="text" value="'+filter_value+'"/></div></div>');
				table_fields.append(field_filter);
			}
		}

		if(d_id){
			$.getJSON("/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=" + d_id, function (data) {
				if (data.aaData.length == 0){
					callback('missing dataset meta');
					return;
				}
				meta=JSON.parse(data.aaData[0].JSON);
				set_table(meta);
			});
		}

		
		
		/*
		setTimeout( function() {
				db_table.change();
		}, 300 );
		*/
    }  

    this.getPropForm = function (container){
		var synonym_field=$("#synonym",container);
		var v;
		if(synonym_field.prop("checked"))
			v='true';
		else
			v='false';
			
		//db_field=$('#db_field',container).val();
		var ref_detail=$("#ref_detail",container);		
		var db_map_field=ref_detail.val();
		data=ref_detail.data('meta');
		var gtype='';
		for (var i = 0; i < data.columns.length; i++) {						
			if (data.columns[i].fieldname==db_map_field) {
				gtype=data.columns[i].widget.properties.gtype;
				break;
			}
		}
		
		val=this.tableSelect.getVal();
		if(val==-1 || val=='-1' || val==undefined || val=='')
			return {};
		//data=JSON.parse(tables[val].JSON);
		var filter={};
		var db_field=[];
		for (var i = 0; i < data.columns.length; i++) {
			var table_field_container=$('#'+data.columns[i].fieldname, container);
			v=$('#filter_value',table_field_container).val();
			if (v!=undefined && v!='') 
				filter[data.columns[i].fieldname]=v;			
			if ( $('#field_used',table_field_container).prop("checked") ){
				db_field.push(data.columns[i].fieldname);
				filter[data.columns[i].fieldname]=v;
			}

		}
		ref_detail=$("#ref_detail",container);
		var data=ref_detail.data('meta');
		

				
		return {
				dataset_id: val,
				db_field: db_field,  //$('#db_field option:selected',container).text(),
				ref_detail:ref_detail.val(),
				reftablename: this.reftablename,
				//table_name: this.table_name,
				type: $("#type",container).val(),
				synonym: v,
				gtype: gtype,
				control_type: $("#control_type",container).val(),
				reltype: $("#reltype",container).val(),
				another: $('#another',container).val(),
				filter: filter,
				tableview: $('#tableview',container).val(),
				maxrowcount: $('#maxrowcount',container).val(),
			};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.fieldname;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;		
		tempobj.type = 'widget_proxy';	
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'widget_filter';
		tempobj.sWidth = '200px';
		return tempobj;
	}

	this.hasFilter = function (){
		return false;
	}
	this.olap = function(node, value, meta){	
		this.init_olap(node, value);
		var vis=false;
		var sz=$("<div class='form-item' ><label>Select field</label><br>\
		<select  id='db_field' name='db_field' ><option>Select the field </option></select>min color<input style='width:100px;' class='min_color' value='000000'/>\
		max color<input style='width:100px;' class='max_color' value='000000'/><button id='change_map' class='btn btnApply' type='button'>Apply</button></div>");
		var change_map=$('#change_map',sz);
		change_map.click(function(){
			meta.layer.update_map();
		});
		
		sz.hide();
		this.group.after(sz);
		// $(".min_color",sz).colorpicker();
		// $(".max_color",sz).colorpicker();
		this.group.change(function(){
			if(vis)
				sz.hide();
			else
				sz.show();
			vis=!vis;
		});
		select_input=$('#db_field',sz);
		for (var i = 0; i < meta.columns.length; i++) {
			option = $('<option value="'+meta.columns[i].fieldname+ '">'+ meta.columns[i].title+ ' (' + meta.columns[i].fieldname+ ') </option>');
			select_input.append(option);
		}
	}
	

    return this;
}
extend(w_details, w_base);
$.widgets.RegisterWidget('details', w_details);function w_edit(object) {	
	var widget=this;
    this.object = object;
	// Common name of widget
	this.widget_name = 'edit';
	this.form_element = '';
	this.input;
	
	this.user_visible = true;
	this.rname = 'Строка';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		var con=form_element;
		var label = $('<label for="inp_'+this.object.fieldname+'">'+this.object.title+'</label>');
		input = $('<input title="'+this.object.description+'" class="form-control" type="text"/>');		
		form_element.append(input);
		this.input = input;
		if(value==='NULL')
			value='';
		else if(value!=undefined && value!='')
			this.input.val(removeescapeHtml(value));
		
		if (this.metaid !== undefined) {
			w_settings = {
				autofill    : true,
				cacheLength : 10,
				max         : 5,
				autofocus   : true,
				highlight   : true,
				mustMatch   : true,
				selectFirst : true,
				delay: 500,
				source		: function (request, response) {
					if(request.term.length<3)
						return;
					var result=[];					
					$.ajax({
						url: '/dataset/list?f='+ widget.metaid + '&iDisplayStart=0&iDisplayLength=10&iSortingCols=1&iSortCol_0=1&distinct=t&f_' + widget.object.fieldname +'=' + request.term + '&s_fields=' + widget.object.fieldname,
						dataType: "json",						
						success: function(data) {
							for (var i = 0; i < data.aaData.length; i++) {
								result.push(data.aaData[i][ widget.object.fieldname ]);
							}
							response(result);
						}
					});
				},
				appendTo    : form_element
			};

			this.input.autocomplete(w_settings, {
				dataType:'json',
				change: function(event, ui) {
					con.change();
					widget.sendmessage(widget.val);
				},
				formatItem: function(row, i, n) {                                                        
				  return row.value;
				},
			});	
			
			this.input.change(function(){
				widget.val=widget.input.val();
				widget.sendmessage(widget.val);
			});
		}
		this.changevalue('test1212121','');	
	
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';
		
		return this.input.val();
    }
    this.getUserVal=function (val){
		if(val==null || val==='NULL')
			return '';
		if(this.gr_fn=='none' )
			return '';
		return val;	   
    }
    // Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<div class='tab_field' ><label class='tab_field_label'>Поиск по полю (только тип tsvector)</label><input class='tab_field_input' type='text' id='tsvector'/></div>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined){
			$('#w_edit_size',container).val(this.object.widget.properties.size);
			$('#tsvector',container).val(this.object.widget.properties.tsvector);
		}		
    }
    this.getPropForm = function (container){
		//var wsize=$('#w_edit_size',container).val();
		var tsvector=$('#tsvector',container).val();
	    return {tsvector:tsvector};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		tempobj.sWidth = this.object.widget.properties.size*10+'px';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_edit, w_base);
$.widgets.RegisterWidget('edit', w_edit);(function($) {
/**
	* A filter widget based on data in a table column.
	* 
	* @class AutocompleteFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/

	
	$.AutocompleteFilterWidget = function( $Container, column, dt_table ) {
		var widget = this;
		widget.column = column;		
		widget.$Container = $Container;
		widget.oDataTable = dt_table;

		widget.asFilters = [];
		widget.sSeparator = ';';
		widget.iMaxSelections = -1;
		
		//widget.$Autocomplete = $( '<input type="text" id="f_'+widget.oColumn.sName+'" size="'+widget.oColumn.sWidth+'"/>' );
		widget.$Autocomplete = $( '<input class="form-control" type="text" id="'+widget.column.fieldname+'"/>' );
		widget.$Autocomplete.autocomplete({		
				//source: widgets.$AutocompleteSource + "&field=" + widget.oColumn.sName,
				source: widget.column.$AutocompleteSource + "&field=" + widget.column.fieldname,
				
				minLength: 2,
				select: function(event, ui) {
					var sSelected = ui.item.value, sText, $TermLink, $SelectedOption; 
					ui.item.value = "";
					if ( '' === sSelected ) {
						// The blank option is a default, not a filter, and is re-selected after filtering
						return;
					}
					if  (!($.inArray(sSelected, widget.asFilters))) {
						return;
					}
					sText = $( '<div>' + sSelected + '</div>' ).text();
					$TermLink = $( '<a class="filter-term" href="#"></a>' )
						.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
						.text( sText )
						.click( function() {
							// Remove from current filters array
							widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
								return sFilter != sSelected;
							} );
							$TermLink.remove();
							if ( widgets.$TermContainer && 0 === widgets.$TermContainer.find( '.filter-term' ).length ) {
								widgets.$TermContainer.hide();
							}
							widget.fnFilter();
							return false;
						} );
					widget.asFilters.push( sSelected );
					if ( widgets.$TermContainer ) {
						widgets.$TermContainer.show();
						widgets.$TermContainer.prepend( $TermLink );
					} else {
						widget.$Autocomplete.after( $TermLink );
					}
				
					widget.$Autocomplete.val( '' );
					widget.fnFilter();
				
				
				}
			});		
		widget.$label = $('<label for="f_' + widget.column.fieldname + '">' + widget.column.title + '</label><br>');
		widget.$Container.append( widget.$label );
		widget.$Container.append( widget.$Autocomplete );
		//widget.fnDraw();
	};
	
	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.AutocompleteFilterWidget.prototype.fnFilter = function() {
		var widget = this;
		var asEscapedFilters = [];
		var sFilterStart, sFilterEnd;
		if ( widget.asFilters.length > 0 ) {
			// Filters must have RegExp symbols escaped
			$.each( widget.asFilters, function( i, sFilter ) {
				asEscapedFilters.push( $.fnRegExpEscape( sFilter ) );
			} );
			// This regular expression filters by either whole column values or an item in a comma list
			sFilterStart = widget.sSeparator ? '(^|' + widget.sSeparator + ')(' : '^(';
			sFilterEnd = widget.sSeparator ? ')(' + widget.sSeparator + '|$)' : ')$';
			widget.oDataTable.fnFilter( sFilterStart + asEscapedFilters.join('|') + sFilterEnd, widget.iColumn, true, false );
		} else { 
			// Clear any filters for this column
			widget.oDataTable.fnFilter( '', widget.iColumn );
		}
		setTimeout( function() {
			if($.openlayermap !== undefined)
				$.openlayermap.rdrw("brio"); // TODO Refactoring !!!
        }, 150 );

	};
}(jQuery));


﻿function w_email(object) {
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'email';
	// Additional properties array
	this.user_visible = true;
	this.rname = 'email';	

	
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		
		input =  $('<input class="form-control" type="email" title="'+this.object.description+'"/>');
		form_element.append(input);		
		if(value!=undefined && value!='')
			input.val(removeescapeHtml(value));
		this.input=input;
		this.changevalue('test1212121','');	
    }
    
	
	function validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(emailField) == false){
            throw 'Invalid Email Address';
        }
        
	}
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';		
		val= this.input.val();
		if(val!='')
			validateEmail(val);
		return val;
    }
	
	this.getUserVal=function (val){
		//YYYY-MM-DD HH:MM:SS
		if (val==undefined || val=='')
			return '';
		if(this.gr_fn=='none' )
			return '';
		
	    return val;
    }
    
	// Getting JSON of field for further saving

	// Additional properties form
    this.viewPropForm = function (container){
				
    }    
    
    this.getPropForm = function (container){				
	    return {};
    }
   
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = 'string';
		tempobj.type = 'widget_proxy';		
/*		tempobj.submit ='Ok';
		tempobj.cancel ='Cancel';		*/
		tempobj.sWidget = '';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';

		return tempobj;
	}
	this.hasFilter = function (){
		return true;
	}

    return this;
}

extend(w_email, w_base);
$.widgets.RegisterWidget('email', w_email);function w_file(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'file';
	// Additional properties array
	var addproparr = new Array();
	this.form_element = '';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;

		var form_element_container = $('<div/>');
		form_element_container.addClass('form-inline');
		var input = $('<input valign="top" type="text" id="' + this.object.fieldname + '"/>');		
		form_element_container.append(input);
		var button = $('<button class="btn" type="button">Open</button>');
		form_element_container.append(button);
		form_element.append(form_element_container);
		
		var options = new Object();
		options.filter = this.object.widget.properties.exts;

		button.click(function() {
			OpenFileDlg(function(path){
				input.val(path);
				input.trigger('change');
			},options);
		});
		
		if (value!=undefined && value!='') {
			input.val(value);
		}
		
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
		return $('#'+this.object.fieldname, this.form_element).val();
    }
   
    this.viewPropForm = function (container){
		var ps=$("<label>File extensions (write extensions using comma)</label><input type='text' id='f_ext' value=''/><br/>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.exts!=undefined   )
		   $('#f_ext', container).val(this.object.widget.properties.exts);

		return;
    }    
    this.getPropForm = function (container){		
	    return {exts: $('#f_ext', container).val()};
    } 
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
//		tempobj.type = 'text';		
		tempobj.sWidget = 'file';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		tempobj.type = 'widget_proxy';				

		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_file, w_base);
$.widgets.RegisterWidget('file', w_file);function w_file_save(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	// Common name of widget
	var widget_name = 'file_save';


	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;

		var form_element_container = $('<div/>');
		form_element_container.addClass('form-inline');
		var input = $('<input type="text" id="' + this.object.fieldname + '"/>');
		form_element_container.append(input);
		var img=$('<button class="btn" type="button">Save</button>');
		form_element_container.append(img);
		form_element.append(form_element_container);
		var options = new Object();
		options.filter=this.object.widget.properties.exts;
		img.click(function() {
			SaveFileDlg(function(path){
				//c('Path is: ' + path);
				input.val(path);
			}, options);
		});
		
		if(value!=undefined && value!='')
			input.val(value);
    }
    
	// Getting value of input field
    this.getVal = function (){
		return $('#'+this.object.fieldname,this.form_element).val();
    }
   
	// Additional properties form
    this.viewPropForm = function (container){	   
		var ps=$("<label>File extensions (write extensions using comma)</label><input type='text' id='f_ext' value=''/><br/>");
		container.append(ps);
		var ps=$("<label>File style </label><textarea style='padding: 0px; width: 450px; height:80px;  margin-bottom: 1px;' id ='mapfilestyle'></textarea><br/>");
		container.append(ps);		
		var open = $('<input value="open" type="button" class="btn"/>');		
		container.append( open );

		if(this.object.widget.properties!=undefined ){
			if(this.object.widget.properties.exts!=undefined   ){
			   $('#f_ext', container).val(this.object.widget.properties.exts);
			}
			
			if(this.object.widget.properties.mapfilestyle!=undefined   ){
			   $('#mapfilestyle', container).val(this.object.widget.properties.mapfilestyle);
			}
		}
	    open.click(function(){
			var options = new Object();
			options.filter='style';

			OpenFileDlg(function(path){
					readFile(path, function(data){
						$('#mapfilestyle', container).val(data);
					});
				}, options);
		});
		return;
    }    
    this.getPropForm = function (container){		
	    return {exts: $('#f_ext', container).val(), mapfilestyle: $('#mapfilestyle', container).val()};
    } 
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'text';		
		tempobj.sWidget = 'file';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';

		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_file_save, w_base);
$.widgets.RegisterWidget('file_save', w_file_save);

var clipboard_buffer=''

function addzero(v, cnt){  
	v=v+'';
	while (cnt>v.length){
	  v='0'+v;
	}  
	return v;
}

function addsec(v){  
	v=v+'';
	ps=v.split('.');
	for(i=ps[0].length; i<2; i++){
		v='0'+v;
	}
	/*
	while (2>v.length){
	  v='0'+v;
	} */ 	
	while (5>v.length){
		if(v.length==2)
			v=v+'.';		
		else
			v=v+'0';
	}  
	return v;
}

function roundPlus(x, n) { //x - число, n - количество знаков 
	if(isNaN(x) || isNaN(n)) return false;
	var m = Math.pow(10,n);
	return Math.round(x*m)/m;
}
  
  
function Dec2minute_sec(val) {
	if(val<0)
		val=-val;
	
	var sec=val*3600;
	var gr=Math.floor(sec/3600);
	sec=sec % 3600;
	var mn = Math.floor(sec / 60);
	sec = roundPlus(sec % 60, 2);
	/*
	var gr=Math.floor(val);
	var lon_gr = addzero(gr,3);
	var mn=Math.floor((val - gr) * 60);
	var lon_mn = addzero(mn,2);	
	var sec=Math.floor( (((val - gr) * 60 - mn) * 60) * 100 ) / 100;
	var lon_sec = addsec(sec,5);
	*/
	var lon_gr = addzero(gr,3);
	var lon_mn = addzero(mn,2);
	var lon_sec = addsec(sec,5);
	var value =lon_gr+"°"+lon_mn+"'"+lon_sec+"''";
	return value;
};
  

function minute_sec2Dec(val){
	if(val ==undefined || val=='')
		return 0;
	var sign=val.toUpperCase().charAt(0);
	sign=sign=='E' || sign=='N';
	val=val.substr(1);
	arr = val.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var sec = sec/3600;
	var mn = arr[1]/60;
	var g = parseInt(arr[0],10)+mn+sec;	
	if(!sign)
		g=-g;	
	return g;
}

function wkt2decimal(val){
	var re=/MULTIPOINT\((-?[0-9]*\.?[0-9]*)\s(-?[0-9]*\.?[0-9]*)\)/
	var arr = re.exec(val);			
	if (arr==null || arr.length<3)
		return {lat:0, lon:0};
	return {lon:arr[1], lat:arr[2]};
}

function wkt2minute_sec(val){
	var re=/MULTIPOINT\((-?[0-9]*\.?[0-9]*)\s(-?[0-9]*\.?[0-9]*)\)/
	var arr = re.exec(val);			
	if (arr==null || arr.length<3)
		return {lat:0, lon:0};
	lat=Dec2minute_sec(arr[2]);
	lon=Dec2minute_sec(arr[1]);
/*	
	if(arr[2]<0)
		lat='S'+lat;
	else
		lat='N'+lat;
	if(arr[1]<0)
		lon='W'+lon;
	else
		lon='E'+lon;
*/	
	if(arr[2]<0)
		lat='S'+lat;
	else
		lat='N'+lat;
	if(arr[1]<0)
		lon='W'+lon;
	else
		lon='E'+lon;	
	return {lat:lat, lon:lon};
}



function ToDec(string) {
	in_arr  =  string.split(' ');
	
	var lon_vl = in_arr[1];	
	var lon_sign=lon_vl.toUpperCase().charAt(0);
	if(lon_sign=='E' || lon_sign=='W'){		
		lon_vl=lon_vl.substr(1);
	}
	else
		lon_sign='E';
	
	var lat_vl = in_arr[0];
	var lat_sign=lat_vl.toUpperCase().charAt(0);
	if(lat_sign=='N' || lat_sign=='S'){		
		lat_vl=lat_vl.substr(1);
	}
	else
		lat_sign='N';
		
	arr = lon_vl.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var lon_sec = sec/3600;
	var lon_mn = arr[1]/60;
	var lon = parseInt(arr[0],10)+lon_mn+lon_sec;	
	if(lon_sign!='E')
		lon=-lon;
	
	arr = lat_vl.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var lan_sec = sec/3600;
	var lan_mn = arr[1]/60;
	var lat = parseInt(arr[0],10)+lan_mn+lan_sec;
	if(lat_sign!='N')
		lat=-lat;
	
					
	var value = lon + " " + lat;
	return value;
};


function FromDec4coord(string, lon) {
	if(string=='')
		string = '0';		
	
	sign='N';
	if(lon){
		sign='E';
		if(string+0<0)
			sign='W';		
	}
	else{
		if(string+0<0)
			sign='S';		
	}
	res=Dec2minute_sec(string)

	
	var value =sign+res;
	return value;
};

function FromDec(string) {
	if(string=='')
		arr={0:'0',1:'0'};	
	else
		arr  =  string.split(' ');
	
	var value =FromDec4coord(arr[1],false) +" "+ FromDec4coord(arr[0],true);
	return value;
};

(function($) {
	//var pasteEventName = ($.browser.msie ? 'paste' : 'input') + ".coordinates";
	var iPhone = (window.orientation != undefined);
	var geos_init=false;

	$.coordinates = {
		//Predefined character definitions
		definitions: {
			'9': "[0-9]",
//			'N': "[NW]",
			'a': "[A-Za-z]",
			'*': "[A-Za-z0-9]"
		},
		dataName:"rawMaskFn"
	};
	$.fn.extend({
		//Helper Function for Caret positioning
		caret: function(begin, end) {
			if (this.length == 0) return;
			if (typeof begin == 'number') {
				end = (typeof end == 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						var range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					var range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		coordinates: function(coordinates, settings) {
			geos_init=true;
			if (!coordinates && this.length > 0) {
				var input = $(this[0]);
				return input.data($.coordinates.dataName)();
			}
			settings = $.extend({
				editpointmode: "default",
				placeholder: "_",
				degree_template:"N999°99'99.99''",
				completed: null,
				decimal:false
			}, settings);
	
			// Создание дополнительных элементов управления
			var input = $(this[0]);
			string=input.val();
			
			var lon = $( '<input class="coordinate form-control" id="lon_geos_editing" size="10"/>' );
			lon.attr( 'autocomplete','off' );
			var lon_buffer=settings.degree_template.replace(/9/g,"0").replace(/N/g,"E");
			var lon_firstValue='';
			this.after(lon);

			var lat= $( '<input class="coordinate form-control" id="lat_geos_editing" size="10"/>' );
			lat.attr( 'autocomplete','off' );				
			this.after(lat);
			var lat_buffer=settings.degree_template.replace(/9/g,"0");
			var lat_firstValue='';
			
			var initval=false;
			input.attr('style', 'display: none;');
			input.val('MULTIPOINT()');
			
			input.bind("setvalue.coordinates", function (e) {
					//var input=$('#'+e.target.id, this.form);
					var input=$(this);
					val=input.val();
					if(settings.decimal){
						r=wkt2decimal(val);
						lat_buffer=r.lat;
						lat_firstValue=r.lat;
						lon_buffer=r.lon;
						lon_firstValue=r.lon;
					}
					else{
						r=wkt2minute_sec(val);
						lat_buffer=r.lat;
						lat_firstValue=r.lat;
						lon_buffer=r.lon;
						lon_firstValue=r.lon;
					}
					
					initval=true;					
					lon.val(lon_buffer);
					lat.val(lat_buffer);
					writeBuffer();				
					initval=false;
				}
			);
			
			input.bind("decimal.coordinates", function (e) {					
					settings.decimal=true;
					var input=$(this);
					val=input.val();
					
					r=wkt2decimal(val);
					lat_buffer=r.lat;
					lat_firstValue=r.lat;
					lon_buffer=r.lon;
					lon_firstValue=r.lon;
					
					initval=true;					
					lon.val(lon_buffer);
					lat.val(lat_buffer);
					writeBuffer();				
					initval=false;
				}
			);
			
			input.bind("minute_sec.coordinates", function (e) {					
					settings.decimal=false;
					var input=$(this);
					val=input.val();
					
					r=wkt2minute_sec(val);
					lat_buffer=r.lat;
					lat_firstValue=r.lat;
					lon_buffer=r.lon;
					lon_firstValue=r.lon;					
					
					initval=true;					
					lon.val(lon_buffer);
					lat.val(lat_buffer);
					writeBuffer();				
					initval=false;
				}
			);			
			
			function clearBuffer(sub_input, start, end) {
				for (var i = start; i < end && i < len; i++) {
					if (tests[i])
						if(sub_input.attr('id')=='lon_geos_editing') buffer_lon[i] = settings.placeholder;
						else buffer_lan[i] = settings.placeholder;
				}
			};
			function writeBuffer() {
				var vlat, vlon;
				if(settings.decimal){
					vlon=lon_buffer;
					vlat=lat_buffer;
				}
				else{
					vlon=minute_sec2Dec(lon_buffer);
					vlat=minute_sec2Dec(lat_buffer);
				}
				test=vlon+' '+vlat;				
				test='MULTIPOINT('+test+')';				
				input.val(test);
				//return s;
			};			
		
			function testCoordinate(vl, lon){
				var numbers='0123456789';
				if(settings.decimal){
					if(!test_number(vl))
						return false;
					vl=parseFloat(vl);					
					if( (lon && (vl>180 || vl<-180)) || (!lon && (vl>90 || vl<-90)) )
						return false;
					else
						return true;
				}
				else{
					if (vl.length<3) return false;
					fs=vl.charAt(0);
					if(!(((fs=='N' || fs=='S') && !lon) || ((fs=='E' || fs=='W') && lon) ))
						return false;
					//degree_template:"N999°99'99''",
					
					for (var j=1; j < settings.degree_template.length; j++){
						if(settings.degree_template.charAt(j)=='9'){
							if ((numbers.indexOf(vl.charAt(j))==-1) )
								return false;
						}
						else{
							if (vl.charAt(j)!=settings.degree_template.charAt(j))
								return false;					
						}					
					}
					vl=vl.substr(1);
					var parts = vl.split(/\D/,4);
					if (parts[0]<0 || parts[0]>180)  return false;
					if (parts[1]<0 || parts[1]>=60)  return false;
					if (parts[2]<0 || parts[2]>=60)  return false;
					if (parts[3]<0 || parts[3]>=100)  return false;					
				}
				return true;
			};
		
///////////////////////
			var defs = $.coordinates.definitions;
			var tests = [];
			var partialPosition = coordinates.length;
			var firstNonMaskPos = null;
			var len = settings.degree_template.length;
			function keydownEvent(e) {
				var trg=$(e.target);
				var k=e.which;				
				//console.log('key down');				
					//backspace, delete, and escape get special treatment
				if(k == 8 || k == 46 || (iPhone && k == 127)){
					var pos = trg.caret(),
						begin = pos.begin,
						end = pos.end;
					
					var s_val= trg.val();
					var future_val='';
					if(pos.end-pos.begin!=0){
						future_val=s_val.substring(0,pos.begin)+s_val.substring(pos.end,s_val.length);
					}
					else if (k==8){
						future_val=s_val.substring(0,pos.begin-1)+s_val.substring(pos.begin,s_val.length);						
					}
					else if (k==46){
						future_val=s_val.substring(0,pos.begin)+s_val.substring(pos.begin+1,s_val.length);						
					}
					
					if (testCoordinate(future_val, e.target.id=='lon_geos_editing')/* tests[p].test(c)*/) {							
						if(e.target.id=='lon_geos_editing'){
							lon_buffer=future_val;
							lon.val(lon_buffer);
						}
						else{
							lat_buffer=future_val;
							lat.val(lat_buffer);
						}
						writeBuffer();
//						var next = seekNext(p);
						trg.caret(pos.begin);						
					}
					return false;
				} else if (k == 27) {//escape
					if(e.target.id=='lon_geos_editing')
						lon.val(lon_firstValue);
					else
						lat.val(lat_firstValue);					
					
					//trg.caret(0, checkVal(trg));
					return false;
				}
				else{
					//console.log('write buffer');					
					//writeBuffer(trg);
				}				
				return true;
			};
			
			function seekNext(pos) {
				while (++pos <= settings.degree_template.length && settings.degree_template[pos]!='9');
				if(pos==0)
					return 1;
				return pos;
			};
			
			function seekPrev(pos) {
				while (--pos >= 0 && settings.degree_template[pos]!='9');
				return pos;
			};
			
			function keypressEvent(e) {
				var trg=$(e.target);
				//console.log('keypress');		
				var k = e.which,
					pos = trg.caret();
				
				if (e.ctrlKey || e.altKey || e.metaKey || k<32) {//Ignore
					return true;
				} else if (k) {
					var c = String.fromCharCode(k);
					if(c==',')
						c='.';
					c=c.toUpperCase();
					var s_val= trg.val();
					var future_val='';
					var p=0;
					if(settings.decimal){
						future_val=s_val.substring(0,pos.begin)+c+s_val.substring(pos.end,s_val.length);
					}
					else{						
						if(pos.begin==0){
							future_val=c+s_val.substr(1);
							p=0;
						}
						else {
							p = seekNext(pos.begin - 1);
							if (p < len) {
								future_val=s_val.substring(0,p)+c+s_val.substring(p+1,s_val.length);
							}
							else
								return false;
						}					
					}
					
					if (testCoordinate(future_val, e.target.id=='lon_geos_editing')/* tests[p].test(c)*/) {							
						if(e.target.id=='lon_geos_editing'){
							lon_buffer=future_val;
							lon.val(lon_buffer);
						}
						else{
							lat_buffer=future_val;
							lat.val(lat_buffer);
						}
						writeBuffer();
						var next = seekNext(p);
						if(!settings.decimal)
							trg.caret(p+1);
						else
							trg.caret(pos.begin+1);
					}					
					return false;
				}
			};
			
			function pasteval(e) {
				e.preventDefault();
				var future_val = (e.originalEvent || e).clipboardData.getData('text/plain') || prompt('Paste something..');				
				future_val=future_val.replace(/,/g,".");
				if (testCoordinate(future_val, e.target.id=='lon_geos_editing')/* tests[p].test(c)*/) {							
					if(e.target.id=='lon_geos_editing'){
						lon_buffer=future_val;
						lon.val(lon_buffer);
					}
					else{
						lat_buffer=future_val;
						lat.val(lat_buffer);
					}
					writeBuffer();					
				}	
			};

			lon.bind("keydown.coordinates", keydownEvent);
			lat.bind("keydown.coordinates", keydownEvent);
			lon.bind("keypress.coordinates", keypressEvent);
			lat.bind("keypress.coordinates", keypressEvent);			
			lon.bind("paste",pasteval);
			lat.bind("paste",pasteval);
			writeBuffer();			
		}
		
	});
	
	
})(jQuery);


function w_geometry(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	this.fieldtype = 'point';
	var widget_name = 'point';
	// Additional properties array
	var addproparr = new Array();
	this.mapfile='map_point_'+makeid()+'.map';
	this.fullpath='F:/DATA/GISSTORAGE/mapkeeper/'+this.mapfile;
	this.parser=null;
	this.geometry_type='';
	

	// HTML element creation
	this.pnt_container;
	this.pnt_part;
	

	// Additional properties form
    this.viewPropForm = function (container){
		var ps=$("<label>Режим: </label><select id='point_mode' name='point_mode'><option value='single'>Одна точка</option><option value='multi'>Мультиточки</option></select><br>");
		container.append(ps);
		var ps=$("<label>Подпись объекта: </label><input type='text' id='label'/><br>");
		container.append(ps);
		$('#label',container).val(this.object.widget.properties.label);
		try {
			$('#point_mode',container).val(this.object.widget.properties.mode);
		} catch(e) { }
		var ps=$("<label>Ввод координат: </label><select id='edit_mode' name='edit_mode'><option value='default'>Градусы</option><option value='extended'>Градусы и десятичное представление</option></select>");
		container.append(ps);
		try {
			$('#edit_mode',container).val(this.object.widget.properties.editpointmode);
		} catch(e) { }
		return;		
    } 
    this.getPropForm = function (container){
	    return {mode: $('#point_mode',container).val(), editpointmode: $('#edit_mode',container).val(), label:$('#label',container).val()};
    }
	

	this.hasFilter = function (){
		return true;
	}

    return this;
}

extend(w_geometry, w_base);

	// Getting value of input field
w_geometry.prototype.getVal = function (){
	return this.getGeometry();	
}


w_geometry.prototype.getDatatableProperties = function (){
	var tempobj = new Object();
	tempobj.bVisible = true;	
	tempobj.sTitle = this.object.title;
	tempobj.sName = this.object.fieldname;
	tempobj.sType = this.object.type;			
	tempobj.type = 'widget_proxy';	
	tempobj.oWidget = this;
	tempobj.sWidgetProperties = this.object.widget.properties;
	tempobj.sFilterName = 'GeometryFilterWidget';
/*	tempobj.submit ='Ok';
	tempobj.cancel ='Cancel';*/
	tempobj.onblur = '';
	return tempobj;
}

w_geometry.prototype.addpart=function(){
		if(this.pnt_container===undefined)
			return;
		var tabs=$('#tabs',this.pnt_container);
		$(tabs).addClass('gp_activate');
		cnt=$( ".geom_part" , tabs).length;
		if(this.g_type!='point' && this.multi){
			var link_container=$('<li><a class="part_href" href="#tabs-'+cnt+'">'+cnt+' sketch</a> <i  title="Delete field" id="del_img" class="icon-remove"></i> </div>');
			//class="ui-button-icon-primary ui-icon ui-icon-closethick"
			$("#tab_list",tabs).append(link_container);
		}		
		var part_container=$('<div id="tabs-'+cnt+'" class="nav nav-list geom_part"/>');//btn-group btn-group-vertical
				
		tabs.append(part_container);
		var widget=this;
		//btn=$('<button class="new btn frigth" type="button">+</button>');
		if(this.g_type!='point' ){
			btn = $('<a class="new btn frigth" href="#"><i class="icon-plus-sign"></i></a>');
			btn.click(function() {
				widget.addpoint(part_container, $('.w_points',part_container).length, 0, 0);				
			});
			part_container.append(btn);
		}
		this.pnt_part=part_container;
		if(this.g_type!='point' && this.multi){
			tabs.tabs("refresh");
			tabs.tabs( "option", "active", cnt-1 );
		
			var del_btn=$("#del_img", link_container);
			del_btn.click(function(){
				link_container.remove();
				part_container.remove();
				var cnt=1;
				$('.part_href',tabs).each(function(){
					input=$(this);
					input.text(cnt+' sketch');
					cnt++;
				});
				
				return false
			});
		}
		
		var pnt_list=$('<div id="pnt_list"/>');//btn-group btn-group-vertical
		part_container.append(pnt_list);
		//part_container.append(btn);
		//tabs.sortable( "refresh" );
		tabs.find(".ui-tabs-nav").sortable({
			axis: "x",
			stop: function () {
				tabs.tabs("refresh");
			},
			sort: function(event, ui) {
				if ($(ui.item).hasClass("w_add_part")) {
					return false;
				}
			}
		});
		if(this.g_type!='point' && this.multi){
			tabs.tabs('option','active',cnt);
		}
		
		return part_container;
}

w_geometry.prototype.addpoint=function (part,index,x,y){
	while(x<-180)
		x+=360;
		
	var pnts=$('.w_points',part);
	var widget=this;
	if(index<pnts.length){
		input=$($('input',pnts[index])[0]);
		if(x!=undefined && y!=undefined){
			tr_coord = 'MULTIPOINT('+x + ' ' + y+')';
			input.val(tr_coord);
			input.trigger('setvalue.coordinates');
		}		
	}
	else{
		var d=$('<div class="w_points form-inline"/>');		
		pnt_list=$('#pnt_list',part);
		pnt_list.append( d ); 
		var input = $( '<input type="text"/>' );
        input.attr( 'autocomplete','off' );        
        d.append( input ); 
		input.coordinates("999°99\'99\'\'N", {placeholder: "0", decimal:decimalcoordinates} );	
		input.trigger('setvalue.coordinates');
		input.change(function() {
			//widget.Layer.destroyFeatures();
			widget.updateLayer();
		});
		if(x!=undefined && y!=undefined){
			tr_coord = 'MULTIPOINT('+x + ' ' + y+')';
			input.val(tr_coord);
			input.trigger('setvalue.coordinates');
		}
		//var btns=$("<i title='Move field down' id='movedown_field' class='icon-arrow-down'></i><i id='moveup_field' title='Move field up' class='icon-arrow-up'></i><i  title='Delete field' id='delete_field' class='icon-remove'></i>");
		//|| (this.g_type=='point' && !this.multi
		if(this.g_type!='point' ){
			var btns=$("<i  title='Delete field' id='delete_field' class='icon-remove'></i>");
			d.append(btns);
			
			$('#delete_field',d).click(function(){
				d.remove();
				//widget.Layer.destroyFeatures();
				widget.updateLayer();
				return false
			});
		}
		var cp=$("<i  title='Copy' id='copy_field' class='fas fa-copy'></i>");
		d.append(cp); 
		cp.click(function(){
			clipboard_buffer=input.val();			
			return false
		});
		d.append($('<span>&nbsp;</span>'));
		var pst=$("<i  title='Paste' id='paste_field' class='fas fa-paste'></i>");
		d.append(pst);
		pst.click(function(){
			input.val(clipboard_buffer);
			input.trigger('setvalue.coordinates');
			return false
		});
		d.click(function(){
			//$('.gp_activate', this.parentNode).addClass('gp_unactivate');
			$('.gp_activate', this.parentNode).removeClass('gp_activate');	
			//$(this).removeClass('gp_unactivate');
			$(this).addClass('gp_activate');	
		});

		all_pnts=$('.w_points', part);
		if(all_pnts.length>1){
			pnt_list.sortable({
				revert: true
			});
		}
		else if (all_pnts.length>2)
			pnt_list.sortable( "refresh" );		
	}
}

w_geometry.prototype.getGeometry=function(){
		var widget=this;
		var parts = new Array();
		var IsNULL=true;
		$( ".geom_part" , this.pnt_container).each(function( index ) {
			var pnts = new Array();
			var first_v=undefined;
			$( ".w_points" , this).each(function( index ) {
				input=$($('input', this)[0]);
				value=input.val();
				if(value=='MULTIPOINT()')
					return;
				wkt = new Wkt.Wkt();
				wkt.read(value);
				
				pnts[pnts.length]=wkt.components[0][0].x+' '+wkt.components[0][0].y;
				if(wkt.components[0][0].x!=0 || wkt.components[0][0].y!=0) 
					IsNULL=false;
				if(first_v===undefined)
					first_v=pnts[0];
			
			});
			if (widget.g_type=='polygon' && first_v!=undefined)
				pnts[pnts.length]=first_v;
			if (pnts.length>0)
				parts[parts.length]=pnts;
		});
		if(parts.length==0)
			return '';
		var str=widget.geometry_type;
		if(widget.geometry_type=='POINT')
			str='MULTIPOINT';
		if(widget.geometry_type=='LINESTRING')
			str='MULTILINESTRING';
		if(widget.geometry_type=='POLYGON')
			str='MULTIPOLYGON';
		hackmulti=true;
	
	
		if(this.g_type=='polygon' && hackmulti)
			str+='((';
		if(this.g_type=='line' && hackmulti)
			str+='(';			
		for(var i=0; i < parts.length; i++){			
			if (i!=0)
				str+=', ';
			str+='(';
			str+=parts[i].join(", ");
			str+=')';
		}
		if(this.g_type=='polygon' && hackmulti)
			str+='))';
		if(this.g_type=='line' && hackmulti)
			str+=')';
		if(IsNULL)
			return '';
		else
			return str;
}

w_geometry.prototype.setvalue = function(value, only_part){
	var part;
	if(only_part){
		if(this.g_type!='point' && this.multi){
		var activeTabIdx = this.tabs.tabs('option','active').index()-1;
			part=$($( ".geom_part" , this.pnt_container)[activeTabIdx]);
		}
		else
			part=this.pnt_part;
	}
	if(value!=undefined && value!=''){
		wkt = new Wkt.Wkt();
		try { // Catch any malformed WKT strings
			wkt.read(value);
		} catch (e1) {
			alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
			return;
		}
		
		config = map.defaults;

		if(wkt.type=='point'){
			if(wkt.components.length==1){
				var p_ind=0;
				$('.w_points', this.pnt_part).each(function( index ) {
					if($( this ).hasClass('gp_activate')){
						p_ind=index;					
					}
				});
				this.addpoint(this.pnt_part, p_ind, wkt.components[0].x, wkt.components[0].y);			
			}
			else{
				for(var i=0; i<wkt.components.length; i++){
					this.addpoint(this.pnt_part, i, wkt.components[i].x, wkt.components[i].y);
				}
			}
		}		
		if(wkt.type=='multipoint' ){
			for(var i=0; i<wkt.components[0].length; i++){
				this.addpoint(this.pnt_part, i, wkt.components[0][i].x, wkt.components[0][i].y);
			}
		}		
/*		else if(wkt.type=='point'){
			this.addpoint(this.pnt_part, 0, wkt.x, wkt.y);
		}*/
		else if(wkt.type=='linestring'){
			if(wkt.components.length==0)
				return;
			
			part=$($( ".geom_part" , this.pnt_container)[0]);
			if(part.length==0)
				part=this.addpart();
			$('.w_points',part).remove();					
			for(var j=0; j<wkt.components.length; j++){
				this.addpoint(part, j, wkt.components[j].x, wkt.components[j].y);
			}
								
		}		
		else if(wkt.type=='multilinestring'){
			if(wkt.components.length==0)
				return;
			for(var i=0; i<wkt.components.length; i++){
				part=$($( ".geom_part" , this.pnt_container)[i]);
				if(part.length==0)
					part=this.addpart();
				$('.w_points',part).remove();					
				for(var j=0; j<wkt.components[i].length; j++){
					this.addpoint(part, j, wkt.components[i][j].x, wkt.components[i][j].y);
				}
			}					
		}
		else if(wkt.type=='polygon'){
			if(wkt.components.length==0)
				return;
			for(var i=0; i<wkt.components.length; i++){
				part=$($( ".geom_part" , this.pnt_container)[i]);
				if(part.length==0)
					part=this.addpart();
				$('.w_points',part).remove();					
				for(var j=0; j<wkt.components[i].length-1; j++){
					this.addpoint(part, j, wkt.components[i][j].x, wkt.components[i][j].y);
				}
			}					
		}		
		else if(wkt.type=='multipolygon'){
			if(wkt.components.length==0)
				return;
			for(var i=0; i<wkt.components[0].length; i++){
				part=$($( ".geom_part" , this.pnt_container)[i]);
				if(part.length==0)
					part=this.addpart();
				$('.w_points',part).remove();					
				for(var j=0; j<wkt.components[0][i].length-1; j++){
					this.addpoint(part, j, wkt.components[0][i][j].x, wkt.components[0][i][j].y);
				}
			}					
		}
	}
}


w_geometry.prototype.getrect = function(value){
	var part;
	var max_x=-1000, min_x=1000, max_y=-1000, min_y=1000;
	
	
	if(value!=undefined && value!=''){
		wkt = new Wkt.Wkt();
		try { // Catch any malformed WKT strings
			wkt.read(value);
		} catch (e1) {
			alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
			return;
		}		
		

		if(wkt.type=='point'){			
			for(var i=0; i<wkt.components.length; i++){					
				min_x=Math.min(wkt.components[i].x, min_x);
				max_x=Math.max(wkt.components[i].x, max_x);
				min_y=Math.min(wkt.components[i].y, min_y);
				max_y=Math.max(wkt.components[i].y, max_y);				
			}			
		}		
		if(wkt.type=='multipoint' ){
			for(var i=0; i<wkt.components[0].length; i++){				
				min_x=Math.min(wkt.components[0][i].x, min_x);
				max_x=Math.max(wkt.components[0][i].x, max_x);
				min_y=Math.min(wkt.components[0][i].y, min_y);
				max_y=Math.max(wkt.components[0][i].y, max_y);				
			}
		}
		else if(wkt.type=='linestring'){			
			for(var j=0; j<wkt.components.length; j++){
				//this.addpoint(part, j, wkt.components[j].x, wkt.components[j].y);
				min_x=Math.min(wkt.components[j].x, min_x);
				max_x=Math.max(wkt.components[j].x, max_x);
				min_y=Math.min(wkt.components[j].y, min_y);
				max_y=Math.max(wkt.components[j].y, max_y);				
			}
		}		
		else if(wkt.type=='multilinestring'){
			for(var i=0; i<wkt.components.length; i++){								
				for(var j=0; j<wkt.components[i].length; j++){
//					this.addpoint(part, j, wkt.components[i][j].x, wkt.components[i][j].y);
					min_x=Math.min(wkt.components[i][j].x, min_x);
					max_x=Math.max(wkt.components[i][j].x, max_x);
					min_y=Math.min(wkt.components[i][j].y, min_y);
					max_y=Math.max(wkt.components[i][j].y, max_y);					
				}				
			}					
		}
		else if(wkt.type=='polygon'){
			for(var i=0; i<wkt.components.length; i++){								
				for(var j=0; j<wkt.components[i].length-1; j++){
					//this.addpoint(part, j, wkt.components[i][j].x, wkt.components[i][j].y);
					min_x=Math.min(wkt.components[i][j].x, min_x);
					max_x=Math.max(wkt.components[i][j].x, max_x);
					min_y=Math.min(wkt.components[i][j].y, min_y);
					max_y=Math.max(wkt.components[i][j].y, max_y);					
				}
			}					
		}		
		else if(wkt.type=='multipolygon'){
			for(var i=0; i<wkt.components[0].length; i++){									
				for(var j=0; j<wkt.components[0][i].length-1; j++){
					//this.addpoint(part, j, wkt.components[0][i][j].x, wkt.components[0][i][j].y);
					min_x=Math.min(wkt.components[0][i][j].x, min_x);
					max_x=Math.max(wkt.components[0][i][j].x, max_x);
					min_y=Math.min(wkt.components[0][i][j].y, min_y);
					max_y=Math.max(wkt.components[0][i][j].y, max_y);					
					
				}
			}					
		}
	}
	return {max_x:max_x, min_x: min_x, max_y: max_y, min_y:min_y};
}

w_geometry.prototype.activateDrawing = function(){
//	if(this.Layer==null) return;
	var widget=this;	
	
	//g_unactivate
	if(!this.form_element || this.form_element.length==0)
		return;
	$('.g_activate', this.form_element[0].parentNode).addClass('g_unactivate');
	$('#tabs', this.form_element[0].parentNode).hide();
	$('.g_activate', this.form_element[0].parentNode).removeClass('g_activate');	
	this.form_element.removeClass('g_unactivate');
	this.form_element.addClass('g_activate');
	$('#tabs', this.form_element).show();

	if(typeof add_map_handle !=="undefined" ){
		add_map_handle(function (gobject, mode){
			var wkt = new Wkt.Wkt();
			wkt.fromObject(gobject);
			var value = wkt.write();
			widget.setvalue(value);			
			widget.updateLayer();
		}, this.g_type, widget);
		widget.updateLayer();
	}
		
}

w_geometry.prototype.g_assign = function(form_element, value){	
	this.form_element = form_element;
	this.pnt_container = form_element;
	var widget=this;
	this.geom_object=null;
	
	if(this.g_type=='point'){
		if(this.multi)
			this.geometry_type='POINT';
		else
			this.geometry_type='MULTIPOINT';
	}
	else if(this.g_type=='line'){
		if(this.multi)
			this.geometry_type='MULTILINESTRING';
		else
			this.geometry_type='LINESTRING';	
			this.geometry_type='MULTILINESTRING';
	}
	else if(this.g_type=='polygon'){
		if(this.multi)
			this.geometry_type='MULTIPOLYGON';
		else
			this.geometry_type='POLYGON';	
			//this.geometry_type='MULTIPOLYGON';
	}
	
	
		
	form_element.click(function() {
		widget.activateDrawing();		
	});
	form_element.on("remove", function () {
		stop_map_edit(widget);
	})
	/*
	if(this.lvis){
		var label = $('<div style="display: inline-block;text-align: right;vertical-align: top">'+this.object.title+'</div>');
		this.pnt_container.append(label);
	}*/
	/*
	maplink=$('<a href="#">Map</a>');
	this.pnt_container.append(maplink);
	maplink.click(function(){
		if(!$('#mapcontainer').is(":visible")){
			$('#mapcontainer').dialog({
					  resizable: false,
					  height:'800',
					  width: '800',
					  modal: true,
					  buttons: {
						"Close": function() {							
							$( this ).dialog( "close" );							
						}
					  }
				});
		}
		
		map.invalidateSize(true);
	});*/
	var tabs=$('<div id="tabs"/>');
	//tabs.hide();
	this.pnt_container.append(tabs);
	
	ul=$('<ul id="tab_list" style="margin-bottom: 0px;" class="nav nav-tabs" />');
	tabs.append(ul);
	if(this.g_type!='point' && this.multi){
		var link_container=$('<li class="w_add_part" id="li-o"><a href="#tabs-0"></a><button id="add_part" class="new">+</button> </li>');		
		var add_btn=$('#add_part',link_container);
		add_btn.click(function() {
			widget.addpart();				
		});
		ul.append(link_container);
	}
//	var part_container=$('<li id="tabs-0" style="margin-bottom: 0px;" class="nav nav-lis"/>');
//	tabs.append(part_container);
	if(this.g_type!='point' && this.multi)
		tabs.tabs();
	var part=this.addpart();
	this.tabs=tabs;	
	this.setvalue(value);
	if(this.g_type=='point' && !this.multi){
		gparts=$( ".geom_part" , this.pnt_container);
		if(gparts.length==1){
			if ($( ".w_points" , gparts[0]).length==0){
				widget.addpoint(gparts[0],0,0,0);
			}
		};
	}
	this.form_element.addClass('g_unactivate');
}

w_geometry.prototype.updateLayer = function(){
	if(window.drawnItems !== undefined ){
		drawnItems.clearLayers();	

		val=this.getGeometry();
		if(val=='') return;	

		wkt = new Wkt.Wkt();
		try { // Catch any malformed WKT strings
			wkt.read(val);
		} catch (e1) {
			alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
			return;
		}
		if(this.g_type!='point')
			var activeTabIdx = this.tabs.tabs('option','active').index()-1;
		
		//this.geom_object = wkt.toObject(map.defaults); // Make an object	
			if(wkt.type=='point'){			
				for(var i=0; i<wkt.components.length; i++){
					this.geom_object=new L.circle([wkt.components[i].y,wkt.components[i].x], 200);				
					drawnItems.addLayer(this.geom_object);
				}
			}	
			if(wkt.type=='multipoint'){			
				for(var i=0; i<wkt.components[0].length; i++){
					this.geom_object=new L.circle([wkt.components[0][i].y,wkt.components[0][i].x], 500, {
						color: 'red',
						fillColor: '#f03',
						radius: 500
					});
					/*
					this.geom_object=new L.circle([wkt.components[0][i].y,wkt.components[0][i].x], {
						color: 'red',
						fillColor: '#f03',
						fillOpacity: 0.5,
						radius: 500
					})*/;
					//this.geom_object.addTo(map);
					drawnItems.addLayer(this.geom_object);
				}
			}		
	/*		else if(wkt.type=='point'){
				this.addpoint(this.pnt_part, 0, wkt.x, wkt.y);
			}*/
			else if(wkt.type=='linestring'){
				if(wkt.components.length==0)
					return;
				pnts=[];
				for (var i=0; i<wkt.components[activeTabIdx].length;i++){
					el=[wkt.components[i].y, wkt.components[i].x];
					pnts.push(el);
				}	
				this.geom_object=new L.Polyline(pnts);
				drawnItems.addLayer(this.geom_object);
			}		
			else if(wkt.type=='multilinestring'){
				if(wkt.components.length==0)
					return;
				pnts=[];
				for (var i=0; i<wkt.components[activeTabIdx].length;i++){
					el=[wkt.components[activeTabIdx][i].y, wkt.components[activeTabIdx][i].x];
					pnts.push(el);
				}							
				this.geom_object=new L.Polyline(pnts);
				drawnItems.addLayer(this.geom_object);
			}
			else if(wkt.type=='polygon'){
				if(wkt.components.length==0)
					return;
				pnts=[];
				for (var i=0; i<wkt.components[activeTabIdx].length;i++){
					el=[wkt.components[activeTabIdx][i].y, wkt.components[0][activeTabIdx][i].x];
					pnts.push(el);
				}
				this.geom_object=new L.Polygon(pnts);					
				drawnItems.addLayer(this.geom_object);
			}		
			else if(wkt.type=='multipolygon'){
				if(wkt.components.length==0)
					return;
				pnts=[];
				for (var i=0; i<wkt.components[0][activeTabIdx].length;i++){
					el=[wkt.components[0][activeTabIdx][i].y, wkt.components[0][activeTabIdx][i].x];
					pnts.push(el);
				}			
				this.geom_object=new L.Polygon(pnts);			
				drawnItems.addLayer(this.geom_object);
			}		
	}
}

/**
	* A filter widget based on data in a table column.
	* 
	* @class GeometryFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/
	$.GeometryFilterWidget = function ($Container, column, dt_table) {
		var widget = this;
		widget.SelectionLayer = null;
		widget.selectionControl = null;
		widget.column = column;
		widget.$Container = $Container;
		widget.oDataTable = dt_table;
	
		widget.SelectionItems = new L.FeatureGroup();
		widget.refFilterList = [];
	
		var metaDistrict = {
			"fieldname": "2171",
			"title": "Район",
			"description": "confreport",
			"visible": true,
			"type": "integer",
			"widget": {
				"name": "classify",
				"properties": {
					"dataset_id": 2172,
					"db_field": ["full_name"], // TODO full_name
					"control_type": "autoselect"
				}
			}
		};
	
		var metaRegion = {
			"fieldname": "2172",
			"title": "Субъект РФ",
			"description": "confreport",
			"visible": true,
			"type": "integer",
			"widget": {
				"name": "classify",
				"properties": {
					"dataset_id": 2171,
					"db_field": ["name"],
					"control_type": "autoselect"
				}
			}
		};
		// var user_widget=$.widgets.get_widget_byname('user');
	
		var widgetDistrict = $.widgets.get_widget(metaDistrict);
		var widgetRegion = $.widgets.get_widget(metaRegion);
	
		this.changevalue = function (fieldname, value) {
			var refName = ''
			switch (widget.asFilter) {
				case 'District':
					refName = widgetDistrict.getUserVal(value);
					break;
				case 'Region':
					refName = widgetRegion.getUserVal(value);
					break;
				default:
					throw new Error('Unsupported filter type');
					break;
			}		
	
			item = {
				dataset_id: fieldname,
				name: refName,
				value: value
			};
			this.refFilterList.push(item);
			this.fnFilter();
			widgetDistrict.input.val('');
		}	
		widgetDistrict.listnerlist.push(this);
		widgetRegion.listnerlist.push(this);
	
		widget.$btnOpenFile = $('<a class="" id="btnOpen">Open file</a>');
		widget.$regionAutocomplete = $('<div>Субъект РФ</div>');
		widget.$districtAutocomplete = $('<div>Район субъекта РФ</div>');
	
		widgetDistrict.assign(widget.$districtAutocomplete, '');
		widgetRegion.assign(widget.$regionAutocomplete, '');
	
		var options = new Object();
		options.filter = 'kml,kmz,wkt';
		widget.$btnOpenFile.click(function () {
			
		});
	
		widget.$regionAutocomplete.hide();
		widget.$districtAutocomplete.hide();	
	
		widget.asFilter = 'All';
		widget.sSeparator = ';';
		widget.iMaxSelections = -1;
		widget.$GeometryRange = $('<select class="form-control fltr_geom" />');
		var v = 'All';
		var option = $('<option />').val(v).append('Вся карта');
		widget.$GeometryRange.append(option);
	
		var v = 'Extent';
		var option = $('<option />').val(v).append('Экстент');
		widget.$GeometryRange.append(option);
	
		var v = 'Selection';
		var option = $('<option />').val(v).append('Внутри полигона');
		widget.$GeometryRange.append(option);
	
		var v = 'Region';
		var option = $('<option />').val(v).append('Регион');
		widget.$GeometryRange.append(option);
	
		var v = 'District';
		var option = $('<option />').val(v).append('Район');
		widget.$GeometryRange.append(option);
	
		var v = 'File';
		var option = $('<option />').val(v).append('Файл');
		widget.$GeometryRange.append(option);
	
		var SelectionItems;
		widget.$GeometryRange.change(function () {
			widget.asFilter = $(this).val();
			widget.$regionAutocomplete.hide();
			widget.$districtAutocomplete.hide();
			if (widget.asFilter == 'Selection') {
				map.addLayer(widget.SelectionItems);
				add_map_handle(function (gobject, mode) {
					//var wkt = new Wkt.Wkt();
					//wkt.fromObject(gobject);
					//var value = wkt.write();
					widget.SelectionItems.addLayer(gobject);
					widget.fnFilter();
					//alert(value);
					//widget.setvalue(value);			
					//widget.updateLayer();
				}, "polygon", this);
	
			}
			else if (widget.asFilter == 'Extent') {
				map.addLayer(widget.SelectionItems);
				add_map_handle(function (gobject, mode) {
					var wkt = new Wkt.Wkt();
					wkt.fromObject(gobject);
					var value = wkt.write();
					widget.SelectionItems.addLayer(gobject);
					widget.fnFilter();
					//alert(value);
					//widget.setvalue(value);			
					//widget.updateLayer();
				}, "rectangle", this);
	
			}
			else if (widget.asFilter == 'Region') {
				widget.$regionAutocomplete.show();
			}
			else if (widget.asFilter == 'District') {
				widget.$districtAutocomplete.show();
			}
			else if (widget.asFilter == 'File') {
				widget.$GeometryRange.val('All');
				OpenFileDlg(function (path) {
					readFile(path, function (data) {
						widget.$GeometryRange.val('Selection');
						widget.$GeometryRange.trigger('change');
		
						if (path.slice(path.lastIndexOf('.') + 1) == 'wkt') {
							wkt = new Wkt.Wkt();
		
							try { // Catch any malformed WKT strings
								wkt.read(data);
							} catch (e1) {
								try {
									wkt.read(data.value.replace('\n', '').replace('\r', '').replace('\t', ''));
								} catch (e2) {
									if (e2.name === 'WKTError') {
										alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
										return;
									}
								}
							}
		
							obj = wkt.toObject(this.map.defaults); // Make an object
		
							if (Wkt.isArray(obj)) { // Distinguish multigeometries (Arrays) from objects
								for (i in obj) {
									if (obj.hasOwnProperty(i) && !Wkt.isArray(obj[i])) {
										obj[i].name = get_filename(path) + '_' + i;
										widget.SelectionItems.addLayer(obj[i]);
									}
								}
							} else {
								obj.name = get_filename(path);
								widget.SelectionItems.addLayer(obj);
							}
						}
						else {
							layers = L.KML.parseKML(data);
							/*var wkt = new Wkt.Wkt();
							wkt.fromObject(layers);
							var value = wkt.write();*/
							for (i = 0; i < layers.length; i++) {
								if (layers[i]._popup != undefined && layers[i]._popup._content != undefined && layers[i]._popup._content != '')
									layers[i].name = layers[i]._popup._content;
								else
									layers[i].name = path;
		
								widget.SelectionItems.addLayer(layers[i]);
							}
						}
						widget.fnFilter();
					});
		
		
				}, options);
			}		
			else {
				map.removeLayer(widget.SelectionItems);
				stop_map_edit(this);
			}
	
			widget.fnFilter();
		});
	
		//widget.$label = $('<label for="from_' + widget.oColumn.sName + '">' + widget.oColumn.sTitle + '</label><br>');
		//widget.$Container.append( widget.$label );
		widget.$Container.append(widget.$GeometryRange);
		widget.$Container.append(widget.$regionAutocomplete);
		widget.$Container.append(widget.$districtAutocomplete);
		//widget.$Container.append(widget.$btnOpenFile);
		widget.filter_items = $('<div/>');
		widget.$Container.append(widget.filter_items);
		//widget.filter_items.hide();
	
		//widget.fnDraw();		
	};
	
	
	$.GeometryFilterWidget.prototype.update_list = function () {
		var widget = this;
		widget.filter_items.empty();
		var i = 0;
		sel_range_val = widget.$GeometryRange.val();
		if (sel_range_val == 'Selection' || sel_range_val == 'Extent') {
			widget.SelectionItems.eachLayer(function (layer) {
				i++;
				var TermCont = $('<div><a id="btnSave" title="Save the polygon"><i class="icon-briefcase"></i></a></div>');
				var layer_id = widget.SelectionItems.getLayerId(layer);
				if (layer.name)
					sText = $('<div>' + layer.name + '</div>').text();
				else
					sText = $('<div>Polygon ' + i + '</div>').text();
				var $TermLink = $('<a class="filter-term" href="#"></a>')
					.addClass('filter-term-' + sText.toLowerCase().replace(/\W/g, ''))
					.text(sText)
					.click(function () {
						// Remove from current filters array
						layer_id = $(this).data("layer_id");
						widget.SelectionItems.removeLayer(layer_id);
						widget.fnFilter();
						return false;
					});
				$TermLink.data("layer_id", layer_id);
	
				TermCont.append($TermLink);
				widget.filter_items.append(TermCont);
				$('#btnSave', TermCont).click(function () {
					var wkt = new Wkt.Wkt();
					wkt.fromObject(layer);
					var value = wkt.write();
					SaveFileDlg(function (path) {
						saveFile(path, value);
					}, { filter: 'wkt' });
	
				});
	
			});
		}
		else if (sel_range_val == 'District' || sel_range_val == 'Region') {
		// else if (sel_range_val == 'District' ) {
			widget.refFilterList.forEach(function (item) {
				var $displayItem = $('<div class="filter-item"></div>');
				var $TermLink = $('<a class="filter-term" href="#"></a>')
					.data("spatial-bound", item)
					.text(item.name)
					.click(function () {
						var thisSpatialBound = $(this).data("spatial-bound");
						// Remove from current filters array
						widget.refFilterList = widget.refFilterList.filter(function(i) {
							return thisSpatialBound != i;
						});
						widget.fnFilter();
						return false;
					});
	
				$displayItem.append($TermLink);
				widget.filter_items.append($displayItem);
			});		
		}	
		if ($('.filter-term', widget.filter_items).length > 0) {
			widget.filter_items.show();
		}
		else {
			widget.filter_items.hide();
		}
	};
	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.GeometryFilterWidget.prototype.fnFilter = function () {
		var widget = this;
		if (widget.asFilter == '' || widget.asFilter == 'All') {
			widget.oDataTable.fnFilter('', widget.column.fieldname);
		}
		else if (widget.asFilter == 'Selection' || widget.asFilter == 'Extent') {
			var s_branch = '';
			this.SelectionItems.eachLayer(function (layer) {
				coords='';
				for(var i=0; i<layer._latlngs[0].length; i++){
					if(coords!='')
						coords +=', ';
					coords +=layer._latlngs[0][i].lng + ' ' + layer._latlngs[0][i].lat;
				}
				coords +=', ';
				coords +=layer._latlngs[0][0].lng + ' ' + layer._latlngs[0][0].lat;
				coords='POLYGON(('+coords+'))';
				
				//var wkt = new Wkt.Wkt();
				//wkt.fromObject(layer);
				//var value = wkt.write();
				if (s_branch != '')
					s_branch += ';';
				s_branch += coords;
				
			});
			widget.oDataTable.fnFilter(s_branch, widget.column.fieldname, true, false);
		}
		else if (widget.asFilter == 'District' || widget.asFilter == 'Region') {
			var s_branch = '';
			var geoTableId = null;
			switch (widget.asFilter) {
				case 'District':
					geoTableId = '2172';
					break;
				case 'Region':
					geoTableId = '2171';
					break;
				default:
					throw new Error('Unsupported fiter type');
					break;
			}
			widget.refFilterList.forEach(function (item) {
				var itemString = 'geotable:' + geoTableId + ':' + item.value + ':geom';
				if (s_branch != '') { s_branch += ';' };
				s_branch += itemString;
			});
			// debugger;
			widget.oDataTable.fnFilter(s_branch, widget.column.fieldname, true, false);
		}
		this.update_list();
	
		setTimeout(function () {
			$('.g_layer', $('#layer_list')).each(function (index) {
				ml = $(this).data("mlayer");
				//if(widget.oColumn.oWidget.metaid==ml.layer_id)
				ml.redraw();
			});
		}, 150);
	};
	
	
function w_hidden(object) {
    this.object = object;
    this.node='';	
	var widget_name = 'hidden';
	this.user_visible = true;
	this.rname = 'Hidden';
	this.visible=false;
	this.assign = function(form_element, value){	
		this.form_element = form_element;

		this.input = $('<input type="hidden"/>');
		this.input.attr('id', this.object.fieldname);		
		form_element.append(this.input);		
		if(value!=undefined && value!='')
			this.input.val(value);
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
    this.getVal = function (){
		return this.input.val();
    }
	
    
    

    
    this.viewpropForm=function (){
	    return;
    }    
    
    this.getprop=function (){
	    return this.object;
    }
    
    this.getDatatableProperties=function (){
		var tempobj = new Object();
		tempobj.bVisible = this.object.visible;	
		tempobj.sTitle = this.object.fieldname;
		tempobj.sWidth = '30px';
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'text';		
		tempobj.sWidget = 'text';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';	
	    return tempobj;
    }
    return this;
}


extend(w_hidden, w_base);
$.widgets.RegisterWidget('hidden', w_hidden);

var slider_str='<div id="slider" style="position: absolute;top: 10px; width: 100%; display: none;">\
    <div id="slider1_container" style="position: relative; width: 600px; margin: auto;    height: 300px;">\
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">\
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block; background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;"> </div>\
            <div style="position: absolute; display: block; background: url(/img/slider/loading.gif) no-repeat center center; top: 0px; left: 0px;width: 100%;height:100%;"> </div>\
        </div>\
        <div id="slider_content" u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">\
        </div>\
        <div u="thumbnavigator" class="jssort09" style="position: absolute; bottom: 0px; left: 0px; height:60px; width:600px;">\
            <div style="filter: alpha(opacity=40); opacity:0.4; position: absolute; display: block; background-color: #ffffff; top: 0px; left: 0px; width: 100%; height: 100%;"></div>\
            <div u="slides">\
                <div u="prototype" style="POSITION: absolute; WIDTH: 600px; HEIGHT: 60px; TOP: 0; LEFT: 0;">\
                    <div u="thumbnailtemplate" style="font-family: verdana; font-weight: normal; POSITION: absolute; WIDTH: 100%; HEIGHT: 100%; TOP: 0; LEFT: 0; color:#000; line-height: 60px; font-size:20px; padding-left:10px;"></div>\
                </div>\
            </div>\
        </div>\
        <div u="navigator" class="jssorb01" style="bottom: 16px; right: 10px;"> <div u="prototype"></div> </div>\
        <span u="arrowleft" class="jssora05l" style="top: 123px; left: 8px;"> </span>\
        <span u="arrowright" class="jssora05r" style="top: 123px; right: 8px;"> </span>\
		<div style="width: 20px;right: 10px;position: absolute;">\
			<a href="#" id="close_btn" class="ui-dialog-titlebar-close ui-corner-all" role="button"><span class="ui-icon ui-icon-closethick">close</span></a>\
		</div>\
    </div>\
</div>';
var jssor_slider1;




function viewimage(a){
	var cImage = $('<div class="image_form" title="Image">\
			<img src="'+$(a).attr('src')+'"/>\
			</div>');

			cImage.dialog({
				resizable: true,
				height: 'auto',
				width: '50%',
				modal: false,
				buttons: {
					"Close": function () {				
						$(this).dialog("close");
						cImage.remove();
					}
				}
			});
}


function w_image(object) {
	// Object for creation of widget
    this.object = object;
	// Common name of widget
	var widget_name = 'image';
	this.user_visible = true;
	this.rname = 'Изображение';
	// Additional properties array
	var addproparr = new Array();
	this.form_element = '';
	var images;
	var pluscard;
	var props={};
	if(this.object && this.object.widget && this.object.widget.properties)
		props = this.object.widget.properties;
	var userDataString = $.cookie('userData') || localStorage.user || '{}';
	var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));		

	this.addImg=function(user_id,path, title, link, mode){
		//if(user.id==user_id){
		//	link="/fm?cmd=file&target="+gethash(path);
		//}
		
		var exist=false;
		$( ".img_path" , this.form_element).each(function( index ) {
			var file_div=$(this);
			if(path==file_div.data("path")){
				//exist=true;
			}			
		});	
		if(exist)
			return;
		if(title===undefined || title==='undefined')
			title='';
		
		if(mode=='view'){
			if(props.notitle===undefined || props.notitle===true)
// 				var card=$('<div class="span2 img_path"> \
// <a href="#" class="thumbnail"><img onclick="viewimage(this)" src="'+link+'" class="smallimage" alt="" onload="" style="margin-left: 0px; margin-right: 0px; margin-top: 2px;" ></a>\
// </div>');
				var card=$('<div class="card" style="width: 18rem;">\
				<img onclick="viewimage(this)" src="'+link+'" class="card-img-top" >\
				<div class="card-body">\
				</div>\
				</div>');
				
			else
				var card=$('<div class="card" style="width: 18rem;">\
					<img onclick="viewimage(this)" src="'+link+'" class="card-img-top" >\
					<div class="card-body">\
					<h5 class="card-title">'+title+'</h5>\
					</div>\
				</div>');
// 				var card=$('<div class="span2 img_path"> \
// <a href="#" class="thumbnail"><img onclick="viewimage(this)" src="'+link+'" class="smallimage" alt="" onload="" style="margin-left: 0px; margin-right: 0px; margin-top: 2px;" ><p>'+title+'</p></a>\
// </div>');
		}
		else{
			if(props.notitle===undefined || props.notitle===true)
// 				var card=$('<div class="span2 img_path"> <i title="Delete image" class="icon-remove close"/>\
// <a href="#" class="thumbnail"><img onclick="viewimage(this)" src="'+link+'" class="smallimage" alt="" onload="" style="margin-left: 0px; margin-right: 0px; margin-top: 2px;" ></a>\
// </div>');
				var card=$('<div class="card" style="width: 18rem;"><i title="Delete image" class="fas fa-window-close close"/>\
				<img onclick="viewimage(this)" src="'+link+'" class="card-img-top" >\
				<div class="card-body">\
				  <h5 class="card-title"></h5>\
				  <p class="card-text"> </p>\
				</div>\
			  </div>');
			else
				var card=$('<div class="card" style="width: 18rem;"><i title="Delete image" class="fas fa-window-close close"/>\
					<img onclick="viewimage(this)" src="'+link+'" class="card-img-top" >\
					<div class="card-body">\
					<h5 class="card-title"></h5>\
					<p class="card-text"></p>\
					</div>\
				</div>');
// 				var card=$('<div class="span2 img_path"> <i title="Delete image" class="icon-remove close"/>\
// <a href="#" class="thumbnail"><img onclick="viewimage(this)" src="'+link+'" class="smallimage" alt="" onload="" style="margin-left: 0px; margin-right: 0px; margin-top: 2px;" ></a><input type="text" class="span2" id="title" value="'+title+'"/>\
// </div>');
		}
		images.prepend(card);
		
		
		card.data('path', path);
		card.data('user_id', user_id);
		$('.close', card).click(function() {
			card.remove();
		});
	}
	
	// HTML element creation
    this.assign = function(form_element, value){

		var widget=this;
		this.form_element = form_element;
		this.form_element.addClass('imagecontainer');
		
					


		//var current_image_path=$.cookie("current_image_path");
		var current_image_path=localStorage.getItem('current_image_path');		
		
		/*
		if(current_image_path==null && $.cookie('userData')!=null){
			userdata=$.cookie('userData');
			ud=JSON.parse(decodeURIComponent(userdata == 'undefined' ? '{username:""}' : userdata));
			current_image_path='/' + ud.username;
		}
		
		var label = $('<span>Current path for upload (click here to change):</span>');
		form_element.append(label);

		
		var cpath = $('<div></div>');
		label.append(cpath);
		label.click(function() {
			OpenFolderDlg(function(path){
				localStorage.setItem('current_image_path', path);
				//$.cookie('current_image_path', path);
				current_image_path=path;
				cpath.html(current_image_path);
			});
		});	
		
		
		cpath.html(current_image_path);
		

		var btnsdiv=$('<div style=""></div>');
		form_element.append(btnsdiv);
		var select_image=$('<button class="btn" type="button">Select file in the cloud</button>');
		btnsdiv.append(select_image);
		
		var uploaddiv=$('<div class="fileUpload btn"><span>Upload</span><input type="file" id="image_upload" class="upload" /></div>');
		var upload_image=$("input", uploaddiv);
		btnsdiv.append(uploaddiv);		
		var label = $('<label>Image list</label>');
		form_element.append(label);		
		*/
		/*
		images = $('<ul class="thumbnails"/>');
		form_element.append(images);*/
		images=form_element;
// 		pluscardvar=$('<li class="span2 addcard"> \
// <a href="#" class="thumbnail"><i class="icon-plus"></i>Add image</a>\
// </li>');
		pluscardvar=$('<a href="#" class="thumbnail"><i class="icon-plus"></i>Add image</a>');
		images.append(pluscardvar);
		
		
		/*
		upload_image.change(function(){
			uploadfilename='';
			var control = document.getElementById("image_upload");
			var i = 0,
			files = control.files,
			len = files.length;		
			if(len==0){			
				return false;
			}			
			if (current_image_path=='' || current_image_path===undefined || current_image_path==null){
				alert('You should select dir to upload!');
				return false;
			}
			uploadfilename=current_image_path+dir_separator+files[0].name;		
			
			var form = new FormData();
			form.append("cmd", "upload");
			dir=get_directory(uploadfilename);
			form.append("target", gethash(dir));
			form.append("files", control.files[0]);			
			
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				console.log("Отправка завершена");
				widget.addImg(user.id, uploadfilename,'');		
			};
			xhr.open("post", "/fm", true);
			xhr.send(form);
		});*/
		var options = new Object();
		options.filter='gif,png,jpg,jpeg';
		pluscardvar.click(function() {
			OpenFileDlg(function(path){
				widget.addImg(user.id, path, "", "/fm/op?cmd=file&target="+gethash(path));
			},options);
		});
		
		
		if(value!=undefined && value!=''){	
			if(widget.doc)
				var row_id=widget.doc.id;		
			else
				var row_id=-1;
			files=value.split(';');
			for (i=0; i < files.length; i++) {
				s=files[i].split('###');
				if(s!=undefined && s.length>0){
					p='';
					n='';
					user_id='';
					if(s.length==2){
						p=s[0];
						n=s[1];
					}
					else if (s.length==3){
						p=s[1];
						n=s[2];
						user_id=s[0];						
					}
					else
						continue;
					this.addImg(user_id,getpath(p), n, '/dataset/file?f='+widget.metaid+'&row_id='+row_id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id);
				}
			}
		}
/*
		var view_image=$('<div>View</div>');
		view_image.click(function() {			
			$( ".img_path" , widget.form_element).each(function( index ) {
				var file_div=$(this);
				path=file_div.data("path");
				links.push('/geothemes/bl_file?dataset_id='+widget.metaid+'&row_id='+widget.curent_row_id+'&fieldname='+widget.object.fieldname+'&filehesh='+gethash(path));				
			});	
			var gallery = blueimp.Gallery(links);
			
		});
		this.form_element.append(view_image);*/
    }
    
	// Getting value of input field
    this.getVal = function (){
		var files=new Array();
		$( ".card" , this.form_element).each(function( index ) {
			var file_div=$(this);
			title=$('#title',this).val();
			path=file_div.data("path");
			
			
			var userDataString = $.cookie('userData') || localStorage.user || '{}';
			var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));
			if(user.id)
				user_id=user.id;
			else		
				user_id=file_div.data("user_id");
			files[files.length]=user_id+'###'+gethash(path)+'###'+title;
		});	
		return files.join(';');
    }
	this.hasFilter = function (){
		return false;
	}

    this.getUserVal=function (val, view){
		if(this.gr_fn=='none' )
			return '';		
		var widget=this;
		var row_id=widget.cntr;
		if(widget.doc)
				var row_id=widget.doc.id;	
		if(this.gr_fn!=null || typeof val =='number' )
			return val;
		images = $('<ul class="thumbnails"/>');
		//form_element.append(images);
		
		
		res='';
		var links=new Array();
		if(val!=undefined && val!=''){			
			var files=val.split(';');	
			
			
			var unique = 'img_'+randomString(10);
			
			if(files.length>0){				
				res='<i id="'+unique+'" class="icon-play-circle"></i> ';//icon-play-circle
				if(view){
					res+='';
					var default_value=''
					for (i=0; i < files.length; i++) {
						s=files[i].split('###');
						if(s!=undefined && s.length>0 ){
							p='';
							n='';
							user_id='';
							if(s.length==2){
								p=s[0];
								n=s[1];
							}
							else if (s.length==3){
								p=s[1];
								n=s[2];
								user_id=s[0];						
							}
							else
								continue;
							if(n==''){
								n=default_value;										
							}
							link='/dataset/file?f='+widget.metaid+'&row_id='+row_id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id;							
							
							res+='<img title="'+n+'" class="smallimagelist" src="'+link+'" />';
							this.addImg(user_id,getpath(p), n, '/dataset/file?f='+widget.metaid+'&row_id='+row_id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id, "view");

						}
					}
					return images.html()
					
				}
					setTimeout(function() {
						$('#'+unique).click(function(){
							$('#slider').remove();
							$('body').append(slider_str);
							
							var slider_div=$('#slider_content');
							slider_div.empty();
							
							var trow=$(this).parents('tr');
							aData=trow.data('doc');
							var default_value=''
							if(aData){								
								for (var i = 0; i < widget.doc_template.doc_description.columns.length; i++) {
									if(widget.doc_template.doc_description.columns[i].widget_object===undefined)
											continue;
									if(widget.doc_template.doc_description.columns[i].widget_object.widget_name===undefined || !widget.doc_template.doc_description.columns[i].widget_object.widget_name in ['edit','Classificator','date'])
										continue;
									if (widget.doc_template.doc_description.columns[i].fieldname in aData){
										value=aData[widget.doc_template.doc_description.columns[i].fieldname];
										if(value=='' || value===undefined || value==null)
											continue;
										value=widget.doc_template.doc_description.columns[i].widget_object.getUserVal(value);
										
										if(default_value!='')
											default_value+=', ';
										default_value+=value;
									}									
								}
							}
							var images='';
							for (i=0; i < files.length; i++) {
								s=files[i].split('###');
								if(s!=undefined && s.length>0 ){
									p='';
									n='';
									user_id='';
									if(s.length==2){
										p=s[0];
										n=s[1];
									}
									else if (s.length==3){
										p=s[1];
										n=s[2];
										user_id=s[0];						
									}
									else
										continue;
									if(n==''){
										n=default_value;										
									}
									link='/dataset/file?f='+widget.metaid+'&row_id='+aData.id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id;
									/*									
									obj={title:n, href:link, type: 'image/jpeg'};
									links.push(obj);									*/
									
									images+='<div>\
										<img u=image src="'+link+'" />\
										<div u="thumb">'+n+'</div>\
									</div>';

								}
							}
							
							slider_div.html(images);
							
							//var gallery = blueimp.Gallery(links);
							var options = {
								$AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
								$FillMode: 1,
								$AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
								$AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
								$PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

								$ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
								$SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
								$MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
								//$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
								//$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
								$SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
								$DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
								$ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
								$UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
								$PlayOrientation: 2,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
								$DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

								$BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
									$Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
									$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
									$ActionMode: 3,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
									$Lanes: 2,                                      //[Optional] Specify lanes to arrange items, default value is 1
									$SpacingX: 10,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
									$SpacingY: 10                                    //[Optional] Vertical space between each item in pixel, default value is 0
								},

								$ArrowNavigatorOptions: {
									$Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
									$ChanceToShow: 1                                //[Required] 0 Never, 1 Mouse Over, 2 Always
								},

								$ThumbnailNavigatorOptions: {
									$Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
									$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
									$ActionMode: 0,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
									$DisableDrag: true,                             //[Optional] Disable drag or not, default value is false
									$Orientation: 2                                 //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
								}

							};


							$('.ui-icon-closethick', $('#slider').show()).click(function(){
								$('#slider').remove();
							});						

							jssor_slider1 = new $JssorSlider$("slider1_container", options);
							var bodyWidth = document.body.clientWidth;
							//var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
							if (bodyWidth)
								//jssor_slider1.$ScaleWidth(Math.min(parentWidth-300, 600));
								jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 1200));
							$("#slider1_container").zIndex(7200);

							return false;
						});
						
						$(document).keyup(function(e) {
							if (e.which == 27) $('#slider').remove();
						});
					}, 100);				
				

			}
			
		}
		
		
	    return res;
    }
	
   
    this.viewPropForm = function (container){
	   
		var ps=$("<label>Default image directory</label><input type='text' id='image_dir' value=''/><br/>");
		var input = $('#image_dir', ps);
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.image_dir!=undefined   )
		   $('#image_dir', ps).val(this.object.widget.properties.image_dir);

		var img=$('<button class="btn" type="button">Open</button>');
		container.append(img);
		
		var ps1=$("<label>Has not title:</label><input type='checkbox'  value=true /><br/>");
		container.append(ps1);
		this.notitle = $('input', ps1);
		
		if(this.object.widget.properties!=undefined && this.object.widget.properties.notitle   )
		   this.notitle.attr('checked', true);
		var options = new Object();
		 
		img.click(function() {
			OpenFileDlg(function(path){
				input.val(path);
				input.trigger('change');
			},options);
		});
		   
		return;
    }    
    this.getPropForm = function (container){
		
	    return {image_dir: $('#image_dir', container).val(), notitle: this.notitle.attr('checked')};
    } 
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'file';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sWidth = '12px';

		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_image, w_base);

$.widgets.RegisterWidget('image', w_image);

function w_line(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'line';
	this.fieldtype = 'line';
	this.user_visible = true;
	this.rname = 'Линия';	
	// HTML element creation
    this.assign = function(form_element, value){	
		if(this.object.widget.properties.mode == 'multi')
			this.multi=true;		
		else
			this.multi=false;
		this.g_type='line';		
		this.g_assign(form_element, value);
		return;
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return this.getGeometry();
    }
    
	// Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		if(this.gr_fn=='count' || typeof val =='number' )
			return val;
		
		if (val == null) return 'Кликните для редактирования'
		else return 'Линия';
    }    
	
	this.hasFilter = function (){
		return true;
	}
	// Additional properties form
	this.viewPropForm = function (container){
		var ps=$("<label>Режим: </label><select id='line_mode' name='line_mode'><option value='single'>Одна линия</option><option value='multi'>Мультилинии</option></select><br>");
		container.append(ps);
		var ps=$("<label>Подпись объекта: </label><input type='text' id='label'/><br>");
		container.append(ps);
		if(this.object.widget.properties!=undefined)
			$('#label',container).val(this.object.widget.properties.label);
		
		try {
			$('#line_mode',container).val(this.object.widget.properties.mode);
		} catch(e) { }
		var ps=$("<label>Ввод координат: </label><select id='edit_mode' name='edit_mode'><option value='default'>Градусы</option><option value='extended'>Градусы и десятичное представление</option></select>");
		container.append(ps);
		try {
			$('#edit_mode',container).val(this.object.widget.properties.editpointmode);
		} catch(e) { }
		return;		
    } 
    this.getPropForm = function (container){
	    return {mode: $('#line_mode',container).val(), editpointmode: $('#edit_mode',container).val(), label:$('#label',container).val(), gtype: 'line'};
    }
	
    return this;
}

extend(w_line, w_geometry);
$.widgets.RegisterWidget('line', w_line);﻿


function w_links(object) {
	var widget=this;
	var _self=this;
	// Object for creation of widget	
    this.object = object;
	
	this.cntr = '';
	// Common name of widget
	var widget_name = 'links';
	// Additional properties array
	var addproparr = new Array();
	this.user_visible = true;
	this.rname = 'links';		
	this.fieldtype = 'integer';
	// id of selected value
	var sel_id = '';
	var ft= new Array();
	this.tables=[];
	this.filter={};
	this.fields='';
	var widget=this;
	var askdataset_id='';
	
	if(this.object!=undefined && this.object.widget!=undefined){
		
		var props = this.object.widget.properties;
		if(props && props.dataset_id){
			askdataset_id = props.dataset_id;
			getdatasetmeta(props.dataset_id  , function(error, table_json){
				if(error!=null)
					return;
				table_json.metaid = props.dataset_id;			
				if(widget.fields!=''){
					var displayfields=widget.fields.split(',');
					if(displayfields.length>0){
						displayfields.push('id');			
						for(var i=table_json.columns.length - 1; i>=0; i--){				
							if( table_json.columns[i].fieldname==props.reftablename)
								table_json.columns[i].visible=false;				
							if((displayfields.length>0 && displayfields.indexOf(table_json.columns[i].fieldname)==-1) )
								table_json.columns.splice(i,1);
						}
					}
				}
				widget.table_json=table_json;
				widget.doc_frm= new doc_template(table_json);
				widget.doc_frm.init_form(function(){		
			
				});
			});
			if(props!=undefined && props.db_field!=null && props.db_field!=undefined)
				this.fields=props.db_field.join(',');
			
		}
	}
	
	this.drawobjects=function (view){
		this.object_container.empty()
		for (var i = 0; i < _self.objectlist.length; i++) {
			if(_self.objectlist[i].del === true)
				continue;
			if(!view){
				if(_self.objectlist[i].action=='add')
					var row=$('<div class="col-sm-4 cardlist">\
					<div class="card text-center" style="width: 22rem;">\
						<div class="card-header">\
						<ul class="nav nav-pills card-header-pills">\
						<li class="nav-item"><a class="nav-link active edit-link" href="#">Edit </a></li>\
						<li class="nav-item"><a class="nav-link del-link" href="#"> Delete </a></li>\
						</ul>\
						</div>\
						<div class="card-body object_text">\
						</div>\
						</div>\
					</div>');
				else
					var row=$('<div class="col-sm-4 cardlist">\
					<div class="card text-center" style="width: 22rem;">\
						<div class="card-header">\
						<ul class="nav nav-pills card-header-pills">\
						<li class="nav-item"><a class="nav-link del-link" href="#"> Delete </a></li>\
						</ul>\
						</div>\
						<div class="card-body object_text">\
						</div>\
						</div>\
					</div>');
			}
			else
				var row=$('<div class="col-sm-4 cardlist">\
				<div class="card text-center" style="width: 22rem;">\
					<div class="card-header">\
					<ul class="nav nav-pills card-header-pills">\
					</ul>\
					</div>\
					<div class="card-body object_text">\
					</div>\
					</div>\
				</div>');
			row.data('doc',_self.objectlist[i])
			this.object_container.append(row);
			widget.doc_frm.show_form($('.object_text',row), _self.objectlist[i], '', true);
		}
		$('.del-link', this.object_container).click(function(){
			if(confirm("Delete?")){						
				var doc=$(this).parents('.cardlist').data('doc');
				index=_self.objectlist.indexOf(doc);
				if(index!=-1)
					_self.objectlist.splice(index,1)
				
				_self.drawobjects(false);
			}
		});
		$('.edit-link', this.object_container).click(function(){
			var doc=$(this).parents('.cardlist').data('doc');
			var AddDlg = $('<div class="image_form" title="Item">\
			</div>');
			if(widget.doc_frm){
				var row=$('<div class="form'+askdataset_id+'"><div class="fields"></div><div class="btn-group"> <button class="btn add">Save</button>  <button class="btn cancel">Cancel</button> </div></div>');
				AddDlg.append(row);
				widget.doc_frm.show_form($('.fields',row), doc, '', false);
				
				$('.add',row).click(function(){
					widget.doc_frm.save_form(function(newobject, msg){
						if (msg.status != 'ok') {
							var error_box = $('.text-error', container);
							if (error_box.length > 0) {
								error_box.html(msg);
							}
							else
								alert(msg);
							return;
						}
						else{
							index=_self.objectlist.indexOf(doc);
							if(index!=-1){
								_self.objectlist[index]=newobject;
								_self.objectlist[index].action='add';
							}
							_self.drawobjects(false);
							row.remove();
							AddDlg.dialog("close");
							AddDlg.remove();
						}
							
					}, false);
				})	
				$('.cancel',row).click(function(){
					
					row.remove();
					AddDlg.dialog("close");
					AddDlg.remove();
				})					
			}
			
			AddDlg.dialog({
				resizable: true,
				height: 'auto',
				width: 'auto',
				modal: false,
				buttons: {
				}
			});
						
			
		});		
	}
	
	this.AddItem=function (){		
		var AddDlg = $('<div class="image_form" title="Item">\
		</div>');
		if(widget.doc_frm){
			var row=$('<div class="form'+askdataset_id+'"><div class="fields"></div><div class="btn-group"> <button class="btn add">Save</button>  <button class="btn cancel">Cancel</button> </div></div>');
			AddDlg.append(row);
			widget.doc_frm.show_form($('.fields',row), {}, '', false);
			//btns.hide();
			$('.add',row).click(function(){
				widget.doc_frm.save_form(function(newobject, msg){
					if (msg.status != 'ok') {
						var error_box = $('.text-error', container);
						if (error_box.length > 0) {
							error_box.html(msg);
						}
						else
							alert(msg);
						return;
					}
					else{
						newobject.action='add';
						_self.objectlist.push(newobject);
						_self.drawobjects(false);
						row.remove();
						AddDlg.dialog("close");
						AddDlg.remove();
					}
						
				}, false);
			})	
			$('.cancel',row).click(function(){				
				row.remove();
				AddDlg.dialog("close");
				AddDlg.remove();
			})					
		}
		
		AddDlg.dialog({
			resizable: true,
			height: 'auto',
			width: 'auto',
			modal: false,
			buttons: {
			}
		});
				
	}
	
	this.SelectItem=function (){		
		var AddDlg = $('<div class="image_form" title="Item">\
		</div>');

		// var metadataset_select = {
		// 	"fieldname": "row",
		// 	"title": "Row select",
		// 	"description": "",
		// 	"visible": true,
		// 	"widget": {
		// 		"name": "theme_select",
		// 		properties: {
		// 			dataset_id: askdataset_id
		// 		}
		// 	}
		// };
		
		// var dataset_select = $.widgets.get_widget(metadataset_select);
		function SetVal(row){
			doc=row.data('doc');
			newobject=doc;
			newobject.add=true;
			newobject.action='select';
			_self.objectlist.push(newobject);
			_self.drawobjects(false);				
			AddDlg.dialog("close");
			AddDlg.remove();
		}

		var row=$('<div class="form'+askdataset_id+'"><div class="fields"></div></div>');
		AddDlg.append(row);
		// dataset_select.assign($('.fields', row), '');	
		var tabcon = $('.fields', row);


		var sAjaxSource = "/dataset/list?f=" + askdataset_id + '&count_rows=true';
		widget.gtable = new gdataset(widget.table_json, askdataset_id, tabcon, sAjaxSource, undefined, false);
		widget.gtable.drawtablehead();
		widget.gtable.row_on_page_count=5;
		widget.gtable._fnReDraw();
		widget.gtable.rowselect.push(SetVal);
		var tr = $('#dt_fltr', tabcon);			
		tr.show("slow");
			

		
		AddDlg.dialog({
			resizable: true,
			height: 'auto',
			width: 'auto',
			modal: false,
			buttons: {				
				"Cancel": function () {				
					$(this).dialog("close");
					AddDlg.remove();
				}
			}
		});	
	}

    this.assign = function(form_element, value){		
		this.form_element = form_element;
		var menu=$('<div class="container row"></div>');		
		form_element.append(menu);
		var object_container=$('<div class="container row"></div>');
		this.object_container=object_container;
		
		form_element.append(object_container);
		
		items = $('<ul class="thumbnails"/>');
		
		form_element.append(items);
		
		pluscardvar=$('<div class="addcard"> \
			<button type="button" class="btn btn-secondary btn-sm add">Add</button>\
			<button type="button" class="btn btn-secondary btn-sm select">Select</button>\
		</div>');
		menu.append(pluscardvar);
		$('.add',pluscardvar).click(function(){
			_self.AddItem();
		});
		$('.select',pluscardvar).click(function(){
			_self.SelectItem();
		});
		
		if(Array.isArray(value))
			_self.objectlist=value;
		else
			_self.objectlist=[];
		_self.drawobjects(false);
    }
    
	
    this.getVal = function (){
		res=JSON.stringify(this.objectlist, function(key, value) {
			if (key == 'trow') {
			  return undefined; // удаляем все строковые свойства
			}
			return value;
		});
		return JSON.parse(res);
		//return this.objectlist;
		
		
		var doclist=[];		
		for (var i = 0; i < this.objectlist.length; i++) {
			//if(this.objectlist[i].add===true){
				//var newobj={docid:doc_id, datasetid:master_id, classname:classname, rowid:this.objectlist[i].id, rowdatasetid:askdataset_id, action:'add'};
				doclist.push({id:this.objectlist[i].id, action:this.objectlist[i].action});
			//}
		}		
		return doclist;
    }
		
	
	this.getUserVal=function (val, view){
		if(this.gr_fn=='none' )
			return '';		
		if(val=='NULL')
			return '';
		var props = this.object.widget.properties;
		if($.isArray(val) && this.table_json && val.length>0){
			if(!view)
				return val.length;
			
			var object_container=$('<ul class="thumbnails object_container"></ul>');
			this.object_container=object_container;
			
			if(Array.isArray(val))
				_self.objectlist=val;
			else
				_self.objectlist=[];
			_self.drawobjects(false);
			return object_container.html();
			
		}
		else
			return '-';
    }    

	// Additional properties form
    this.viewPropForm = function (container){
		var widget=this;
		var props = this.object.widget.properties;
		if(props===undefined)
			props={};
		var sz="<div class='tabselect'></div>fields<input type='text' class='fields'/>mark<input type='text' class='mark'/>";
		container.append(sz);
		
		
		
		var metaTableSelect = {
					"fieldname": "",
					"title": "",
					"description": "",
					"visible": true,
					"widget": {
						"name": "theme_select",
						properties: {}
					}
				};
		var tableSelect = $.widgets.get_widget(metaTableSelect);
		tableSelect.assign($('.tabselect', container), props.dataset_id);
		$('.fields',container).val(props.fields);
		$('.mark',container).val(props.mark);
		
		this.tableSelect=tableSelect;
    }  

    this.getPropForm = function (container){
		return {
				dataset_id: this.tableSelect.getVal(),
				fields: $('.fields',container).val(),
				mark: $('.mark',container).val()
			};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.fieldname;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;		
		tempobj.type = 'widget_proxy';	
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'widget_filter';
		tempobj.sWidth = '200px';
		return tempobj;
	}

	this.hasFilter = function (){
		return false;
	}
	this.olap = function(node, value, meta){	
		this.init_olap(node, value);
		var vis=false;
		var sz=$("<div class='form-item' ><label>Select field</label><br>\
		<select  id='db_field' name='db_field' ><option>Select the field </option></select>min color<input style='width:100px;' class='min_color' value='000000'/>\
		max color<input style='width:100px;' class='max_color' value='000000'/><button id='change_map' class='btn btnApply' type='button'>Apply</button></div>");
		var change_map=$('#change_map',sz);
		change_map.click(function(){
			meta.layer.update_map();
		});
		
		sz.hide();
		this.group.after(sz);
		// $(".min_color",sz).colorpicker();
		// $(".max_color",sz).colorpicker();
		this.group.change(function(){
			if(vis)
				sz.hide();
			else
				sz.show();
			vis=!vis;
		});
		select_input=$('#db_field',sz);
		for (var i = 0; i < meta.columns.length; i++) {
			option = $('<option value="'+meta.columns[i].fieldname+ '">'+ meta.columns[i].title+ ' (' + meta.columns[i].fieldname+ ') </option>');
			select_input.append(option);
		}
	}
	

    return this;
}
extend(w_links, w_base);
$.widgets.RegisterWidget('links', w_links);
function viewmapdetail(elmnt){
	var mapname=$(elmnt);
	ul=$('ul',mapname.parent().parent());
	if (ul.is(":visible") == true){
		ul.hide();
	}
	else{
		ul.show();
	}
};

function viewmapclick(elmnt){
	m_checkbox=$(elmnt);
	if(m_checkbox.css('display')=='none')
		return;
	var maps=$(".mapitem", $('#maplist_div'));
	for(var i=0; i<maps.length; i++){
		var map=$(maps[i]);
		var doc_data_base=map.data('doc');
		var value=doc_data_base['json'];
		if(typeof value == "string"){
			value=JSON.parse(value);
			doc_data_base['json']=value;
		}
		hidemap(value);
	}
	var map=m_checkbox.parents(".mapitem");
	var doc_data_base=map.data('doc');
	var value=doc_data_base['json'];
	if(typeof value == "string"){
		value=JSON.parse(value);
		doc_data_base['json']=value;
	}
	
	inputval=m_checkbox.is(':checked');
	$( "input" , $('#maplist_div')).prop( "checked", false );
	m_checkbox.prop( "checked", inputval);	
	if(inputval)
		viewmap(value, doc_data_base['mapcenter'], doc_data_base['mapscale'], doc_data_base['overlay'] );	
};


function w_map(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	var input;
	// Common name of widget
	var widget_name = 'Map';
	this.user_visible = true;
	this.rname = 'Формирование карты';

	var _self = this;
	



	// HTML element creation
    this.assign = function(form_element, map){
		function itemchange(item){
			layer=item.data("layer");
			layer.name=$("input[type='text']",item).val();
			layer.viewtab=$("input[type='checkbox']", item).prop( "checked" );
		}
		function resetfilter(item){
			layer=item.data("layer");
			layer.filter=undefined;			
			$('#filter',item).html('');
		}
		function copyfilter(item){
			layer=item.data("layer");
			for(var i = 0; i < datasets.length; i++){
				if(datasets[i].dataset_id==layer.id){
					layer.filter=datasets[i].FilterValues;
					$('#filter',item).html(JSON.stringify(layer.filter));
					break;
				}
			}
			//layer.name=$("input[type='text']",item).val();
			//layer.viewtab=$("input[type='checkbox']", item).prop( "checked" );
		}
		this.form_element = form_element;		
		var input = $('<div class="row-fluid"><input id="mapname" class="widget_input"  style="width:280px;" type="text" /></div>');
		form_element.append(input);		
		var layerlist = $('<div class="row-fluid"><div class="span8"><ul id="layerlist"></ul></div></div>');
		form_element.append(layerlist);
		layerlist=$('#layerlist', form_element);
		if (typeof map == 'string') {
			try{
			   var map = JSON.parse(map);
			}
			catch(e){
			   map=undefined;
			}
		}
		if(map!==undefined){			
			$('#mapname',this.form_element).val(map.name);
			for(var i=0; i<map.layerlist.length; i++){
				var ch=''
				if(map.layerlist[i].viewtab)
					ch="checked";
				var l=$('<li><input type="text" value="' + map.layerlist[i].name + '"/> View table <input type="checkbox" '+ch+'/><a id="copy" href="#">Copy filter</a> filter: <div id="filter">'+JSON.stringify(map.layerlist[i].filter)+'</div> <a id="reset" href="#">Reset filter</a><div class="close box-close-btn">x</div></li>');
				layerlist.append(l);
				$('input',l).change(function(){
					item=$(this).parent('li');
					itemchange(item);					
				});				
				l.data("layer",map.layerlist[i]);
				$('.box-close-btn', l).click(function(){
					$(this).parent().remove();
				});
				$('#copy',l).click(function(){					
					copyfilter($(this).parent('li'));
				});
				$('#reset',l).click(function(){					
					resetfilter($(this).parent('li'));
				});
			}
		}
		
		layerlist.sortable({
			revert: true
		});
		
		inputWrapper = $('<div class="row-fluid"><div id="theme_select" class="span6"></div><div class="span1"><button id="btn_add" class="btn btn-small" type="button">Add</button></div></div>');
		form_element.append(inputWrapper);
		
		var theme_select=$.widgets.get_widget_byname('theme_select');
		cont=$('#theme_select', inputWrapper);
		theme_select.assign(cont, '');
		
		$('#btn_add', form_element).click(function(){
			var layer_id = theme_select.getVal();
			getdatasetmeta(layer_id, function(error, table_json){
				if(error!=null)
					return;
							
				var layer={id:layer_id, name:table_json.title, viewtab:false};
				var l=$('<li><input type="text" value="' + layer.name + '"/> View table <input type="checkbox"/><a id="copy" href="#">Copy filter</a>filter:<div id="filter">'+layer.filter+'</div> <a id="reset" href="#">Reset filter</a><div class="close box-close-btn">x</div></li>');
				layerlist.append(l);
				layerlist.sortable( "refresh" );	
				l.data("layer",layer);
				$('input',l).change(function(){					
					itemchange($(this).parent('li'));
				});
				$('.box-close-btn', l).click(function(){
					$(this).parent().remove();
				});				
				$('#copy',l).click(function(){					
					copyfilter($(this).parent('li'));
				});
				$('#reset',l).click(function(){					
					resetfilter($(this).parent('li'));
				});
			});
		});
    }
	
    this.getVal = function (){		
		var map={name:$('#mapname',this.form_element).val(), layerlist:[]};
		$('ul#layerlist > li',this.form_element).each(function(){
			l=$(this).data("layer");
			map.layerlist.push(l);
		});
	    return map;//JSON.stringify(map);;
    }
	
    this.getUserVal=function (map){
		if(this.gr_fn=='none' )
			return '';		
		if(map === undefined) 
			return '';		
		
		if (typeof map == 'string') {
			try{
			   var map = JSON.parse(map);
			}
			catch(e){
			   map=undefined;
			}
		}
		if(map === undefined) 
			return '';		
		var res='<div><div class="form-check" value="1">\
		<input class="mapwidget form-check-input" type="checkbox" onclick="viewmapclick(this)"/>\
		<label onclick="viewmapdetail(this)" class="mapname form-check-label">'+map.name+'</label>\
		</div><ul class="mapdetails">';		
		for(var i=0; i<map.layerlist.length; i++){
			res+='<li>' + map.layerlist[i].name + '</li>';			
		}
		res+="</ul></div>";
		return res;
    }
  

  
	// Additional properties form
    this.viewPropForm = function (){};
	
    this.getPropForm = function (){return {};};
 	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'select';		
		tempobj.sWidget = 'select';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}
    return this;
}
extend(w_map, w_base);
$.widgets.RegisterWidget('Map', w_map);


(function($) {
	//var pasteEventName = ($.browser.msie ? 'paste' : 'input') + ".coordinates";
	var iPhone = (window.orientation != undefined);
	var geos_init=false;

	$.fn.extend({
		caret: function(begin, end) {
			if (this.length == 0) return;
			if (typeof begin == 'number') {
				end = (typeof end == 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						var range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					var range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		numberTest: function(settings) {
			settings = $.extend({
				onlypositive:false,
				before: "10",
				after: "10",
			}, settings);	
			
			var input = $(this[0]);
			var buffer=input.val();
			
			
			function testNumber(vl){
				var numbers='0123456789';
				if(vl=='')
					return true;
				if(!test_number(vl))
					return false;				
				if(vl<0){
					if(settings.onlypositive)
						return false;
					vl=-vl;
				}
				vl=parseFloat(vl)+'';				
				arr  =  vl.split('.');
				if(arr[0].length>parseInt(settings.before))
					return false;				
				if(arr.length>1 && arr[1].length>parseInt(settings.after))
					return false;
				
				return true;
			};
		

			function keydownEvent(e) {
				var trg=$(e.target);
				var k=e.which;				
				//console.log('key down');				
					//backspace, delete, and escape get special treatment
				if(k == 8 || k == 46 || (iPhone && k == 127)){
					var pos = trg.caret(),
						begin = pos.begin,
						end = pos.end;
					
					var s_val= trg.val();
					var future_val='';
					if(pos.end-pos.begin!=0){						
						future_val=s_val.substring(0,pos.begin)+s_val.substring(pos.end,s_val.length);
					}
					else if (k==8){
						future_val=s_val.substring(0,pos.begin-1)+s_val.substring(pos.begin,s_val.length);						
					}
					else if (k==46){
						future_val=s_val.substring(0,pos.begin)+s_val.substring(pos.begin+1,s_val.length);						
					}					
					if (testNumber(future_val)) {
						buffer=future_val;
						input.val(future_val);
						if(k == 8)
							trg.caret(pos.begin-1);
						else
							trg.caret(pos.begin);
					}
					return false;
				}								
				return true;
			};
			
			
			
			function keypressEvent(e) {
				var trg=$(e.target);			
				var k = e.which,
					pos = trg.caret();
				
				if (e.ctrlKey || e.altKey || e.metaKey || k<32) {//Ignore
					return true;
				} else if (k) {
					var c = String.fromCharCode(k);
					if(c==',')
						c='.';					
					var s_val= trg.val();
					var future_val='';
					future_val=s_val.substring(0,pos.begin)+c+s_val.substring(pos.end,s_val.length);					
					if (testNumber(future_val)) {
						buffer=future_val;
						input.val(future_val);
						trg.caret(pos.begin+1);
						trg.trigger('change');
					}					
					return false;
				}
			};

			input.bind("keydown", keydownEvent);			
			input.bind("keypress", keypressEvent);
			buffer=input.val();	
		}
		
	});
	
	
})(jQuery);


function w_number(object) {
	var widget=this;
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'number';
	// Additional properties array
	var addproparr = new Array();
	var form_cont;
	this.user_visible = true;
	this.rname = 'Число';	
	this.fieldtype = 'number';
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		//var label = $('<div style="height: 24px; width:80px;display: inline-block;text-align: right;">'+this.object.title+'</div>');
		var label = $('<label for="inp_'+this.object.fieldname+'">'+this.object.title+'</label>');
		var input = $('<input class="form-control" title="'+this.object.description+'" type="text" />');
		this.input=input;
		input.attr('id', this.object.fieldname);
		/*
		if(this.lvis){
			form_element.append(label);
		}*/
		form_element.append(input);
		
		after=10;
		if(this.object.widget.properties!=undefined && this.object.widget.properties.after!=undefined  && this.object.widget.properties.after!='')
		   after=this.object.widget.properties.after;
		before=10;
		if(this.object.widget.properties!=undefined && this.object.widget.properties.before!=undefined && this.object.widget.properties.before!='' )
		   before=this.object.widget.properties.before;
		
		
		
		if(value!=undefined && value!='')
			input.val(value);
		else if(object.default_value && object.default_value!='empty'){
			this.input.val(removeescapeHtml(object.default_value));
		}
		input.numberTest({default_point: ".", before:before, after:after} );
		this.changevalue('test1212121','');	
		this.input.bind('change', function(select){
			widget.val=widget.input.val();
			widget.sendmessage(widget.val);
		});
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';

		val=this.input.val();
		//val=$('#'+this.object.fieldname).val();
		if(val=='.') val='';
		if(val.length>1 && val.charAt(val.length-1)=='.')
			val=val.slice(0,val.length-1);
	    return val;
    }
	
    // Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(val==null)
			return '';
		if(this.gr_fn=='none' )
			return '';
		after=10;
		if(this.object.widget.properties!=undefined && this.object.widget.properties.after!=undefined  && this.object.widget.properties.after!='')
		   after=this.object.widget.properties.after;		
		val=val+'';
		arr  =  val.split('.');
		if(arr.length>1 && arr[1].length>parseInt(after)){
			if(parseInt(after)==0)
				arr[1]='';
			else
				arr[1]=arr[1].substr(0,parseInt(after));			
		}
		if (arr.length>1 && arr[1]!='' )
			arr[0]=arr[0]+'.'+arr[1];
		
		return arr[0];
		if(this.object.widget.properties!=undefined && this.object.widget.properties.measure!=undefined ){
			return val;//+' '+this.object.widget.properties.suffix_user;
		}else{
			return val;
		}
	   
    }
	
	
	this.olap = function(node, value){	
		this.init_olap(node, value);		
		this.group.append($('<option value="max">max</optiion>'));
		this.group.append($('<option value="min">min</optiion>'));
		this.group.append($('<option value="avg">average</optiion>'));
		this.group.append($('<option value="sum">sum</optiion>'));
	}

	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'number';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'number_filter';
		tempobj.sWidth = '30px';

		return tempobj;
	}
	
	this.hasFilter = function (){
		return true;
	}	

	// Additional properties form
    this.viewPropForm = function (container){
		var widget=this;
		function mesure_options(lvl,parent) {
			var option='';
			return option;
			var res=JSON.parse($.ajax({
				type: "GET",
				url: '/geothemes/getMesure?mode=prop&lvl='+lvl+'&parent='+parent,
				async: false,
			}).responseText);
			if($.isArray(res))
				res.forEach( function(item){ option=option+'<option value="'+item.value+'">'+item.label+'</option>'; } );
			return option;
		}
		var ps=$("<label>Measure:</label><input type='radio' id='measure1' name='measure' value=1 /><label>yes</label><input type='radio' id='measure2' name='measure' value=0 /><label>no</label>");
		//container.append(ps);
		ps=$("<div id='measure_form'><label>Type: </label><select id='measure_type' name='measure_type'>"+mesure_options(0,0)+"</select><br><label>Value: </label><select id='measure_value' name='measure_value'>"+mesure_options(1,1)+"</select><br><label>Unit for base: </label><select id='measure_unitbase' name='measure_unitbase'>"+mesure_options(2,1)+"</select><br/><label>Unit for user: </label><select id='measure_unituser' name='measure_unituser'>"+mesure_options(2,1)+"</select></div>")
		//container.append(ps);
		$('#measure_form', container).css('display', 'none');
		if(this.object.widget.properties!=undefined && this.object.widget.properties.measure!=undefined ){
		   $('#measure'+this.object.widget.properties.measure, container).prop('checked', true);;
		   $('#measure_form', container).css('display', 'block');
		   $('#measure_type', container).val(this.object.widget.properties.measure_type);
		   setTimeout(function() {
			$('#measure_value', container).val(widget.object.widget.properties.measure_val);
		   }, 200);
		   setTimeout(function() {
		   $('#measure_unitbase', container).val(widget.object.widget.properties.measure_user);
		   $('#measure_unituser', container).val(widget.object.widget.properties.measure_base);
		   },500);
		}
		container.on('change','input[name=measure]',function(){
			if ($('input[name=measure]:radio:checked', container).val() == 1) 	
				$('#measure_form', container).css('display', 'block');
			if ($('input[name=measure]:radio:checked', container).val() == 0) 
				$('#measure_form', container).css('display', 'none');
		});
		container.on('change','#measure_type',function(){
			$('#measure_value', container).html(mesure_options(1,$('#measure_type', container).val()));
			$('#measure_unitbase', container).html(mesure_options(2,$('#measure_value', container).val()));
			$('#measure_unituser', container).html(mesure_options(2,$('#measure_type', container).val()));
		});
		container.on('change','#measure_value',function(){
			$('#measure_unitbase', container).html(mesure_options(2,$('#measure_value', container).val()));
			$('#measure_unituser', container).html(mesure_options(2,$('#measure_type', container).val()));
		});
		
		var ps=$("<div class='tab_field'><label class='tab_field_label'>Default value</label><input class='tab_field_input' type='text' id='default_value' /></div>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.default_value!=undefined )
		   $('#default_value', container).val(this.object.widget.properties.default_value);

		var ps=$("<div class='tab_field'><label class='tab_field_label'>Digits before the point </label><input class='tab_field_input' type='text' id='before' /></div>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.before!=undefined )
		   $('#before', container).val(this.object.widget.properties.before);

 	    var ps=$("<div class='tab_field'><label class='tab_field_label'>Digits after the point </label><input class='tab_field_input' type='text' id='after' /></div>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.after!=undefined )
		   $('#after', container).val(this.object.widget.properties.after);
		   
		return;
    }    
    
    this.getPropForm = function (container){
		function mesure_unit(id) {
			if(id==null || id==undefined)
				return {suffix: '', factor: ''};
			var option='';
			var res=JSON.parse($.ajax({
				type: "GET",
				url: '/geothemes/getMesure?id='+id,
				async: false,
			}).responseText);
			if($.isArray(res)){
				res.forEach( function(item){ option=option+'<option value="'+item.value+'">'+item.label+'</option>'; } );
				return {suffix: res[0].suffix, factor: res[0].factor};
			}
			else
				return {suffix: '', factor: ''};
		}
		var default_value=$('#default_value',container).val();
		var before=$('#before',container).val();
		var after=$('#after',container).val();
		if ($('#measure', container).val() == 0) 
			return {before: before, default_value:default_value, after:after, measure: 0}
		else {
			var measure_user = $('#measure_unituser', container).val();
			var measure_base = $('#measure_unitbase', container).val();
			var suffix_user = mesure_unit(measure_user).suffix;
			return {before: before, default_value:default_value, after:after, measure:1, measure_user: measure_user, measure_base: measure_base, suffix_user : suffix_user};
		}
    }
	
    return this;
}

extend(w_number, w_base);
$.widgets.RegisterWidget('number', w_number);var number_operators={
PropertyIsEqualTo:'=',
PropertyIsNotEqualTo:'!=',
PropertyIsLessThan:'>',
PropertyIsLessThanOrEqualTo:'>=',
PropertyIsGreaterThan:'<',
PropertyIsGreaterThanOrEqualTo:'<='
}

var rigth_operators={
PropertyIsEqualTo:'=',
PropertyIsNotEqualTo:'!=',
PropertyIsLessThan:'<',
PropertyIsLessThanOrEqualTo:'<=',
PropertyIsGreaterThan:'>',
PropertyIsGreaterThanOrEqualTo:'>='
}

	/**
	* A filter widget based on data in a table column.
	* 
	* @class AutocompleteFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/	
	$.number_filter = function( $Container, column, dt_table ){
		var widget = this;
		widget.column = column;		
		widget.$Container = $Container;
		widget.oDataTable = dt_table;
		
		widget.asFilters = [];
		widget.sSeparator = ';';
		widget.iMaxSelections = -1;
		
		//
		//widget.$label = $('<label for="f_' + widget.oColumn.sName + '">' + widget.oColumn.sTitle + '</label><br>');
		//widget.$Container.append( widget.$label );
		d=$('<div class="input-group fltr_number"></div>');
		widget.$Container.append( d );
		
		var input=$( '<input class="form-control fltr_'+widget.column.fieldname+'"  type="text" />' );
		d.append( input );
		
		cf1=$('<div class="input-group-append">\
		<span class="input-group-text"><i class="fas fa-filter"></i></span>\
	  </div>');
		d.append( cf1 );
		widget.$label=$('span', cf1)
		//widget.$Container.width('100px');		
		
		// d=$('<div style="display: inline-block;vertical-align: top;"></div>');
		// widget.$label = $( '<a class="btn" style="padding: 0px 3px;" href="#"><i class="icon-edit"></i></a>' );
		// widget.$Container.append( d );
		// d.append( widget.$label );	
		
		var term_container=$('<div></div>');
		widget.$Container.append( term_container );
		
		input.keypress(function(e) {
			if(e.which == 13) {
				if(input.val()=='')
					return false;
				if(!test_number(input.val()))
					return false;				
				var sText=input.val()+'='+widget.column.title;
				var sSelected=input.val()  + '-PropertyIsEqualTo';
				if  (!($.inArray(sSelected, widget.asFilters))) {
					return;
				}
				input.val('');
				var $TermLink = $( '<a class="filter-term" href="#"></a>' )
					.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
					.text( sText )
					.click( function() {
						// Remove from current filters array
						widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
							return sFilter != sSelected;
						} );
						$TermLink.remove();
						if ( widget.$TermContainer && 0 === widget.$TermContainer.find( '.filter-term' ).length ) {
							widget.$TermContainer.hide();
						}
						widget.fnFilter();
						return false;
					} );
				widget.asFilters.push( sSelected );
				term_container.append( $TermLink ); 
				widget.fnFilter();
			}
		});
		
		
		widget.$label.click(function(){
			var p=$( '<div  title="Adding a rule"><form class="form-inline">\
			<input class="form-control mb-2 mr-sm-2" type="text" id="from" />\
			<select class="form-control mb-2 mr-sm-2" id="left_operand" /> \
			'+widget.column.title+'\
			<select class="form-control mb-2 mr-sm-2" id="right_operand" />\
			<input class="form-control mb-2 mr-sm-2" type="text" id="to" />\
			</form></div>');
			var left_value=$('#from', p);
			var rigth_value=$('#to', p);
			var left_operand=$('#left_operand', p);
			for(var key in number_operators) {
				if(key=='PropertyIsGreaterThanOrEqualTo')
					op=$('<option selected value="'+key+'">'+number_operators[key]+'</option>');
				else
					op=$('<option value="'+key+'">'+number_operators[key]+'</option>');
				
				left_operand.append(op);
			}
			
			var right_operand=$('#right_operand', p);
			for(var key in rigth_operators) {
				if(key=='PropertyIsLessThanOrEqualTo')
					op=$('<option selected value="'+key+'">'+rigth_operators[key]+'</option>');
				else
					op=$('<option value="'+key+'">'+rigth_operators[key]+'</option>');
				right_operand.append(op);
			}
			
			p.dialog({
				  resizable: false,
				  height:'auto',
				  width:'auto',
				  modal: true,
				  buttons: {
					"Add": function() {
						var val_d_from = left_value.val(),
							val_d_to = rigth_value.val();
						if (( val_d_from == '' ) && ( val_d_to == '' )) {
							// The blank range
							return;
						}				
						
						var sSelected = '';
						var sText='';
						if(val_d_from!=''){
							sSelected+=val_d_from  + '-' + left_operand.val();
							sText=val_d_from + number_operators[left_operand.val()];
						}
						sText+=widget.column.title;
						if(val_d_to!=''){
							if(sSelected!='')
								sSelected+='-';
							sSelected+=val_d_to+'-'+right_operand.val();
							sText+=rigth_operators[right_operand.val()]+val_d_to;
						}
						var $TermLink, $SelectedOption; 
						
						if  (!($.inArray(sSelected, widget.asFilters))) {
							return;
						}
						//sText = $( '<div>' + sText + '</div>' ).text();
						$TermLink = $( '<a class="filter-term" href="#"></a>' )
							.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
							.text( sText )
							.click( function() {
								// Remove from current filters array
								widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
									return sFilter != sSelected;
								} );
								$TermLink.remove();
								if ( widget.$TermContainer && 0 === widget.$TermContainer.find( '.filter-term' ).length ) {
									widget.$TermContainer.hide();
								}
								widget.fnFilter();
								return false;
							} );
						widget.asFilters.push( sSelected );
						term_container.append( $TermLink ); 
						widget.fnFilter();
						
						
						$( this ).dialog( "close" );
						p.remove();
					},
					Cancel: function() {
						$( this ).dialog( "close" );
						p.remove();
					}
				}
			});		
		
		});
	};
	
	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.number_filter.prototype.fnFilter = function() {
		var widget = this;
		var asEscapedFilters = [];
		var sFilterStart, sFilterEnd;
		if ( widget.asFilters.length > 0 ) {			
			widget.oDataTable.fnFilter( widget.asFilters.join(';') , widget.column.fieldname, true, false );
		} else { 
			// Clear any filters for this column
			widget.oDataTable.fnFilter( '', widget.column.fieldname);
		}
		setTimeout( function() {
			$('.g_layer',$('#layer_list')).each(function(index){
				ml=$(this).data("mlayer");
				ml.redraw();
			});							
		}, 150 );
	};

function w_objectview(object) {
    this.object = object;
    this.node='';	
	var widget_name = 'objectview';
	this.user_visible = true;
	this.rname = 'objectview';
	this.visible=false;
	

    this.assign = function(form_element, value){	
		this.form_element = form_element;

		this.input = $('<input type="hidden"/>');
		//this.input.attr('id', this.object.fieldname);		
		form_element.append(this.input);		
		if(value!=undefined && value!='')
			this.input.val(value);
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
    this.getVal = function (){
		return this.input.val();
    }
    

    
    this.viewPropForm = function (container){
		var widget=this;
		var props = this.object.widget.properties;
		var sz="\
			<div class='classify'/>\
			<label>Select object type </label>\
			<div  id='db_table'>\
			</div>\
			\
		";
		container.append(sz);
		var db_table=$("#db_table",container);
			

		var metaTableSelect = {
			"fieldname": "",
			"title": "",
			"description": "",
			"visible": true,
			"widget": {
				"name": "theme_select",
				properties: {}
			}
		};
		
		this.tableSelect = $.widgets.get_widget(metaTableSelect);
		if(props){
			widget.dataset_id=props.dataset_id;			
		}
		this.tableSelect.assign(db_table, widget.dataset_id);
		
		
		helpobject={};
		helpobject.changevalue=function(fieldname, value){
			widget.dataset_id=value;
		}
		this.tableSelect.listnerlist.push(helpobject);


    }  
    
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		if(val==null)
			return '';
		var unique=randomString(10);
		var widget=this;
		setTimeout(function(){
				$('.'+unique).click(function(){
					var trow=$(this).parents('tr');
					var aData=trow.data('doc');
					if(!widget.object.fieldname || widget.object.fieldname==''){
						widget.object.fieldname='id';
					}
					if(!aData){
						aData={};
						aData[widget.object.fieldname]=$('.'+unique).html();

					}
					if(aData){
						var eRowform;
						if(widget.object.widget.properties.dataset_id){
							
							getdatasetmeta(widget.object.widget.properties.dataset_id, function(error, table_json){
								if(error!=null)
									return;
							
								table_json.metaid = widget.object.widget.properties.dataset_id;
								var doc_frm= new doc_template(table_json);
								$.getJSON("/dataset/list?f="+widget.object.widget.properties.dataset_id+"&iDisplayStart=0&iDisplayLength=1&f_id=" + aData[widget.object.fieldname], function(subdata) {
									if(subdata.aaData.length>0){
										var doc=subdata.aaData[0];
										eRowform=$('<div title="'+doc_frm.doc_description.title+'"><div class="dlg_form"></div></div>');
										
										doc_frm.init_form(function(){
											// setTimeout(function(){
												doc_frm.show_form($('.dlg_form',eRowform), doc, '', true);
												eRowform.dialog({
													  resizable: true,
													  height:'auto',
													  width: 1100,
													  maxWidth: 1400,
													  maxHeight: 600,
													  modal: true,
													  buttons: {
														"Close": function() {								
															doc_frm.close();
															eRowform.remove();
														}
													  },
													  close: function( event, ui ) {
															doc_frm.close();															
															eRowform.remove();
													  }
												});
											//}, 500);
										});

									}
								});
								
							});
							
							
							
						}
						
						
					}
				});
			}, 200);
		return ' <a class="CustomView '+unique+'" href="#" title="'+val+'" disabled="disabled">'+val+'</a>';
    }
	
	this.getPropForm = function (container){
				
		return {
				dataset_id: this.dataset_id,				
			};
    }
    
    this.getDatatableProperties=function (){
		var tempobj = new Object();
		tempobj.bVisible = this.object.visible;	
		tempobj.sTitle = this.object.fieldname;
		tempobj.sWidth = '30px';
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'text';		
		tempobj.sWidget = 'text';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';	
	    return tempobj;
    }
    return this;
}


extend(w_objectview, w_base);
$.widgets.RegisterWidget('objectview', w_objectview);



/**
	* A filter widget based on data in a table column.
	* 
	* @class GeometryFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/
	$.OwnerFilterWidget = function( $Container, column, dt_table, filtervalue ){
		var widget = this;	
		widget.column = column;		
		widget.$Container = $Container;
		widget.oDataTable = dt_table;		
		
		
		
		widget.asFilter = 'All';
		widget.sSeparator = ';';
		widget.iMaxSelections = -1;
		widget.$GeometryRange = $('<select style="width:110px;" />');
		var v='All';
		var option = $('<option />').val(v).append('All');
		widget.$GeometryRange.append(option);

		var v='curent';
		var option = $('<option />').val(v).append('Mine');
		widget.$GeometryRange.append(option);

			
		var SelectionItems;
		widget.$GeometryRange.change(function() {	
			widget.asFilter=$(this).val();
			widget.fnFilter();	
		});
		if(filtervalue){
			widget.$GeometryRange.val('curent');
			widget.oDataTable.fnFilter( filtervalue, widget.column.fieldname);
		}
		
		//widget.$label = $('<label for="from_' + widget.oColumn.sName + '">' + widget.oColumn.sTitle + '</label><br>');
		//widget.$Container.append( widget.$label );
		widget.$Container.append( widget.$GeometryRange );
	};	
	
	
	
	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.OwnerFilterWidget.prototype.fnFilter = function() {		
		var widget = this;	
		if ( widget.asFilter=='' || widget.asFilter=='All') {
		    widget.oDataTable.fnFilter( '', widget.column.fieldname);			
		}
		else {
			var userDataString = $.cookie('userData') || localStorage.user || '{}';
			var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));
			widget.oDataTable.fnFilter( user.id, widget.column.fieldname, true, false );
		}
	};	

function w_params(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'parameters';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		input = $( '<div id="'+this.object.fieldname+'"/>' );
		form_element.append($('<label>'+this.object.title+'</label>'));
		form_element.append(input);
		if(value!=undefined)
			createparamsform(value,input);
		else
			createparamsform(null,input);	
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
		var params=get_params_json(this.form_element);
	    return JSON.stringify(params);
    }
    

    this.viewPropForm = function (container){
		var ps=$("<label>Selet options</label><input type='text' id='select_options'  name='select_options' />");
		container.append(ps);
		$('#select_options',ps).val(this.object.widget.properties.options);
		return;		
		
		var htmlstr = '';
		for (var i = 0; i < addproparr.length; i++) {
			var ttitle = '';
			try {
				eval('ttitle =  this.object.widget.properties.' + addproparr[i]['propname'] + ';');
			} catch (e) {
				c(e.message);
			}
			htmlstr = htmlstr + '<div class="form-field"><label>' + addproparr[i]['proptitle'] + '</label><input type="text" name="additionalprops_' + addproparr[i]['propname'] + '_' + this.cntr + '" value="' + ttitle + '" placeholder="' + addproparr[i]['propdescription'] + '"/><input type="hidden" name="additionalprops_' + this.cntr + '_' + i + '" value="' + addproparr[i]['propname'] + '"></div>';
		}
		$('[name="type' + this.cntr + '"]').val('string');
	    return htmlstr;
    }
    this.getPropForm = function (container){
		var wsize=$('#w_edit_size',container).val();
	    return {size:wsize};
    }
 	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'textarea';		
		tempobj.sWidget = 'select';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';

		return tempobj;
	}	
	
    return this;
}


extend(w_params, w_base);
$.widgets.RegisterWidget('parameters', w_params);
function w_password(object) {	
	var widget=this;
    this.object = object;
	// Common name of widget
	this.widget_name = 'password';
	this.form_element = '';
	this.input;
	
	this.user_visible = true;
	this.rname = 'Пароль';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		var con=form_element;
		var label = $('<label for="inp_'+this.object.fieldname+'">'+this.object.title+'</label>');
		input = $('<input title="'+this.object.description+'" class="form-control" type="password"/>');		
		form_element.append(input);
		this.input = input;
		this.input.val(value);
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';		
		return this.input.val();
    }
    this.getUserVal=function (val){
		return '';		
    }
    // Additional properties form
    this.viewPropForm = function (container){
		
    }
    this.getPropForm = function (container){		
	    return {};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'password';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterw_password';
		tempobj.sWidth = this.object.widget.properties.size*10+'px';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_password, w_base);
$.widgets.RegisterWidget('password', w_password);function w_point(object) {
	// Object for creation of widget
    this.object = object;
	// Common name of widget
	this.fieldtype = 'point';
	this.geometry_type = 'Point';
	var widget_name = 'Point';
	// Additional properties array
	var addproparr = new Array();
	this.mapfile='map_point_'+makeid()+'.map';
	this.parser=null;
	this.user_visible = true;
	this.rname = 'Точка';
	// HTML element creation
	this.pnt_container;
	this.pnt_part;
	
    this.assign = function(form_element, value){
		if(this.object.widget.properties.mode == 'multi')
			this.multi=true;		
		else
			this.multi=false;
		this.g_type='point';		
		this.g_assign(form_element, value);
		return;
    }
    
	// Getting value of input field
    this.getVal = function (){
		return this.getGeometry();
	    //return $('#'+this.object.fieldname,this.form_element).val();
    }
    
	// Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		if(this.gr_fn=='count' || typeof val =='number' )
			return val;
			
		if (val !== null && (typeof val === 'object') && (val.name)) {
			return val.name;
		}
		if (val=== undefined || val==null) return "";
		var tempval = str_replace(')', '', str_replace('POINT(', '',str_replace('MULTIPOINT(', '', val)));		
		var temparr = tempval.split(',');
		var re=/MULTIPOINT\(([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\)/;
		var myArray = re.exec(val);
		if (myArray!=null && myArray.length<3) {
			var tempstr = '';
			var stopper = 2;
			if (temparr.length < stopper)
				stopper = temparr.length;
			for (var i = 0; i < stopper; i++) {
				tempstr += FromDec(temparr[i]) + '; ';
			}
			return tempstr + '... кликните для полного списка'
		}
		if(decimalcoordinates){
			coords=temparr[0].split(' ');
			return coords[1]+' '+coords[0];
		}
		else
			return FromDec(temparr[0]);
    }

	this.olap = function(node, value){	
		this.init_olap(node, value);		
		this.group.append($('<option value="regions">Group by regions</optiion>'));
		this.group.append($('<option value="districts">Group by districts</optiion>'));
	}
    
	// Additional properties form
    this.viewPropForm = function (container){
		var ps=$("<label>Режим: </label><select id='point_mode' name='point_mode'><option value='single'>Одна точка</option><option value='multi'>Мультиточки</option></select><br>");
		container.append(ps);
		var ps=$("<label>Подпись объекта: </label><input type='text' id='label'/><br>");
		container.append(ps);
		//if('properties' in  this.object.widget && 'label' in this.object.widget.properties && this.object.widget.properties.label!=undefined && this.object.widget.properties.label!='')
			
		try {
			$('#point_mode',container).val(this.object.widget.properties.mode);
			$('#label',container).val(this.object.widget.properties.label);
		} catch(e) { }
		var ps=$("<label>Ввод координат: </label><select id='edit_mode' name='edit_mode'><option value='default'>Градусы</option><option value='extended'>Градусы и десятичное представление</option></select>");
		container.append(ps);
		try {
			$('#edit_mode',container).val(this.object.widget.properties.editpointmode);
		} catch(e) { }
		return;		
    } 
    this.getPropForm = function (container){
	    return {mode: $('#point_mode',container).val(), editpointmode: $('#edit_mode',container).val(), label:$('#label',container).val(), gtype: 'point'};
    }
	
	
	this.hasFilter = function (){
		return true;
	}

    return this;
}

extend(w_point, w_geometry);
$.widgets.RegisterWidget('point', w_point);function w_polygon(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'polygon';
	this.fieldtype = 'polygon';
	
	this.user_visible = true;
	this.rname = 'Полигон';		
	// HTML element creation
    this.assign = function(form_element, value){	
		if(this.object.widget.properties.mode == 'multi')
			this.multi=true;		
		else
			this.multi=false;
		this.g_type='polygon';		
		this.g_assign(form_element, value);
		return;		
    }
	
	// Getting value of input field
    this.getVal = function (){
		return this.getGeometry();
    }    
	
    this.getUserVal=function (val){
			if(this.gr_fn=='none' )
				return '';			
		if(this.gr_fn=='count' || typeof val =='number' )
			return val;
		
		if (val == null) return 'Кликните для редактирования'
		else return 'Полигон';
    }
    
	// Additional properties form
	this.viewPropForm = function (container){
		var ps=$("<label>Режим: </label><select id='polygon_mode' name='polygon_mode'><option value='single'>Один полигон</option><option value='multi'>Мультиполигон</option></select><br>");
		container.append(ps);
		var ps=$("<label>Подпись объекта: </label><input type='text' id='label'/><br>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.label!=undefined && this.object.widget.properties.label!='')
			$('#label',container).val(this.object.widget.properties.label);

		try {
			$('#polygon_mode',container).val(this.object.widget.properties.mode);
		} catch(e) { }
		var ps=$("<label>Ввод координат: </label><select id='edit_mode' name='edit_mode'><option value='default'>Градусы</option><option value='extended'>Градусы и десятичное представление</option></select>");
		container.append(ps);
		try {
			$('#edit_mode',container).val(this.object.widget.properties.editpointmode);
		} catch(e) { }
		return;		
    } 
    this.getPropForm = function (container){
	    return {mode: $('#polygon_mode',container).val(), editpointmode: $('#edit_mode',container).val(), label:$('#label',container).val(), gtype: 'polygon'};
    }
    return this;
}

extend(w_polygon, w_geometry);
$.widgets.RegisterWidget('polygon', w_polygon);﻿function w_readonly(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'readonly';
	// Additional properties array
	this.form_element = '';
	
	this.user_visible = true;
	this.rname = 'readonly';
	this.fieldtype = 'string';

	
    this.assign = function(form_element, value){
		this.value=value;
		return value;
    }
    
	// Getting value of input field
    this.getVal = function (){
		return this.value;
    }
    
	// Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		return val;
	    // return $('#'+this.object.fieldname).val();
    }

	// Additional properties form
    this.viewPropForm = function (container){
		//var sz=$("<label>Size of field</label><input type='text' name='w_edit_size' id='w_edit_size' placeholder='20'/>");
		//container.append(sz);		
    }
    this.getPropForm = function (container){
		return {};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'autoedit';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		return tempobj;
	}

	this.hasFilter = function (){
		return false;
	}	

    return this;
}

extend(w_readonly, w_base);
$.widgets.RegisterWidget('readonly', w_readonly);

function w_rectangle(object) {
	var widget=this;
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	this.fieldtype = 'polygon';
	var widget_name = 'rectangle';
	// Additional properties array
	this.x1;
	this.x2;
	this.y1;
	this.y2;
    var BoxLayer;
	// HTML element creation
    this.assign = function(form_element, value){
		this.form_element = form_element;
		form_element.addClass('g_rectangle_container');
		var widget=this;
		var tab = $('\
		<div>\
		<div class="row"><div class="col"></div><div class="col"><input class="form-control" id="Y1"  type="text"/></div><div class="col"></div></div>\
		<div class="row"><div class="col"><input class="form-control" id="X1"  type="text"/></div><div class="col"></div><div class="col"><input class="form-control" id="X2"  type="text"/></div></div>\
		<div class="row"><div class="col"></div><div class="col"><input class="form-control" id="Y2"  type="text"/></div><div class="col"></div></div>\
		</div>');

		save = $('<input value="save"  type="button" class="btn"/>');
		open = $('<input value="open" type="button" class="btn"/>');
		form_element.append( tab );
		form_element.append( save );
		form_element.append( open );

		this.x1=$('#X1', form_element);
		this.y1=$('#Y1', form_element);
		this.x2=$('#X2', form_element);
		this.y2=$('#Y2', form_element);
/*
		this.x1.coordinates("999°99\'99\'\'N", {placeholder: "0"} );
		this.y1.coordinates("999°99\'99\'\'N", {placeholder: "0"} );
		this.x2.coordinates("999°99\'99\'\'N", {placeholder: "0"} );
		this.y2.coordinates("999°99\'99\'\'N", {placeholder: "0"} );
*/
		save.click(function() {
			SaveFileDlg(function(path){
				saveFile(path, 'POLYGON(('+widget.x1.val()+' '+widget.y1.val()+','+widget.x1.val()+' '+widget.y2.val()+', '+widget.x2.val()+' '+widget.y2.val()+', '+widget.x2.val()+' '+widget.y1.val()+', '+widget.x1.val()+' '+widget.y1.val()+'))');
			});
		});

		open.click(function() {
			OpenFileDlg(function(path){
			readFile(path, function(data){
				wkt = new Wkt.Wkt();
				try { // Catch any malformed WKT strings
					wkt.read(data);
				} catch (e1) {
					alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
					return;
				}
				widget.setvalue(wkt);
				return;
				var reg = /([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)/;
			var myArray=reg.exec(data);
			widget.x1.val(FromDec2(myArray[1]));
			widget.y1.val(FromDec2(myArray[2]));
			widget.x2.val(FromDec2(myArray[5]));
			widget.y2.val(FromDec2(myArray[6]));
		    widget.x1.trigger('setvalue.coordinate');
			widget.y1.trigger('setvalue.coordinate');
			widget.x2.trigger('setvalue.coordinate');
			widget.y2.trigger('setvalue.coordinate');
			});
			});
		});

		if (value!=undefined && value!=''){
			wkt = new Wkt.Wkt();
			try { // Catch any malformed WKT strings
				wkt.read(value);
			} catch (e1) {
				alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
				return;
			}
			this.setvalue(wkt);
		}

		form_element.click(function() {
			widget.activateDrawing();
		});

		$(document).click(function(event) { 
			if (!$(event.target).closest(form_element).length) {
				$('.g_activate', widget.form_element[0].parentNode).addClass('g_activate well');
				$('#tabs', widget.form_element[0].parentNode).hide();
				$('.g_activate', widget.form_element[0].parentNode).removeClass('g_unactivate');
				widget.form_element.removeClass('g_activate well');
				widget.form_element.addClass('g_unactivate');
				$('#tabs', this.form_element).show();
				
				stop_map_edit(widget);
			}        
		});
	}


	// Getting value of input field
    this.getVal = function (){
	// WKT
	    return 'MULTIPOLYGON((('+this.x1.val()+' '+this.y1.val()+','+this.x1.val()+' '+this.y2.val()+', '+this.x2.val()+' '+this.y2.val()+', '+this.x2.val()+' '+this.y1.val()+', '+this.x1.val()+' '+this.y1.val()+')))';
    }
    // Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		if (val == null) return 'Кликните для редактирвоания'
		else {
			var reg = /([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)/;
			var myArray=reg.exec(val);
			return 'Область('+FromDec2(myArray[1])+' '+FromDec2(myArray[2])+', '+FromDec2(myArray[5])+' '+FromDec2(myArray[6])+')';
		}
    }
	// Additional properties form

    this.viewPropForm = function (container){
		var ps=$("<label>Режим: </label><select id='point_mode' name='point_mode'><option value='single'>Одна точка</option><option value='multi'>Мультиточки</option></select><br>");
		container.append(ps);
		try {
			$('#point_mode',container).val(this.object.widget.properties.mode);
		} catch(e) { }
		var ps=$("<label>Ввод координат: </label><select id='edit_mode' name='edit_mode'><option value='default'>Градусы</option><option value='extended'>Градусы и десятичное представление</option></select>");
		container.append(ps);
		try {
			$('#edit_mode',container).val(this.object.widget.properties.editpointmode);
		} catch(e) { }
		return;
    }
    this.getPropForm = function (container){
	    return {mode: $('#point_mode',container).val(), editpointmode: $('#edit_mode',container).val()};
    }

	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';
		tempobj.sWidget = 'singlepolygon';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'GeometryFilterWidget';
		tempobj.submit ='Ok';
		tempobj.cancel ='Cancel';
		tempobj.onblur = '';
		return tempobj;
	}
	this.hasFilter = function (){
		return true;
	}

    return this;
}

//extend(w_rectangle, w_base);

function setpointval(input, v, lon){
	//tr_coord = FromDec4coord(v, lon);
	input.val(v);
	//input.trigger('setvalue.coordinates');
}

w_rectangle.prototype.setvalue = function(wkt){
	function wraplong(v){
		v=v % 360;
		if(v > 180){
			v= v - 360;
		}
		else if(v<-180){
			v=v+360;
		}
		return v;
	}
	if(wkt.components.length==0)
		return;
	var min_x=wraplong(wkt.components[0][0].x);
	var max_x=wraplong(wkt.components[0][0].x);
	var min_y=wkt.components[0][0].y;
	var max_y=wkt.components[0][0].y;
	pnts=[];
	for(var j=0; j<wkt.components[0].length-1; j++){
		min_x=Math.min(min_x, wraplong(wkt.components[0][j].x));
		min_y=Math.min(min_y, wkt.components[0][j].y);
		max_x=Math.max(max_x, wraplong(wkt.components[0][j].x));
		max_y=Math.max(max_y, wkt.components[0][j].y);
		el=[wkt.components[0][j].y, wkt.components[0][j].x];
		pnts.push(el);

	}
	setpointval(this.x1, min_x, true);
	setpointval(this.x2, max_x, true);
	setpointval(this.y1, min_y, false);
	setpointval(this.y2, max_y, false);
	if(drawnItems!==undefined){
		if(this.geom_object!=undefined){
			drawnItems.clearLayers();//removeLayer(this.geom_object);
		}
		this.geom_object=new L.Polygon(pnts);
		drawnItems.addLayer(this.geom_object);
	}
}

w_rectangle.prototype.activateDrawing = function(){
	var widget=this;
	//g_unactivate
	$('.g_activate', this.form_element[0].parentNode).addClass('g_unactivate well');
	$('#tabs', this.form_element[0].parentNode).hide();
	$('.g_activate', this.form_element[0].parentNode).removeClass('g_activate');
	this.form_element.removeClass('g_unactivate well');
	this.form_element.addClass('g_activate');
	$('#tabs', this.form_element).show();

	add_map_handle(function (gobject, mode){
		drawnItems.addLayer(gobject);
		var wkt = new Wkt.Wkt();
        wkt.fromObject(gobject);
		widget.setvalue(wkt);
	}, 'rectangle', widget);
}

//extend(w_rectangle, w_base);
$.widgets.RegisterWidget('rectangle', w_rectangle);
function w_ref(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'ref';
	// Additional properties array
	var addproparr = new Array();
	this.user_visible = true;
	this.rname = 'Кнопка';
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;

		var unique = this.object.fieldname;
		if (this.object.htmlname === undefined) {
			this.object.htmlname = this.object.fieldname;
		}
		var tempstr = '';
		form_element.html(tempstr);
		var el = $('#' + unique, form_element);
		if(value!=undefined && value!='')
			el.val(value);		
		
		el.attr( 'widget', widget_name)
		$.widgets.formlist.push([widget_name, form_element]); 
    }

	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname).val();
    }

    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		var props = this.object.widget.properties;		
		$('div').first().append('<div id="vrBerg234gsdv" style="display:none;"></div>');
			if (typeof props.type !== undefined) {
				switch (props.type) {
					case 'button':
						var obj = $('<button />');
						obj.attr('class', 'btn btn-mini');
						obj.attr('type', 'button');
						if ("href" in props) obj.attr('onclick', 'window.open("' + this.applyTaxonomy(props.href) + '", "_blank")');
						if ("exec" in props)
						{
							if (props.exec !== '')
							{
								obj.attr('onclick', this.applyTaxonomy(props.exec));
							}
						}
						obj.html(props.inner);
						if (props.inner == "Modify") obj.html('<i class="icon-wrench"></i>');
						if (props.inner == "View") obj.html('<i class="icon-pencil"></i>');						
						break;						
					case 'link':
						var obj = $('<a />');
						obj.attr('href', this.applyTaxonomy(props.href));
						obj.attr('target', props.target);
						obj.attr('style', 'cursor: pointer; border-bottom: 1px dashed black;');
						obj.html(props.inner);
						break;
				}
				$('#vrBerg234gsdv').append(obj);
			} else {
				//c('Unable to detect REF type');
			}
		var tempo = $('#vrBerg234gsdv').html();
		$('#vrBerg234gsdv').remove();
		return tempo;
    }
    
	/**
	*	applyTaxonomy - substituting predefined constants
	*/
	var taxonomy = new Array();
	/* Filling the contants */
	taxonomy['CONST_TABLE_ID'] = this.cntr;
	taxonomy['ROWID'] = this.cntr;
	this.applyTaxonomy = function(link) {
		taxonomy['CONST_TABLE_ID'] = this.cntr;
		taxonomy['ROWID'] = this.cntr;
		for(var index in taxonomy) {
			link = link.replace(index, taxonomy[index]);
		}
		return link;
	}
	
    this.viewPropForm = function (container){
		var ps = "<label>Вид</label><br><select id='type' name='type'><option value='button'>Кнопка</option><option value='link'>Ссылка</option></select><br><label>Внутренний текст <span style='cursor: pointer; border-bottom: 1px dashed black;' id='infoabout_inner'>(?)</span></label><br><input type='text' id='inner'  name='inner' /><br><label>Ссылка</label><span style='cursor: pointer; border-bottom: 1px dashed black;' id='infoabout_href'>(?)</span><br><input type='text' id='href'  name='href' /><br><label>Открывать ссылку в новом окне?</label><select id='target'><option value='_blank'>Открывать</option>option value='_self'>Не открывать</option></select><br><label>Выполнить функцию</label><span style='cursor: pointer; border-bottom: 1px dashed black;' id='infoabout_exec'>(?)</span><br><input type='text' id='exec' name='exec' />";
		container.append(ps);
		
		$('#infoabout_inner',container).click(function() {
			alert('Текст, который будет отображаться внутри кнопки или ссылки');
		});
		$('#infoabout_href',container).click(function() {
			alert('Ссылка, которая будет открываться при клике');
		});
		$('#infoabout_exec',container).click(function() {
			var taxonomy_disp = '';
			for (var key in taxonomy) {
				taxonomy_disp += key + ';';
			}
			alert('Введите функцию, которая бы вызывалась по нажатию на виджет. Словарь: ' + taxonomy_disp);
		});
		try {
			$('#exec',container).val(this.object.widget.properties.exec);
			$('#type',container).val(this.object.widget.properties.type);
			$('#inner',container).val(this.object.widget.properties.inner);
			$('#href',container).val(this.object.widget.properties.href);
			$('#target',container).val(this.object.widget.properties.target);
		} catch(e) { }
		
		return;
    }
	
    this.getPropForm = function (container){
	    return {
			exec: $('#exec',container).val(),
			type: $('#type',container).val(),
			inner: $('#inner',container).val(),
			href: $('#href',container).val(),
			target: $('#target',container).val()
		};
    }
	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'autoedit';
		tempobj.oWidget = this;		
		tempobj.sWidget = 'ref';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}	
    return this;
}

extend(w_ref, w_base);
$.widgets.RegisterWidget('ref', w_ref);
function w_select(object) {
	// Object for creation of widget   
	this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	this.user_visible = true;
	this.rname = 'Список';
	var widget_name = 'select';
	this.fieldtype = 'string';

	this.assign = function (form_element, value) {
		this.form_element = form_element;
		this.input = $('<select title="' + this.object.description + '" class="form-control" />');
		var input = this.input;
		form_element.append(this.input);
		vals = this.object.widget.properties.options.split(';');
		//var vals={};//{'true':true, 'false': false, 'unknown':'NULL'};
		for (var i = 0; i < vals.length; i++) {
			vs = vals[i].split(':');
			if (vs[0] == value || vs[0] == '' + value)
				option = $('<option selected value="' + vs[0] + '">' + vs[1] + '</option>');
			else
				option = $('<option value="' + vs[0] + '">' + vs[1] + '</option>');
			input.append(option);
		}
		//input.append($('<option value="NULL">--</option>'));
	}

	this.getVal = function () {
		return this.input.val();
	}
	this.getUserVal = function (value) {
		if(this.gr_fn=='none' )
			return '';		
		vals = this.object.widget.properties.options.split(';');
		//var vals={};//{'true':true, 'false': false, 'unknown':'NULL'};
		for (var i = 0; i < vals.length; i++) {
			vs = vals[i].split(':');
			if (vs[0] == value || vs[0] == '' + value)
				return vs[1];
		}
		return '';
	}

	this.viewPropForm = function (container) {
		var ps = $("<label>Select options, example: 1:ok; 2:true; 3:unknown</label><input type='text' id='select_options'  name='select_options' />");
		container.append(ps);
		try {
			$('#select_options', container).val(this.object.widget.properties.options);
		} catch (e) {

		}
		return;
	}
	this.getPropForm = function (container) {
		var options = $('#select_options', container).val();
		return { options: options };
	}

	this.hasFilter = function (){
		return true;
	}	

	this.getDatatableProperties = function () {
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'select';
		tempobj.sWidget = 'select';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;		
		tempobj.sFilterName = 'widget_filter';

		return tempobj;
	}

	//extend(this, new w_edit(object)); 		
	return this;
}


extend(w_select, w_base);
$.widgets.RegisterWidget('select', w_select);
function w_select_table(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'select_table';


	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		input = $( '<select id="'+this.object.fieldname+'"/>' );
		form_element.append(input);
		$.getJSON("/geothemes/gettables",{ajax: 'true'}, function(j){
          var options = '';
          for (var i = 0; i < j.length; i++) {
			if(i==0)
				user_schema=j[i].schema;
            options += '<option value="'+j[i].schema+'.'+ j[i].name+ '">' + j[i].schema+'.'+j[i].name+ '</option>';
          }
		  input.html(options);
        });		
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname,this.form_element).val();
    }
    
	// Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<label>Size of field</label><input type='text' name='w_select_table_size' id='w_select_table_size' placeholder='20'/>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined)
			$('#w_select_table_size',container).val(this.object.widget.properties.size);
		return;
    }
    this.getPropForm = function (container){
		var wsize=$('#w_select_table_size',container).val();
	    return {size:wsize};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'edit';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_select_table, w_base);
$.widgets.RegisterWidget('select_table', w_select_table);﻿function w_symbol(object) {
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'symbol';
	// Additional properties array
	this.user_visible = true;
	this.rname = 'Map symbol';	

	var input;
	
	this.drawsymbol=function(canvas, txt){		
		ctx=canvas[0].getContext('2d');
		w=canvas[0].width;
		h=canvas[0].height;
		ctx.fillStyle = "white";
		ctx.fillRect(0, 0, w, h);
		o=[];
		lines=txt.split('\n');
		for(var i=0; i<lines.length; i++){
			pnts=lines[i].split(' ');
			if(pnts.length==2){
				x=parseFloat(pnts[0]);
				y=parseFloat(pnts[1]);
				if(!isNaN(x) && !isNaN(y))
					o.push([x,y])
			}
		}
		if(o.length>1)		
			draw_object(ctx, o, w/2, h/2, 40, 0);
	}
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		//var label = $('<div style="height: 30px;width:200px;display: inline-block;text-align: right;">'+this.object.title+'</div>');
		var label = $('<label for="'+this.object.fieldname+'">'+this.object.title+'</label>');
		input =  $('<textarea title="'+this.object.description+'" style="padding: 0px; width: 450px; height:80px;  margin-bottom: 1px;" id ="' + this.object.fieldname + '"></textarea>');
		form_element.append(input);
		//var el = $('#' + this.object.fieldname, form_element);
		if(value!=undefined && value!='')
			input.val(removeescapeHtml(value));
		
		input.attr( 'name', this.object.fieldname);
		input.attr( 'widget', widget_name);		
		
		
		canvas=$('<canvas id="rule_sign_head" width="80" height="45">Обновите браузер</canvas>');
		form_element.append(canvas);
		var widget=this;
		input.keyup(function(){
			widget.drawsymbol(canvas, input.val());
		});
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';		
	    return input.val();
    }
	
	this.getUserVal=function (val){
		//YYYY-MM-DD HH:MM:SS
		if(this.gr_fn=='none' )
			return '';
		if (val==undefined || val=='')
			return '';
		
	    return val;
    }
    
	// Getting JSON of field for further saving

	// Additional properties form
    this.viewPropForm = function (container){
			
    }    
    
    this.getPropForm = function (container){		
		
	    return {};
    }
   
	
	this.hasFilter = function (){
		return false;
	}

    return this;
}

extend(w_symbol, w_base);
$.widgets.RegisterWidget('symbol', w_symbol);function w_tableaccess(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'tableaccess';
	this.fieldtype = 'tableaccess';
	
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		var unique = this.object.fieldname;
		if (this.object.htmlname === undefined) {
			this.object.htmlname = this.object.fieldname;
		}
		var tempstr = '';
		form_element.html(tempstr);
		var el = $('#' + unique, form_element);
		if(value!=undefined && value!='')
			el.val(value);		
		
		el.attr( 'widget', widget_name)
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname).val();
    }
    
	/**
	*	getUserVal
	*/
    this.getUserVal = function (val) {
		if(this.gr_fn=='none' )
			return '';		
		//return val;
		if (val === undefined) {
			return '';
		}
		$('div').first().append('<div id="vrBerg234gsdv" style="display:none;"></div>');
		var temp_div=$('#vrBerg234gsdv');
		
		var obj = $('<a href="#">'+val+'</a>');
		temp_div.append(obj);
		obj.attr('onclick', 'createDataTable( '+this.cntr+', null); $("ul.nav li.active").removeClass("active"); $("#table_list").hide();');
	
		var tempo = temp_div.html();
		temp_div.remove();
		return tempo;
    }
  
	
	
    this.viewPropForm = function (container){

		return;
    }
	this.hasFilter = function (){
		return true;
	}
    this.getPropForm = function (container){
		
		return;
    }
	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'autoedit';
		tempobj.oWidget = this;
		tempobj.sWidget = 'tableaccess';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		return tempobj;
	}
    return this;
}

extend(w_tableaccess, w_base);
$.widgets.RegisterWidget('tableaccess', w_tableaccess);
function w_tableowner(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'tableowner';
	// Additional properties array
	var addproparr = new Array();
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		var unique = this.object.fieldname;
		if (this.object.htmlname === undefined) {
			this.object.htmlname = this.object.fieldname;
		}
		var tempstr = '';
		form_element.html(tempstr);
		var el = $('#' + unique, form_element);
		if(value!=undefined && value!='')
			el.val(value);		
		
		el.attr( 'widget', widget_name)
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname).val();
    }
    
	/**
	*	getUserVal
	*/
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';				
		if (val===undefined) {
			return '';
		}
		
		
		
		res=JSON.parse($.ajax({
					type: "GET",
					url  : '/user_list/list?s_fields=fullname,id&f_id='+val,
					async: false,
				}).responseText);
			
		if (res.aaData===undefined || res.aaData.length!=1) return 'id:'+val;
		return res.aaData[0].fullname;
			
    }
	
    this.viewPropForm = function (container){
		return;
    }
	
    this.getPropForm = function (container){		
		return {};
    }
	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'autoedit';
		tempobj.oWidget = this;
		tempobj.sWidget = 'tableowner';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'OwnerFilterWidget';
		return tempobj;
	}
	
	this.hasFilter = function (){
		return true;
	}
    return this;
}

extend(w_tableowner, w_base);

$.widgets.RegisterWidget('tableowner', w_tableowner);
function w_table_access(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'table_access';
	// Additional properties array
	var addproparr = new Array();
	addproparr[0] = new Array();
	addproparr[0]['propname'] = 'size';
	addproparr[0]['proptitle'] = 'Размер';
	addproparr[0]['propdescription'] = '16';

	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		var label = $('<label for="'+this.object.fieldname+'">'+this.object.title+':</label>');
		var input = $('<input type="text"/>');
		input.attr('id', this.object.fieldname);		
		form_element.append(label);
		form_element.append(input);
		
		if(value!=undefined && value!='')
			input.val(value);
		w_settings= {autofill : true,
				cacheLength : 10,
				max : 5,
				autofocus : true,
				highlight : true,
				mustMatch : true,
				selectFirst : true,
				source  : '/dataset/list?f='+this.metaid+'&field='+this.object.fieldname,
				appendTo: form_element
		};
		input.autocomplete(w_settings, 
			{
				dataType:'json',
				parse : function(data) {                                                                                                                    
				  return $.map(data, function(item)
				  {
					  return {
							  data : item,
							  value : item.Key,
							  result: item.value                                                                                     
							 }
				  })
				 },
				formatItem: function(row, i, n) {                                                        
					  return row.value;
				  },
			});	
				
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname,this.form_element).val();
    }
    
	// Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		return val;
	    // return $('#'+this.object.fieldname).val();
    }

	// Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<label>Size of field</label><input type='text' name='w_edit_size' id='w_edit_size' placeholder='20'/>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined)
			$('#w_edit_size',container).val(this.object.widget.properties.size);
		return;
		var htmlstr = '';
		for (var i = 0; i < addproparr.length; i++) {
			var ttitle = '';
			try {
				eval('ttitle =  this.object.widget.properties.' + addproparr[i]['propname'] + ';');
			} catch (e) {
				c(e.name);
			}
			htmlstr = htmlstr + '<div class="form-field"><label>' +  addproparr[i]['proptitle'] + '</label><input type="text" name="additionalprops_' + addproparr[i]['propname'] + '_' + this.cntr + '" value="' + ttitle + '" placeholder="' + addproparr[i]['propdescription'] + '"/><input type="hidden" name="additionalprops_' + this.cntr + '_' + i + '" value="' + addproparr[i]['propname'] + '"></div>';
		}
		$('[name="type' + this.cntr + '"]').val('string');
	    return htmlstr;
    }
    this.getPropForm = function (container){
		var wsize=$('#w_edit_size',container).val();
	    return {size:wsize};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'w_table_access';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		return tempobj;
	}

	this.hasFilter = function (){
		return false;
	}	

    return this;
}

extend(w_table_access, w_base);

$.widgets.RegisterWidget('table_access', w_table_access);
﻿function w_textarea(object) {
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'textarea';
	// Additional properties array
	this.user_visible = true;
	this.rname = 'Текст';	

	var input;
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		//var label = $('<div style="height: 30px;width:200px;display: inline-block;text-align: right;">'+this.object.title+'</div>');
		var label = $('<label for="'+this.object.fieldname+'">'+this.object.title+'</label>');
		input =  $('<textarea title="'+this.object.description+'" id ="' + this.object.fieldname + '"></textarea>');
		form_element.append(input);
		var counter = $('<div></div>');
		form_element.append(counter);
		
		input.on('change keyup paste', function() {
			str=input.val();			
			counter.html('<label class="lang_r">Количество символов: '+str.length+'. Количество слов: '+ str.split(/ +(?:\S)/).length+'.</label> <label class="lang_e">The sign count: '+str.length+'. The word count: '+ str.split(/ +(?:\S)/).length+'.</label>');
		});
		//var el = $('#' + this.object.fieldname, form_element);
		if(value!=undefined && value!='')
			input.val(removeescapeHtml(value));
		
		input.attr( 'name', this.object.fieldname);
		input.attr( 'widget', widget_name);
		this.changevalue('test1212121','');	
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';
		str=input.val();
		//if(str.length<500 || str.length>3000)
		//	throw 'Abstract is small or very big. The size must be in interval from 500 until 3000 symbols.';
	    return input.val();
    }
	
	this.getUserVal=function (val){
		//YYYY-MM-DD HH:MM:SS
		if(this.gr_fn=='none' )
			return '';		
		if (val==undefined || val=='')
			return '';
		
	    return val;
    }
    
	// Getting JSON of field for further saving

	// Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<label>Поиск по полю (только тип tsvector)</label><input type='text' id='tsvector'/>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined){		
			$('#tsvector',container).val(this.object.widget.properties.tsvector);
		}		
    }    
    
    this.getPropForm = function (container){		
		var tsvector=$('#tsvector',container).val();
	    return {tsvector:tsvector};
    }
   
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = 'string';
		tempobj.type = 'widget_proxy';		
/*		tempobj.submit ='Ok';
		tempobj.cancel ='Cancel';		*/
		tempobj.sWidget = '';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';

		return tempobj;
	}
	this.hasFilter = function (){
		return true;
	}

    return this;
}

extend(w_textarea, w_base);
$.widgets.RegisterWidget('textarea', w_textarea);function w_thememoderation(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'thememoderation';
	this.user_visible = true;
	this.rname = 'Moderation';
	this.fieldtype = 'thememoderation';

	// Additional properties array
	var addproparr = new Array();

	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		var unique = this.object.fieldname;
		if (this.object.htmlname === undefined) {
			this.object.htmlname = this.object.fieldname;
		}
		var tempstr = '';
		form_element.html(tempstr);
		var el = $('#' + unique, form_element);
		if(value!=undefined && value!='')
			el.val(value);		
		
		el.attr('widget', widget_name)		
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname).val();
    }
    
	/**
	*	getUserVal
	*/
    this.getUserVal = function (val) {	
		$('div').first().append('<div id="scsfkjefqscwkc" style="display:none;"></div>');			
		var obj = $('<span />');

		val = parseInt(val);
		//0 - accepted, 1 - waiting moderation, 2 - rejected 
		if (val >= 0) {
			newobj = $('<div />');
			newobj.addClass('btn-group');
			newobj.attr('style', 'padding-right: 20px;');

			if (val < 0) 				
				val=-val;			
				
			if (val === 1) {
				obj.html('<span class="label label-success">Accepted record</span>');
				$('#moderation_accept_button', newobj).attr('disabled', 'disabled');
			} else if (val === 2) {
				obj.html('<span class="label label-info">Waiting for moderation</span>');
			} else if (val === 3) {
				obj.html('<span class="label label-important">Rejected record</span>');
				$('#moderation_reject_button', newobj).attr('disabled', 'disabled');
			}

			$('#scsfkjefqscwkc').append(newobj);
		} else {//else values - waiting moderation but there is not rigths
			obj.html('<span class="label label-info">Waiting for moderation</span>');
			//c('Error parsing the widget value ' + val);
		} //end if
		
		$('#scsfkjefqscwkc').append(obj);
		var tempo = $('#scsfkjefqscwkc').html();
		$('#scsfkjefqscwkc').remove();
		return tempo;
    }
	
	/*
	*	applyTaxonomy - substituting predefined constants
	*/
	var taxonomy = new Array();
	/* Filling the contants */
	taxonomy['CONST_TABLE_ID'] = this.cntr;
	taxonomy['ROWID'] = this.cntr;
	this.applyTaxonomy = function(link) {
		taxonomy['CONST_TABLE_ID'] = this.cntr;
		taxonomy['ROWID'] = this.cntr;
		for(var index in taxonomy) {
			link = link.replace(index, taxonomy[index]);
		}
		return link;
	}
	
	
    this.viewPropForm = function (container){
		return;
    }
	
    this.getPropForm = function (container){
		return;
    }
	
	this.hasFilter = function (){
		return true;
	}	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'thememoderation';
		tempobj.oWidget = this;
		tempobj.sWidget = 'thememoderation';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'ThemeModerationFilterWidget';
		return tempobj;
	}
    return this;
}

extend(w_thememoderation, w_base);
$.widgets.RegisterWidget('thememoderation', w_thememoderation);
(function($) {
	/**
	* A filter widget based on data in a table column.
	* 
	* @class DateRangeFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/
	$.ThemeModerationFilterWidget = function($Container, oDataTableSettings, i, widgets) {
		var widget = this;
		widget.iColumn = i;
		widget.oColumn = oDataTableSettings.aoColumns[i];
		widget.$Container = $Container;
		widget.oDataTable = oDataTableSettings.oInstance;
		widget.asFilters = [];
		widget.sSeparator = '';
		widget.iMaxSelections = -1;
		
		widget.$Container.append('<label>Show<label><br><select id="view_moderated_records_mode"><option value="0">all records</option><option value="1">Accepted records</option><option value="2">waiting for modearation</option><option value="3">rejected records</option></select>');

		$('#view_moderated_records_mode', widget.$Container).change(function() {
			var selectedvalue = $('#view_moderated_records_mode').val();
			var searchvalue = '';
			
			$.cookie('moderation_records_view_' + THEME_METAID, selectedvalue);			
			widget.asFilters = new Array();
			if(selectedvalue!='0')
				widget.asFilters.push(searchvalue);
			widget.fnFilter();
		});
		
		if ($.cookie('moderation_records_view_' + THEME_METAID) !== null) {
			setTimeout(function() {
				$('#view_moderated_records_mode', widget.$Container).val($.cookie('moderation_records_view_' + THEME_METAID));
				$('#view_moderated_records_mode', widget.$Container).trigger('change');
			}, 150);
		}
	};


	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.ThemeModerationFilterWidget.prototype.fnFilter = function() {
		var widget = this;
		widget.oDataTable.fnFilter(widget.asFilters, widget.iColumn, true, false);
	};		
	
}(jQuery));function w_theme_attr_select(object) {
	// Object for creation of widget   
	this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'w_theme_attr_select';
	// Self instance
	var _self = this;
	// Current theme
	var _currentTheme = false;


	var _form_element;

	
	var input;

	

	this.changevalue = function (fieldname, value) {
		var widgetContainer = _self.form_element.find(".js-widget-container").first();
		widgetContainer.empty();
		input = $('<select id="' + this.object.fieldname + '"/>');
		widgetContainer.append(input);

		if (value) {
			getdatasetmeta(value  , function(error, data){
				if(error!=null)
					return;
				_currentTheme = data;
				var options = '<option value="">Select theme attribute</option>';
				for (var i = 0; i < data.columns.length; i++) {
					options += '<option value="' + data.columns[i].fieldname + '">' + data.columns[i].title + ' (' + data.columns[i].fieldname + ') </option>';
				}

				input.html(options);
			});
			

			input.change(function () {
				var themeColumn = false;
				var currentVal = _self.getVal();
				for (var i = 0; i < _currentTheme.columns.length; i++) {
					if (_currentTheme.columns[i].fieldname === currentVal) {
						themeColumn = _currentTheme.columns[i];
					}
				}

				if (themeColumn) {
					_self.sendmessage({ value: currentVal, themeColumn: themeColumn });
				} else {
					throw "Unable to detect theme column";
				}
			});
		}
	};


	this.assign = function (form_element, value) {
		this.form_element = form_element;

		var widgetContainer = $('<div class="js-widget-container"/>');
		this.form_element.append(widgetContainer);
		if (this.relationlist.length === 1) {
			var span = $('<span class="muted">Waiting for theme_select change</span>');
			widgetContainer.append(span);
		} else {
			var span = $('<span class="muted">Unable to find related theme_select</span>');
			widgetContainer.append(span);
		}
	}

	// Getting value of input field
	this.getVal = function () {
		tablename = $('#' + this.object.fieldname, this.form_element).val();
		result = tablename;
		return result;
	}

	this.viewPropForm = function (container) {
		var node = $("<label>Corresponding fieldname to watch</label><input type='text' class='js-fieldname-to-watch' />");
		container.append(node);

		$('.js-fieldname-to-watch', container).val("");
		if (this.object.widget.properties.filter && this.object.widget.properties.filter.themeSelectToWatch) {
			$('.js-fieldname-to-watch', container).val(this.object.widget.properties.filter.themeSelectToWatch);
		}
	}

	this.getPropForm = function (container) {
		var val = $('.js-fieldname-to-watch', container).val();
		var obj = { filter: {} };
		obj.filter.themeSelectToWatch = val;
		return obj;
	}

	this.getDatatableProperties = function () {
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'select';
		tempobj.sWidget = 'select';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}
	//extend(this, new w_edit(object)); 		
	return this;
}


extend(w_theme_attr_select, w_base);
$.widgets.RegisterWidget('theme_attr_select', w_theme_attr_select);function w_theme_attr_value_select(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'w_theme_attr_value_select';
	// Additional properties array
	var _self = this;
	
	var tables=null;
	this.correspondingWidget;
	
	
	this.changevalue = function(fieldname, value) {
		var widgetContainer = _self.form_element.find(".js-widget-container").first();
		widgetContainer.empty();
		this.correspondingWidget = $.widgets.get_widget(value.themeColumn);			
		this.correspondingWidget.assign(widgetContainer);
	};
	
    this.assign = function(form_element, value) {
		this.form_element = form_element;
		
		var widgetContainer = $('<div class="js-widget-container"/>');
		this.form_element.append(widgetContainer);
		if (this.relationlist.length === 1) {
			var span = $('<span class="muted">Waiting for corresponding field change</span>');
			widgetContainer.append(span);
		} else {
			var span = $('<span class="muted">Unable to find related theme_attribute_select</span>');
			widgetContainer.append(span);
		}
    }
    
	// Getting value of input field
    this.getVal = function () {
		if (this.correspondingWidget) {
			return this.correspondingWidget.getVal();
		} else {
			return false;
		}
    }

    this.viewPropForm = function (container){
		var ps = $("<label>Corresponding theme attribute fieldname</label><input type='text' class='js-attribute-fieldname-to-watch' />");
		container.append(ps);
		$('.js-attribute-fieldname-to-watch', container).val("");
		if (this.object.widget.properties && this.object.widget.properties.themeAttributeSelectToWatch) {
			$('.js-attribute-fieldname-to-watch', container).val(this.object.widget.properties.themeAttributeSelectToWatch);
		}
    }
	
    this.getPropForm = function (container){
		var val = $('.js-attribute-fieldname-to-watch', container).val();
		var obj = {filter: {}};
		obj.filter.themeAttributeSelectToWatch = val;
		return obj;
    }
 	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'select';		
		tempobj.sWidget = 'select';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}	
	//extend(this, new w_edit(object)); 		
    return this;
}


extend(w_theme_attr_value_select, w_base);
$.widgets.RegisterWidget('theme_attr_value_select', w_theme_attr_value_select);
function w_theme_select(object) {
	// Object for creation of widget   
    this.object = object;
	var widget=this;
	// Counter of element
	this.cntr = '';
	var input;
	// Common name of widget
	var widget_name = 'theme_select';
	this.user_visible = true;
	this.rname = 'Выбор таблицы';

	
	var useSelectionControl;
	var correspondingDataset;
	
	this.dataset_id=-1;
	this.displayfields=[];
	this.tabledraw=null;
	this.value='';
	
		
		
	if(this.object && this.object.widget && this.object.widget.properties && this.object.widget.properties.dataset_id){
		this.dataset_id=this.object.widget.properties.dataset_id;
		if(this.object.widget.properties.fields){
			this.displayfields=this.object.widget.properties.fields.split(',');
			this.displayfields.push('id');		
		}
	}
	else{
		this.dataset_id=100;
	}
	getdatasetmeta(this.dataset_id, function(error, table_json){
		if(error!=null)
			return;
		table_json.metaid = widget.dataset_id;			
		for(var i=table_json.columns.length - 1; i>=0; i--){				
			//if( table_json.columns[i].fieldname==props.reftablename)
			//	table_json.columns[i].visible=false;				
			if((widget.displayfields.length>0 && widget.displayfields.indexOf(table_json.columns[i].fieldname)==-1) )
				table_json.columns.splice(i,1);
		}
		widget.table_json=table_json;
		widget.doc_frm= new doc_template(table_json);
		widget.doc_frm.init_form(function(){
			if(widget.tabledraw)
				widget.tabledraw();
	
		});
	});
	
	/**
	 * If global dataset storage is set, corresponding dataset is extracted.
	 *
	 * @param {Number} id Dataset id
	 *
	 * @return {Object}
	 */
	function getCorrespondingDataset(id) {
		var dataset = false;
		var id = parseInt(id);

		if (window.datasets !== undefined) {
			for (var i = 0; i < datasets.length; i++) {
				if (parseInt(datasets[i].dataset_id) === id) {
					dataset = datasets[i];
					break;
				}
			}
		}

		return dataset;
	}


	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		this.value=[];
		if(typeof value)
			this.value=value;
		var props = this.object.widget.properties;
		var tab=$('\
		<div class="row no-gutters">\
			<div class="col-10 selectedval form-control px-lg-3">\
				Empty\
			</div>\
			<div class="col-2">\
				<button type="button" class="btn searchbtn">Select</button>\
			</div>\
		</div>\
		\
		');
		var selectDlg=$('<div class="subtab"></div>');
		//<a class="btn" href="#" id="btnEditTab" title="Edit the table"><i class="icon-pencil"></i></a>
		var subtab=selectDlg;//$('.subtab',selectDlg);
		
		this.table_container=subtab;
		//subtab.html(this.getUserVal(value));
		form_element.append(tab);

		
		$('.searchbtn',tab).click(function(){
			selectDlg.dialog({
				resizable: true,
				height: '800',
				width: '800px',
				modal: true,
				buttons: {
					"Close": function () {				
						$(this).dialog("close");						
					}
				}
			});
		})
		if(value && value!=''){
			var sAjaxSource="/dataset/list?f="+this.dataset_id+'&count_rows=false';
			$.ajax({
				url: sAjaxSource + '&iDisplayStart=0&iDisplayLength=1&f_id='+value,
				dataType: "json",
				success: function (data) {
					if (data.aaData) {
						setcontent(data.aaData[0]);
					}
				}
			});
		}

		function setcontent(doc){
			$('.selectedval', tab).empty();
			widget.curentdoc=doc;
			widget.doc=doc;
			widget.gtable.doc_frm.show_form($('.selectedval', tab), doc, '', true);
		}
		
		function SetVal(row){			
			
			doc=row.data('doc');
			setcontent(doc)
			if(widget.dataset_id==geodatasetguid ||widget.dataset_id==100)
				widget.value=doc.guid;
			else
				widget.value=doc.id;
			widget.sendmessage(widget.value);
			selectDlg.dialog("close");
		}
		
		function tableDraw(){
				var sAjaxSource = "/dataset/list?f=" + widget.dataset_id + '&count_rows=true';
				widget.gtable = new gdataset(widget.table_json, widget.dataset_id, subtab, sAjaxSource, undefined, false);
				widget.gtable.drawtablehead();
				widget.gtable.row_on_page_count=5;
				widget.gtable._fnReDraw();
				widget.gtable.rowselect.push(SetVal);
				var tr = $('#dt_fltr', subtab);			
				tr.show("slow");
				if(widget.value && widget.value!=''){
					$.getJSON("/dataset/list?f="+ widget.dataset_id+"&iDisplayStart=0&iDisplayLength=1&f_id=" + widget.value, function(data) {			
						if(data.aaData.length!=1)
							return;
						var doc    = data.aaData[0];
						$('.subtab', selectDlg).empty();			
						widget.gtable.doc_frm.show_form($('.subtab', selectDlg), doc, '', true);
					});					
				}
		}
		if(widget.table_json){
			tableDraw();
		}
		else{
			this.tabledraw=tableDraw;
		}
		
		
		
		
		return;	
		
    }

    this.getVal = function (){
		
		return this.value;
		
		var geos_row_selected = $('tr.geos_row_selected', this.table_container);
		if (geos_row_selected.length != 1)
			return '';
		
		var doc = geos_row_selected.data('doc');
		this.doc=doc;
		return 	doc.id;
		
		value = input.val();
		if (correspondingDataset !== false && useSelectionControl.find("input").prop("checked") === true) {
			value += ":" + btoa(JSON.stringify(correspondingDataset.FilterValues));
		}

	    return value;
    }
    
	// Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<div class='tab_field'><label class='tab_field_label'>Список полей</label><input class='shownfields' type='text'/></div><div class='tab_field'><label class='tab_field_label'>dataset_id</label><input class='dataset_id' type='text' /></div>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined){
			$('.shownfields',container).val(this.object.widget.properties.fields);
			$('.dataset_id',container).val(this.object.widget.properties.dataset_id);
		}
	};
	
    this.getPropForm = function (){
		var fields=$('.shownfields',container).val();
		var dataset_id=$('.dataset_id',container).val();
		return {fields:fields, dataset_id:dataset_id};
	};
 	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'select';		
		tempobj.sWidget = 'select';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}
    return this;
}
extend(w_theme_select, w_base);
$.widgets.RegisterWidget('theme_select', w_theme_select);

function w_user(object) {	
	var widget=this;
    this.object = object;
	// Common name of widget
	var widget_name = 'user';
	this.form_element = '';
	this.input;
	
	this.user_visible = true;
	this.rname = 'User';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		var con=form_element;
		var label = $('<label for="inp_'+this.object.fieldname+'">'+this.object.title+'</label>');
		input = $('<input title="'+this.object.description+'" class="form-control" id="inp_'+this.object.fieldname+'" type="text"/>');		
		form_element.append(input);
		this.input=$('input',form_element);
		if(value!=undefined && value!=''){
			this.input.val(removeescapeHtml(value));
			this.val=value;
		}
		
			w_settings = {
				autofill    : true,
				cacheLength : 10,
				max         : 5,
				autofocus   : true,
				highlight   : true,
				mustMatch   : true,
				selectFirst : true,
				delay: 500,
				source		: function (request, response) {
					if(request.term.length<3)
						return;
					var result=[];					
					$.ajax({
						url: '/dataset/list?f=5&iDisplayStart=0&iDisplayLength=10&iSortingCols=1&iSortCol_0=1&distinct=t&f_username=' + request.term + '&s_fields=username,id',
						dataType: "json",						
						success: function(data) {
							response($.map(data.aaData, function (item) {
									
									return {
										label: item.username,
										id: item.id
									};
								}));
						}
					});
				},
				appendTo    : form_element
			};

			this.input.autocomplete(w_settings, {
				dataType:'json',
				change: function(event, ui) {
					con.change();
					widget.sendmessage(widget.val);
				},
				select: function (event, ui) {
							event.preventDefault();
							widget.input.attr('userid', ui.item.id);
							//sel_id = ui.item.id;
							widget.input.val(ui.item.label);						
							widget.val=ui.item.id;
							widget.username=ui.item.label;
							widget.sendmessage(ui.item.id);	
						},
				formatItem: function(row, i, n) {                                                        
				  return row.value;
				},
			});	
			
			/*this.input.change(function(){
				widget.val=widget.input.val();
				widget.sendmessage(widget.val);
			});*/
		
		this.changevalue('test1212121','');	
	
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';
		
		return this.val;
    }
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		if(val==null)
			return '';		
		var uid=randomString(10);
		$.ajax({
			url: '/user_list/list?f_id='+ val,
			dataType: "json",						
			success: function(data) {
				if(!data.aaData || data.aaData.length==0)
					return;
				$('#'+uid).html(data.aaData[0].fullname);
				
			}
		});
		return '<b><a href="/user/profile/'+val+'" class="purple user i-forum-author"> <span id="'+uid+'"></span></a></b>';
    }
    // Additional properties form
    this.viewPropForm = function (container){
			
    }
    this.getPropForm = function (container){	
		
	    return {};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		tempobj.sWidth = this.object.widget.properties.size*10+'px';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_user, w_base);
$.widgets.RegisterWidget('user', w_user);
/**
* A filter widget based on data in a table column.
* 
* @class AutocompleteFilterWidget
* @constructor
* @param {object} $Container The jQuery object that should contain the widget.
* @param {object} column The column metadata.
* @param {object} dt_table The table object.
* @param {object} filtervalue The initial filter value.
*/	
$.widget_filter = function( $Container, column, dt_table, filtervalue ){
	var widget = this;
	widget.column = column;		
	widget.$Container = $Container;
	widget.oDataTable = dt_table;
	widget.asFilters = [];
	widget.sSeparator = ';';

	
	widget.control = $('<div class="input-group"></div>');
	widget.$Container.append( widget.control );	
	widget.$TermContainer=$('<div></div>');
	widget.$Container.append( widget.$TermContainer );	

	function init(){
		widget.control.empty();
		widget.column.widget_object.assign(widget.control,'');
		var btn_div=$('<div class="input-group-append"><span class="input-group-text addconstraint"><i class="fas fa-search-plus"></i></span></div>');
		widget.control.append(btn_div);
		btn = $('.addconstraint',btn_div);
		btn.click(function() {		
			var val=widget.column.widget_object.getVal();
			widget.add(val);
			widget.fnFilter();
		});
		widget.btnOk=btn;
	}
	init();

	

	
	//widget.$label = $('<label for="f_' + widget.oColumn.sName + '">' + widget.oColumn.sTitle + '</label><br>');
	//widget.$Container.append( widget.$label );
	this.add=function(val){
		
		if ( val =='' ) {
			// The blank option is a default, not a filter, and is re-selected after filtering
			return;
		}
		if  (!($.inArray(val, widget.asFilters))) {
			return;
		}
		var strval=widget.column.widget_object.getUserVal(val);
		sText = $( '<div>' + strval + '</div>' ).text();
		var $TermLink = $( '<a class="filter-term" href="#"></a>' )
			.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
			.text( sText )
			.click( function() {
				// Remove from current filters array
				widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
					return sFilter != val;
				} );
				$TermLink.remove();
				widget.fnFilter();
				return false;
			} );
		widget.asFilters.push( val );		
		widget.$TermContainer.prepend( $TermLink );		
		init();
	}
	this.clear=function(){
		widget.asFilters=[];
		widget.$TermContainer.empty();
	}
	
	
	//widget.$Container.append( btn );
	if(filtervalue){
		widget.add(filtervalue);		
	}
	
};

/**
* Perform filtering on the target column.
* 
* @method fnFilter
*/
$.widget_filter.prototype.fnFilter = function() {
	var widget = this;
	var asEscapedFilters = [];
	if ( widget.asFilters.length > 0 ) {
		$.each( widget.asFilters, function( i, sFilter ) {
			asEscapedFilters.push( sFilter );
		} );
		widget.oDataTable.fnFilter( asEscapedFilters.join(';') , widget.column.fieldname );
	} else { 
		// Clear any filters for this column
		widget.oDataTable.fnFilter( '', widget.column.fieldname );
	}
	setTimeout( function() {
		$('.g_layer',$('#layer_list')).each(function(index){
			ml=$(this).data("mlayer");
			ml.redraw();
		});							
	}, 150 );
};

function editcell(dt_table, doc_description, cell){	
	if(doc_description.rights.charAt(1)=='0')
		return;
	if(!cell.is("td"))
		cell=cell.parents('td');
	// var dv = $('<div id="test_div_id" style="width:100%"></div>');			
	// cell.append(dv);
	

	
	var fieldname=cell.attr('fieldname');
	var row=cell.parent("tr");
	var doc_data_base=row.data('doc');
	var value=doc_data_base[fieldname];
	var title=fieldname;
	var widget;
	for (var i = 0; i < doc_description.columns.length; i++) {					
		if(doc_description.columns[i].fieldname==fieldname){
			widget=doc_description.columns[i].widget_object;
			if(doc_description.columns[i].title && doc_description.columns[i].title!='')
				title=doc_description.columns[i].title;
			
			

			break;
		}
	}
	var test_div=$('<div title="Cell editing">\
	<form >\
		<div class="form-group">\
			<label for="">'+title+'</label>\
			<div class="editable-input" ></div>\
		</div>\
		<div class="form-group form-check">\
			<input type="checkbox" class="form-check-input" id="setall">\
			<label class="form-check-label" for="exampleCheck1">Set value to all empty.</label>\
		</div>\
		<button id="btn_ok" type="submit" class="btn btn-primary btn-sm editable-submit">Ok</button>\
		<button id="btn_cancel" type="button"= class="btn btn-default btn-sm editable-cancel">Cancel</button>\
  	</form>\
  	</div>');
	if(doc_description.rights.charAt(1)!='4')
		$('#setall',test_div).hide();
	
	
	function save(){
		var for_all=false;		
		if($('#setall',test_div).prop("checked") && doc_description.rights.charAt(1)=='4'){
			for_all = confirm("Отменить операцию нельзя. Вы уверены?");
		}
		var value=widget.getVal();
		res={};
		res.dataset_id=doc_description.metaid;
		if(value===undefined || value=='')
			v='NULL';
		else
			v=value;
		res[fieldname]=v;
		$.each(dt_table.FilterValues, function(index, value) {
			if(value!='' && value!==undefined)
				res['f_'+index]=value;					
		});		
		if(!for_all){			
			res.f_id=doc_data_base.id;			
		}
		else{
			res.group_update=true;
		}

		str_res=JSON.stringify({document:res});
		$.ajax({
			type: "POST",
			url: "/dataset/update?f=" + doc_description.metaid,
			contentType : 'application/json',
			data: str_res,
			success: function(msg){
				msg=JSON.parse(msg);
				if(msg.status=='ok'){
					test_div.dialog("destroy");					
					doc_data_base[fieldname]=value;
					widget.cntr=doc_data_base.id;
					element = widget.getUserVal(value);					
					var min_width='20px';
					if(widget.getDatatableProperties().sWidth!=undefined)
						min_width=widget.getDatatableProperties().sWidth;										
					cell.html('<div>'+element+'</div>');
					cell.attr('title',removeescapeHtml(element));
					if(for_all)
						dt_table._fnReDraw();
				}
				else
					alert(msg);
			}
		});
	}
	$('#btn_ok',test_div).click(function(){
		save();
	});
	$('test_div').bind('keydown', function(e) {
		if (e.which == 27) {
			test_div.dialog("destroy");
			var e = $.Event('keydown');
			e.keyCode = 27; 						
		}
		else if (e.which == 13) {
			save();
		}				
	 });
	
	$('#btn_cancel',test_div).click(function(){
		test_div.dialog("destroy");
		//dv.poshytip('destroy');
		var e = $.Event('keydown');
		e.keyCode = 27; 
		dv.remove();
	});
	test_div.dialog({
  width: 500
});
	// cell.popover({
	// 	template: test_div,
	// 	placement: "top",
	// 	html: true
	// }); 
	var d = $('.editable-input', test_div);
	widget.assign( d, value);
	setTimeout(function(){
		if($('input[type="text"]:first', test_div).length>0)
			$('input[type="text"]:first', test_div).focus();
		else
			$('textarea:first', test_div).focus();
	},500);
	if(widget)
		widget.activateDrawing();
	$("form", test_div).submit(function(e){
		e.preventDefault();
	});
};function w_wps_method(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'wps_method';


	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		var select_input = $( '<select id="'+this.object.fieldname+'"/>' );
		form_element.append(select_input);
		var host=null;
		var port=null;
		var path=null;
		var paramcontainer=null;
		var jsbody=null;
		var jsname=null;
		var params_json="";
		var wpstype=null;
		function changeMethodList(){
			select_input.empty();
			if (port==null || host==null || path==null || host.val()=='')
				return;
			var p={identifier: '', wpshost: host.val(), wpsport: port.val(), wpspath: path.val() };
			//var exWPSM = new WPSMethod(p, MethCallback);
			//var maincont = exWPSM.DescribeProcess("makeParametersForm", "paramholder");
			var exWPSS = new WPSServer(p, null);
			exWPSS.GetCapabilitiesAsItem(function (mname){
				select_input.append($('<option value="'+mname+'">'+mname+'</option>'));
			}); 			
		}
		setTimeout(function(){
			host=$('#wpshost',form_element.parent());
			port=$('#wpsport',form_element.parent());
			path=$('#wpspath',form_element.parent());
			wpstype=$('#wpstype',form_element.parent());
			jsbody=$('#jsbody',form_element.parent());
			jsname=$('#name',form_element.parent());
			
			if (host.length>0){
				host.bind('change', function (e){
					changeMethodList();
				});
			}			
			if (port.length>0){
				port.bind('change', function (e){
					changeMethodList();
				});
			}	
			if (path.length>0){
				port.bind('change', function (e){
					changeMethodList();
				});
			}
			if (jsname.length>0){
				jsname.bind('change', function (e){
					generatejsbody();
				});
			}			
		}, 1000);
		select_input.bind('change',function(e){
			if (port==null || host==null || path==null || host.val()==''|| select_input.val()=='')
				return;
			var p={identifier: select_input.val(), wpshost: host.val(), wpsport: port.val(), wpspath: path.val() };
			var exWPSS = new WPSServer(p, null);
			exWPSS.DescribeProcessAsJSON(function(param_json){
				paramcontainer=$('#params',form_element.parent());
				paramcontainer.empty();
				createparamsform(param_json,paramcontainer);	
				params_json=param_json;
				generatejsbody();
			});
		
		} );
		function generatejsbody(){
			if(wpstype.val()=="js") return;
			jsbody.empty();
			if (port==null || host==null || path==null || jsname==null || host.val()==''|| select_input.val()=='')
				return;
			var txt="function "+jsname.val()+"("
			for(var i=0; i<params_json.length; i++){
				if(i!=0) txt+=", ";
				txt+=params_json[i].fieldname;
			}
			txt+="){\n";
			txt+="var parstr = "
			for(var i=0; i<params_json.length; i++){
				if(i!=0) txt+='+","+';
				txt+='"'+params_json[i].fieldname+':"'+params_json[i].fieldname;
			}
			txt+=";\n";
			txt+='var method = new WPSMethod({wpshost: "' + host.val() + '", wpsport: "' + port.val() + '", wpspath: "' + path.val() + '", identifier: "' + select_input.val() + '", params: { parstr }});\n';
			txt+="method.Execute();";
			txt+="}\n";
			jsbody.val(txt);
		};
		/*
		$.getJSON("/geothemes/gettables",{ajax: 'true'}, function(j){
		 //alert('ok');
          var str_options = '';
          for (var i = 0; i < j.length; i++) {
            str_options += '<option value="'+j[i].schema+'.'+ j[i].name+ '">' + j[i].schema+'.'+j[i].name+ '</option>';
          }
		  select_input.html(str_options);
        });*/
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname,this.form_element).val();
    }
    
    this.viewPropForm = function (container){
		var sz=$("<label>Size of field</label><input type='text' name='w_wps_method_size' id='w_wps_method_size' placeholder='20'/>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined)
			$('#w_wps_method_size',container).val(this.object.widget.properties.size);
		return;
    }
    this.getPropForm = function (container){
		var wsize=$('#w_wps_method_size',container).val();
	    return {size:wsize};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'edit';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_wps_method, w_base);
$.widgets.RegisterWidget('wps_method', w_wps_method);