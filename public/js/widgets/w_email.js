﻿function w_email(object) {
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'email';
	// Additional properties array
	this.user_visible = true;
	this.rname = 'email';	

	
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		
		input =  $('<input class="form-control" type="email" title="'+this.object.description+'"/>');
		form_element.append(input);		
		if(value!=undefined && value!='')
			input.val(removeescapeHtml(value));
		this.input=input;
		this.changevalue('test1212121','');	
    }
    
	
	function validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(emailField) == false){
            throw 'Invalid Email Address';
        }
        
	}
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';		
		val= this.input.val();
		if(val!='')
			validateEmail(val);
		return val;
    }
	
	this.getUserVal=function (val){
		//YYYY-MM-DD HH:MM:SS
		if (val==undefined || val=='')
			return '';
		if(this.gr_fn=='none' )
			return '';
		
	    return val;
    }
    
	// Getting JSON of field for further saving

	// Additional properties form
    this.viewPropForm = function (container){
				
    }    
    
    this.getPropForm = function (container){				
	    return {};
    }
   
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = 'string';
		tempobj.type = 'widget_proxy';		
/*		tempobj.submit ='Ok';
		tempobj.cancel ='Cancel';		*/
		tempobj.sWidget = '';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';

		return tempobj;
	}
	this.hasFilter = function (){
		return true;
	}

    return this;
}

extend(w_email, w_base);
$.widgets.RegisterWidget('email', w_email);