﻿function w_textarea(object) {
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'textarea';
	// Additional properties array
	this.user_visible = true;
	this.rname = 'Текст';	

	var input;
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		//var label = $('<div style="height: 30px;width:200px;display: inline-block;text-align: right;">'+this.object.title+'</div>');
		var label = $('<label for="'+this.object.fieldname+'">'+this.object.title+'</label>');
		input =  $('<textarea title="'+this.object.description+'" id ="' + this.object.fieldname + '"></textarea>');
		form_element.append(input);
		var counter = $('<div></div>');
		form_element.append(counter);
		
		input.on('change keyup paste', function() {
			str=input.val();			
			counter.html('<label class="lang_r">Количество символов: '+str.length+'. Количество слов: '+ str.split(/ +(?:\S)/).length+'.</label> <label class="lang_e">The sign count: '+str.length+'. The word count: '+ str.split(/ +(?:\S)/).length+'.</label>');
		});
		//var el = $('#' + this.object.fieldname, form_element);
		if(value!=undefined && value!='')
			input.val(removeescapeHtml(value));
		
		input.attr( 'name', this.object.fieldname);
		input.attr( 'widget', widget_name);
		this.changevalue('test1212121','');	
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';
		str=input.val();
		//if(str.length<500 || str.length>3000)
		//	throw 'Abstract is small or very big. The size must be in interval from 500 until 3000 symbols.';
	    return input.val();
    }
	
	this.getUserVal=function (val){
		//YYYY-MM-DD HH:MM:SS
		if(this.gr_fn=='none' )
			return '';		
		if (val==undefined || val=='')
			return '';
		
	    return val;
    }
    
	// Getting JSON of field for further saving

	// Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<label>Поиск по полю (только тип tsvector)</label><input type='text' id='tsvector'/>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined){		
			$('#tsvector',container).val(this.object.widget.properties.tsvector);
		}		
    }    
    
    this.getPropForm = function (container){		
		var tsvector=$('#tsvector',container).val();
	    return {tsvector:tsvector};
    }
   
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = 'string';
		tempobj.type = 'widget_proxy';		
/*		tempobj.submit ='Ok';
		tempobj.cancel ='Cancel';		*/
		tempobj.sWidget = '';
		tempobj.oWidget = this;
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';

		return tempobj;
	}
	this.hasFilter = function (){
		return true;
	}

    return this;
}

extend(w_textarea, w_base);
$.widgets.RegisterWidget('textarea', w_textarea);