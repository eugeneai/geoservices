﻿
function showfilteredvalues(unique, fieldname, value){
	ntable=$('.'+unique);
	if(ntable.length==0)
		return;
	table=ntable.data('table');
	table.olapreset();
	for (var i = 0; i < table.metadata.columns.length; i++) {
		if (table.metadata.columns[i].fieldname==fieldname) {
			if(table.metadata.columns[i].filter_object && table.metadata.columns[i].filter_object.clear){
				table.metadata.columns[i].filter_object.clear();
				table.metadata.columns[i].filter_object.add(value);
				table.metadata.columns[i].filter_object.fnFilter();
			}
			break;			
		}
	}

}

function w_classify(object) {
	var widget = this;
	// Object for creation of widget	
	this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	this.widget_name = 'Classificator';
	// Additional properties array
	var addproparr = new Array();
	this.user_visible = true;
	this.rname = 'Справочник';
	this.fieldtype = 'integer';
	// id of selected value
	var sel_id = '';
	var ft = new Array();
	this.tables = [];
	this.filter = {};
	this.fields = '';
	if (this.object != undefined && this.object.widget != undefined) {
		var props = this.object.widget.properties;
		if (props != undefined && props.db_field != null && props.db_field != undefined){
			this.fields = props.db_field.join(',');
			this.fields+=','+props.synonym;
		}
	}

	this.assign = function (form_element, value) {
		var widget = this;
		var displayfields = widget.fields.split(',');
		
		if((value=='' || value==null )&& object.default_value!='empty')
			value=object.default_value;

		if (typeof value === 'object' && value !== undefined) {
			widget.val = value.id;
			title = '';
			/*
			for (fname in value) {
				if (displayfields.indexOf(fname)!=-1)
					title+=' ' + value[fname];
			}*/
			for (var i = 0; i < displayfields.length; i++) {
				if (value[displayfields[i]])
					title += ' ' + value[displayfields[i]];
			}
			this.strval = title;
		}
		else {
			var id_val = parseInt(value);
			if (!isNaN(id_val)) {
				this.strval = this.getUserVal(id_val);
			}
			this.val = value;
		}

		this.form_element = form_element;
		var props = this.object.widget.properties;

		this.fieldname = this.object.fieldname;

		var key_val = '';
		if ($.isArray(value)) {
			key_val = value[0];
		}
		else {
			key_val = value;
		}

		switch (props.control_type) {
			case 'autoselect':
				this.input = $('<input title="' + this.object.description + '" type="text" class="form-control" />');
				var input = this.input;
				form_element.append(this.input);
				this.input.prop("disabled", true);
				break
			case 'select':
				this.input = $('<select title="' + this.object.description + '" class="form-control" />');
				var input = this.input;
				form_element.append(this.input);
				this.input.prop("disabled", true);
				break
			case 'radio':
				this.radio_list = $('<div/>');
				form_element.append(this.radio_list);
				break
			default:
				break;
		}

		this.changevalue('test1212121', '');
	}

	// Getting value of input field
	this.getVal = function () {
		if (this.visible != undefined && this.visible == false)
			return '';
		var props = this.object.widget.properties;
		if (props.db_field == null || props.db_field === undefined)
			return '';
		switch (props.control_type) {
			case 'radio':
				return $('input[name="' + this.object.fieldname + '"]:checked', this.form_element).val();
				break
			default:
				if(this.input.val()=='')
					return 'NULL';
				return this.val;
		}

		return this.val;//input.val();
	}

	this.changevalue = function (fieldname, value) {
		if (!this.checkVisibility(fieldname, value))
			return;

		var props = this.object.widget.properties;
		this.filter[fieldname] = value;
		var filter_values = {};
		if (props != undefined && props.filter != undefined) {
			for (fname in props.filter) {
				filter_values[fname] = props.filter[fname];
			}
			for (fname in props.filter) {
				for (fn in this.filter) {
					if (this.filter[fn] != '')
						filter_values[fname] = filter_values[fname].replace(new RegExp('#' + fn, 'g'), this.filter[fn]);
				}
			}
		}
		var ready = true;
		for (fn in filter_values) {
			if (filter_values[fn] != undefined && filter_values[fn] != '' && filter_values[fn].indexOf('#') != -1) {
				ready = false;
				break;
			}
		}

		var filter_str = '';
		for (fn in filter_values) {
			if (filter_values[fn] != undefined && filter_values[fn] != '') {
				filter_str += '&f_' + fn + '=' + filter_values[fn];
			}
		}

		var displayfields = widget.fields.split(',');
		//if(props.db_field==null || props.db_field===undefined)

		switch (props.control_type) {
			case 'autoselect':
				this.input.remove();
				this.input = $('<input class="form-control" type="text"  />');
				var input = this.input;
				this.form_element.append(this.input);
				if (!ready) {
					widget.input.prop("disabled", true);
					return;
				}
				w_settings = {
					autofill: true,
					cacheLength: 10,
					max: 5,
					autofocus: true,
					highlight: true,
					mustMatch: true,
					selectFirst: true,
					source: function (request, response) {
						widget.val='NULL';
						if (request.term.length < 3)
							return;
						$.ajax({
							url: '/dataset/list?f=' + props.dataset_id + '&iDisplayStart=0&iDisplayLength=10&s_fields=id,' + widget.fields + filter_str + '&f_' + displayfields[0] + '=' + request.term,
							dataType: "json",
							success: function (data) {
								console.log(data);
								response($.map(data.aaData, function (item) {
									console.log(item);
									title = '';
									for (var i = 0; i < displayfields.length; i++) {
										if (item[displayfields[i]] && typeof(item[displayfields[i]])!='object' ) {
											if (title != '')
												title += ' ';
											title += item[displayfields[i]];
										}
									}
									//if(props.synonym && props.synonym!='' && props.synonym in item ){
									//	title+=' ('+item[props.synonym]+')'
									//}
									return {
										row: item,
										label: title,
										id: item.id
									};
								}));
							}

						});
					},

					appendTo: widget.form_element
				};
				this.input.autocomplete(w_settings,
					{
						dataType: 'json',
						parse: function (data) {
							return $.map(data, function (item) {
								return {
									data: item,
									label: item.names,
									result: item.names,
									value: item.id
								}
							})
						},
						select: function (event, ui) {
							event.preventDefault();
							sel_id = ui.item.id;
							input.val(ui.item.label);
							if ($('input[title="binom"]').val() == '')
								$('input[title="binom"]').val(ui.item.label);
							if(props.synonym && props.synonym!='' && props.synonym in ui.item.row ){
								synonym=ui.item.row[props.synonym];
								if(synonym && synonym!='' ){
									if(typeof(synonym)=='object'){
										if(synonym.id)
											sel_id=synonym.id;										
									}
									else
										sel_id=synonym;
								}
							}
							widget.val = sel_id;
							widget.sendmessage(widget.val);
						},
	//					change: function( event, ui ) {
		//					widget.val = 'NULL';
			//			},
						search: function (event, ui) {
							widget.val = '';
						},
						formatItem: function (item, i, n) {
							return item.names;
						},
					});
				this.input.on("blur", function () {
					if (widget.val == '') {
						$(this).val('');
					}
				});
				
				if (this.val != '') {
					this.input.val(this.strval);
				};

				break
			case 'select':
				this.input.find('option').remove();
				if (!ready) {
					widget.input.prop("disabled", true);
					return;
				}

				$.ajax({
					url: '/dataset/list?f=' + props.dataset_id + '&iDisplayStart=0&iDisplayLength=200&iSortingCols=1&iSortCol_0=1&s_fields=id,' + this.fields + filter_str,
					dataType: "json",
					method: "POST",
					//data: {term: request.term},
					success: function (data) {
						widget.input.prop("disabled", false);
						widget.input.find('option').remove();
						if (widget.val == '' || widget.val === undefined)
							option = $('<option selected value="">----</option>');
						else
							option = $('<option value="">----</option>');

						widget.input.append(option);

						for (var i = 0; i < data.aaData.length; i++) {
							var str = '';
							for (var j = 0; j < props.db_field.length; j++) {
								if (props.db_field[j] in data.aaData[i] && data.aaData[i][props.db_field[j]] != null)
									str += ' ' + data.aaData[i][props.db_field[j]];
							}
							if (widget.val != '' && widget.val != undefined && widget.val == data.aaData[i].id)
								option = $('<option selected value="' + data.aaData[i].id + '">' + str + '</option>');
							else
								option = $('<option value="' + data.aaData[i].id + '">' + str + '</option>');
							widget.input.append(option);
						}

						if (props.another) {
							if (widget.val != '' && widget.val != undefined && widget.val == -1)
								option = $('<option selected value="-1">' + props.another + '</option>');
							else
								option = $('<option value="-1">' + props.another + '</option>');
							widget.input.append(option);
						}

						setTimeout(function () {
							widget.input.trigger('change');
						}, 500);
					}
				});
				this.input.change(function (select) {
					var newval = widget.input.val();
					if (widget.val == newval)
						return;
					widget.val = newval;
					widget.sendmessage(widget.val);
				});

				//widget.val=input.val();


				break
			case 'radio':
				this.radio_list.empty();
				if (!ready) {
					return;
				}

				$.ajax({
					url: '/dataset/list?f=' + props.dataset_id + '&iDisplayStart=0&iDisplayLength=100&iSortingCols=1&iSortCol_0=1&s_fields=id,' + this.fields + filter_str,
					dataType: "json",
					//data: {term: request.term},
					success: function (data) {
						for (var i = 0; i < data.aaData.length; i++) {
							var str = '';
							for (var j = 0; j < props.db_field.length; j++) {
								if (props.db_field[j] in data.aaData[i] && data.aaData[i][props.db_field[j]] != null)
									str += ' ' + data.aaData[i][props.db_field[j]];
							}
							if (widget.val != '' && widget.val != undefined && widget.val == data.aaData[i].id)
								option = $('<input class="form-control" checked type="radio" name="' + widget.object.fieldname + '" value="' + data.aaData[i].id + '"/> ' + str + '<br/>');
							else
								option = $('<input class="form-control" type="radio" name="' + widget.object.fieldname + '" value="' + data.aaData[i].id + '"/> ' + str + '<br/>');
							widget.radio_list.append(option);
						}
						if (props.another) {
							if (widget.val != '' && widget.val != undefined && widget.val == -1)
								option = $('<input class="form-control" checked type="radio" name="' + widget.object.fieldname + '" value="-1"/> ' + props.another + '<br/>');
							else
								option = $('<input class="form-control" type="radio" name="' + widget.object.fieldname + '" value="-1"/> ' + props.another + '<br/>');
							widget.radio_list.append(option);
						}
						$('input[type=radio]', widget.radio_list).change(function () {
							widget.val = this.value;
							widget.sendmessage(widget.val);
						});
					}
				});
				break
			default:
				break;
		}
	}

	this.getUserVal = function (val) {
		if(this.gr_fn=='none' )
			return '';

		if (val == 'NULL')
			return '';
		if (val === undefined || val == null || val == '') return "";
		if (this.gr_fn == 'count')
			return val.id;
		var props = this.object.widget.properties;
		if (typeof val === 'object' && this.gr_fn != 'groupby') {
			var resstr = '';
			for (fieldname in val) {
				fname = fieldname;

				if (fname != '' && props.db_field.indexOf(fname) != -1 && val[fieldname])
					resstr += val[fieldname] + ' ';
			}
			return resstr;
		}
		if (typeof val === 'object' && this.gr_fn == 'groupby')
			val = val.id;

		if (!isNaN(val)) {
			if (props.another && (val == -1 || val == '-1')) {
				return props.another;
			}
			if (props.db_field == null || props.db_field === undefined)
				return val;
			fields = props.db_field.join(',');
			res = JSON.parse($.ajax({
				type: "GET",
				url: '/dataset/list?f=' + props.dataset_id + '&iDisplayStart=0&iDisplayLength=1&s_fields=' + fields + '&f_id=' + val,
				async: false,
			}).responseText);

			if (res.aaData === undefined || res.aaData.length != 1) return 'id:' + val;
			var resstr = '';
			for (var i = 0; i < props.db_field.length; i++) {
				if (props.db_field[i] in res.aaData[0] && res.aaData[0][props.db_field[i]] != undefined && res.aaData[0][props.db_field[i]] != '')
					resstr += res.aaData[0][props.db_field[i]] + ' ';
			}
			
			return '<a href="#" onclick="showfilteredvalues(\''+this.table.metadata.unique+'\', \''+widget.object.fieldname+'\', '+val+')">'+resstr+'</a>';

		}
		if ($.isArray(val) && props.another && (val[0] == -1 || val[0] == '-1')) {
			return props.another;
		}

		str = '';
		for (var ff = 1; ff < val.length; ff++) {
			if (typeof val[ff] == 'string' || typeof val[ff] == 'number') {
				if (str != '')
					str += ' ';
				str += val[ff];
			}
			else if (typeof val[ff] == 'boolean') {
				if (str != '')
					str += ' ';
				if (val[ff])
					str += 'true';
				else
					str += 'false';
			}
		}
		return str;

	}

	// Additional properties form
	this.viewPropForm = function (container) {
		var widget = this;
		var props = this.object.widget.properties;
		var sz = "<div class='tab_field' ><label class='tab_field_label'>Ref. table</label>\
		<div  id='db_table' class='main_field_prop' ></div></div>\
		<div class='tab_field'><label class='small_tab_field_label'>Relation type</label>\
		<select id='reltype' class='form-control small_tab_field_input'><option value='bijection'>1:1</option>\
   <option value='ref'>many:1</option>\
   <option value='detail'>1:many</option></select>\
		<label class='small_tab_field_label'>Control</label>\
		<select class='form-control tab_field_select' id='control_type' name='control_type' ><option value='select'>Select</option><option value='autoselect'>Auto complete</option><option value='radio'>Radio buttons</option></select></div>\
		<div class='tab_field main_field_prop'><label class='tab_field_label'>Another value</label><input  class='form-control tab_field_input' type='text' id='another'/></div>\
		<div class='tab_field main_field_prop'><label class='tab_field_label'>Main synonym field</label><input  class='form-control tab_field_input synonym' type='text' /></div>\
		<div class='main_field_prop'><table><thead><tr><td class='td_head'>Field</td><td class='td_head'>View</td><td class='td_head'>Filter</td><td class='td_head'>Add column</td></tr></thead><tbody id='ref_fields'></tbody></table></div>\
		";
		container.append(sz);
		
		var metaTableSelect = {
					"fieldname": "",
					"title": "",
					"description": "",
					"visible": true,
					"widget": {
						"name": "theme_select",
						properties: {}
					}
				};
		this.tableSelect = $.widgets.get_widget(metaTableSelect);
		
		var db_table = $("#db_table", container);
		
			
		
		var db_map = $("#db_map", container);
		var ref_fields = $("#ref_fields", container);
		this.table_name = '';
		if (props != undefined && props.reftablename != undefined) {
			this.reftablename = props.reftablename;
		}
		else
			this.reftablename = 't' + randomString(10);// $('.classify').length;

		//createfield(null, container);
/*
		$.getJSON("/dataset/list?f=100&iDisplayStart=0&iDisplayLength=450&iSortingCols=1&iSortCol_0=0&sSortDir_0=asc&bSortable_0=true", function (data) {
			var sortedProp = "name";
			widget.tables = data.aaData.sort(function compare(a, b) {
				if (a[sortedProp] < b[sortedProp])
					return -1;
				else if (a[sortedProp] > b[sortedProp])
					return 1;
				else
					return 0;
			});//iSortingCols

			for (var i = 0; i < data.aaData.length; i++) {
				var option;
				if (props != undefined && props.dataset_id != undefined && props.dataset_id == data.aaData[i].id) {
					option = $('<option selected value="' + i + '">' + data.aaData[i].name + '(' + data.aaData[i].d_table + ')</option>');
					//this.table_name=data.aaData[i][3]+'.'+data.aaData[i][2];
				}
				else
					option = $('<option value="' + i + '">' + data.aaData[i].name + '(' + data.aaData[i].d_table + ')</option>');
				db_table.append(option);
			}
			db_table.change();
			//input.html(options);

		});*/

		var options = '';
		var table_fields = $("#table_fields", container);
		var synonym_field = $(".synonym", container);

		if (props != undefined && props.another) {
			$('#another', container).val(props.another);
		}
		if (props != undefined && props.synonym) {
			synonym_field.val(props.synonym)			
		}
		var control_type = $("#control_type", container);
		if (props != undefined && props.control_type != undefined) {
			control_type.val(props.control_type);
		}
		var reltype = $("#reltype", container);
		if (props != undefined && props.reltype != undefined) {
			reltype.val(props.reltype);
		}


		var farr = ['point', 'polygon', 'line', 'rect', 'rectangle', 'baikalcity', 'political_division'];

		var swidget = ['number', 'edit', 'textarea', 'boolean', 'point', 'polygon', 'line', 'rect'];

		function set_table(data){
			db_map.empty();
			ref_fields.empty();
			
			
			db_table.data('meta', data);
			for (var i = 0; i < data.columns.length; i++) {
				if (data.columns[i].widget === undefined)
					continue;
				var selected = '';
				//if (farr.indexOf(data.columns[i].widget.name) != -1) {
				if (props != undefined && props.geom != undefined && props.geom == data.columns[i].fieldname)
					selected = $('<option selected value="' + data.columns[i].fieldname + '">' + data.columns[i].title + ' (' + data.columns[i].fieldname + ') </option>');
				else
					selected = $('<option value="' + data.columns[i].fieldname + '">' + data.columns[i].title + ' (' + data.columns[i].fieldname + ') </option>');
				db_map.append(selected);
				//}
				//if(swidget.indexOf(data.columns[i].widget.name)==-1)
				//	continue;				
				if (props != undefined && props.db_field != undefined && $.isArray(props.db_field) && props.db_field.indexOf(data.columns[i].fieldname) != -1)
					selected = 'checked';
				var filter_value = '';
				if (props != undefined && props.filter != undefined && data.columns[i].fieldname in props.filter)
					filter_value = props.filter[data.columns[i].fieldname];
				field_filter = $('<div id="' + data.columns[i].fieldname + '"><input class="form-control" id="field_used" ' + selected + ' type="checkbox"/>' + data.columns[i].title + '<input id="filter_value" type="text" value="' + filter_value + '" style="width: 100px;"/></div>');
				//table_fields.append(field_filter);

				var field_row = $('<tr><td>' + data.columns[i].title + '(' + data.columns[i].fieldname + ')</td><td class="td_body"><input class="form-check-input" id="field_used_' + data.columns[i].fieldname + '" ' + selected + ' type="checkbox"/></td><td class="td_body"><input class="form-control tab_field_input" id="filter_' + data.columns[i].fieldname + '" type="text" value="' + filter_value + '" /></td><td class="td_body"><button type="button" class="submit btn" id="f_add"><i class="fas fa-plus"></i></button></td></tr>');
				ref_fields.append(field_row);
				$('.submit', field_row).data('column', data.columns[i]);
			}
			$('.submit', ref_fields).click(function () {
				var b = $(this);
				field = b.data('column');
				if (!field)
					field = {};
				//field.sqlname=$("#db_map",container).val();
				field.sqltablename = data.tablename;
				field.tablename = widget.reftablename;
				//field.fieldname=widget.reftablename+'_'+field.fieldname;
				if (!field.sqlname)
					field.sqlname = field.fieldname;
				field.fieldname = 'ref_' + field.fieldname + '_' + randomString(4);
				field.parentfield = $("#field_name", container.parent().parent()).val();
				createfield(field, $('#tablefields'));
			});
		}
		helpobject={};
		helpobject.changevalue=function(fieldname, value){
			data = JSON.parse(widget.tableSelect.curentdoc.JSON);
			set_table(data);
		}
		this.tableSelect.listnerlist.push(helpobject);
		d_id='';
		if(props)
			d_id=props.dataset_id
		this.tableSelect.assign(db_table, d_id);
		
		if(props.dataset_id ){
			getdatasetmeta(props.dataset_id  , function(error, data){
				if(error==null)
					set_table(data);
				
			});
			
		}
		
		
	}

	this.getPropForm = function (container) {
		var synonym_field = $(".synonym", container);


		//db_field=$('#db_field',container).val();
		var db_map = $("#db_map", container);
		var db_map_field = db_map.val();
		//data=db_map.data('meta');
		var db_table = $("#db_table", container);
		data = db_table.data('meta');
		/*
		var gtype='';
		for (var i = 0; i < data.columns.length; i++) {						
			if (data.columns[i].fieldname==db_map_field && data.columns[i].widget.properties) {
				gtype=data.columns[i].widget.properties.gtype;
				break;
			}
		}
		*/
		val = this.tableSelect.getVal();
		if (val == -1 || val == '-1' || val == undefined || val == '')
			return {};
		//data=JSON.parse(tables[val].JSON);
		var filter = {};
		var db_field = [];



		for (var i = 0; i < data.columns.length; i++) {
			v = $('#filter_' + data.columns[i].fieldname, container).val();
			if (v != undefined && v != '')
				filter[data.columns[i].fieldname] = v;
			if ($('#field_used_' + data.columns[i].fieldname, container).prop("checked")) {
				db_field.push(data.columns[i].fieldname);
				filter[data.columns[i].fieldname] = v;
			}

		}

		return {
			dataset_id: this.tableSelect.getVal(),
			db_field: db_field,  //$('#db_field option:selected',container).text(),
			geom: db_map.val(),
			reftablename: this.reftablename,
			//table_name: this.table_name,
			type: $("#type", container).val(),
			synonym: synonym_field.val(),
			//gtype: gtype,
			control_type: $("#control_type", container).val(),
			reltype: $("#reltype", container).val(),
			another: $('#another', container).val(),
			filter: filter
		};
	}

	this.getDatatableProperties = function () {
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.fieldname;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'widget_filter';
		tempobj.sWidth = '200px';
		return tempobj;
	}

	this.hasFilter = function () {
		return true;
	}
	this.olap = function (node, value, meta) {

		this.init_olap(node, value);
		return;
		var vis = false;
		var sz = $("<div class='form-item' ><label>Select field</label>\
		<select  id='db_field' name='db_field' ><option>Select the field </option></select>min color<input style='width:100px;' class='min_color' value='000000'/>\
		max color<input style='width:100px;' class='max_color' value='000000'/><button id='change_map' class='btn btnApply' type='button'>Apply</button></div>");
		var change_map = $('#change_map', sz);
		change_map.click(function () {
			meta.layer.update_map();
		});

		sz.hide();
		this.group.after(sz);
		// $(".min_color", sz).colorpicker();
		// $(".max_color", sz).colorpicker();
		this.group.change(function () {
			if (vis)
				sz.hide();
			else
				sz.show();
			vis = !vis;
		});
		select_input = $('#db_field', sz);
		for (var i = 0; i < meta.columns.length; i++) {
			option = $('<option value="' + meta.columns[i].fieldname + '">' + meta.columns[i].title + ' (' + meta.columns[i].fieldname + ') </option>');
			select_input.append(option);
		}
	}


	return this;
}
extend(w_classify, w_base);
$.widgets.RegisterWidget('classify', w_classify);