/*
 * Calipso MainMenu plugin that look good.
 */
L.Control.div_btn = L.Control.extend({
	options: {
		position: 'topleft',
		title: 'Menu',
		email: '',
		intentedIcon: 'icon-th',
		show_dialog: undefined
	},

	onAdd: function( map ) {
		this._map = map;
		var container = L.DomUtil.create('div', 'leaflet-bar');
		var wrapper = document.createElement('div');
		container.appendChild(wrapper);
		
		var link = L.DomUtil.create('a', '', wrapper);
		L.DomUtil.create('i', this.options.intentedIcon+' glyphicon', link);
		link.href = '#';
		link.style.width = '26px';
		link.style.height = '26px';
		//link.style.backgroundImage = 'url(' + this._icon + ')';
		//link.style.backgroundSize = '26px 26px';
		//link.style.backgroundRepeat = 'no-repeat';
		link.title = this.options.title;
		
		//this._addImage();
		//L.DomUtil.create('i', this.options.intentedIcon + extraClasses, this.link);

		var stop = L.DomEvent.stopPropagation;
		L.DomEvent
			.on(link, 'click', stop)
			.on(link, 'mousedown', stop)
			.on(link, 'dblclick', stop)
			.on(link, 'click', L.DomEvent.preventDefault)
			.on(link, 'click', this._toggle, this);

		//this.options.show_dialog = document.getElementById('adminMenu');//document.createElement('div');
		//menu.div = 'adminMenu';
		//menu.style.display = 'none';
		//menu.style.position = 'absolute';
		//menu.style.left = '27px';
		//menu.style.top = '0px';
		//menu.style.width = '95%';
		//menu.style.zIndex = -10;
		//container.appendChild(menu);
		return container;
	},

	_toggle: function() {
		if(this.options.show_dialog){
			if( this.options.show_dialog[0].style.display != 'block' ) {
				this.options.show_dialog[0].style.display = 'block';
			} else {
				this._collapse();
			}
		}
	},
	
	

	_collapse: function() {
		if(this.options.show_dialog){
			this.options.show_dialog[0].style.display = 'none';
		}
	},

	_nominatimCallback: function( results ) {
		if( results && results.length > 0 ) {
			var bbox = results[0].boundingbox;
			this._map.fitBounds(L.latLngBounds([[bbox[0], bbox[2]], [bbox[1], bbox[3]]]));
		}
		this._collapse();
	},

	_callbackId: 0,

	/* jshint laxbreak: true */
	_icon: "../img/glyphicons-halflings.png"
});


L.div_Button = function( btnIcon , btnTitle , show_div ) {
  var newControl = new L.Control.div_btn;
  if (btnIcon) newControl.options.intentedIcon = btnIcon;
  
  if (btnTitle) newControl.options.title = btnTitle;
  
  if (btnTitle) newControl.options.show_dialog = show_div;

  return newControl;
};
