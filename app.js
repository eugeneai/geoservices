var config = require("./conf.json");
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const pg = require('pg');
const session = require('express-session');
const pgSession = require('connect-pg-simple')(session);
var passport       = require('passport');




var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var dataset = require('./routes/dataset');
var datafile = require('./routes/datafile');
var filemanager = require('./routes/filemanager');
var wps = require('./routes/wps');
var map = require('./routes/map');
var dataset_middleware  = require('./controllers/middleware/datasetmiddleware');
var geo_users  = require('./controllers/middleware/geo_users');


const db_config = {
  user : config.modules.postgres.config.db_user,
  password : config.modules.postgres.config.db_passwd,
  database : config.modules.postgres.config.db_name,
  host : config.modules.postgres.config.db_host,
  max : 5, // max number of clients in the pool
  connectionTimeoutMillis : 5000,
  idleTimeoutMillis : 30000
};

var pgPool = new pg.Pool(db_config);
global.pgPool = pgPool;

pgPool.connect((err, client, done) => {
  if (err) {
      log.error(err.message);
      log.error('could not connect to database');
  } else {    
    dataset.pr_init(pgPool);      
  }
});

var app = express();
app.use(cookieParser());

const sessionMiddleware=session({
  store: new pgSession({
    pool : global.pgPool,                // Connection pool
    tableName : 'session'   // Use another table-name than the default "session" one
  }),
  secret: 'process.env.FOO_COOKIE_SECRET',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 } // 30 days
});

// app.use('/users', usersRouter);
app.use(sessionMiddleware);
app.use(passport.initialize());
app.use(passport.session());


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('sanitize').middleware);

app.use(dataset_middleware.loaddataset);
app.use(geo_users.userupdate);


app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/dataset', dataset);
app.use('/fm', filemanager);
app.use('/wps', wps);
app.use('/map', map);

app.use('/datafile', datafile);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
